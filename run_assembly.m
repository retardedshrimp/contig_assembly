% add's path to the folder where all the functions are
addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));

settings = SETTINGS.settings(); % 



% initiates the GUI
GUI.main_gui(settings)


% tic  %this would generate the zero model, if we had loaded
% theoreticBarcodes
% zeroModel = ZEROMODEL.zero_model_longest_sequence(theoreticBarcodes);
% toc

load ZERO.mat; % load zero model

settings.zeroModel = zeroModel;

% could choose any seq from database, but would need to load it first
%barcode2 = NUMERICAL.rescale_barc(theoreticBarcodes{1346},settings);
load barcode2.mat;
load Consensus_pUUH.mat;

settings.nRandom = 10; % how many random barcodes to take

% computes gumbel and gev fit
[gumbelFit, gevParams, corCoef, corCoefRev, corCoefVec] = COMPARISON.evd_with_shorter_barcode_randomised( barcode2, size(barcode,2), settings );

% normFit = fitdist(corCoefVec(:),'Normal'); % since this includes gumbel, results should be similar
% 
% COMPARISON.compare_distribution_to_data_normal(corCoefVec(:), normFit,1);
% COMPARISON.compare_distribution_to_data_gumbel([corCoef, corCoefRev], gumbelFit,1);
% COMPARISON.compare_distribution_to_data_GEV([corCoef, corCoefRev], gevParams,1);

