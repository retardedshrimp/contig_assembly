\documentclass{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{subfigure}
\usepackage[left=2cm,right=2cm]{geometry}

\title{Spectral analysis of competitive binding-based barcodes}

\begin{document}
%\maketitle


%[aut,lag] =  COMPARISON.autocor_coefficients_fft(probb, 'circular');


%http://users.ece.gatech.edu/mrichard/Gaussian%20FT%20and%20random%20process.pdf

%\section{Different way to compute autocorrelation}

%Here we provide a spectral analysis of DNA barcodes. \\

\section{Spectral analysis of theoretical barcodes}

\subsection{Definition of a theoretical barcode}

As a set-up, we are given a model to compute a vector of probabilities 

\begin{equation}
p = (p(0),~\ldots ,~p(N-1)),
\end{equation}

 where $0 \leq p(i) \leq 1$ for a DNA sequence of length $N$.\\

The distribution of $p(i)$'s depends both on the model and the DNA sequences themselves. In our considerations, the model will be fixed. For a fixed model, we will try to estimate the distribution of the $p(i)$'s  based on many DNA sequences. \\

This is possible for example for plasmid sequences, since there are a few thousands of known plasmid sequences in the RefSeq database.\\

Given a vector of probabilities $p$, a theoretical barcode is computed as a convolution of vector $p$ and a Gaussian kernel 

\begin{equation}\phi(k) =  e^{-\frac{k^2}{2\sigma^2}},\end{equation}
with $k \leq \lfloor \frac{N}{2} \rfloor$ for $N$ odd, and $ \frac{N}{2}<k \leq \frac{N}{2} $ for $N$ even,

\begin{equation}
I (k) = p * \phi(k) = \sum_{i=0}^{N-1}p(i)\cdot \phi(i-k),~~ 0 \leq k \leq N-1
\end{equation}

We could then make the barcode periodic by choosing the kernel to be periodic, i.e. $\phi(k+N)= \phi(k)$, since then it would follow that $I(k+N) = I(k)$.

\subsection{Fourier transform of the theoretical barcode}

Here we compute the Fourier transform of the theoretical barcode $I(k)$. By definition, for $\omega_k = \frac{2\pi k}{N}$, $k=0\ldots N-1$, and using the fact that $\phi(x) = \phi(-x)$, this is given as

\begin{align}
I(\omega_k) &= \sum_{l=0}^{N-1} \left(  \sum_{j=0}^{N-1}p(j)\cdot \phi(j-l)\right) e^{-i\omega_k l} =
\sum_{j=0}^{N-1} p(j)\left( \sum_{l=0}^{N-1} \phi(j-l)  e^{-i\omega_k l} \right)=\nonumber\\ 
&=\sum_{j=0}^{N-1} p(j)e^{-i\omega_k j}\left( \sum_{l=0}^{N-1} \phi(j-l)  e^{-i\omega_k (l-j)} \right)=\nonumber\\&=
\sum_{j=0}^{N-1} p(j)e^{-i\omega_k j}\phi(\omega_k) = p(\omega_k) \cdot \phi(\omega_k)
\end{align}

\subsection{Fourier transform of the covariance function}
Now we define a theoretical autocovariance function, $r(k)$, as

\begin{equation}
r(k) = E(I*I^*(k)) = E\sum_{t=0}^{N-1} I(t)I^*(t-k)
\end{equation}

Now, the Fourier transform of the autocovariance is computed in the same way as for the barcode itself,


\begin{align}
r(\omega_k) &= E\sum_{l=0}^{N-1} \left(  \sum_{j=0}^{N-1}I(j)\cdot I^*(j-l)\right) e^{-i\omega_k l} =E
\sum_{j=0}^{N-1} I(j)\left( \sum_{l=0}^{N-1} I^*(j-l)  e^{-i\omega_k l} \right)=\nonumber\\ 
 &=E
\sum_{j=0}^{N-1} I(j)\left( \sum_{l=0}^{N-1} I(j-l)  e^{-i\omega_k (-l)} \right)^*=\nonumber\\ 
&=E\sum_{j=0}^{N-1} I(j)e^{-i\omega_k j}\left( \sum_{l=0}^{N-1} I(j-l)  e^{-i\omega_k (j-l)} \right)^*=\nonumber\\&=
E\sum_{j=0}^{N-1} I(j)e^{-i\omega_k j}I^*(\omega_k) = E(I(\omega_k) \cdot I^*(\omega_k))
\end{align}

 and therefore

\begin{equation}
r(\omega_k) = E(I(\omega_k)\cdot I^*(\omega_k))= E(|I(\omega_k)|^2)
\end{equation}

Then the autocovariance function is recovered as 

\begin{equation}
r(k) = \sum_{l=0}^{N-1} \left(E(|I(\omega_l)|^2)\right) e^{i\omega_l k}
\end{equation}

Therefore, if we define an estimate $\hat{E}$ of $E$, we can approximate the autocovariance function well. Since in the end we are interested in writing the autocovariance function with respect to the probability vector $p$, we can rewrite $r(\omega_k)$ as

\begin{align}
r(\omega_k)= E(|I(\omega_k)|^2) = E(|p(\omega_l) \cdot \phi(\omega_k)|^2 = |\phi(\omega_k)|^2\cdot E|p(\omega_k)|^2
\end{align}

\subsection{Estimate of the covariance function}

Now, if we have a set of $M$ probability vectors $p_i$ (these could come from all the sequences in the plasmid database), we define the mean estimate

\begin{equation}
\hat E(|p(\omega_l)|^2) = \frac{1}{M} \sum_{i=1}^M|p_i(\omega_l)|^2
\end{equation}

and then the Fourier transform of the estimated autocovariance function $\hat r(\omega_k)$ is

\begin{equation}
\hat r(\omega_k) =  |\phi(\omega_k)|^2\cdot \frac{1}{M} \sum_{i=1}^M|p_i(\omega_k)|^2 
\end{equation}

In general, we will have that expected value of any element of $I$ is non-zero, i.e. $EI = \mu$. \\

Therefore the the autocovariance function has to be adjusted, and now we take

\begin{equation}
\bar r(k) = E((I-\mu)*(I^*(k)-\mu^*))=E\sum_{t=0}^{N-1} (I(t)-\mu)(I^*(t-k)-\mu^*)
\end{equation}

Now the notation becomes rather cumbersome. However, we can again define an estimate for $\mu$, $\hat \mu$, which is computed in the same way as the estimate for the covariance function, 
\begin{equation}
\hat\mu(I) = \frac{1}{N} \sum_{j=0}^{N-1} I(j)
\end{equation} 

We also note the following identity holds:

\begin{equation}
\sum_{t=0}^{N-1}I(t)\hat\mu^*(I)+\sum_{t=0}^{N-1}I(t)\hat\mu^*(I)-\mu(I)\mu^*(I)= \hat \mu (r)
\end{equation}

This is true because autocovariance $r$ is assumed to be real, and then for the first term
\begin{align}
E\sum_{t=0}^{N-1}I(t)\hat\mu^* &= E\sum_{t=0}^{N-1}I(t)  \frac{1}{N} \sum_{j=0}^{N-1} I^*(j) = \frac{1}{N}\sum_{t=0}^{N-1} \left(E\sum_{j=0}^{N-1}I(j)I^*(j-t) \right)=\nonumber\\= \frac{1}{N}\sum_{t=0}^{N-1} r(t)
\end{align}

and similarly for the other two terms. Finally we arrive at an estimate of the autocovariance function,

\begin{equation}
\tilde r(k) = E\hat r(k) - \hat \mu(r) 
\end{equation}
 
%Then the final step would be to normalise $\tilde r$ so that the autocorrelation would be between $-1$ and $1$.

\subsection{Practical estimation for samples of different lengths}

In practice, the plasmids will be of varying lengths, and therefore we have to be careful about how we do the averaging. 

This can be done by interpolating Fourier amplitudes  $|p_i(\omega_k)|$ to the same length $N_{max}$, and then renormalising, so that the corresponding vector in the real space would have the same mean and standart deviation as the original one. \\

To do so, we do not even have to rescale barcodes $p_i$ before transforming (although it still can give us some numeric issues). \\

Since in Fourier space,  

\begin{equation}\label{eq:1}
|p_i(0)| = \sum_k p_i(k)
\end{equation}

and 

\begin{equation}\label{eq:2}
\frac{1}{N_i-1}\frac{1}{N_i} \sum_j |p_i(\omega_j)|^2 = \frac{1}{N_i-1}\left(\hat\mu(p_i^2)- (\hat\mu(p_i))^2\right) = \hat\sigma(p_i)
\end{equation}

We want the same equations to hold for the interpolated amplitudes $|p_i
^{interp}(\omega_j)|$ as well. Making sure that \ref{eq:1} is satisfied is straight-forward as we can just leave the amplitude for the first frequence unchanged. \\

For the second condition, we denote $N_{int} = N_{max}\cdot(N_{max}-1)$, $N^m_i =N_i\cdot (N_i-1)$ use a renormalization factor 

\begin{equation}
C_i=\sqrt{\frac{\frac{N_{int}}{N^m_i}\cdot \sum_k|p_i(\omega_k)|^2-|p_i
^{interp}(0)|^2}{\sum_k|p_i|^{interp}(\omega_k)|^2-|p_i
^{interp}(0)|^2}};
\end{equation}

Then we rescale
\begin{equation}
p_i^{interp}(\omega_k) = p_i^{interp}(\omega_k)\cdot C_i,~ k=2\ldots N_{max}
\end{equation}

The sequences $p_i^{interp}(\omega_k)$ so obtained clearly satisfy \ref{eq:1} and \ref{eq:2}. The autocovariance function computed using the plasmid database can be seen in \ref{fig:1}.

%In the plasmid database, the plasmids are of different base-pair lengths, therefore we still face another problem, of how to correctly estimate the autocorrelation for the barcode when the plasmid's sampled are not of the same length. A simple solution would be to consider the first half of the transform (the other half is symmetric), and interpolate this half to the required length. Then we can average over these, an eventually fold the frequencies so that we would have the whole transform.




\begin{figure}[h!]
\includegraphics[width=1\textwidth]{FIGURES/autocorr/autocov.eps}
\caption{$\tilde r(k)$ for plasmid database using spectral theory}\label{fig:1}
\end{figure}

\begin{figure}[h!]
\includegraphics[width=1\textwidth]{FIGURES/autocorr/pixel.eps}
\caption{$\tilde r(k)$ in pixel resolution, as in previous paper.}\label{fig:1}
\end{figure}

\subsection{Random barcode generation based on the estimate autocovariance}

The next step is to compute randomize barcodes based on the theoretical autocovariance function. The first method, already used in the previous paper \cite{Muller}, is called phase randomisation.

This method uses the estimate of $|I(\omega_k)| = \sqrt{r(\omega_k) }$. By multiplying the amplitude of $I(\omega_k)$ by random phases $e^{i\alpha}$, so that $\bar I(\omega_k) = |I(\omega_k)|e^{i\alpha_k}$, the covariance estimate does not change, since $|\bar I(\omega_k)|^2= |I(\omega_k)|^2$.

An example of a phase randomised barcode in such way is seen in \ref{fig:2}. The advantage of the method is that we are able to generate random barcodes in base-pair resolution \textbf{and} they share the mean and intensity of the original barcodes without being rescaled. 

A further improvement on this will be to allow the mean and standard deviation of the randomised barcodes to vary, based on some confidence intervals for the mean and standard deviation of the whole database.

Then we can look at the extreme value distribution plots for sequences and contigs. \ref{fig:5} and \ref{fig:6} show a few extreme value distributions for 14th contig from the given data-set. Contig is of 62374 bp length. Further, all the methods considered give quite a good R-squared when we take 1000 random barcodes, close to $0.95$.

\begin{figure}
\includegraphics[width=1\textwidth]{FIGURES/autocorr/phase_rand.eps}
\caption{Phase randomised barcode of 200000 base-pair length.}\label{fig:2}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{FIGURES/autocorr/phase2.eps}
\caption{Phase randomised barcode of 20000 base-pair length.}\label{fig:3}
\end{figure}

\begin{figure}
\includegraphics[width=1\textwidth]{FIGURES/autocorr/phase3.eps}
\caption{Phase randomised barcode of 300000 base-pair length.}\label{fig:4}
\end{figure}

\subsection{Comparison}
Now we have established a nice way to compute randomised barcodes in the base-pair resolution, we can do all the theory/experiment comparisons in base-pair resolution.
\newpage


\begin{figure}
\includegraphics[width=1\textwidth]{FIGURES/evd/evdGEV.eps}
\caption{Maximum correlation coefficient distribution for pUUH plasmid with GEV.}\label{fig:5}
\end{figure}


\begin{figure}
\includegraphics[width=1\textwidth]{FIGURES/evd/evdExact.eps}
\caption{Maximum correlation coefficient distribution for pUUH plasmid with fitted exact distribution}\label{fig:6}
\end{figure}


\begin{figure}
\includegraphics[width=1\textwidth]{FIGURES/evd/evdGEV2.eps}
\caption{Maximum correlation coefficient distribution for 14th contig of pUUH plasmid with GEV.}\label{fig:5}
\end{figure}


\begin{figure}
\includegraphics[width=1\textwidth]{FIGURES/evd/evdExact2.eps}
\caption{Maximum correlation coefficient distribution for 14th contig of pUUH plasmid with fitted exact distribution}\label{fig:6}
\end{figure}

\begin{thebibliography}{30}

%\bibitem{Nilson}
%Nilsson, Adam N., et al. "Competitive binding-based optical DNA mapping for fast identification of bacteria-multi-ligand transfer matrix theory and experimental applications on Escherichia coli." Nucleic acids research (2014): gku556.

\bibitem{Muller}
Muller, Vilhelm, et al. "Rapid Tracing of Resistance Plasmids in a Nosocomial Outbreak Using Optical DNA Mapping." ACS Infectious Diseases (2016).

\end{thebibliography}


\end{document}
