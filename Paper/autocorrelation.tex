\documentclass{article}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{subfigure}

\title{Spectral analysis of competitive binding-based barcodes}

\begin{document}
%\maketitle


%[aut,lag] =  COMPARISON.autocor_coefficients_fft(probb, 'circular');


%http://users.ece.gatech.edu/mrichard/Gaussian%20FT%20and%20random%20process.pdf

%\section{Different way to compute autocorrelation}

%Here we provide a spectral analysis of DNA barcodes. \\

\section{Spectral analysis of theoretical barcodes}

\subsection{Definition of a theoretical barcode}

As a set-up, we are given a model to compute a vector of probabilities 

\begin{equation}
p = (p(0),\ldots ,p(N-1)),
\end{equation}

 where $0 \leq p(i) \leq 1$ for a DNA sequence of length $N$.\\

The distribution of $p(i)$'s depends both on the model and the DNA sequences themselves. In our considerations, the model will be fixed. For a fixed model, we will try to estimate the distribution of the $p(i)$'s  based on many DNA sequences. \\

This is possible for example for plasmid sequences, since there are a few thousands of known plasmid sequences in the RefSeq database.\\

Given a vector of probabilities $p$, a theoretical barcode is computed as a convolution of vector $p$ and a Gaussian kernel 

\begin{equation}\phi(k) =  e^{-\frac{k^2}{2\sigma^2}},\end{equation}
with $k \leq \lfloor \frac{N}{2} \rfloor$ for $N$ odd, and $ \frac{N}{2}<k \leq \frac{N}{2} $ for $N$ even,

\begin{equation}
I (k) = p * \phi(k) = \sum_{i=0}^{N-1}p(i)\cdot \phi(k-i),~~ 0 \leq k \leq N-1
\end{equation}

We could then make the barcode periodic by choosing the kernel to be periodic, i.e. $\phi(k+N)= \phi(k)$, since then it would follow that $I(k+N) = I(k)$.

\subsection{Fourier transform of the theoretical barcode}

Here we compute the Fourier transform of the theoretical barcode $I(k)$. By definition, for $\omega_k = \frac{2\pi k}{N}$, $k=0\ldots N-1$, this is given as

\begin{align}
I(\omega_k) &= \sum_{l=0}^{N-1} \left(  \sum_{j=0}^{N-1}p(j)\cdot \phi(l-j)\right) e^{-i\omega_k l} =
\sum_{j=0}^{N-1} p(j)\left( \sum_{l=0}^{N-1} \phi(l-j)  e^{-i\omega_k l} \right)=\nonumber\\ 
&=\sum_{j=0}^{N-1} p(j)e^{-i\omega_k j}\left( \sum_{l=0}^{N-1} \phi(l-j)  e^{-i\omega_k (l-j)} \right)=\nonumber\\&=
\sum_{j=0}^{N-1} p(j)e^{-i\omega_k j}\phi(\omega_k) = p(\omega_k) \cdot \phi(\omega_k)
\end{align}

\subsection{Fourier transform of the covariance function}
Now we define a theoretical autocovariance function, $r(k)$, as

\begin{equation}
r(k) = E(I*I(k)) = E\sum_{t=0}^{N-1} I(t)I(t-k)
\end{equation}

Now, the Fourier transform of the autocovariance is computed in the same way as for the barcode itself, and equals

\begin{equation}
r(\omega_k) = E(|I(\omega_k)|\cdot|I(\omega_k)|)= E(|I(\omega_k)|^2)
\end{equation}

Then the autocovariance function is recovered as 

\begin{equation}
r(k) = \sum_{l=0}^{N-1} \left(E(|I(\omega_l)|^2)\right) e^{i\omega_l k}
\end{equation}

Therefore, if we define an estimate $\hat{E}$ of $E$, we can approximate the autocovariance function well. Since in the end we are interested in writing the autocovariance function with respect to the probability vector $p$, we can rewrite $r(\omega_k)$ as

\begin{align}
r(\omega_k)= E(|I(\omega_k)|^2) = E(|p(\omega_l) \cdot \phi(\omega_k)|^2 = |\phi(\omega_k)|^2\cdot E|p(\omega_k)|^2
\end{align}

\subsection{Estimate of the covariance function}

Now, if we have a set of $M$ probability vectors $p_i$ (these could come from all the sequences in the plasmid database), we define the mean estimate

\begin{equation}
\hat E(p(\omega_l)) = \frac{1}{M} \sum_{i=1}^M|p_i(\omega_l)|^2
\end{equation}

and then the Fourier transform of the estimated autocovariance function $\hat r(\omega_k)$ is

\begin{equation}
\hat r(\omega_k) =  |\phi(\omega_k)|^2\cdot \frac{1}{M} \sum_{i=1}^M|p_i(\omega_k)|^2 
\end{equation}

In general, we will have that the mean of $I(k)$ is non-zero, i.e. $E(I(k)) = \mu$. Then the autocovariance function has to be adjusted as

\begin{align}
\bar r(k) &= E((I-\mu)*(I(k)-\mu)) = E\sum_{t=0}^{N-1} (I(t)-\mu)(I(t-k)-\mu)\nonumber\\=&=  E\sum_{t=0}^{N-1} I(t)I(t-k) - \mu^2 = r(k) - \mu^2
\end{align}

\newpage

\section{Spectral analysis of the signal}
Here we explain how to apply the spectral analysis methods in the analysis of theoretical plasmid sequences.

In experimental flourescence measurements of the YOYO-1 dye, the intensity profile of a barcode over $N$ pixels has a form


\begin{equation}
I_{experimental} (x) = \begin{cases}\sum_{i=1}^N p(i)\cdot \phi(x-i)+\eta(x),~ 1 \leq x \leq N \\ 0,~ otherwise\end{cases}
\end{equation}

here $\eta(x)$ denotes Gaussian noise at position $x$, and $\phi(x)$ is a linear filter (for example a truncated Gaussian filter), and $p_i$ denotes the binding probabilities of a linear filter to each pixel.

The experimental measurements can be accurately simulated if the underlying DNA sequence is known. Using competitive binding theory \cite{Nilson}, we can compute binding probabilities $\hat{p}_i$ for each basepair $i$ . In that case we can define a model of the intensity profile for the barcode in the $N_{bp}$  base-pair resolution as 


\begin{equation}
I_{theoretical} (x) = \begin{cases}\sum_{i=1}^{N_{bp}}\hat{p}(i)\cdot \phi(x-i),~ 0 \leq x \leq N_{bp} \\ 0,~ otherwise
\end{cases}\end{equation}


Here $\phi(x)$ is a Gaussian low-pass filter of $8\sigma$ width (sometimes also called truncated Gaussian kernel),

\begin{equation}
\begin{cases}
\phi(x) = e^{-\frac{x^2}{2\sigma^2}},~ |x|\leq 4\sigma  \\ 
0,~ |x| > 4\sigma
\end{cases}
\end{equation}

For notation simplicity we'll assume $\sigma$ to be integer.

%Further we usually consider a normalised version of $\phi(\textbf{X})$ for numerical stability. Hence we consider $\phi(\textbf{X})$ divided by  $\sum_{i} \phi(X_i) $.

The theoretical barcode in pixel resolution is then computed by sampling $I_{theoretical}$ at $M$ points, and hence

\begin{equation}
I_{barcode}(k) = I_{theoretical} (x)|_{x_k}, ~k=1\ldots M 
\end{equation}


Here we have to be a little careful on how we sample $I_{theoretical}$. The simplest choice would be to choose $M$ equidistant points on the theoretical barcode, for example $ x_k= k\cdot pixelWidth$.

Since the width of a truncated Gaussian filter is $8\sigma$, there will be at most $ 8\sigma +1$ summands for each pixel $k$. If the underlying DNA sequence is linear, we will have $ 2\sigma $ summands at the ends. This will give us "end effects", we might need to "cut" the barcode and consider only the pixels with the same number of summands. Therefore the model can be rewritten


\begin{align}
I_{barcode}(k) &= \sum_{j=-4\sigma+x_k}^{4\sigma+x_k} \hat{p}(j)\cdot \phi(x_k-j) = \sum_{m=0}^{4\sigma} \hat{p}(m-4\sigma+x_k)\phi(4\sigma-m)=\nonumber\\&= \sum_{j=-4\sigma}^{4\sigma} \phi(j)\hat{p}(x_k-j)
\end{align}

Then if we consider the Fourier transform of this, 

\begin{align}
I_{barcode}(\omega)&=\sum_{t=1}^M\left(\sum_{j=-4\sigma}^{4\sigma} \phi(j)\hat{p}(x_k-j)\right)\cdot e^{ -i\omega t} = \sum_{j=-4\sigma}^{4\sigma} \phi(j) \sum_{t=1}^M\hat{p}(x_k-j)^{ -i\omega t}=\nonumber\\ &=\sum_{j=-4\sigma}^{4\sigma} \phi(j) \hat{p}(\omega)e^{i\omega j}
\end{align}

So shift in binding probability vector appears as a phase factor. We could then consider the absolute value of the transform, 

\begin{equation}|I_{barcode}(\omega)|^2
= |\hat{p}(\omega)|^2 \cdot \left(\sum_{j,l=-4\sigma}^{4\sigma} \phi(j)\phi(l) e^{i\omega (j-l)}\right)
\end
{equation}

It is worth noting that this is related to periodogram spectral estimator, which is often used to determine "hidden periodicities" in a time series \cite{spectral}. 

But we are more interested in computing the autocovariance function for our model, rather than anything else. If instead of barcode we consider it's autocovariance function $r_{barcode}(k)$, we would arrive at a similar formula (proof in \cite{spectral})

\begin{equation}
r_{barcode}(\omega) = |\phi(\omega)|^2 r_{\hat{p}}(\omega)
\end{equation}



Now, the Fourier transform of a truncated Gaussian $\phi(\omega)$ has a well known form (see  \cite{url1}), while $r_{\hat{p}}(\omega)$ can be estimated to be Gaussian \ref{fig:1}. Therefore the autocovariance function of the barcode would closely approximate a Gaussian as well.

Therefore, we can approximate an estimator of a autocovariance function by a Gaussian (for example using non-linear least squares method). This gives us a motivation to use the method in \cite{gaussian} to generate stationary random barcodes with a predefined autocovariance.

\newpage

\section{Negativity of autocovariance estimator}
It can happen that the estimator of the autocovariance is negative at some lags, even though the real autocovariance is never negative. This can be explained by \cite{spectral}, chapter 3, where it is explained how the sum of autocovariances, coming from random sequences with an unknown mean, can sum up to $0$.  The same property holds for our data, which has negative values in the autocovariance function.


\begin{thebibliography}{30}

\bibitem{Nilson}
Nilsson, Adam N., et al. "Competitive binding-based optical DNA mapping for fast identification of bacteria-multi-ligand transfer matrix theory and experimental applications on Escherichia coli." Nucleic acids research (2014): gku556.

\bibitem{Muller}
Muller, Vilhelm, et al. "Rapid Tracing of Resistance Plasmids in a Nosocomial Outbreak Using Optical DNA Mapping." ACS Infectious Diseases (2016).

\bibitem{schreiber1}
Schreiber,~T. Constrained randomization of time series data. Physical
Review Letters {\bf 80}, 2105-2108 (1998).

\bibitem{markov}
Tavaré, Simon. "Some probabilistic and statistical problems in the analysis of DNA sequences." Lectures on mathematics in the life sciences 17 (1986): 57-86.

\bibitem{lena}
Nyberg, Lena K., et al. "Rapid identification of intact bacterial resistance plasmids via optical mapping of single DNA molecules." Scientific Reports 6 (2016).

\bibitem{devries} 
De Vries, Sven, and Rakesh V. Vohra. "Combinatorial auctions: A survey." INFORMS Journal on computing 15.3 (2003): 284-309.
 
 \bibitem{hoesel} Van Hoesel, Stan, and Rudolf Müller. "Optimization in electronic markets: examples in combinatorial auctions." Netnomics 3.1 (2001): 23-33.
 
 \bibitem{rothkopf}Hxstad, R. M., M. H. Rothkopf, and A. Pekec. Computationally manageable combinatorial auctions. Technical Report 95-09, DIMACS, Rutgers university, 1995.
 
 \bibitem{sandholm}Sandholm, Tuomas, and Subhash Suri. "Improved algorithms for optimal winner determination in combinatorial auctions and generalizations." AAAI/IAAI. 2000.
 
 \bibitem{fisher}Fisher, Marshall L. "The Lagrangian relaxation method for solving integer programming problems." Management science 50.12supplement (2004): 1861-1871.
 
 \bibitem{kutanoglu}
 Kutanoglu, Erhan, and S. David Wu. "On combinatorial auction and Lagrangean relaxation for distributed resource scheduling." IIE transactions 31.9 (1999): 813-826.
 
 \bibitem{zhou}
 Zhou, Yunhong. "Improved multi-unit auction clearing algorithms with interval (multiple-choice) knapsack problems." International Symposium on Algorithms and Computation. Springer Berlin Heidelberg, 2006.

\bibitem{hybrid}Michalak, Tomasz, et al. "A hybrid exact algorithm for complete set partitioning." Artificial Intelligence 230 (2016): 14-50.

\bibitem{debruijn} Liu, Juntao, et al. "BinPacker: Packing-Based De Novo Transcriptome Assembly from RNA-seq Data." PLoS Comput Biol 12.2 (2016): e1004772.

\bibitem{debruijn2} Liu, Bo, Dixian Zhu, and Yadong Wang. "deBWT: parallel construction of Burrows–Wheeler Transform for large collection of genomes with de Bruijn-branch encoding." Bioinformatics 32.12 (2016): i174-i182.

\bibitem{comb1} ul Hassan, Umair, and Edward Curry. "Efficient task assignment for spatial crowdsourcing: A combinatorial fractional optimization approach with semi-bandit learning." Expert Systems with Applications 58 (2016): 36-56.

\bibitem{comb2} Nguyen, Thanh, Ahmad Peivandi, and Rakesh Vohra. "Assignment problems with complementarities." Journal of Economic Theory 165 (2016): 209-241.

\bibitem{continuous}Johnson, Norman L., Samuel Kotz, and N. Balakrishnan. "Continuous univariate distributions, vol. 2 of wiley series in probability and mathematical statistics: applied probability and statistics." (1995). Chapter 32.

\bibitem{beta}
McDonald, James B., and Yexiao J. Xu. "A generalization of the beta distribution with applications." Journal of Econometrics 66.1 (1995): 133-152.

\bibitem{neff}
Clusel, Maxime, and Eric Bertin. "Global fluctuations in physical systems: a subtle interplay between sum and extreme value statistics." International Journal of Modern Physics B 22.20 (2008): 3311-3368.

\bibitem{gaussian}
Dietrich, C. R., and Garry Neil Newsam. "Fast and exact simulation of stationary Gaussian processes through circulant embedding of the covariance matrix." SIAM Journal on Scientific Computing 18.4 (1997): 1088-1107.

\bibitem{gaussian2}
Wood, Andrew TA, and Grace Chan. "Simulation of stationary Gaussian processes in [0, 1] d." Journal of computational and graphical scirtatistics 3.4 (1994): 409-432.

\bibitem{greene}
Greene, William H. "Econometric analysis (International edition)." (2000).

\bibitem{circ}Pollock, David Stephen Geoffrey, Richard C. Green, and Truong Nguyen, eds. Handbook of time series analysis, signal processing, and dynamics. Academic Press, 1999.

\bibitem{spectral}
Stoica, Petre, and Randolph L. Moses. Introduction to spectral analysis. Vol. 1. Upper Saddle River: Prentice hall, 1997.

\bibitem{url1}
 http://math.stackexchange.com/questions/1446041/fourier-transform-of-a-truncated-gaussian-function
 
\end{thebibliography}

\section*{Figures}


\begin{figure}[h!]
\includegraphics[width=0.8\textwidth]{FIGURES/autocorr/gaussian.eps}
\caption{$|r_{\hat{p}}(\omega)|$ for pUUH plasmid.}\label{fig:1}
\end{figure}


\begin{figure}[h!]
\subfigure[$p_i$ for 592 basepairs.]{
    %\rule{4cm}{3cm}
    \includegraphics[width=0.8\textwidth]{FIGURES/autocorr/a1.eps}
    \label{fig:subfig1}
}
\subfigure[Probability vector $p_j$.]{
    %\rule{4cm}{3cm}
   \includegraphics[width=0.6\textwidth]{FIGURES/autocorr/aut1.eps}
    \label{fig:subfig1}
}

\subfigure[Autocorr of $p_j$]{
    %\rule{4cm}{3cm}
   \includegraphics[width=0.6\textwidth]{FIGURES/autocorr/lag.eps}
    \label{fig:subfig1}
}
\end{figure}




\end{document}


 Then we expect to still have a model

\begin{equation}
I'_{barcode}(k) = \sum_{i=1}^Mp'(x_i)\cdot \phi'(x_k-x_i)=   \sum_{i=1}^Mp'(x_i)\cdot \phi'(pixelWidth(k-i)),~ 1 \leq k \leq M 
\end{equation}

We are then interested in the structure of the binding probability vector $p'$, and the linear filter $\phi'$ for this model, since they are necessary for spectral analysis. 

If we 

In reality, pixelisation will work as somehow "averaging" the sum of many Gaussians, and hence $I_{barcode}$ could be more accurately described by choosing a different low-pass filter accordingly.

We are now interested in producing "zero model" barcodes $I_{random}$, that would have statistically the same properties as the theoretical barcodes.


If we assume that the underlying statistical process is stationary, zero mean, and has a prescribed covariance, then we can simulate this process fast \cite{gaussian}.

These should have zero mean and prescribed autocovariance, which should be the same as for $I_{barcode}(k)$. 

The covariance function for $y(t)$ is defined as

\begin{equation}
r(k)= E(y(t)y^*(t-k))
\end{equation}



However, autocorrelation function for $I_{barcode}(k)$ is not accurate for small $M$.  

% Fourier transform of a truncated Gaussian
% http://math.stackexchange.com/questions/1446041/fourier-transform-of-a-truncated-gaussian-function
%https://en.wikipedia.org/wiki/Scale_space_implementation


\newpage


Apparently there is a difference on which method to use to compute autocorrelation. xcorr works in a different way than autocorr. Autocorr (the same as our circular autocorr) detrends the data before computing the correlation coefficient, by substracting the mean first.  I need to write in this document more about the mathematical side of autocorrelation and correlation coefficients and what is happening when we compute them and what should we do with them and how.

Consider an exponential sequence $x = a^n,n\geq 0$. We want to compute the autocorrelation function of this sequence.


- We can look how correlated the probability vector for there to be a Gaussian are.

\section{Autocorrelation of probability sequence}
First we start by computing the probability vector