

% We will need to run commands from these folders
addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']), genpath([pwd '/Results' ]));

% load settings file
settings = SETTINGS.settings(); % 


saveFolder ='Results/randomRsq';


% defaults settings for the test
settings.contigSize = 70;
settings.nRandom = 1000;
settings.uncReg = 0;
m = 100;
alpha = 5;

%fBarcode = normrnd(0,1,1,m);
bar = ZEROMODEL.generate_autocorr_rand_seq(settings.contigSize,1 ,'phaserand');
fBarcode = bar{1};

%fBarcode = barcode(1:m);

tt = randi(1000,1);

saveFile = sprintf('refBar%d.mat',tt);

saveF = strcat(saveFolder, saveFile);
save(saveF, '-v7.3', 'bar');

%randBar = cell(1,settings.nRandom);
%for i=1:settings.nRandom
%    randBar{i} = normrnd(0,1,1,settings.contigSize);
%end

randBar = ZEROMODEL.generate_autocorr_rand_seq(2*m,settings.nRandom ,'phaserand',barcode);

%save(saveF, '-append','randBar');

%size(randBar)



[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients_rv(randBar, fBarcode, settings );


saveFile = sprintf('many%d.mat',tt);

saveF = strcat(saveFolder, saveFile);
save(saveF, '-v7.3', 'bar');

nNum = 100;
xx = round(linspace(100,settings.nRandom,nNum));
xVals = [];
nVals = [];
rSq1 = [];
rSq2 = [];
rSq3 = [];

for i=1:nNum
	f = @(y) FUNCTIONS.n_fun_test(y,corCoef(1:xx(i),:));
	x0 = [30];
	x2 = fsolve(f,x0,optimoptions('fsolve','Display','off'));

	xVals =[xVals x2];
	
	%x2
	N2 = FUNCTIONS.rootN_numeric(x2,corCoef(1:xx(i),:));

	nVals = [nVals N2];
	%F = FUNCTIONS.ev_n(x,corCoef(:));
	%F

	%save(saveF, '-append', 'x', 'N','x2','N2');
	%N2 = FUNCTIONS.rootN(x,corCoef(:));

	%rSquared = COMPARISON.compute_r_squared( corCoef(1:xx(i),:), [x,N], 'exactfull' );
	%rSquared = COMPARISON.compute_r_squared( corCoef(1:xx(i),:), [x2,N2], 'exactfull' );
	rSquaredA = COMPARISON.compute_r_squared( corCoef(1:xx(i),:), [x2,N2], 'exactfull' );

	evdPar =  COMPARISON.compute_distribution_parameters(corCoef(1:xx(i),:),'gumbel');
	rSq32 = COMPARISON.compute_r_squared(corCoef(1:xx(i),:), evdPar, 'gumbel' );

	evdParGEV =  COMPARISON.compute_distribution_parameters(corCoef(1:xx(i),:),'gev');
	%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )
	
	%COMPARISON.compare_distribution_to_data( corCoef(:), [x2,N2], 'exactfull' )

	rSquared3 = COMPARISON.compute_r_squared(corCoef(1:xx(i),:), evdParGEV, 'gev' );

	rSq1 = [rSq1 rSquaredA];
	rSq2 = [rSq2 rSquared3];
	rSq3 = [rSq3 rSq32];


end
	save(saveF, '-append', 'rSq1', 'rSq2','xVals','nVals','xx');

	figure, hold on;plot(xx,rSq1); 
	plot(xx,rSq2),plot(xx,rSq3)
	legend({'exact','gev','gumbel'});
	xlabel('Number of phase randomised barcodes');
