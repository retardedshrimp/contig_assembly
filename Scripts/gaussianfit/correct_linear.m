% Should be more careful in using fft's for linear & circular convolutions!

% this is just for one barcode

load plasmid_puuh.mat;

prob = CBT.cb_netropsin_vs_yoyo1_plasmid(plasmid,settings.NETROPSINconc,settings.YOYO1conc,1000,true);

tic
psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);


nonzeroKernelLen = 4*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);
%kernel = kernel-mean(kernel);

% since it's linear, have to pad
probSeq = padarray(prob, [nonzeroKernelLen,0]);

psfKernel = zeros(size(probSeq));
psfKernel(ceil((size(probSeq,1) - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;
psfKernel = fftshift(psfKernel);
%psfKernel = psfKernel-mean(psfKernel);  
seqq = ifft(fft(probSeq).*conj(fft(psfKernel)));

probPostPSF = seqq(nonzeroKernelLen + (1:size(prob,1)));
toc

tic
probPostPSF2 = CBT.apply_point_spread_function(prob,psfSigmaWidth_bp, true, 4);
toc

[pp,lag] =  COMPARISON.autocor_coefficients_fft(prob, 'linear');
figure, plot(pp)
[pp,lag] =  COMPARISON.autocor_coefficients_fft(probSeq, 'linear');
figure,plot(fftshift(abs(fft(pp)).^2))

settings.contigSize = 300;
settings.nRandom = 10000;
settings.uncReg = 0;
m = 100;
%alpha = 5;

sequences = ZEROMODEL.generate_random_sequences(settings.contigSize,settings.nRandom,'gaussian' );

sseq = zeros(1,settings.contigSize);

for i=1:settings.nRandom
    sseq = sseq + sequences{i};
end
sseq = sseq./settings.nRandom;

autCoefs = cell(1,settings.nRandom);

for i=1:settings.nRandom
    [autCoefs{i},~]=  COMPARISON.autocor_coefficients_fft(sequences{i}, 'covar');
end

aaut= zeros(1,settings.contigSize);
for k=1:settings.nRandom
    aaut = aaut+autCoefs{k};
end

tN = 100;
aaut = aaut./settings.nRandom;
figure, plot(aaut)
alpha = 6;
t=0:0.01:tN-1;
autoCorr = exp(-(t./alpha).^2 );
figure,
plot(t+1,autoCorr)
hold on
plot(1:tN,aaut(1:tN))

alpha = 1/6.^2;
t=0:0.01:tN-1;
autoCorr = exp(-(t./alpha).^2 );
figure,
plot(t+1,autoCorr)

ffd = [];
for i=1:10000
   ffd = [ffd mean(probPostPSF(i:end-i+1))];
end

% for small sample of linear filter

%st = 100000;
prob2 = zeros(1,size(prob,1));

xx = [];
aa = [];
bb = [];
dd = [];
flB = floor(psfSigmaWidth_bp);
st = 2*flB+1;

for nt=1:300
    %if st < 2*psfSigmaWidth_bp+1 
    prob2 = zeros(1,size(prob,1));

    prob2(st-2*flB:st+2*flB-1)=prob(st-2*flB:st+2*flB-1);
    probPixel = CBT.apply_point_spread_function(transpose(prob2),flB, true, 4);
    [a,b] = max(probPixel);
    aa = [aa a];
    bb = [bb b];
    [c,d] = max(prob(st-2*flB:st+2*flB-1));
    dd = [dd d];
    testSeq = probPixel(b:b+2*flB-1)./a;
    
    t = transpose(0:size(testSeq,1)-1);
    f = @(y) exp(-t.^2./(2*y.^2))-testSeq;

    gg =  @(y) exp(-t.^2./(2*y.^2));
    xx = [xx lsqnonlin(f,flB)];

    st = st+pixelWidth_bps;
end
%figure,plot(probPixel)

% comparing to gaussian

%figure,plot(probPixel(st:end))


bb2 = CBT2.cb_two_ligand(plasmid,settings);

prob2 = zeros(1,size(bb2,2));
st = 100000;

prob2(st:st+pixelWidth_bps-1)=bb2(st:st+pixelWidth_bps-1);
%figure,plot(bb2(st:st+pixelWidth_bps-1))

probPixel = CBT.apply_point_spread_function(transpose(prob2),psfSigmaWidth_bp, true, 4);
figure,plot(probPixel(st:end))
