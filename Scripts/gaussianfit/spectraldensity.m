% compute spectral density here


%barr = IMPORT.read_fasta('plasmid.1.1.genomic.fna');
load 
psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
%psfSigmaWidth_bp = 400;

nn = 10;
prob = cell(1,nn);
yVec = cell(1,nn);
aut = cell(1,nn);
paut = cell(1,nn);

for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    if size(barr{ii},2) > 10000
        prob{ii} = CBT.cb_netropsin_vs_yoyo1_plasmid(barr{ii},settings.NETROPSINconc,settings.YOYO1conc,1000,false);
        yVec{ii} = CBT.apply_point_spread_function(prob{ii},psfSigmaWidth_bp, true, 4);
        [aut{ii},lag] =  COMPARISON.autocor_coefficients_fft(transpose(yVec{ii}), 'linear');
    end
    
    % aa = abs(fft(prob{ii}));
    %figure, plot(aa(2:end))
end

ii =  2

nonzeroKernelLen = 4*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);



for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    if size(barr{ii},2) > 10000
        [aut{ii},lag] =  COMPARISON.autocor_coefficients_fft(transpose(yVec{ii}), 'circular');
        probSeqLen = length(barr{ii});
        psfKernel = zeros(size(barr{ii}));
        psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;
        [paut{ii},lag] =  COMPARISON.autocor_coefficients_fft(psfKernel, 'circular');
        p2 = fft(paut{2});
        v2 = (abs(fft(prob{ii}))).^2;
        
    end
    % aa = abs(fft(prob{ii}));
    %figure, plot(aa(2:end))
end

