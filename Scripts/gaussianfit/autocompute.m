%
%barr = IMPORT.read_fasta('plasmid.1.1.genomic.fna');
ii = 2;

prob = CBT2.cb_two_ligand(barr{ii});

aa = abs(fft(prob));
figure, plot(aa(2:end))


aa = real(fft(prob));
figure, plot(aa(2:end))

nn = 10;
prob = cell(1,nn);
for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    prob{ii} = CBT.cb_netropsin_vs_yoyo1_plasmid(barr{ii},settings.NETROPSINconc,settings.YOYO1conc,1000,true);
    aa = abs(fft(prob{ii}));
    %figure, plot(aa(2:end))
end

probb = transpose(prob{1});
[aut,~] = COMPARISON.cross_correlation_coefficients_fft(probb,probb);
[aut,lag] =  COMPARISON.autocor_coefficients_fft(probb, 'circular');

%bb = CBT.cb_netropsin_vs_yoyo1_plasmid(barr{ii},settings.NETROPSINconc,settings.YOYO1conc,1000,true);

%aa = abs(fft(bb));
%figure, plot(aa(2:end))
sz = [];
for ii=1:nn
    sz = [sz size(prob{ii},1)];
end

for ii=1:nn
    probInt{ii} = interp1(prob{ii}, linspace(1,size(prob{ii},1),min(sz)));
    aa = abs(fft(probInt{ii}));
    figure, plot(aa(2:end))
end

for ii=1:nn
    aa = abs(fft(prob{ii}));
    aa = interp1(aa, linspace(1,size(aa,1),max(sz)));

    figure, plot(zscore(aa(2:end)))
end

for ii=1:nn
    [acor,lag] = xcorr(probInt{ii}-mean(probInt{ii}),'coeff');
    figure, plot(acor)
end

pr = [];
% finite energy?
for i=1:20
    pr = [pr; prob{1}];
    sum((pr-mean(pr)).^2)
end

spD = abs(fft(prob{1})).^2;

