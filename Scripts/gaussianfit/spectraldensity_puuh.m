% compute spectral density here


%barr = IMPORT.read_fasta('plasmid.1.1.genomic.fna');
barr = {plasmid(1:50000),plasmid(1:100000),plasmid};

psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
%psfSigmaWidth_bp = 400;

nn = 3;
prob = cell(1,nn);
yVec = cell(1,nn);
aut = cell(1,nn);
paut = cell(1,nn);


for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    if size(barr{ii},2) > 10000
        prob{ii} = CBT.cb_netropsin_vs_yoyo1_plasmid(barr{ii},settings.NETROPSINconc,settings.YOYO1conc,1000,true);
        yVec{ii} = CBT.apply_point_spread_function(prob{ii},psfSigmaWidth_bp, true, 4);
        prob{ii} = prob{ii}(2000:end-2000);
        yVec{ii} =  yVec{ii}(2000:end-2000);
        %[aut{ii},lag] =  COMPARISON.autocor_coefficients_fft(transpose(yVec{ii}), 'linear');
    end
    
    % aa = abs(fft(prob{ii}));
    %figure, plot(aa(2:end))
end


for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    if size(barr{ii},2) > 10000
        [aut{ii},lag] =  COMPARISON.autocor_coefficients_fft(transpose(yVec{ii}), 'linear');
        %aut{ii} = autt(floor(size(lag,2)/2)+1:end);
        [paut{ii},lag] =  COMPARISON.autocor_coefficients_fft(transpose(prob{ii}), 'linear');
        %paut{ii} =  pautt(floor(size(lag,2)/2)+1:end);
    end
    
    % aa = abs(fft(prob{ii}));
    %figure, plot(aa(2:end))
end



nonzeroKernelLen = 4*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);
%[pp,lag] =  COMPARISON.autocor_coefficients_fft(kernel, 'circular');

ffDet = cell(1,nn);
p2 = cell(1,nn);
bb = cell(1,nn);

for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    if size(barr{ii},2) > 10000
        %[aut{ii},lag] =  COMPARISON.autocor_coefficients_fft(transpose(yVec{ii}), 'circular');
        probSeqLen = length(prob{ii});
        psfKernel = zeros(size(prob{ii}));
        psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;
        
        [pp,lag] =  COMPARISON.autocor_coefficients_fft(psfKernel, 'linear');
        ffDet{ii} = fft(pp); % so this has a deterministic structure
        
        %[paut{ii},lag] =  COMPARISON.autocor_coefficients_fft(psfKernel, 'circular');
        p2{ii} = fft(paut{ii});
        %v2 = (abs(fft(prob{ii}))).^2;   
        bb{ii} = abs(p2{ii}).^2.*ffDet{ii};
    end
end

probSeq = padarray(prob{1}, [nonzeroKernelLen,0]);
psfKernel = zeros(size(probSeq));
psfKernel(ceil((size(probSeq,1) - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;
psfKernel = fftshift(psfKernel);

seqq = ifft(fft(probSeq).*conj(fft(psfKernel)));
probPostPSF = seqq(nonzeroKernelLen + (1:size(prob{1},1)));

v2 = (abs(fft(prob{1}))).^2;    

