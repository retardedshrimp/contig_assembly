

% We will need to run commands from these folders
addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']), genpath([pwd '/Results' ]));

% load settings file
settings = SETTINGS.settings(); % 


saveFolder ='Results/randomRsq';


% defaults settings for the test
settings.contigSize = 70;
settings.nRandom = 1000;
settings.uncReg = 0;
m = 100;
alpha = 5;

%fBarcode = normrnd(0,1,1,m);
bar = ZEROMODEL.generate_autocorr_rand_seq(m,1 ,'random',1,alpha);
fBarcode = bar{1};

tt = randi(1000,1);

saveFile = sprintf('refBar%d.mat',tt);

saveF = strcat(saveFolder, saveFile);
save(saveF, '-v7.3', 'bar');

%randBar = cell(1,settings.nRandom);
%for i=1:settings.nRandom
%    randBar{i} = normrnd(0,1,1,settings.contigSize);
%end

randBar = ZEROMODEL.generate_autocorr_rand_seq(settings.contigSize,settings.nRandom ,'random',1,alpha );

save(saveF, '-append','randBar');

%size(randBar)

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );

save(saveF, '-append', 'corCoef', 'corCoefAll');


%f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
%x0 = [30];
% = fsolve(f,x0,optimoptions('fsolve','Display','off'));
%x

f = @(y) COMPARISON.rootFunction2(y,corCoefAll(:));
x0 = [30];
x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
%x
%m =size(corCoefAll(:),1);


%x = 20;
N = FUNCTIONS.rootN_numeric(x,corCoef(:));
x
N


f = @(y) FUNCTIONS.n_fun_test(y,corCoef(:));
x0 = [30];
x2 = fsolve(f,x0,optimoptions('fsolve','Display','off'));

x2
N2 = FUNCTIONS.rootN_numeric(x2,corCoef(:));

%F = FUNCTIONS.ev_n(x,corCoef(:));
%F

save(saveF, '-append', 'x', 'N','x2','N2');
%N2 = FUNCTIONS.rootN(x,corCoef(:));

rSquared = COMPARISON.compute_r_squared( corCoef(:), [x,N], 'exactfull' );
rSquaredA = COMPARISON.compute_r_squared( corCoef(:), [x2,N2], 'exactfull' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
rSquared2 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );

evdParGEV =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )

rSquared3 = COMPARISON.compute_r_squared(corCoef(:), evdParGEV, 'gev' );

rSq = [rSquared rSquaredA rSquared2 rSquared3];

save(saveF, '-append', 'rSq', 'evdParGEV');
save(saveF, '-append', 'settings');% 

