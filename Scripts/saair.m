    
load plasmid_puuh.mat;

ntSeq = plasmid;

psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

bb = CBT.cb_netropsin_vs_yoyo1_plasmid(ntSeq,settings.NETROPSINconc,settings.YOYO1conc,0,true);
bb2 = CBT2.cb_two_ligand(ntSeq,settings);
barrr = cell(1,5258);
for i=1:5258
    i
    
    if size(barr{i},2) > 10000
        ntSeq = barr{i};
        numWsCumSum = cumsum((ntSeq == 'A')  | (ntSeq == 'T')  |  (ntSeq== 'W'));
        wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
        barcode_bpRes = -zscore(CBT.apply_point_spread_function(wwwwPresence, psfSigmaWidth_bp));
        barcode_pxRes = barcode_bpRes(1:pixelWidth_bps:end);
        barrr{i} =  barcode_pxRes;
    end
end

%ss = ZEROMODEL.generate_contig_barcode(plasmid,settings);
autC = cell(1,5258);
for i=1:5258
    i
    if ~isempty(barrr{i})
     	[autC{i},~] = COMPARISON.cross_correlation_coefficients_fft(barrr{i},barrr{i});

    end
end

dSize = 15;
kk = cell(1,5258);

for i=1:5258
    i
    if ~isempty(autC{i}) && size(autC{i},2)> dSize
     	kk{i} = autC{i}(1:dSize);
    end
end
        
ss = zeros(1,dSize);
nm  = 0;
for i=1:5258
    i
    if ~isempty(autC{i}) && size(autC{i},2)> dSize
     	ss = ss + kk{i};
        nm = nm+1;
    end
end
ss = ss/nm;

figure, plot(ss)
 legend({'autocorrelation'});
xlabel('lag');
ylabel('autocorrelation');
title('averaged autocorrelation for theory barcodes');
grid('on')


