    
load plasmid_puuh.mat;

ntSeq = plasmid;

psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

barrr = cell(1,5258);
for i=1:5258
    i
    
    if size(barr{i},2) > 10000
        ntSeq = barr{i};
        numWsCumSum = cumsum((ntSeq == 'C')  | (ntSeq == 'G') );
        wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
        barcode_bpRes = CBT.apply_point_spread_function(wwwwPresence, psfSigmaWidth_bp);
        barcode_pxRes = barcode_bpRes(1:pixelWidth_bps:end);
        barrr{i} =  barcode_pxRes;
    end
end

%ss = ZEROMODEL.generate_contig_barcode(plasmid,settings);
autC = cell(1,5258);
for i=1:5258
    i
    if ~isempty(barrr{i})
            [autC{i},~] = xcorr(barrr{i},'coeff');

     	%[autC{i},~] = COMPARISON.cross_correlation_coefficients_fft(barrr{i},barrr{i});

    end
end

dSize = 15;
kk = cell(1,5258);

for i=1:5258
    i
    if ~isempty(autC{i}) && size(autC{i},2)> dSize
     	kk{i} = autC{i}(1:dSize);
    end
end
        
ss = zeros(1,dSize);
nm  = 0;
for i=1:5258
    i
    if ~isempty(autC{i}) && size(autC{i},2)> dSize
     	ss = ss + kk{i};
        nm = nm+1;
    end
end
ss = ss/nm;

figure, plot(ss)
 legend({'autocorrelation'});
xlabel('lag');
ylabel('autocorrelation');
title('averaged autocorrelation for theory barcodes');
grid('on')



%ss = ZEROMODEL.generate_contig_barcode(plasmid,settings);
autC = cell(1,5258);
mn = 400;
for i=1:5258
    i
    if ~isempty(barrr{i}) && size(barrr{i},2)>mn
        autC{i} = parcorr(barrr{i}(1:floor(mn/2)),floor(mn/2)-1);
     	%[autC{i},~] = COMPARISON.cross_correlation_coefficients_fft(barrr{i},barrr{i});

    end
end

dSize = 199;
kk = cell(1,5258);

for i=1:5258
    i
    if ~isempty(autC{i}) && size(autC{i},2)> dSize
     	kk{i} = autC{i}(1:dSize);
    end
end
        
ss = zeros(1,dSize);
nm  = 0;
for i=1:5258
    i
    if ~isempty(autC{i}) && size(autC{i},2)> dSize
     	ss = ss + kk{i};
        nm = nm+1;
    end
end
ss = ss/nm;

figure, plot(ss)
 legend({'autocorrelation'});
xlabel('lag');
ylabel('autocorrelation');
title('averaged autocorrelation for theory barcodes');
grid('on')

coefs = cell(1,5028);

for i=1:5028
    i
    if ~isempty(barrr{i}) && size(barrr{i},2) > 70
        if size(barrr{i},2) < size(barcode,2)
            [cc1, cc2] = COMPARISON.cross_correlation_coefficients_fft(barrr{i},barcode);
        else
            [cc1, cc2] = COMPARISON.cross_correlation_coefficients_fft(barcode,barrr{i});   
        end
        coefs{i} = [cc1,cc2];

    end
end

maxx = zeros(1,5028);
for i=1:5028
    if ~isempty(coefs{i})
        maxx(i) = max(coefs{i});
    end
end

 [sortedX,sortingIndices] = sort(maxx,'descend');
 
 siz = [];
 for i=1:400
     siz = [siz size(barrr{sortingIndices(i)},2)];
 end
