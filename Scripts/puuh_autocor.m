%
% this function needs comments! 

% it's the first step towards understanding the autocorr stuff!

% [sequences, names] = IMPORT.read_fasta('DA1500 all both 13.fasta');
%  sequences{29} = seq28{1};
% sequences{220} = seq282{1};
% names{220} = '28.2';
%  [seq28, name28] = IMPORT.read_fasta('28.1.fasta');
%  [seq282, name282] = IMPORT.read_fasta('28.2.fasta');

load all_contigs.mat;

nn = size(names,2);

prob = cell(1,nn);

for ii =1:nn
    %prob{ii} = CBT2.cb_two_ligand(barr{ii});
    prob{ii} = CBT.cb_netropsin_vs_yoyo1_plasmid(sequences{ii},settings.NETROPSINconc,settings.YOYO1conc,1000,true);
    %aa = abs(fft(prob{ii}));
    %figure, plot(aa(2:end))
end


psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

tic
probPostPSF2 = CBT.apply_point_spread_function(prob{14},psfSigmaWidth_bp, true, 4);
toc

figure,plot(fftshift(abs(fft(psfKernel))))


% This is important as it computes the intensity in two different ways!
endLen = 3000;
cutSeq = probPostPSF2(endLen:end-endLen);
probSeq =  prob{14}(endLen:end-endLen);


nonzeroKernelLen = 4*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);

probSeqLen = length(cutSeq);
psfKernel = zeros(size(cutSeq));
psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;


figure,plot(abs(ifft(probSeq)));

est1 = length(cutSeq)*abs(fft(psfKernel)).*abs(ifft(probSeq));
figure, plot(est1)
%est3 = abs(fft(psfKernel)).*abs(fliplr(fft(probSeq)));

est2 = abs(fft(cutSeq));

figure, plot(est1), hold on, plot(est2)
legend('|Ith(omega)|','|phi(omega)|*|p(omega)|')

[autCoef,~]=  COMPARISON.autocor_coefficients_fft(cutSeq, 'covar');
est3 = abs(fft(autCoef));
figure,plot(autCoef)
figure,plot(est3)

gg = abs(fft(probSeq)).^2.*fft(psfKernel);
figure,plot(abs(gg))

%%% 
% here we compare different ways to compute autocorr
zval = zscore(cutSeq);
fft1 = fft(zval);
abs1 = abs(fft1).^2;

figure, plot(abs1)

fft2 = fft(abs1);
figure, plot(real(fft2)/abs(fft2(1)))

autC = COMPARISON.autocor_coefficients_fft(transpose(zval),'circular');
figure, plot(autC)
hold on,
plot(real(fft2)/abs(fft2(1))) % comparing, they are the same!



nonzeroKernelLen = 3*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);

probSeqLen = length(cutSeq);
psfKernel = zeros(size(cutSeq));
psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;


% issue that rhs does not have zero mean!
est1 = abs(fft(probSeq)).^2.*abs(fft(psfKernel));
fft3 = real(fft(est1));
subsmean = fft3-mean(fft3);

 figure,plot(subsmean/subsmean(1))
hold on 
plot(real(fft2)/real(fft2(1))) % comparing, they are the same!
