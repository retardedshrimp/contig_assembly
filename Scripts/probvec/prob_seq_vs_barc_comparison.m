% prob vector -barcode comparison


load Consensus_pUUH.mat;
%load dd_mean.mat;
load plasmid_puuh.mat;

psfSigmaWidth_bp = 1.1157e+03;

prob = CBT.cb_netropsin_vs_yoyo1_plasmid(plasmid,settings.NETROPSINconc,settings.YOYO1conc,1000,true);
ker = ZEROMODEL.gaussian_kernel(length(prob),psfSigmaWidth_bp);
theorBarcode= ifft(fft(prob).*conj(real(fft(transpose(ker)))));

linV = linspace(1, length(barcode),length(theorBarcode));
interpBarcode = interp1(barcode,linV);

fft1 = fft(theorBarcode);


kerF  =conj(real(fft(transpose(ker)))) ;

figure,plot(kerF)
figure,plot(fft(prob))
ub = fft(prob).*kerF;
figure,plot(ub)
figure,plot(abs((abs(ub)./kerF)))

ab = fft(prob).*kerF;
u = ifft(ab);

ac =fft(u);
figure,plot(imag(ac))
hold on
plot(imag(ab))

figure,plot(abs(ac./kerF))
a1 = abs(ac(1:1000)./kerF(1:1000));
a2 = abs(ab./kerF);



%figure, plot(abs(fft1./fftshift(kerF)))
