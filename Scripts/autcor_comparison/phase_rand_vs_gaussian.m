
load Consensus_pUUH.mat;
load thyDatabase.mat;

figure,plot(barcode)
xlabel('pixel'), ylabel('intensity'), title('pUUH consensus barcode')

I = abs(fft(barcode)).^2;
figure,plot(I)
xlabel('pixel'), ylabel('intensity'), title('abs(fft(barcode))^2')

autC1 = real(fft(I));
figure, plot(autC1/autC1(1))
xlabel('pixel'), ylabel('intensity'), title('normalised fft of  abs(fft(barcode))^2')

% autC = COMPARISON.autocor_coefficients_fft(barcode,'circular');
% plot(autC)
theoreticalBarcodes = ZEROMODEL.generate_barcodes_for_contigs({seqq{1}},settings);
xx = CBT.gen_zscaled_cbt_barcode(plasmid,settings);
xx = CBT.gen_zscaled_cbt_barcode(plasmid,settings);

xx = CBT.gen_zscaled_cbt_barcodes({seqq{1:10}},settings);

psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

bb = CBT.cb_netropsin_vs_yoyo1_plasmid(plasmid,settings.NETROPSINconc,settings.YOYO1conc,0,true);
barcode_bpRes = zscore(CBT.apply_point_spread_function(bb, psfSigmaWidth_bp));
barcode_pxRes = barcode_bpRes(1:pixelWidth_bps:end);



I = abs(fft(barcode_bpRes)).^2;
figure,plot(I)
xlabel('pixel'), ylabel('intensity'), title('abs(fft(barcode))^2')

autC1 = real(fft(I));
figure, plot(autC1/autC1(1))
xlabel('pixel'), ylabel('intensity'), title('normalised fft of  abs(fft(barcode))^2')


% seqq = IMPORT.read
seqq = IMPORT.read_fasta('plasmid.1.1.genomic.fna');
set = {};
for i=1:size(seqq,2)
    if size(seqq{i},2)>8925
        set = [set seqq{i}];
    end
end

xx = CBT.gen_zscaled_cbt_barcodes(set{1},settings);

% compute binding probs

% probVec = cell(1,size(set,2));
% parfor j=1:size(set,2)
%    j
%    hSet = CBT.cb_netropsin_vs_yoyo1_plasmid(set{j},settings.NETROPSINconc,settings.YOYO1conc,0,true);
%    probVec{j} = hSet;
% end
load 'DATABASE_3430_prob_seq.mat';

% compute autocorr function based on the binding probs

nn = 100;
autocorEst = cell(1,nn);
fftEst = cell(1,nn);

for indSeq = 1:nn;
    cutSeq = probVec{indSeq};
    nonzeroKernelLen = 3*2*psfSigmaWidth_bp;
    nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
    kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);

    probSeqLen = length(cutSeq);
    
    psfKernel = zeros(size(cutSeq));
    psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;


    % issue that rhs does not have zero mean!
    est1 = abs(fft(cutSeq)).^2.*abs(fft(psfKernel));
    fft3 = real(fft(est1));
    subsmean = fft3-mean(fft3);
    autocorEst{indSeq} = subsmean/subsmean(1);
    fftEst{indSeq} = est1;
end

mm=[];
for iInd=1:nn
   mm = [mm size( fftEst{iInd},1)];
end

maxL = max(mm);
newV = cell(1,nn);
ssum = zeros(1, maxL);
for  iInd=1:nn
    intV = linspace(1,mm(iInd),maxL);
    newV{iInd} = abs(interp1(fftEst{iInd},intV ));
    ssum = ssum+newV{iInd};
end
ssum=ssum./nn;
ssum(1) = 0;

figure, plot(ssum)

fft3 = real(fft(ssum));
%figure, plot(fft3)
  subsmean = fft3-mean(fft3);
figure, plot(subsmean/subsmean(1))


%subsmean = fft3;

figure,plot(subsmean/subsmean(1))
hold on 
plot(real(fft2)/real(fft2(1))) % comparing, they are the same!


barcode_bpRes = zscore(CBT.apply_point_spread_function(probVec{1}, psfSigmaWidth_bp));
barcode_pxRes = barcode_bpRes(1:pixelWidth_bps:end);

% change these so that rolling back would give mean 0
est1 = abs(fft(cutSeq)).^2.*abs(fft(psfKernel));

    
% 
% theoreticalBarcodes = ZEROMODEL.generate_barcodes_for_contigs({seqq{2}},settings);
% figure, plot(xx)
% hold on
% plot(theoreticalBarcodes{1})
