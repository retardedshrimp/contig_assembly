lengthSeq = 10;

probSeq = rand(100,1);
[f,xi] = ksdensity(probVec{15});
figure
plot(xi,f);


nn = size(probVec,2);
mm=[];
for iInd=1:nn
   mm = [mm size( probVec{iInd},1)];
end

maxL = max(mm);
newV = cell(1,nn);
ssum = zeros(1, maxL);
for  iInd=1:nn
    intV = linspace(1,mm(iInd),maxL);
%     ab = fft(abs(fft(zscore(probVec{iInd}))).^2);
%     autoCor = real(ab)/real(ab(1));

    autoCor = abs(fft(zscore(probVec{iInd}))).^2;
    newV{iInd} = interp1(autoCor,intV );
    ssum = ssum+newV{iInd};
end
ssum=ssum./nn;

figure, plot(ssum)

% 
% autoCor = real(ab)/real(ab(1));
% figure, plot(real(ab)/real(ab(1)))
% 
% 
%  r = mvnrnd(ones(size(autoCor)),transpose(real(fft(autoCor))), 1);
%  figure,plot(r)

