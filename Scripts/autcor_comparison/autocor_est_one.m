
%load 'DATABASE_3430_prob_seq.mat';

psfSigmaWidth_bp = 1.1157e+03;


saveFolder ='Results/randomRsq';


for iInd= 105;
    
    fftEst1 = (abs(fft(probVec{iInd}))).^2;
    
    ker = ZEROMODEL.gaussian_kernel(length(fftEst1),psfSigmaWidth_bp);

    fftEst2 = (abs(fft(ker))).^2;

    fftEst = fftEst1.*transpose(fftEst2);

    autocorEstTilde =ifft(fftEst);

    autocorEst = autocorEstTilde - mean(autocorEstTilde);

    autocorEstNormalised = autocorEst/autocorEst(1);

    f1 = figure( 'Visible','off'); plot(autocorEstNormalised)
    xlabel('basepair')
    ylabel('autocorrelation')
    
    tt = randi(1000,1);

    saveFile = sprintf('refBar%d.eps',iInd);
    saveF = strcat(saveFolder, saveFile);
    saveas(f1,saveF,'epsc') 

end