
%load 'DATABASE_3430_prob_seq.mat';
%load 'DATABASE_1_500_with_unc_reg.mat';
load 'DAT_2000.mat';

psfSigmaWidth_bp = 1.1157e+03;

% compute estimate of autocorr
[autocorEstTilde1,fftEst ]= AUTOC.estimate_autocorr(probVec, settings);
autocorEst = autocorEstTilde - mean(autocorEstTilde);
% 
seqVec = {};
for i=1:length(probVec)
    i
    ker = ZEROMODEL.gaussian_kernel(length(probVec{i}),psfSigmaWidth_bp);
    seqVec{i} = ifft(fft(probVec{i}).*conj(fft(transpose(ker))));
end

% to compute lengths
barcodeLens = cellfun(@length, seqVec);

pixelWidth_bps = round(settings.bpPerNm*settings.camRes);
settings.maxL = round(max(barcodeLens))+1;

[autocorEstTilde2, fftEst]= AUTOC.estimate_autocorr(seqVec, settings,'seq');

seqVecNorm = {};
for i=1:length(seqVec)
    i
    seqVecNorm{i} = zscore(seqVec{i});
end

[autocorEstTilde2, fftEst]= AUTOC.estimate_autocorr(seqVecNorm, settings,'seq');

[autocorEstTilde2, fftEst] = AUTOC.estimate_autocorr(seqVecNorm, settings,'seq');

% this is the meanFFT from before
meanFFT = sqrt(fftEst);

len2 = round(max(barcodeLens)/pixelWidth_bps)+1;

tempfft = meanFFT(2:round(length(meanFFT)/2));
halfL = floor(len2/2+1);
intV = linspace(1,round(length(meanFFT)/2)-1,halfL-1);

intVal = [ interp1(tempfft, intV )];

intVal = [meanFFT(1) intVal fliplr(intVal(1:end))];

len1 = length(meanFFT)*(length(meanFFT)-1);

a = sum(meanFFT.^2)/(len1);

b = sum(intVal.^2)/(len2);


    % if they are not the same, we renormalise intVal values 2:end by
    % constant konst
konst=sqrt((len2*a-intVal(1).^2)/(len2*b-intVal(1).^2));



    % and define intValNorm
intValNorm=[intVal(1) intVal(2:end).*konst];
        

        % Take first half of frequencies of prFFT

        % interpolate

        % recreate fft for all frequences

  % and the Parseval's identity also needs to be satisfied
        %len2 = length(intVal)*(length(intVal)-1);

        % the sums a and b have to be the same
    

%     
% %autocorEstTilde = AUTOC.estimate_autocorr(probVecScaled, settings);



saveFolder ='Results/averagedSeq';

% what length to interpolate to
maxL = 5000637;

% how many sequences to take
nn = 50;


halfL = floor(maxL/2+1);
ssum = zeros(1,maxL);


for iInd= 1:nn;
    %newV{iInd} = abs(interp1(fftEst{iInd},intV ));
    prFFT = abs(fft(probVec{iInd}));

    intV = linspace(1,round(length(prFFT)/2),halfL);

    tempfft = prFFT(1:round(length(prFFT)/2));
    intVal = interp1(tempfft, intV );
    
    % have to add an extra value for even length
    intVal = [intVal fliplr(intVal(2:end))];
    
    % renormalization
    intVal(1) = tempfft(1);
    
    len1 = length(prFFT)*(length(prFFT)-1);
    len2 = length(intVal)*(length(intVal));
    
    a = sum(prFFT.^2)/(len1);
    b = sum(intVal.^2)/(len2);

    intVal2 = intVal;
    
    konst=sqrt((len2*a-intVal(1).^2)/(len2*b-intVal(1).^2));
    intVal2(2:end)=intVal2(2:end).*konst;
    
    b2 = sum(intVal2.^2)/(len2);
    
    ssum = ssum + intVal2.^2;
end
fftEst1 = ssum/nn;


ker = ZEROMODEL.gaussian_kernel(length(fftEst1),psfSigmaWidth_bp);

fftEst2 = (abs(fft(ker))).^2;

fftEst = fftEst1.*fftEst2;

autocorEstTilde =ifft(fftEst);

autocorEst = autocorEstTilde - mean(autocorEstTilde);
%autocorEst = autocorEstTilde;

autocorEstNormalised = autocorEst/autocorEst(1);

figure,plot(autocorEst)


%aacor = autocorEstNormalised(1:592:end);

%mu = 0.8*ones(1,length(aacor));
%mmean = mvnrnd(mu,toeplitz(aacor),1)



% 
% f1 = figure( 'Visible','off'); plot(autocorEstNormalised)
% xlabel('basepair')
% ylabel('autocorrelation')
% 
% tt = randi(1000,1);
% 
% saveFile = sprintf('refBar%d.eps',tt);
% saveF = strcat(saveFolder, saveFile);
% saveas(f1,saveF,'epsc') 
% 
