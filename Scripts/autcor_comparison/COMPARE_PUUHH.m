
load Consensus_pUUH.mat;
load dd_mean.mat;
load plasmid_puuh.mat;

psfSigmaWidth_bp = 1.1157e+03;

prob = CBT.cb_netropsin_vs_yoyo1_plasmid(plasmid,settings.NETROPSINconc,settings.YOYO1conc,1000,true);
ker = ZEROMODEL.gaussian_kernel(length(prob),psfSigmaWidth_bp);
theorBarcode= ifft(fft(prob).*conj(fft(transpose(ker))));

linV = linspace(1, length(barcode),length(theorBarcode));
interpBarcode = interp1(barcode,linV);

%nn = 1000;
%randomSequences = cell(1,nn);
tic
%randomSequences= ZEROMODEL.generate_random_sequences(length(theorBarcode),nn,'phase', meanFFT );
toc

settings.contigSize = length(interpBarcode);

%[autC,~] = COMPARISON.cross_correlation_coefficients_fft(randomSequences{1},interpBarcode);

%[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients({randomSequences{1}}, interpBarcode, settings );

interpData = NUMERICAL.interpolate_in_fourier_space(meanFFT, length(interpBarcode));

numRand = 1000;

[autC,~] =  COMPARISON.compute_correlation([],interpBarcode,interpData,numRand);

[evdPar] = COMPARISON.evd_comparisons(autC(:));


%%%%%
% for a contig
% contig num

load all_contigs.mat;
contigBarcode = cell(1,length(sequences));
for contigNum=1:length(sequences)
    contig = CBT.cb_netropsin_vs_yoyo1_plasmid(sequences{contigNum},settings.NETROPSINconc,settings.YOYO1conc,1000,true);
    ker = ZEROMODEL.gaussian_kernel(length(contig),psfSigmaWidth_bp);
    contigBarcode{contigNum} = ifft(fft(contig).*conj(fft(transpose(ker))));
end


linV = linspace(1, length(contigBarcode{14}),length(contigBarcode{14}));
interpBarcode = interp1(contigBarcode{14},linV);

interpData = NUMERICAL.interpolate_in_fourier_space(meanFFT, length(interpBarcode));

numRand = 1000;

[autC,~] =  COMPARISON.compute_correlation([],interpBarcode,interpData,numRand);

[evdPar] = COMPARISON.evd_comparisons(autC(:));



mm = [];
for contigNum=1:5
    interpData = NUMERICAL.interpolate_in_fourier_space(meanFFT, length(contigBarcode{contigNum}));

    numRand = 1000;
    [autC,~] =  COMPARISON.compute_correlation([],transpose(contigBarcode{contigNum}),interpData,numRand);
    mm = [mm autC];
end


linV = linspace(1, length(barcode),length(theorBarcode));
interpBarcode = interp1(barcode,linV);
[autC,~] =  COMPARISON.compute_correlation_between_two(contigBarcode{25},interpBarcode,interpData,numRand);
figure,plot(autC)

mm = [];
for contigNum=1:30
    interpData = NUMERICAL.interpolate_in_fourier_space(meanFFT, length(contigBarcode{contigNum}));

    numRand = 1000;
    [autC,autV] =  COMPARISON.compute_correlation([],transpose(contigBarcode{contigNum}),interpData,numRand);
    mm = [mm autC];
end



firstBar = zscore(theorBarcode);
secondBar = zscore(interpBarcode);
secondBarRev = fliplr(secondBar);

ccForward = ifft(fft(firstBar).*conj(transpose(fft(secondBar))))/(length(firstBar)-1);
ccBackward = ifft(fft(firstBar).*conj(transpose(fft(secondBarRev))))/(length(firstBar)-1);
ccCoef = [ccForward ccBackward];


figure,plot(ccForward)
figure,plot(ccBackward)
figure, hist(ccCoef(:),400)
figure,plot(ifft(fft(theorBarcode)))

xx = ifft(fft(theorBarcode).*conj(transpose(fft(zscore(fliplr(interpBarcode))))));
figure,plot(xx-mean(xx))

xx = ifft(fft(zscore(circshift(theorBarcode,[200000,0]))).*conj(fft(zscore(theorBarcode))));
figure,plot(xx)






figure,plot(xx)

