% this estimates autocorr based on the average of probability sequences,
% rather that the theoretical sequences

%load 'DATABASE_3430_prob_seq.mat';

nn = 1;
 
mm=[];
for iInd=1:nn
   mm = [mm size( probVec{iInd},1)];
end
maxL = max(mm);

ssum = zeros(1, maxL);
fftEst = cell(1,nn);

for  iInd=1:nn
    intV = linspace(1,mm(iInd),maxL);
    fftEst{iInd} = (abs(fft(probVec{iInd}))).^2;
    %newV{iInd} = abs(interp1(fftEst{iInd},intV ));
    ssum = ssum+interp1(fftEst{iInd},intV );
end

ssum = ssum/nn;

figure, plot(ssum)



psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

nonzeroKernelLen = 3*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);

probSeqLen = maxL;

psfKernel = zeros(maxL,1);
psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;

newKer = fftshift(psfKernel);
ker = ZEROMODEL.gaussian_kernel(probSeqLen,psfSigmaWidth_bp);

figure, plot(newKer)


%%%%
%%%


autfft= (abs(fft(ker))).^2 .* ssum;
bb = ifft(autfft);
figure,plot(bb-mean(bb))

autfft= (abs(fft(newKer))).^2 .* transpose(ssum);
bb = ifft(autfft);

ff = bb-mean(bb);

figure, plot(ff)
% 
% figure,plot(ff/ff(1))
% dd = real(bb);
% figure,plot(dd)
% 
% probSeqLen2 = size(probVec{iInd},1);
% psfKernel2 = zeros(probSeqLen2,1);
% psfKernel2(ceil((probSeqLen2 - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;
% seqq2 = ifft(fft(probVec{iInd}).*conj(fft(fftshift(psfKernel2))));
% 
% gg = dd-mean(seqq2).^2;
% figure,plot(gg/gg(1))
% 
% seqq = ifft(transpose(fft(probVec{1})).*conj(fft(ker)));
% % seqq = ifft(fft(probVec{1}).*conj(fft(newKer)));
% 
% figure,plot(ifft(abs(fft(seqq)).^2))
% 
% autc = ifft(abs(fft(seqq)).^2);
% %autc2 = autc-mean(autc)
% %autc3 = autc2/
% 
% figure,plot((autc-mean(autc)))
% figure,plot(ifft(abs(fft(seqq-mean(seqq))).^2))
% 
% % figure,plot(seqq)
% 
% % 
% % 
% autfft2= (fft(ker).*fft(probVec{1})).^2;
% % bb = ifft(autfft2);
% % figure,plot(bb)
% % 
% % 
