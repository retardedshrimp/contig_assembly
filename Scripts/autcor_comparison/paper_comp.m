
psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

nonzeroKernelLen = 3*2*psfSigmaWidth_bp;
nonzeroKernelLen = round((nonzeroKernelLen - 1)/2)*2 + 1; % round to nearest odd integer
kernel = fspecial('gaussian', [1, nonzeroKernelLen], psfSigmaWidth_bp);

probSeqLen = length(cutSeq);

psfKernel = zeros(size(cutSeq));
psfKernel(ceil((probSeqLen - nonzeroKernelLen)/2) + (1:nonzeroKernelLen)) = kernel;

barcode_bpRes = zscore(CBT.apply_point_spread_function(bb, psfSigmaWidth_bp));

est1 = abs(fft(cutSeq)).^2.*abs(fft(psfKernel)).^2;
fft3 = real(ifft(est1));
figure, plot(fft3)
figure,plot(fft3/(sum(cutSeq)*size(cutSeq,1))-mean(cutSeq))
subsmean = fft3-mean(fft3);

barB = CBT.apply_point_spread_function(bb, psfSigmaWidth_bp);

% this is another way to compute the theoretical seq, without using the
% apply point spread function stuff
seqq = ifft(fft(cutSeq).*conj(fft(fftshift(psfKernel))));

newKer = fftshift(psfKernel);

fft1=fft(newKer);
fft2 = fft(cutSeq);

fft3 = fft1.*fft2;
fft4 = fft(seqq);
figure,plot(real(fft3));
figure,plot(real(fft4(1:100)))
hold on
plot(real(fft3(1:100)))

hold on
 plot(barB)