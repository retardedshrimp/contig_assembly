

%[autocorEstTilde,fftEst,fftEstProb ]= AUTOC.estimate_autocorr(probb, settings);


% load 051216_generate_fft.mat

[ autocorEstTilde,fftEst, fftEst1,meanFFT] = AUTOC.estimate_max_fft( probb, settings );

    psfSigmaWidth_bp = 1.1157e+03;
ker = ZEROMODEL.gaussian_kernel(fftEst1,psfSigmaWidth_bp);
   fftEst2 = (abs(fft(ker))).^2;
            
% Multiply component-wise
fftEst = fftEst1.*fftEst2;

% compute using min
[ autocorEstTilde,fftEst, fftEst1,meanFFT] = AUTOC.estimate_min_fft( probb, settings );
% load meanfft_for_min.mat

% this is averaged prob, which should be used as basis for our calculations
averagedProbAbs = sqrt(fftEstProb);

% can interpolate it

% wrap in a function

mSize = length(averagedProbAbs);
fftPr = averagedProbAbs(1:floor((end+3)/2));

newS = 4358;

f1 = [0:length(1:(newS+3)/2)-1]*(1/newS);
f2 = [0:length(fftPr)-1]*(1/length(averagedProbAbs));

% and the Parseval's identity also needs to be satisfied
len1 = length(averagedProbAbs)*(length(averagedProbAbs)-1);
len2 = newS*(newS-1);

newB = zeros(1, newS);
newB(1) = averagedProbAbs(1)*newS/length(averagedProbAbs);     

newf = interp1(f2(2:end),fftPr(2:end),f1(2:end));

newB(2:length(newf)+1) = newf(1:end);
newB(newS-(1:length(newf))+1) =  newf(1:end);

% the sums a and b have to be the same
a = sum(averagedProbAbs.^2)/(len1); b = sum(newB.^2)/(len2);

% if they are not the same, we renormalise intVal values 2:end by
% constant konst
konst=sqrt((len2*a-newB(1).^2)/(len2*b-newB(1).^2));

% and define intValNorm
intValNorm=[newB(1) newB(2:end).*konst];

%%
%mInt = interp1(meanFFT,linspace(1,length(fftEstProb),length(fftEstProb)/592));


mSize = length(fftEstProb);
fftPr = averagedProbAbs(1:floor((end+3)/2));

newS = length(mInt);

f1 = [0:length(1:(newS+1)/2)-1]*(1/newS);
f2 = [0:length(fftPr)-1]*(1/length(averagedProbAbs));

% and the Parseval's identity also needs to be satisfied
len1 = length(averagedProbAbs)*(length(averagedProbAbs)-1);
len2 = newS*(newS-1);

newB = zeros(1, newS);
newB(1) = averagedProbAbs(1)*newS/length(averagedProbAbs);     

newf = interp1(f2(2:end),fftPr(2:end),f1);

newB(2:length(newf)) = newf(1:end);
newB(newS-(2:length(newf))+2) =  newf(2:end);

% the sums a and b have to be the same
a = sum(prFFT.^2)/(len1); b = sum(newB.^2)/(len2);

% if they are not the same, we renormalise intVal values 2:end by
% constant konst
konst=sqrt((len2*a-newB(1).^2)/(len2*b-newB(1).^2));

% and define intValNorm
intValNorm=[newB(1) newB(2:end).*konst];

psfSigmaWidth_bp = 1.1157e+03;
ker = ZEROMODEL.gaussian_kernel(length(intValNorm),psfSigmaWidth_bp/592);
fftEst2 = (abs(fft(ker))).^2;
            
% Multiply component-wise
fftEst = intValNorm.^2.*fftEst2;

ab = ZEROMODEL.generate_random_sequences(length(intValNorm),1,'phase', intValNorm );

bar = ifft(fft(ab{1}).*conj(fft((ker))));

ab = ZEROMODEL.generate_random_sequences(length(averagedProbAbs),1,'phase', averagedProbAbs );
averagedProbAbs
%fftEst = ab{1}.*fftEst2;

%%

figure,plot(autocorEstTilde)
%below is an example how the meanFFT is computed from two barcodes.
% above it is wrapped in a function
barcodeLens = cellfun(@length, probb);

[sh1,sh2] = min(barcodeLens);

ind1 = sh2;
ind2 =  2;

fft1f = abs(fft(probb{ind1}));
fft2f = abs(fft(probb{ind2}));

fft1 = fft1f(1:(end+1)/2);
fft2 = fft2f(1:(end+3)/2);
f1 = [0:length(fft1)-1]*(1/length(probb{ind1}));
f2 = [0:length(fft2)-1]*(1/length(probb{ind2}));

% and the Parseval's identity also needs to be satisfied
len1 = length(fft2f)*(length(fft2f)-1);
len2 = length(fft1f)*(length(fft1f)-1);

newB = zeros(1, length(fft1f));
newB(1) = fft2f(1)*length(fft1f)/length(fft2f);     

newf = interp1(f2,fft2,f1);
newB(2:length(newf)) = newf(2:end);
newB(length(fft1f)-(2:length(newf))+2) =  newf(2:end);
        
% the sums a and b have to be the same
a = sum(fft2f.^2)/(len1); b = sum(newB.^2)/(len2);

% if they are not the same, we renormalise intVal values 2:end by
% constant konst
konst=sqrt((len2*a-newB(1).^2)/(len2*b-newB(1).^2));

% and define intValNorm
intValNorm=[newB(1) newB(2:end).*konst];

figure,plot(intValNorm(2:end))
hold on
plot(fft1f(2:end))
legend({'interpolated', 'uninterpolated'});

figure,plot(intValNorm(2:end).^2)

ker = ZEROMODEL.gaussian_kernel(sh1,psfSigmaWidth_bp);
fftEst2 = (abs(fft(ker))).^2;
fftEst1 = abs(intValNorm).^2;

fftEst = fftEst1.*fftEst2;

autocorEstTilde =ifft(fftEst);

meanFFT = sqrt(fftEst);

figure,plot(autocorEstTilde)

%%%%
ker = ZEROMODEL.gaussian_kernel(length(probb{ind1}),psfSigmaWidth_bp);
theorBarcode= ifft(fft(probb{ind1}).*conj(fft(transpose(ker))));

%%%%


halfL = floor(length(mmean)/2);
PR1 = exp(2i*pi*rand(1,halfL));
PR2 = fliplr(conj(PR1));

if mod(length(mmean),2)==0
    PR = [1 PR1(1:end-1) 1 PR2(2:end)];
else
     PR = [1 PR1 PR2];
end
randomBarcode = ifft(mmean.*PR);
