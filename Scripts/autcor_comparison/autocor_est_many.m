
%load 'DATABASE_3430_prob_seq.mat';

psfSigmaWidth_bp = 1.1157e+03;


saveFolder ='Results/averagedSeq';

maxL = 502637;
nn = 1;


halfL = floor(maxL/2+1);
ssum = zeros(1,halfL);


for iInd= 1:nn;
    intV = linspace(1,round(length(probVec{iInd}/2)),halfL);
    %newV{iInd} = abs(interp1(fftEst{iInd},intV ));
    tempfft = abs(fft(probVec{iInd})).^2;
    intVal = interp1(tempfft, intV );
    ssum = ssum + intVal;
end
ssum = ssum/nn;

fftEst1 = [ssum fliplr(ssum(2:end))];

ker = ZEROMODEL.gaussian_kernel(length(fftEst1),psfSigmaWidth_bp);

fftEst2 = (abs(fft(ker))).^2;

fftEst = fftEst1.*fftEst2;

autocorEstTilde =ifft(fftEst);

%autocorEst = autocorEstTilde - mean(autocorEstTilde);
autocorEst = autocorEstTilde;

autocorEstNormalised = autocorEst/autocorEst(1);

f1 = figure( 'Visible','off'); plot(autocorEstNormalised)
xlabel('basepair')
ylabel('autocorrelation')

tt = randi(1000,1);

saveFile = sprintf('refBar%d.eps',tt);
saveF = strcat(saveFolder, saveFile);
saveas(f1,saveF,'epsc') 

