

psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);


psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);


seqLen = 20000;

seq=repmat('A',1,seqLen);

ntSeq = [seq 'CCCC' seq];
  
numWsCumSum = cumsum((ntSeq == 'A')  | (ntSeq == 'T')  |  (ntSeq== 'W'));
wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
barcode_bpRes = -zscore(CBT.apply_point_spread_function(wwwwPresence, psfSigmaWidth_bp));
barcode_pxRes = zscore(barcode_bpRes(1:pixelWidth_bps:end));

% bb = CBT.cb_netropsin_vs_yoyo1_plasmid(ntSeq,settings.NETROPSINconc,settings.YOYO1conc,1000,true);
% seqq = zscore(CBT.apply_point_spread_function(bb, psfSigmaWidth_bp));
% resSeq = NUMERICAL.rescale_barc(seqq, settings);

figure,%plot(resSeq)
hold on
plot(barcode_pxRes)

[autC,~] = COMPARISON.cross_correlation_coefficients_fft(barcode_pxRes,barcode_pxRes);

figure, plot(circshift(autC,[0,-30]))

% 
% dd = circshift(barcode_pxRes,[0,-9]);
% figure, plot(dd)
% hold on
% plot(barcode_pxRes)
% 
% 
% dd = circshift(barcode_pxRes,[0,-6]);
% figure, plot(dd)
% hold on
% plot(barcode_pxRes)
% 
% dd = circshift(barcode_pxRes,[0,-7]);
% figure, plot(dd)
% hold on
% plot(barcode_pxRes)
% 
% 
% dd(1)*barcode_pxRes(1)

Mdl = arima('AR',{0.75,0.15},'SAR',{0.9,-0.5,0.5},...
    'SARLags',[12,24,36],'MA',-0.5,'Constant',2,...
    'Variance',1);

rng(1); % For reproducibility
y = zscore(simulate(Mdl,1000));

figure, autocorr(y)


seqLen = 20000;

seq=repmat('A',1,seqLen);

ntSeq = [seq 'CCCC' seq];
  
numWsCumSum = cumsum((ntSeq == 'A')  | (ntSeq == 'T')  |  (ntSeq== 'W'));
wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
barcode_bpRes = CBT.apply_point_spread_function(wwwwPresence, psfSigmaWidth_bp);
barcode_pxRes = zscore(barcode_bpRes(1:pixelWidth_bps:end));


figure, autocorr(barcode(30:60),29)

%--------------------%
psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);


ntSeq = ZEROMODEL.simulate_peaks(10,40000);
  
% numWsCumSum = cumsum((ntSeq == 'C')  | (ntSeq == 'G') );
% wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
wwwwPresence = zeros(1,130000);
wwwwPresence(10000:50000:120000) = 1;
barcode_bpRes = CBT.apply_point_spread_function(wwwwPresence, 0.5*psfSigmaWidth_bp);
barcode_pxRes = barcode_bpRes(1:pixelWidth_bps:end);

figure, plot(barcode_pxRes)

barcode_pxRes = (barcode_pxRes-mean(barcode_pxRes))/std(barcode_pxRes);
[autC,~] = COMPARISON.cross_correlation_coefficients_fft(barcode_pxRes,barcode_pxRes);

figure, plot(autC)

% http://jespertoftkristensen.com/JTK/Blog/Entries/2013/12/29_Efficient_computation_of_autocorrelations%3B_demonstration_in_MatLab_and_Python.html
c = xcorr(barcode_pxRes, size(barcode_pxRes,2)/2, 'coeff');
figure, plot(c)
hold on
c = xcorr(barcode_bpRes, size(barcode_bpRes,2)/2, 'coeff');
figure,plot(c(1:pixelWidth_bps:end))

c = xcorr(barcode, floor(size(barcode,2)/2), 'coeff');
figure, plot(c)



gaussianPeaks = find(wwwwPresence);
figure, 
pdSix = fitdist(transpose(gaussianPeaks),'Kernel','BandWidth',psfSigmaWidth_bp);
x = 0:100:size(wwwwPresence,2);
ySix = pdf(pdSix,x);
plot(x,ySix,'k-','LineWidth',1)

ntSeq = plasmid;

numWsCumSum = cumsum((ntSeq == 'C')  | (ntSeq == 'G') );
wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
gaussianPeaks = find(wwwwPresence);

%figure, 
pdSix = fitdist(transpose(gaussianPeaks),'Kernel','Width',psfSigmaWidth_bp);
x = 0:pixelWidth_bps:size(wwwwPresence,2);
ySix = pdf(pdSix,x);
%plot(x,ySix,'k-','LineWidth',1)


numWsCumSum = cumsum((ntSeq == 'C')  | (ntSeq == 'G'));
wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
barcode_bpRes = CBT.apply_point_spread_function(transpose(wwwwPresence), psfSigmaWidth_bp, false);
barcode_pxRes = barcode_bpRes(1:pixelWidth_bps:end);
figure, plot(barcode_pxRes)
hold on
plot(zscore(ySix))


wwwwPresence = zeros(1,130000);
wwwwPresence(10000:50000:120000) = 1;
gaussianPeaks = find(wwwwPresence);

figure, 
pdSix = fitdist(transpose(gaussianPeaks),'Kernel','BandWidth',psfSigmaWidth_bp,'width', psfSigmaWidth_bp);
x = 0:pixelWidth_bps:size(wwwwPresence,2);
ySix = pdf(pdSix,x);
plot(x,zscore(ySix),'k-','LineWidth',1)

c = xcorr(ySix, floor(size(ySix,2)/2), 'coeff');
figure, plot(c)

[autC,~] = COMPARISON.cross_correlation_coefficients_fft(zscore(ySix),zscore(ySix));
figure,plot(autC)
