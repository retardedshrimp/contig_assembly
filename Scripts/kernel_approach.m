% 31/10/16 we try out kernel approach for fitting a sum of Gaussians

psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);



SixMPG = transpose(10000:2000:12000);
% figure
% histogram(SixMPG)

figure
pdSix = fitdist(SixMPG,'Kernel','BandWidth',2*psfSigmaWidth_bp);
x = 0:pixelWidth_bps:SixMPG(end)+10000;
ySix = pdf(pdSix,x);
plot(x,ySix,'k-','LineWidth',1)

% Plot each individual pdf and scale its appearance on the plot
hold on
for i=1:size(SixMPG,1)
    pd = makedist('Normal','mu',SixMPG(i),'sigma',2*psfSigmaWidth_bp);
    y = pdf(pd,x);
    y = y/size(SixMPG,1);
    plot(x,y,'b:')
end
hold off

% linear version's xcorr
[acor,lag] = xcorr(ySix, size(ySix,2), 'coeff');
figure, plot(acor(size(ySix,2)+1:end))


ntSeq = plasmid;

numWsCumSum = cumsum((ntSeq == 'C')  | (ntSeq == 'G') );
wwwwPresence = 1 * (numWsCumSum(5:end) == numWsCumSum(1:end-4) + 4);
gaussianPeaks = transpose(find(wwwwPresence));

figure
pdSix = fitdist(gaussianPeaks,'Kernel','Width',2*psfSigmaWidth_bp);
x = 0:pixelWidth_bps:size(wwwwPresence,2);
ySix = pdf(pdSix,x);
plot(x,ySix,'k-','LineWidth',1)


[acor,lag] = xcorr(zscore(ySix),'coeff');
figure, plot(acor)

[acor,lag] = xcorr(ySix,'coeff');
figure, plot(acor)

%aa = autocorr(ySix,300);
%figure, plot(aa)

mm = mean(barcode_pxRes);
dd = std(barcode_pxRes);
at = (barcode_pxRes-mm)/dd;
[acor,lag] = xcorr(at);
figure, plot(acor)

zz = at*dd+mm;
[acor,lag] = xcorr(zz);
figure, plot(acor)

[acor,lag] = xcorr(barcode_pxRes);
figure, plot(acor)

[autC,~] = COMPARISON.cross_correlation_coefficients_fft(barcode_pxRes,barcode_pxRes);
figure,plot(autC)
figure, autocorr(barcode_pxRes,300)
