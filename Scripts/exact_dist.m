
% 31/10/16 exact correlation function for exponential process
% following https://se.mathworks.com/help/signal/ref/xcorr.html

a = 0.95; 

N = 28; 

xx = -N-1:0.1:N-1;

yy = a.^(abs(xx)).*(a.^(2.*(N+abs(xx))-1))./log(a);

figure, plot(yy)

a = 0.95;



N = 28;
n = 0:N-1;
lags = -(N-1):(N-1);

x = a.^n;
c = xcorr(x);

figure,
stem(lags,c);
hold on

fs = 10;
nn = -(N-1):1/fs:(N-1);
cc = a.^abs(nn)/(1-a^2);

dd = (1-a.^(2*(N-abs(nn))))/(1-a^2).*a.^abs(nn);
plot(nn,dd)
figure, plot(nn,dd)
