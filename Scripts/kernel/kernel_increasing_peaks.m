% This is used to analyse how the autocorrelation behaves for simulated
% number of peaks at random places along the barcode

% psf settings
psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

% where to save results
saveFolder ='Results/kernel';
label = 'IncreasingPeaks';

% random number
tt = randi(1000,1);

%savePlot = sprintf('%s%d.eps',label,tt);

for numPeaks=2000:100:2000
    
    % save directory
    savePlot = sprintf('%d%s%d.eps',tt,label,numPeaks);
    saveP =  strcat(saveFolder, savePlot);

    % this generates a specified number of peaks at random places
    SixMPG = 0+randi(100000,numPeaks,1);
    SixMPG = sort(SixMPG);

    % fit a kernel of specified width on these
    pdSix = fitdist(SixMPG,'Kernel','BandWidth',2*psfSigmaWidth_bp);
    
    % choose sampling
    x = 0:pixelWidth_bps:SixMPG(end)+0;
    
    % output sequece
    ySix = pdf(pdSix,x);
    
    ySix = ySix-mean(ySix);
    %figure, plot(x(5:end-5), ySix(5:end-5),'k-','LineWidth',1)

    % compute correlation, this assumes zero mean!
    %[acor,lag] = xcorr(zscore(ySix(5:end-5)),'coeff');
    [acor,lag] = xcorr(ySix(5:end-5),'coeff');
    f= figure('visible','off');
    
    % center it
    h2 = plot(-round(size(acor,2))/2:round(size(acor,2)/2)-1,acor,'r', 'linewidth',2); % 'r', 'linewidth',3
    
    % plot with proper labels
    ylabel('autocorrelation')
    xlabel('lag')
    legend({'autocorrelation'})
    saveas(h2,saveP,'epsc') 

    figure, plot(acor)

end


