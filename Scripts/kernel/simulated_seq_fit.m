% This simulates peaks at random places along the sequence, and computes
% theoretical and experimental autocorrelation sequences for it.

sizeSeq = 100000;
numPeaks = 20000;


[ seq, acor ] = KERNEL.generate_sequence( sizeSeq, numPeaks, settings,'circ');
%figure,plot(acor)
[ sigmaEst ] = KERNEL.compute_gaussian_fit( acor,'circ' );


%sigmaEst = 6;
%[ autocorSeq ] = KERNEL.compute_gaussian_autocorr(sigmaEst,transpose(0:size(acor,2)-1));
%
[ autocorSeq ] = KERNEL.compute_gaussian_autocorr(sigmaEst,transpose(-round(size(acor,2))/2:round(size(acor,2)/2)-1));

COMPARISON.compare_eigenvalues(autocorSeq,acor );

% 
% ss = 0;
% numSim = 10000;
% for i=1:numSim
%     [ seq, acor ] = KERNEL.generate_sequence( sizeSeq, numPeaks, settings,'circ');
%         %figure,plot(acor)
%     [ sigmaEst ] = KERNEL.compute_gaussian_fit( acor,'circ' );
%     ss = ss+sigmaEst;
% end
% ss = ss/numSim;

% [ autocorSeq ] = KERNEL.compute_gaussian_autocorr(ss,transpose(-round(size(acor,2))/2:round(size(acor,2)/2)-1));
% 
% 
% [ seq, acor ] = KERNEL.generate_sequence( sizeSeq, numPeaks, settings,'circ');
% 
% COMPARISON.compare_eigenvalues(autocorSeq,acor );

