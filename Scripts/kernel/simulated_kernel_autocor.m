% This is used to analyse how the autocorrelation behaves for simulated
% number of peaks at random places along the barcode

% psf settings
psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
pixelWidth_bps = round(settings.bpPerNm*settings.camRes);

% where to save results
saveFolder ='Results/kernel';
label = 'IncreasingPeaks';

% random number
tt = randi(1000,1);

%savePlot = sprintf('%s%d.eps',label,tt);

nn = 100;

savePlot = sprintf('%d%s%d.eps',tt,label,nn);
saveP =  strcat(saveFolder, savePlot);

aacor = zeros(1,319);
sizeBar = 100000;

for ii=1:nn

    numPeaks=2000;
    % save directory

    % this generates a specified number of peaks at random places
    SixMPG = 0+randi(sizeBar,numPeaks,1);
    SixMPG = sort(SixMPG);

    % fit a kernel of specified width on these
    pdSix = fitdist(SixMPG,'Kernel','BandWidth',2*psfSigmaWidth_bp);

    % choose sampling
    x = 0:pixelWidth_bps:sizeBar+0;

    % output sequece
    ySix = pdf(pdSix,x);

    %ySix = ySix-mean(ySix);
    
    %figure, plot(x(5:end-5), ySix(5:end-5),'k-','LineWidth',1)

    % compute correlation, this assumes zero mean!
    %[acor,lag] = xcorr(zscore(ySix(5:end-5)),'coeff');
    [acor,lag] = xcorr(ySix(5:end-5)-mean(ySix(5:end-5)),'coeff');

    aacor = aacor+acor;
    
    
end

aacor = aacor/nn;

f= figure('visible','off');

% center it
h2 = plot(-round(size(aacor,2))/2:round(size(aacor,2)/2)-1,aacor,'r', 'linewidth',2); % 'r', 'linewidth',3

% plot with proper labels
ylabel('autocorrelation')
xlabel('lag')
legend({'autocorrelation'})
saveas(h2,saveP,'epsc') 

figure, plot(aacor)


corMat2 = toeplitz(aacor);
figure, plot(eig(corMat2))


testSeq = transpose(aacor);

t = transpose(-round(size(aacor,2))/2:round(size(aacor,2)/2)-1);
f = @(y) exp(-t.^2./(2*y.^2))-testSeq;

flB = 2;

gg =  @(y) exp(-t.^2./(2*y.^2));
xx = [lsqnonlin(f,flB)];
xx

figure, plot(gg(xx))
yy = gg(xx);

corMat2 = toeplitz(yy);
figure, plot(eig(corMat2))
hold on
corMat2 = toeplitz(aacor);
plot(eig(corMat2))
legend({'Eigenvalues from Gaussian autocorrelation matrix, Eigenvalues from sample autocorrelation' })


numPeaks=1;
% save directory

% this generates a specified number of peaks at random places
SixMPG = 0+randi(sizeBar,numPeaks,1);
SixMPG = sort(SixMPG);

% fit a kernel of specified width on these
pdSix = fitdist(SixMPG,'Kernel','BandWidth',2*psfSigmaWidth_bp);

% choose sampling
x = 0:pixelWidth_bps:sizeBar+0;

% output sequece
ySix = pdf(pdSix,x);

figure, plot(ySix)

[acor,lag] = xcorr(ySix(5:end-5)-mean(ySix(5:end-5)),'coeff');
figure, plot(acor)


ss = COMPARISON.autocor_coefficients_fft(barcode_bpRes(10:end-10), 'linear2');

corMat2 = toeplitz(acor);
figure, plot(eig(corMat2))
