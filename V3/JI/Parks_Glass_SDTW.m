%Extends classixal SDTW by finding length-constrained minimum
%aveerage (LCMA) segments


function Parks_Glass_SDTW
%% Parameters
commandwindow();
theorySequenceFilepath = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/sequences/T7.fasta';
theorySequenceFilepath2 = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/R100/r100.fasta';

%Get Sequences
import NtSeq.Import.try_import_fasta_nt_seq;
[~, theorySequence] = try_import_fasta_nt_seq(theorySequenceFilepath);
[~, theorySequence2] = try_import_fasta_nt_seq(theorySequenceFilepath2);


%Egna
r = 0.1;                 % restricts the searching path width to r % of  total length of the longest sequence
l = 10;                  % minimum length (in steps of pixels) of LCMA segment. 
treshold = 0.24;
bands = 68;


import CBT.get_default_barcode_gen_settings;
defaultBarcodeGenSettings = get_default_barcode_gen_settings();
barcodeGenSettings = defaultBarcodeGenSettings;

import CBT.gen_zscaled_cbt_barcode;
x = gen_zscaled_cbt_barcode(theorySequence, barcodeGenSettings);
y = gen_zscaled_cbt_barcode(theorySequence2, barcodeGenSettings);
    
%% Main


%x = PhaseRand(x);
disp(length(x));

% %Tampering, adding noise etc
x = awgn(x,4,'measured','linear');
y = awgn(y,4,'measured','linear');
y = smooth(y,6);
x = smooth(x,6);
y = smooth(y,3);
x = smooth(x,3);



%Fits the shortest sequence onto the longest
if length(y)<length(x)
    [y, x] = deal(x,y);
end
%y = flipud(y);

x = cat(1, x(1:105), x(116:end));

disp('Cross correlation:');
disp((xcorr(x,y,0)/(length(x)-1)));

disp(xcorr(x(1:105),y(1:105),0,'coeff'));
disp(length(x));
disp(xcorr(x(106:165),y(116:175),0,'coeff'));



%Fill Accumulated Cost Matrix with boundary condition values. No BC is used
%in P&G, but initial row & column of zero is nessecary for iterative comp.
accumulatedCM(1:length(x)+1,1) = 0;
accumulatedCM(1,1:length(y)+1) = 0;
%accumulatedCM(end,1) = Inf;

%Mean_Variance_Matching.initiate;
%[pathcost, path] = Mean_Variance_Matching.mVM(x,y,costMatrix);



%Compute Cost Matrix & Accumulated Cost
for n=1:length(x)
    for m=1:length(y)
        %Cost matrix in absolute difference
        costMatrix(n,m) = sqrt((x(n)-y(m))^2);
        
        %The following line not nessecary in P&G
        %costMatrix(n,m+length(y)) = sqrt((x(n)-y(m))^2);
                
        %Accumulated cost matrix
        accumulatedCM(n+1,m+1) = costMatrix(n,m) + min([accumulatedCM(n,m), accumulatedCM(n,m+1), accumulatedCM(n+1,m)]);
        
        
        %The following line not nessecary in P&G
        %accumulatedCM(n+1,m+length(y)+1) = costMatrix(n,m) + min([accumulatedCM(n,m), accumulatedCM(n,m+1), accumulatedCM(n+1,m)]);
        
    end
end


%Calculate Distance Function distFctn
distFctnM = accumulatedCM(end,:);
distFctnN = accumulatedCM(:,end);



%Calculate best Warping Path p_star

%Parameters below should probably be set as fctn input in final version
c = bands;                                 %divides x+y into c regions to find a path within each region
r = int16(((length(x)+length(y))/(2*c) -1/2));   %Path width, adjusts dynamically to match c
fprintf('r: %d\n', r);


%Loop for setting up centres of bands. Used to implement width constraint.
for k = 1:c
    if 2*k*r - r + k <= length(y)
        m1(k) = 2*k*r -r + k;
        n1(k) = length(x)+1;
        
        %%Use the following if you want paths to start att minimas of distance fctn.
        %%I've cloncluded this is a bad idea since it affects the paths
        %%flexibility
        %%[~,index] = min(distFctnM(int16((m1(k)-r)):int16((m1(k)+r))));
        %%p_star{k} = int16([n1(k) m1(k)-r-1+index]);
        %%p_cost{k} = 0;
    else
        m1(k) = length(y)+1;
        n1(k) = -(2*k*r -r + k)+2 + length(y) +length(x); %OBS +1 from corner and +1 from boudary conditions =+2
        
        %%Use the following if you want paths to start att minimas of distance fctn.
        %%I've cloncluded this is a bad idea since it affects the paths
        %%flexibility
        %%[~,index] = min(distFctnN(int16((n1(k)-r+1)):int16(n1(k)+r)));
        %%p_star{k} = int16([n1(k)-r+index m1(k)]);
        %%p_cost{k} = 0;
    end
    
    %Sets starting coordinates of the different paths
    p_star{k} = [n1(k), m1(k)];
    p_cost{k} = 0;
    
    
end


%OBS Path start points are determined before loop

%Computes Paths
for k = 1:c
    %Get starting coordinates
    n = p_star{k}(1,1);
    m = p_star{k}(1,2);
    while n > 1 && m > 1    %Continues ultil path reaches row or column one.
        %Checks for the next step
        if abs((n-n1(k))-(m-m1(k))) < r %checks for Sakoe width constraint. Max deviation is +/-r i.e. band width = 2r+1.
            [~,i] = min([accumulatedCM(n-1,m-1), accumulatedCM(n-1,m), accumulatedCM(n,m-1)]); %ordinary path
        elseif (n-n1(k))-(m-m1(k)) < 0 %x-coord at cap, n-step not allowed
            [~,i] = min([accumulatedCM(n-1,m-1), Inf, accumulatedCM(n,m-1)]);
        elseif (n-n1(k))-(m-m1(k)) > 0 %y-coord at cap, m-step not allowed
            [~,i] = min([accumulatedCM(n-1,m-1), accumulatedCM(n-1,m), Inf]);
        end
        if i == 1
            n = n-1;
            m = m-1;
        elseif i == 2
            n = n-1;
        else
            m = m-1;
        end
        

        %p_star stores the coordinates for path k in a 2-column matrix
        %p_cost stores the step costs along path k
        p_star{k} = cat(1, p_star{k}, [n m]);
        if n > 1 && m > 1
            p_cost{k} = cat(1, p_cost{k}, costMatrix(n-1,m-1)); %OBS n&m is in ACM, therefore -1 to get CM values
        end
        
    end
end


%Inverts Warp Paths (because of recursive computation) for plotting purposes
for k = 1:c
    p_cost{k} = flipud(p_cost{k});
    p_star{k} = flipud(p_star{k});
end

% %Creates and stores LCMA segments
% for k = 1:c
%     %p_starLCMA{k,1} = [];
%     lcma = egenLCMA(p_cost{k}, 0.12, 12);
%     p_starLCMA{k} = egenLCMA(p_cost{k}, 0.12, 12);
% %     if isempty(lcma);
% %         continue
% %     end
% %     disp(lcma);
% %     disp(length(lcma(:,1)));
% %     %Following loop converts lcma indices to path coordinates
% %     for p = 1:length(lcma(:,1))
% %         %Stores p LCMA segments of path k
% %         p_starLCMA{k,p} = p_star{k}(lcma(p,1):lcma(p,2));
% %         disp(p_starLCMA{k,p});
% %     end
% %     %celldisp(p_starLCMA);
% %disp(size(p_starLCMA(k,:)));
% %disp(p_starLCMA{k});
% 
% end
% disp(length(p_starLCMA));
% celldisp(p_starLCMA);
% 
% for i = 1:c
%     disp(length(p_starLCMA{k}));
% end

%% Plots
figure
imagesc(accumulatedCM);
colorbar;
colormap('pink');
title('Accumulated Cost Matrix');hold on;
for k = 1:c
    plot(p_star{k}(:,2), p_star{k}(:,1), 'r');hold on;
end
hold off;
%plot(p_star(1:8:end,2),p_star(1:8:end,1),'ro');hold off;


%Surface plot. Wortless.
% figure
% surfc(accumulatedCM);


%Before plotting: adjust the paths plotted (in CM-plot) to account for
%boundary conditions
[m1(:), n1(:)] = deal(m1(:)-1, n1(:)-1);
for k = 1:c
    p_star{k} = p_star{k}-1;
    %p_starLCMA{k} = p_starLCMA{k}-1;
end


figure
reset(gcf);
imagesc(costMatrix);
colorbar;
colormap('pink');
title('Cost Matrix');hold on;
for k = 1:c
    plot(p_star{k}(:,2), p_star{k}(:,1), 'r');hold on;
    %Add following line to add spaced rings in path plot
    %plot(p_star{k}(1:8:end,2), p_star{k}(1:8:end,2), 'ro');hold on; %(1:8:end,2,1),p_star(1:8:end,1,1),'ro');hold on;
    
    %Adds band centres as stars
    plot(m1(k),n1(k),'r*');hold on;
    
    %Adds LCMA segments as thick lines
    lcma = egenLCMA(p_cost{k}, treshold, 30);
    if isempty(lcma) == 0
        for i = 1:length(lcma(:,1))
            plot(p_star{k}(lcma(i,1)+1:lcma(i,2)+1,2), p_star{k}(lcma(i,1)+1:lcma(i,2)+1,1), 'r', 'linewidth', 4);hold on;
            %The +1's above accounts for the "missing rows" in costMatrix
        end
    end
end
hold off;


figure
title('Distance Function X&Y');hold on;
plot(distFctnN);hold on;plot(distFctnM);hold off;


% Not informative for multiple k
figure
for k = 1:c
    plot(p_cost{k});hold on;
end
axis auto;
title('Step cost along OWP');
hold off;


figure
for k = 1:c
    bars(k) = sum(p_cost{k})/length(p_cost{k});
end
bar(bars);
title('Average Cost of Paths');
hold off;


figure
for k = 1:c
    plot(p_star{k}(:,1),p_star{k}(:,2));hold on;
end
title('Optimal Warping Paths (OWP)');
hold off;


%Plot sequences without alignment
figure 
plot(y);hold on;
plot(x);hold off;
title('Sequence Plot (w/o Alignment)');

figure
for i = 1:length(x)
    plot(x(i), y(i), 'ro');hold on;
end
plot(x,y(1:length(x)),'b');hold on;
title('Linear Relation');

%PLot sequences after alignment
%figure 
%y = vertcat(y(p_star(1,2)+1:end),y(1:p_star(1,2)));
%plot(y);hold on;
%plot(x);hold off;
%title('With Alignment');


%This is for stretching out sequence to the best fit. Doesnt work
%x(end+1) = x(end);
%for i=1:length(p_star)-1
%    if p_star(i,1) == p_star(i+1,1)
%        x = vertcat(x(1:i), x(i), x(i+1:end));
%    elseif p_star(i,2) == p_star(i+1,2)
%        x(i) = [];
%    end
%end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

%% Lin et al LCMA function (Unfinished)
% function [minimumAverageSubseq] = lCMA(A, l)
% %Finds the length constrained minimum average (LCMA) of sequence A with
% %length constraint l
% % p is right skew pointers from fctn drs_points
% 
% p = Parks_Glass_SDTW.drs_points(sequence);
% n = length(A);
% 
% for i = 1:n-l+1
%     j = i+l-1;
% 
%     if mean(A(i:j)) < mean(A(j+1:p(j+1)))
%         j = locate(i, j);
%     end
%     g(i) = j;
% end
% 
% minimumAverageSubseq = [i j];
% 
% end
% 
% 
% 
% function [j_star] = locate(i, j)
% %Finds the minimum average subsequence of subsequence A(i:j) 
% end
% 
% 
% function [p] = drs_points(i, j)
% %Finds n right-skew pointers of sequence A and stores them in p
% end
%

%% Egen LCMA function
function [lcma_array] = egenLCMA(a, t, l)
%Finds all length constrained minimum averages (LCMA) of length at least l
%below a treshold average t of the sequence a and stores
%START AND END INCICES of each LCMA segment in a separate row
%as elements in lcma_array (Brute force method)

n = length(a);
lcma_array = [];

for i = 1:n-l
    j = i+l-1;
    while mean(a(i:j+1)) <= mean(a(i:j))
        j = j+1;
        if j == n
            break
        end
    end
    if mean(a(i:j)) <= t
        lcma_array(end+1,:) = [i j];
    end
end

if isempty(lcma_array)
    return;
end

%merge related LCMAs
i = length(lcma_array(:,1));
while i > 1
    if lcma_array(i,2) < lcma_array(i-1,2)
        lcma_array(i,:) = [];
    elseif lcma_array(i,1) < lcma_array(i-1,2)
        lcma_array(i-1,2) = lcma_array(i,2);
        lcma_array(i,:) = [];
    %elseif lcma_array(i,1) > lcma_array(i-1,2)
    end
    i = i - 1;
end

end


%% Generates Purely Random Sequence
function sequence = PureRandSeq(length)
sequence = repmat(char(0),1,length);
for i = 1:length
    x = int8(rand*3);
    if x == 0
        sequence(i) = 'A';
    elseif x == 1
        sequence(i) = 'T';
    elseif x == 2
        sequence(i) = 'G';
    elseif x == 3
        sequence(i) = 'C';
    end
end
end

%% Phase Randomize Barcode
function randomBarcode = PhaseRand(inputBarcode)
ZMfft = fft(inputBarcode);
fi = rand(1,floor((length(ZMfft)-1)/2));
PR1 = exp(2i*pi*fi);
PR2 = fliplr(exp(2i*pi*(-fi)));
disp(length(ZMfft(1,:)));
if mod(length(ZMfft),2) == 0
    fftRand = ZMfft'.*[1 PR1 1 PR2];
else
    fftRand = ZMfft'.*[1 PR1 PR2];
end
randomBarcode = ifft(fftRand)';
end






