%Collects MVM statistics for a given barcode vs different phase randomized
%barcodes
function MVM_stats()
    theorySequenceFilepath = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/sequences/T7.fasta';

    import NtSeq.Import.try_import_fasta_nt_seq;
    [~, theorySequence] = try_import_fasta_nt_seq(theorySequenceFilepath);

    import CBT.get_default_barcode_gen_settings;
    defaultBarcodeGenSettings = get_default_barcode_gen_settings();
    barcodeGenSettings = defaultBarcodeGenSettings;

    import CBT.gen_zscaled_cbt_barcode;
    barcode = gen_zscaled_cbt_barcode(theorySequence, barcodeGenSettings);
    
    
    import CBT.RandBarcodeGen.PhaseRandomization.phase_randomize_barcode;
    
    meanZeroModelFftFreqMags = abs(fft(barcode));

    numRandomizations = 10000;
    meanPathCost = zeros(1, numRandomizations);
    jumps = zeros(1, numRandomizations);
    for randomizationNum = 1:numRandomizations
        phaseRandomizedBarcode = phase_randomize_barcode(meanZeroModelFftFreqMags);
        [meanPathCost(randomizationNum), jumps(randomizationNum)] = Mean_Variance_Matching(barcode, phaseRandomizedBarcode);   
    end
    
    hFig = figure();
    hParent = hFig;
    hAxisMeanPathCost = axes('Parent', hParent);
    histogram(hAxisMeanPathCost, meanPathCost);
    title(hAxisMeanPathCost, 'Average Path Cost');
    xlabel(hAxisMeanPathCost, 'Cost');
    ylabel(hAxisMeanPathCost, 'Occurance');
    
    hFig = figure();
    hParent = hFig;
    hAxisJumps = axes('Parent', hParent);
    histogram(hAxisJumps, jumps);
    title(hAxisJumps, 'Number of Jumps');
    xlabel(hAxisJumps, 'Jumps');
    ylabel(hAxisJumps, 'Occurance');

end
