%Same as Parks_Glass script but implements Bands when computing ACM 


function LCMA_method_revised
%% Parameters
commandwindow();
%Get Sequences

theorySequenceFilepath = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/sequences/T7.fasta';
theorySequenceFilepath2 = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/R100/r100.fasta';

import NtSeq.Import.try_import_fasta_nt_seq;
[~, theorySequence] = try_import_fasta_nt_seq(theorySequenceFilepath);
[~, theorySequence2] = try_import_fasta_nt_seq(theorySequenceFilepath2);

import CBT.get_default_barcode_gen_settings;
defaultBarcodeGenSettings = get_default_barcode_gen_settings();
barcodeGenSettings = defaultBarcodeGenSettings;

import CBT.gen_zscaled_cbt_barcode;
x = gen_zscaled_cbt_barcode(theorySequence, barcodeGenSettings);
y = gen_zscaled_cbt_barcode(theorySequence2, barcodeGenSettings);

%Artivficial Insert and y-permutation
y = cat(1, y, y);
%y = cat(1, y(100:end), y(1:99));
%x = cat(1, x(1:100), x(116:end));
import CBT.RandBarcodeGen.PhaseRandomization.phase_randomize_barcode;
x = phase_randomize_barcode(x);

%Egna
r = 3;                  % restricts the searching path width to r pixels of  total length of the longest sequence
l = 8;                 % minimum length (in steps of pixels) of LCMA segment. 
treshold = 0.12;
bands = 20;
%Parameters below should probably be set as fctn input in final version
c = bands;              %divides x+y into c regions to find a path within each region. Should be choosen
                        %carefully since they may not add upp evenly
bestpathcount = 2;
                        
r = ceil(((length(y)/2)-c)/(2*c));   %Path width, adjusts dynamically to match c
fprintf('r: %d\n', r);


%% Setup

% %Tampering, adding noise etc
% x = awgn(x,1.55,'measured','linear');
% y = awgn(y,1.55,'measured','linear');
% y = smooth(y,5);
% x = smooth(x,5);
% y = smooth(y,7);
% x = smooth(x,7);

%Fits the shortest sequence onto the longest
if length(y)<length(x)
    [y, x] = deal(x,y);
end

%Statistics. Needs alignment.
absDiff = 0;
diff = 0;
nonNanCount = 0;
maxDiff = 0;
for i = 1:length(x)
    if abs(x(i) - y(i)) > maxDiff
        maxDiff = abs(x(i) - y(i));
    end
    absDiff = absDiff + abs(x(i) - y(i));
    diff = diff + (x(i) - y(i));
    nonNanCount = nonNanCount+1;
end
absDiff = absDiff/length(x);
diff = diff/length(x);
disp('Average max difference');
disp(maxDiff);
disp('Total average absolute differene');
disp(absDiff);
disp('Total average difference');
disp(diff);
% Display cross correlations
disp('Cross correlation:');
disp((xcorr(x,y(1:length(x)),0, 'coeff')));
% disp(xcorr(x(1:100),y(1:100),0,'coeff'));
% disp(xcorr(x(101:160),y(116:175),0,'coeff'));


%Compute Cost Matrix
for n=1:length(x)
    for m=1:length(y)
        costMatrix(n,m) = sqrt((x(n)-y(m))^2);      
    end
end

%% ACM
%Fill Accumulated Cost Matrix with boundary condition values
accumulatedCM(1,1) = 0;
accumulatedCM(2:length(x)+1,1) = costMatrix(:,1);
accumulatedCM(1,2:length(y)+1) = costMatrix(1,:);
accumulatedCM(2:end,1:end) = Inf;

%Loop for setting up centres of bands. Used to implement bands. 1's are
%used when computing paths & 2's when computing ACM
for k = 1:c
    %if 2*k*r - r + k <= length(y)
        m2(k) = 2*k*r -r + k;
        n2(k) = 1;
        %m1(k) = 2*k*r -r + k;
        %n1(k) = length(x)+1;
    %else
    %    m1(k) = length(y)+1;
    %    n1(k) = -(2*k*r -r + k)+2 + length(y) +length(x); %OBS +1 from corner and +1 from boudary conditions =+2
    %end
end
for k = 1:c
    n = n2(k);
    m = m2(k);
    while n < length(x)+1 && m < length(y)+1
        n = n+1;
        m = m+1;
    end
    n1(k) = n;
    m1(k) = m;
end
% Compute ACM loop    
for k= 1:c
    for n=2:length(x)+1
        for m=2:length(y)+1
            if abs((n-n2(k))-(m-m2(k))) > r %|| m-n > length(y)/2 +1
                continue
            elseif abs((n-n2(k))-(m-m2(k))) < r
                accumulatedCM(n,m) = costMatrix(n-1,m-1) + min([accumulatedCM(n-1,m-1), accumulatedCM(n-1,m), accumulatedCM(n,m-1)]);
            elseif (n-n2(k))-(m-m2(k)) < 0
                accumulatedCM(n,m) = costMatrix(n-1,m-1) + min([accumulatedCM(n-1,m-1), accumulatedCM(n,m-1)]);
            elseif (n-n2(k))-(m-m2(k)) > 0
                accumulatedCM(n,m) = costMatrix(n-1,m-1) + min([accumulatedCM(n-1,m-1), accumulatedCM(n-1,m)]);
            end
        end
    end
end



%% Paths
%Calculate Distance Functions
distFctnM = accumulatedCM(end,:);
distFctnN = accumulatedCM(:,end);
distFctn = horzcat(distFctnM,distFctnN');


%Loop for setting up starting coordinates of paths
for k = 1:c
    if 2*k*r - r + k <= length(y)
        try
            [~,index] = min(accumulatedCM(end,((m1(k)-r):(m1(k)+r))));
        catch
            [~,index] = min(distFctn((m1(k)-r):(m1(k)+r)));
            if index > length(y)
                index = index - length(y);
            end
        end
        p_star{k} = [n1(k) (m1(k)-r-1+index)];
        p_cost{k} = 0;
    else
       
        try
            [~,index] = min(distFctnN(((n1(k)-r+1)):(n1(k)+r)));
        catch
            if n1(k)-r < 0
                [~,index] = min(distFctnN((1:n1(k)+r)));
            else
                [~,index] = min(distFctnN((n1(k)-r:end)));
            end
        end
        p_star{k} = [((n1(k))-(r)+(index)-1) m1(k)];
        p_cost{k} = 0;
    end
end


%Computes Paths loop
for k = 1:c
    %Get starting coordinates
    n = p_star{k}(1,1);
    m = p_star{k}(1,2);
    while n > 1 && m > 1    %Continues ultil path reaches row or column one.
        %Checks for the next step
        if abs((n-n1(k))-(m-m1(k))) < r-1 %checks for Sakoe width constraint. Max deviation is +/-r i.e. band width = 2r+1.
            %Observe that r-X above restricts the paths to be separated
            %by X pixels. This could be good to prevent adjacent bands to
            %find paths that are very similar in cost.
            [~,i] = min([accumulatedCM(n-1,m-1), accumulatedCM(n-1,m), accumulatedCM(n,m-1)]); %ordinary path
        elseif (n-n1(k))-(m-m1(k)) < 0 %x-coord at cap, n-step not allowed
            [~,i] = min([accumulatedCM(n-1,m-1), Inf, accumulatedCM(n,m-1)]);
        elseif (n-n1(k))-(m-m1(k)) > 0 %y-coord at cap, m-step not allowed
            [~,i] = min([accumulatedCM(n-1,m-1), accumulatedCM(n-1,m), Inf]);
        end
        if i == 1
            n = n-1;
            m = m-1;
        elseif i == 2
            n = n-1;
        else
            m = m-1;
        end
        %p_star stores the coordinates for path k in a 2-column matrix
        %p_cost stores the step costs along path k
        p_star{k} = cat(1, p_star{k}, [n m]);
        if n > 1 && m > 1
            p_cost{k} = cat(1, p_cost{k}, costMatrix(n-1,m-1)); %OBS n&m is in ACM, therefore -1 to get CM values
        end
    end
end
%Inverts Warp Paths (because of recursive computation) for plotting purposes
for k = 1:c
    p_cost{k} = flipud(p_cost{k});
    p_star{k} = flipud(p_star{k});
end
% % Remove exccessive bands. Temporary ugly fix.
% removecount = floor(length(x)/(2*r+1) -1);
% for i=1:removecount
%     p_star{i} = [];
%     p_star{c-i+1} = [];
%     p_cost{i} = [];
%     p_cost{c-i+1} = [];
% end



%% Best Path
lcma_array = cell([c 1]);
pixelcount = zeros(1, c);
for k = 1:c
    lcma_array{k} = egenLCMA(p_cost{k}, treshold, l);
    if ~isempty(lcma_array{k})
        for i = 1:length(lcma_array{k}(:,1))
            pixelcount(k) = pixelcount(k) + lcma_array{k}(i,2) - lcma_array{k}(i,1);
        end
    end
end
index = zeros(1,bestpathcount);
for i = 1:bestpathcount
    [~, index(i)] = max(pixelcount);
    pixelcount(index(i)) = 0;
end
bestcost = 0;
bestpath = zeros(length(x), 2);
%Maps x sequence 1:1 to y
% for i = 1:length(x)
%     found1 = find(p_star{index(1)}(:,1) == i);
%     found2 = find(p_star{index(2)}(:,1) == i);
%     [val, idx] = min(min([p_cost{index(1)}(found1(1):found1(end)); p_cost{index(2)}(found2(1):found2(end))]));
%     bestcost = bestcost + val;
% %     if idx == 1
% %         bestpath(i,:) = p_star{index(1)}
% %     else
%         
%     %end
% end
disp(bestcost/length(x));

%

%% Plots
figure
imagesc(accumulatedCM);
colorbar;
colormap('pink');
title('Accumulated Cost Matrix');hold on;
for k = 1:c
    plot(p_star{k}(:,2), p_star{k}(:,1), 'r');hold on;
    plot(m1(k),n1(k),'r*');hold on;
    plot(m2(k),n2(k),'r*');hold on;
end
hold off;
%plot(p_star(1:8:end,2),p_star(1:8:end,1),'ro');hold off;


%Before plotting: adjust the paths plotted (in CM-plot) to account for
%boundary conditions
[m1(:), n1(:)] = deal(m1(:)-1, n1(:)-1);
for k = 1:c
    p_star{k} = p_star{k}-1;
    %p_starLCMA{k} = p_starLCMA{k}-1;
end


figure
reset(gcf);
imagesc(costMatrix);
colorbar;
colormap('pink');
title('Cost Matrix');hold on;
for k = 1:c
    plot(p_star{k}(:,2), p_star{k}(:,1), 'r');hold on;
    %Add following line to add spaced rings in path plot
    %plot(p_star{k}(1:8:end,2), p_star{k}(1:8:end,2), 'ro');hold on; %(1:8:end,2,1),p_star(1:8:end,1,1),'ro');hold on;
    
    %Adds band centres as stars
    plot(m1(k),n1(k),'r*');hold on;
    
    %Adds LCMA segments as thick lines
    lcma = egenLCMA(p_cost{k}, treshold, l);
    if isempty(lcma) == 0
        for i = 1:length(lcma(:,1))
            plot(p_star{k}(lcma(i,1):lcma(i,2),2), p_star{k}(lcma(i,1):lcma(i,2),1), 'r', 'linewidth', 4);hold on;
            %The +1's above accounts for the "missing rows" in costMatrix
        end
    end
end
hold off;


figure
title('Distance Function X&Y');hold on;
plot(distFctnN);hold on;plot(distFctnM);hold off;


% Not informative for multiple k
figure
for k = 1:c
    plot(p_cost{k});hold on;
end
axis auto;
title('Step cost along OWP');
hold off;


figure
for k = 1:c
    bars(k) = sum(p_cost{k})/length(p_cost{k});
end
bar(bars);
title('Average Cost of Paths');
hold off;


figure
for k = 1:c
    plot(p_star{k}(:,2), p_star{k}(:,1));hold on;
end
set(gca,'Ydir','reverse');
title('Optimal Warping Paths (OWP)');
hold off;


%Plot sequences without alignment
figure 
plot(y);hold on;
plot(x);hold off;
title('Sequence Plot (w/o Alignment)');

figure
for i = 1:length(x)
    plot(x(i), y(i), 'ro');hold on;
end
plot(x,y(1:length(x)),'b');hold on;
title('Linear Relation');


%Surface plot. Wortless.
figure
surfc(costMatrix+5);
zlim([0 30])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


%% Egen LCMA function
function [lcma_array] = egenLCMA(a, t, l)
%Finds all length constrained minimum averages (LCMA) of length at least l
%below a treshold average t of the sequence a and stores
%START AND END INCICES of each LCMA segment per row
%in lcma_array (Brute force method)

n = length(a);
lcma_array = [];

for i = 1:n-l
    j = i+l-1;
    while mean(a(i:j+1)) <= mean(a(i:j))
        j = j+1;
        if j == n
            break
        end
    end
    if mean(a(i:j)) <= t
        lcma_array(end+1,:) = [i j];
    end
end

if isempty(lcma_array)
    return;
end

%merge related LCMAs
i = length(lcma_array(:,1));
while i > 1
    if lcma_array(i,2) < lcma_array(i-1,2)
        lcma_array(i,:) = [];
    elseif lcma_array(i,1) < lcma_array(i-1,2)
        lcma_array(i-1,2) = lcma_array(i,2);
        lcma_array(i,:) = [];
    %elseif lcma_array(i,1) > lcma_array(i-1,2)
    end
    i = i - 1;
end

end


%% Generates Purely Random Sequence
function sequence = PureRandSeq(length)
sequence = repmat(char(0),1,length);
for i = 1:length
    x = int8(floor(rand*4));
    if x == 0
        sequence(i) = 'A';
    elseif x == 1
        sequence(i) = 'T';
    elseif x == 2
        sequence(i) = 'G';
    elseif x == 3
        sequence(i) = 'C';
    end
end
end

%% Phase Randomize Barcode
function randomBarcode = PhaseRand(inputBarcode)
ZMfft = fft(inputBarcode);
fi = rand(1,floor((length(ZMfft)-1)/2));
PR1 = exp(2i*pi*fi);
PR2 = fliplr(exp(2i*pi*(-fi)));
disp(length(ZMfft(1,:)));
if mod(length(ZMfft),2) == 0
    fftRand = ZMfft'.*[1 PR1 1 PR2];
else
    fftRand = ZMfft'.*[1 PR1 PR2];
end
randomBarcode = ifft(fftRand)';
end






