 
function [rights] = Mean_Variance_Matching(barcode1, barcode2, alpha)
commandwindow();
%% Parameters
 if nargin == 0 %~exists(barcode1) || ~exists(barcode2)
%     theorySequenceFilepath = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/sequences/T7.fasta';
%     theorySequenceFilepath2 = '/home/william/b16_johannes/DNABarcodeMatchmaker/ExampleFiles/R100/r100.fasta';   %/R100/r100.fasta';
    theorySequenceFilepath = '/home/william/b16_johannes/DNABarcodeMatchmaker/JI/R100 meck/Copy_of_r100_1300rows.fasta';
    theorySequenceFilepath2 = '/home/william/b16_johannes/DNABarcodeMatchmaker/JI/R100 meck/r100_ins_From910-1170(first260rowsOfT7).fasta'; %% r100_ins_From910-1170(first260rowsOfT7).fasta'

    %Get Sequences
    import NtSeq.Import.try_import_fasta_nt_seq;
    [~, theorySequence] = try_import_fasta_nt_seq(theorySequenceFilepath);
    [~, theorySequence2] = try_import_fasta_nt_seq(theorySequenceFilepath2);
    
    import CBT.get_default_barcode_gen_settings;
    defaultBarcodeGenSettings = get_default_barcode_gen_settings();
    barcodeGenSettings = defaultBarcodeGenSettings;

    import CBT.gen_zscaled_cbt_barcode;
    x = gen_zscaled_cbt_barcode(theorySequence, barcodeGenSettings);
    y = gen_zscaled_cbt_barcode(theorySequence2, barcodeGenSettings);
    alpha = 0.138;
    
else 
    x = barcode1;
    y = barcode2;
end
%% Setup
%Artivficial Insert and y-permutation
% shift = 185;
% y = cat(1, y(shift+1:end), y(1:shift));
% y = cat(1, y, y);
% shift = 50;
% y = cat(1, y(shift+1:end), y(1:shift));
shift = 185;
shift2 = 100;
y = cat(1, y(shift+1:end), y(1:shift));
y = cat(1, y(length(y)-shift2:end), y(1:length(y)-shift2-1));
y = cat(1, y, y);




if length(y)<length(x)
    [y, x] = deal(x,y);
end

%Noise
z = phaseRand(y);
for i = 1:length(y)
   y(i) = (1-alpha)*y(i)+alpha*z(i);
end
z = phaseRand(x);
for i = 1:length(x)
   x(i) = (1-alpha)*x(i)+alpha*z(i);
end


%Compute Cost (Distance) Matrix
for n=1:length(x)
    for m=1:length(y)
        costMatrix(n,m) = sqrt((x(n)-y(m))^2);
    end
end


[pathcost, pathMatrix] = mVM(x,y,costMatrix);


%Backtracks the shortest path and stores path coordinates in path. Adds NaN
%at each jump for plotting purposes
[totcost, pathEndIndex] = min(pathcost(end,:));
path = [length(x) pathEndIndex];
i = length(x);
jumps = 0;
while i > 1
    column = pathMatrix(i, path(end, 2));
    if pathMatrix(i, path(end, 2)) ~= path(end, 2)-1
        %path = cat(1, path, [NaN NaN]);
        jumps = jumps +1;
    end
    path = cat(1, path, [(i-1) column]);
    i = i-1;
end
avgcost = totcost/length(x);
%disp(avgcost);
path = flipud(path);

correctpath = zeros(length(x), 2);
for i = 1:33
    correctpath(i,:) = [i i+shift2+1];
end
%insert:
for i = 34:length(x)
    correctpath(i,:) = [i i+34+shift2+1];
end

rights = 0;
dist = 4;
for i = 1:length(x)
    left = correctpath(i,2) - dist;
    right = correctpath(i,2) + dist;
    if path(i,2) >= left && path(i,2) <= right
        rights = rights+1;
    end
end




%% PLOTS
figure
colormap('Gray');
imagesc(costMatrix);hold on;
plot(correctpath(:,2), correctpath(:,1), 'b.', 'MarkerSize', 8);hold on;
plot(path(:,2), path(:,1), 'ro');hold on;
title('Cost Matrix');
col = colorbar;
ylabel('X Index')
xlabel('Y Index')
col.Label.String = 'Cost';

figure
colormap('Pink');
imagesc(pathcost, [0 100]);
col = colorbar;
ylabel('X Index')
xlabel('Y Index')
col.Label.String = 'Cost';
title('Path Cost');

figure
plot(x);hold on;plot(y);hold off,
ylabel('Intensity')
xlabel('Index')
title('sequences');

figure
colormap('Pink');
imagesc(pathMatrix);colorbar;
title('k-values');


end

%Mean variance matching from paper by Latecki
function [pathcost, pathMatrix] = mVM(x, y, costMatrix)
m = length(x);
n = length(y);
winWidth = n/2;
elasticity = min(n-m, winWidth);

for i = 1:m
    for j = 1:n
        pathcost(i,j) = Inf;
        pathMatrix(i,j) = 0;
    end
end

for j = 1:elasticity + 1
    pathcost(1,j) = costMatrix(1,j)^2;
end

for i = 2:m
    stopk = min(i-1 + elasticity, n);
    for k = i-1:stopk
        stopj = min(k+1+elasticity, n);
        for j = (k+1):stopj
            if pathcost(i,j)>pathcost(i-1,k)+costMatrix(i,j)^2
                pathcost(i,j) = pathcost(i-1,k)+costMatrix(i,j)^2;
                pathMatrix(i,j) = k;
            end
        end
    end
end
end

