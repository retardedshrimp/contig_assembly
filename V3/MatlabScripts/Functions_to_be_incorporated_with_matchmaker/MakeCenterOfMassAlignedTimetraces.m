%=========================================================================%
function [ ] = MakeCenterOfMassAlignedTimetraces(rawKymos)			     
    % MAKECENTEROFMASSALIGNEDTIMETRACES - Takes raw kymographs and 
    %   aligns each time frame to the center-of-mass (actually foreground
    %  intensity) and then makes  1D intensity profiles from them.
    %
    % Inputs:
    %   rawKymos
    %     cell array of raw kymographs
    %
    % Authors:
    %   Saair Quaderi (refactored)
    %   Tobias Ambjörnsson

    numKymos = length(rawKymos);
    if numKymos == 0
        fprintf('No kymographs!\n');
    end
    hFig = figure('Name', 'Center of Mass Aligned Time Traces');
    hPanel = uipanel('Parent', hFig);
    import FancyGUI.FancyTabs.TabbedScreen;
    ts = TabbedScreen(hPanel);
    for kymoNum = 1:numKymos
        rawKymo = rawKymos{kymoNum};
        
        % Remove background
        rawKymoFgMask = imquantize(rawKymo, multithresh(rawKymo,1)) > 1;
        meanBgLevel = mean(rawKymo(~rawKymoFgMask));
        rawKymoBgZeroed = rawKymo - meanBgLevel;
        rawKymoBgZeroed(~rawKymoFgMask) = 0;

        % "Subtract" center of mass for each time frame
        [numRows, numCols] = size(rawKymo);
        centerOfMass = zeros(numRows,1);
        pixelColIdxs = 1:numCols;
        for rowNum = 1:numRows
            singleKymoFrame = rawKymoBgZeroed(rowNum, :);
            centerOfMass(rowNum)= sum(pixelColIdxs.*singleKymoFrame)/sum(singleKymoFrame);
        end
        meanCenterOfMass = mean(centerOfMass);
        kymoCenterOfMassAligned = zeros(numRows, numCols);
        for rowNum = 1:numRows
            singleKymoFrame = rawKymo(rowNum,:);
            singleFrameKymoNew = circshift(singleKymoFrame', -round(centerOfMass(rowNum) - meanCenterOfMass));
            kymoCenterOfMassAligned(rowNum, :) = singleFrameKymoNew;
        end

        rowNumSTT = 3;
        rawKymoBgZeroedSST = rawKymoBgZeroed(rowNumSTT, :);
        rawKymoSST = rawKymo(rowNumSTT, :);
        rowNums = 1:numRows;
        roundedCenterOfMass = round(centerOfMass(rowNums));
            
        hTab = ts.create_tab(sprintf('Kymo %d', kymoNum));
        hTabPanel = uipanel('Parent', hTab);
        tsKymo = TabbedScreen(hTabPanel);
        
        
        
        % Show single time trace
        hTabSTT = tsKymo.create_tab('Single Time Trace');
        hTabPanelSTT = uipanel('Parent', hTabSTT);
        hAxisSTT = gca('Parent', hTabPanelSTT);
        
        plot(hAxisSTT, rawKymoBgZeroedSST, 'b--');
        hold(hAxisSTT, 'on');
        plot(hAxisSTT, rawKymoSST, 'r-');

        % Show the kymograph with background removed
        hTabBgRmvd = tsKymo.create_tab('Background Removed');
        hTabPanelBgRmvd = uipanel('Parent', hTabBgRmvd);
        hAxisBgRmvd = gca('Parent', hTabPanelBgRmvd);
        
        axes(hAxisBgRmvd); %#ok<LAXES>
        imagesc(rawKymoBgZeroed);
        colormap(hAxisBgRmvd, gray());

        % Show raw kymograph with center of mass on top
        hTabCOM = tsKymo.create_tab('Center of Mass');
        hTabPanelCOM = uipanel('Parent', hTabCOM);
        hAxisBgCOM = gca('Parent', hTabPanelCOM);
        
        axes(hAxisBgCOM); %#ok<LAXES>
        imagesc(rawKymo);
        colormap(hAxisBgCOM, gray());
        hold(hAxisBgCOM, 'on');
        plot(roundedCenterOfMass, rowNums,'m*','Linewidth',3);

        % Show the center of mass aligned kymograph
        hTabCOMAK = tsKymo.create_tab('Center of Mass ALigned Kymo');
        hTabPanelCOMAK = uipanel('Parent', hTabCOMAK);
        hAxisBgCOMAK = gca('Parent', hTabPanelCOMAK);
        
        axes(hAxisBgCOMAK); %#ok<LAXES>
        imagesc(kymoCenterOfMassAligned),
        colormap(hAxisBgCOMAK, gray());
        box(hAxisBgCOMAK, 'on')
    end
end
 
  

