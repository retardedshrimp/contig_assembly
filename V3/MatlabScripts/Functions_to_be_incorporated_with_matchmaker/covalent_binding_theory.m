function probBound = covalent_binding_theory(ntSeq, ligandBindingNtSeq)
    % COVALENT_BINDING_THEORY = Calculate theory barcode for covalently binding ligands
    %
    % Inputs:
    %   ntSeq
    %     DNA nucleotide sequence (e.g. 'ATGCAAATGCTTTT')
    %   ligandBindingNtSeq
    %     DNA nucleotide sequence on which the ligand binds (e.g. 'ATGC')
    %
    % Outputs:
    %   probBinding
    %      probability for there being a covalently bound ligand on the
    %       basepair at the same index for the input sequence
    %
    % Warning from Saair:
    %  In some cases binding possibilities can be mutually exclusive (if the
    %  ligand binding sequence contains repeating nucleotides) so giving
    %  probabilities of 100% to every bp in every possible match, as this
    %  code currently does, definitely isn't going to give robust accurate
    %  predictions for all potential inputs even if the binding efficiency
    %  is extremely high (as assumed) since mutual exclusivity isn't taken
    %  into account
    %
    % Authors:
    %   Saair Quaderi (simplified code through refactoring)
    %   Tobias Ambjörnsson (converted Java version to Matlab)
    %   Adam Nilsson (Java Luna verion)
    %
    
    potentialBindingStarts = strfind(ntSeq, ligandBindingNtSeq);
    potentialBindingCounts = histc(potentialBindingStarts + (1:ligandSeqLen) - 1, 1:length(ntSeq));
    probBound = 1.0 * (potentialBindingCounts > 0);
end
