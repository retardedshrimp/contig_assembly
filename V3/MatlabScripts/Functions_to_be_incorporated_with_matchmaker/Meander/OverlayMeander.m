function [x, y] = OverlayMeander( averagedImgInput )
%OVERLAYMEANDER locates and overlays meander on input video.
%   Returns the x and y coordinate arrays of the optimal parametrization.

global D N L B M averagedImg orientation

if nargin == 0
    videoName = 'testvideo.avi';     
    % Rotate the video appropriately.
    [~, averagedImg, ~] = RotateVideo( videoName );
elseif nargin == 1
    averagedImg = averagedImgInput;
end


% Make the image binary using two thresholding methods: adaptive and
% global. Thus we can better deal with uneven illumination in the channel.
seg_I_1 = adaptivethreshold(averagedImg,20,0,0);
thresh = multithresh(averagedImg,1);
seg_I_2 = imquantize(averagedImg,thresh) == 2;
seg_I = seg_I_1 .* seg_I_2;

% Save only the largest connected components.
CC = bwconncomp(seg_I);
numPixels = cellfun(@numel, CC.PixelIdxList);
idxs = find(numPixels < 25);
for i = 1:length(idxs)
    seg_I(CC.PixelIdxList{idxs(i)}) = 0;
end



% Estimate D by finding intensity peaks in the averaged image.
h = sum(seg_I);
[~, locs] = findpeaks(h,'MINPEAKHEIGHT',size(averagedImg,1)*.15);


% From the peak locations, find the best "distance between vertical
% portions" value that explains them.


stdVals = std(seg_I(:,locs(1):locs(end)),0,2);

B = 5;
D = fminsearch(@(x) CalcChannelWidthEnergy(x, locs), mode(diff(locs)));

f = @(coeffs, x) coeffs(1) + coeffs(6) * ...
        (tanh((x - coeffs(2))*coeffs(4)) - tanh((x - coeffs(3))*coeffs(5)));
coeffs0 = [0, find(stdVals > mean(stdVals), 1, 'first'), find(stdVals > mean(stdVals), 1, 'last'), 1, 1, 0.5];
coeffs = nlinfit((1:length(stdVals))', stdVals, f, coeffs0);

% stdFitCurve = f(coeffs, 1:length(stdVals));
% yBeginGuess = find(stdFitCurve > 0.2 * max(stdFitCurve), 1, 'first') + B;
% yEndGuess = find(stdFitCurve > 0.5 * max(stdFitCurve), 1, 'last') + B;

yBeginGuess = coeffs(2)+B;
yEndGuess = coeffs(3)-B;
% 
% yBeginGuess = find(stdVals > .5*mean(stdVals), 1, 'first');
% yEndGuess = find(stdVals > .5*mean(stdVals), 1, 'last');


% The main parameters for the meander.
N = ceil((locs(end) - locs(1)) / D) + 1;
L = yEndGuess - yBeginGuess - (2 * B);
M = 2;

    
% Begin by guessing the meander's location.
xBeginGuess = locs(1);
alphaWideGuess = 1;
alphaTallGuess = 1;
x0 = [xBeginGuess, yBeginGuess + B, alphaWideGuess, alphaTallGuess];


% Find the optimal values for the parameters for the meander via global
% maximization of the overlap between meander parametrization and
% intensities. However, we're not sure whether the meander is oriented with
% a bend in the top left corner, or the lower left corner (corresponding to
% a 180 degree rotation). So, we run two separate optimizations and see
% which orientation has more overlap.

origAveragedImg = averagedImg;
averagedImg(~seg_I) = 0;

% Bend in the top left corner.
orientation = 1; %#ok<NASGU>
[optPars1, fminval1] = fminsearch(@CalcOverlapScore,x0);


% Bend in the top right corner.
orientation = 2; %#ok<NASGU>
[optPars2, fminval2] = fminsearch(@CalcOverlapScore,x0);


if fminval1 > fminval2
    orientation = 2;
    optPars = optPars2;
else
    orientation = 1;
    optPars = optPars1;
end


% Use the values found above to parameterize the meander and overlay it.
[x, y] = ParametrizeMeander(round(optPars(1)), round(optPars(2)), optPars(3), optPars(4));


% Eliminate duplicate points.
for i = length(x):-1:2
    if x(i) == x(i-1) && y(i) == y(i-1)
        x(i) = [];
        y(i) = [];
    end
end

imshow(origAveragedImg);
hold on;
plot(smooth(x), smooth(y),'b','LineWidth',4);

end



%%%%%%%%%%%%%%%%%%%%
%%% Subfunctions %%%
%%%%%%%%%%%%%%%%%%%%

function [score] = CalcOverlapScore(inputArg)
% Scores how "well" the parametrized meander overalps with the image.
%   For use with fminsearch, lower score indicates better overlap.

global averagedImg

% Pass these in as one variable for comply with fminsearch.
xBegin = round(inputArg(1));
yBegin = round(inputArg(2));
alphaWide = inputArg(3);
alphaTall = inputArg(4);

% Parametrize the meander.
[x, y] = ParametrizeMeander(xBegin, yBegin, alphaWide, alphaTall);

% close all
% imagesc(averagedImg), colormap(gray);
% hold on, plot(x,y,'r');

% % Sum the intensities over a circle tracing along the path
intensityArr = zeros(size(x));

radH = 3;
radV = 1;

for i = 1:2*radV:length(x)
    
    xcoord = x(i);
    ycoord = y(i);
    
    xVals = repmat(xcoord-radH:xcoord+radH,2*radV+1,1);
    yVals = repmat((ycoord-radV:ycoord+radV)',1,2*radH+1);
    
    intensityVals = averagedImg(sub2ind(size(averagedImg), yVals, xVals));
    intensityArr(i) = sum(intensityVals(:));
    
end

% score = 1/sum(intensityArr);

score = (1/sum(intensityArr)) * (1 + 0.5*abs(alphaWide - alphaTall));

end



function [x, y] = ParametrizeMeander(xBegin, yBegin, alphaWide, alphaTall)
% Parametrize the meander.

% D:            distance between vertical pieces.
% N:            the number of vertical pieces.
% L:            the height of the vertical pieces.
% B:            height of the curved regions.
% orientation:  1 for "first curve in top left corner"
%               2 for "first curve in bottom left corner"

global D N L B M orientation

LSt = L * alphaTall;
DSt = D * alphaWide;
BSt = B;

dt = 1 / LSt;
cArr=-(DSt/LSt):dt:(DSt/LSt);
x = [];
y = [];

for n = 0:N-1

    if mod(n,2) == 0
        if orientation == 1
            tArr = 0:dt:1;
        else
            tArr = 1:-dt:0;
        end            
    else
        if orientation == 1
            tArr = 1:-dt:0;
        else
            tArr = 0:dt:1;
        end            
    end

    % The vertical bar first
    newXArr = round(n*DSt*ones(1,length(tArr)));
    x(x > max(newXArr)) = max(newXArr);
    x = cat(2, x, newXArr);
    y = cat(2, y, round(tArr*LSt));

    if n < N-1
        
        % Add the next curved segment. 
        lastXVal = x(end);
        newXArr = (n+0.5)*DSt + 0.5*DSt*cArr*L/DSt;
        newXArr(newXArr < lastXVal) = lastXVal;
        x = cat(2, x, newXArr);
        if mod(n,2) == 0            
            if orientation == 1                
                % Top curved segment.
                y = cat(2, y, (1-(LSt*cArr/DSt).^(2*M))*BSt+LSt);
            else
                % Bottom curved segment.
                y = cat(2, y, (LSt*cArr/DSt).^(2*M)*BSt-BSt);
            end            
        else            
            if orientation == 1                
                % Bottom curved segment.
                y = cat(2, y, (LSt*cArr/DSt).^(2*M)*BSt-BSt);
            else                
                % Top curved segment.
                y = cat(2, y, (1-(LSt*cArr/DSt).^(2*M))*BSt+LSt);
            end         
        end
    end      
end

x = round(x + xBegin);
y = round(y + yBegin);   

end




%=========================================================================%
function [ trace ] = CalcIntensityTrace(img, x, y, rad)


% This will store the intensity curve for this time frame.
trace = zeros(1,length(x));
len = 2*rad + 1;


% Calculate the intensity for the first point.
xVals = x(1)-rad:x(1)+rad;
yVals = y(1) * ones(size(xVals));
intensityVals = img(sub2ind(size(img),yVals,xVals));
trace(1) = sum(intensityVals) / len;


% Now calculate the intensity for the remaining points (except the last).
for i = 2:length(x)-1
    
    % We'll average over "rad" pixels on either side of each meander point
    % in the direction of the line perpendicular to the meander.
    p1 = [x(i-1), y(i-1)];
    p2 = [x(i+1), y(i+1)];
    currPoint = [x(i), y(i)];
    
    
    % Calculate the slope at currPoint using the points in front of, and
    % behind, it.
    slope = CalcPerpSlope(p1, p2);
    
    if isinf(slope)
        
        xVals = currPoint(1)-rad:currPoint(1)+rad;
        yVals = currPoint(2) * ones(size(xVals));        
        
    elseif slope == 0
        
        yVals = currPoint(2)-rad:currPoint(2)+rad;
        xVals = currPoint(1) * ones(size(yVals));
        
    else
        
        xVals = currPoint(1)-rad:currPoint(1)+rad;
        yVals = round(currPoint(2) + slope * (xVals-currPoint(1)));
        
    end
    
    intensityVals = img(sub2ind(size(img),yVals,xVals));
    trace(i) = mean(intensityVals);
    
end


% Calculate the intensity for the last point on the meander.
xVals = x(end)-rad:x(end)+rad;
yVals = y(end) * ones(size(xVals));
intensityVals = img(sub2ind(size(img),yVals,xVals));
trace(end) = sum(intensityVals) / len;

end


%=========================================================================%
function [ pSlope ] = CalcPerpSlope(p1, p2)

if p2(2) - p1(2) == 0
    pSlope = 0;
else
    pSlope = -1 / (p2(2) - p1(2)) / (p2(1) - p1(1));
end

end


%=========================================================================%
function [ energy ] = CalcChannelWidthEnergy(widthGuess, locs)

diffs = diff(locs);

energy = 0;
for i = 1:length(diffs)
    distArr = (1:100) * widthGuess;
    enrgArr = (distArr - diffs(i)).^2;
    energy = energy + min(enrgArr);
end

end