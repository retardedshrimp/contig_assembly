classdef AppDataPoolViewScreen < handle
    % APPDATAPOOLVIEWSCREEN 
    
    properties (Constant)
        Version = [0, 0, 1];
        
        HideDataPoolPrefix = 'zzz_';
    end
    properties (Access = private)
        TabbedScreen
        ViewerTabHandle
        ViewerPanelHandle
        
        TabbedScreenNibling % nibling = child of sibling
        
        VerifyBeforeRemovals = true;
        Verbose = false;
        
        DataPoolID = NaN;
    end
    properties (SetAccess = private)
        DataPoolListManager
    end
    properties
        ShowHidden = true;
    end
    
    
    methods
        function [adpvs] = AppDataPoolViewScreen(ts, dataPoolID)
            if nargin < 1
                figTitle = 'App Data Pools Viewer';
                import FancyGUI.FancyTabs.TabbedScreen;
                ts = TabbedScreen.make_tabbed_screen_in_new_fig(figTitle);
            end
            if nargin < 2
                dataPoolID = NaN;
            end

            if isnan(dataPoolID)
                tabTitle = 'App Data Pools Viewer';
            else
                tabTitle = sprintf('Data Pool List: %s', dataPoolID);
            end
            
            import AppMgr.AppDataPoolViewScreen;
            import AppMgr.AppDataPoolMgr;
            
            adpvs.DataPoolID = dataPoolID;
            
            deleteFcn = @(varargin) adpvs.delete();
            [hTabViewer, hPanelViewer] = AppDataPoolViewScreen.make_list_viewer_panel_in_new_tab(ts, tabTitle, deleteFcn);
            adpvs.TabbedScreen = ts;
            adpvs.ViewerTabHandle = hPanelViewer;
            adpvs.ViewerPanelHandle = hTabViewer;
            
            import FancyGUI.FancyList.FancyListMgr;
            dplm = FancyListMgr();
            adpvs.DataPoolListManager = dplm;
            dplm.set_ui_parent(adpvs.ViewerPanelHandle);
            dplm.make_ui_items_listbox();
            
            adpvs.sync_ui_list_data();
            
            import FancyGUI.FancyList.FancyListMgr;
            import FancyGUI.FancyList.FancyListMgrBtnSet;
            
            flmbs1 = FancyListMgrBtnSet();
            flmbs1.NUM_BUTTON_COLS = 2;
            flmbs1.add_button(FancyListMgr.make_select_all_button_template());
            flmbs1.add_button(FancyListMgr.make_deselect_all_button_template());
            
            flmbs2 = FancyListMgrBtnSet();
            flmbs2.NUM_BUTTON_COLS = 1;
            flmbs2.add_button(AppDataPoolViewScreen.make_resync_button_template(adpvs));
            
            if isnan(adpvs.DataPoolID)
                flmbs2.add_button(AppDataPoolViewScreen.make_view_pool_entries_button_template(adpvs));
            end
            dplm.add_button_sets(flmbs1, flmbs2);
        end
        
        function delete(adpvs) % Clean up data super explicitly to reduce any memory leak risk
            if isvalid(adpvs.DataPoolListManager)
                delete(adpvs.DataPoolListManager);
            end
        end
        function [hasDataPoolsMask] = has_data_pool_ids(~, dataPoolIDQueries)
            import AppMgr.AppDataPoolMgr;
            [dataPoolIDs] = AppDataPoolMgr.get_pool_ids();
            n = length(dataPoolIDQueries);
            hasDataPoolsMask = false(n, 1);
            [~, idxQueries, ~] = intersect(dataPoolIDQueries, dataPoolIDs);
            hasDataPoolsMask(idxQueries) = true;
        end
        function [] = sync_ui_list_data(adpvs)
            import AppMgr.AppDataPoolViewScreen;
            dplm = adpvs.DataPoolListManager;
            [itemDisplayNames, dataPoolIDs] = adpvs.get_pool_data(adpvs.DataPoolID);

            numItems = length(dataPoolIDs);
            nonhiddenDataPoolIDIdxs = (1:numItems)';
            if not(adpvs.ShowHidden)
                hideDataPoolPrefix = AppDataPoolViewScreen.HideDataPoolPrefix;
                hideDataMask = strncmp(hideDataPoolPrefix, dataPoolIDs, length(hideDataPoolPrefix));
                nonhiddenDataPoolIDIdxs = nonhiddenDataPoolIDIdxs(not(hideDataMask));
            end
                
            dplm.set_list_items(itemDisplayNames(nonhiddenDataPoolIDIdxs), dataPoolIDs(nonhiddenDataPoolIDIdxs)); 
        end
    end
    
    methods(Static, Access = private)
        function [itemDisplayNamesForListMgr, itemValuesForListMgr] = get_pool_data(dataPoolID)
            import AppMgr.AppDataPoolMgr;
            if (nargin < 1) 
                dataPoolID = NaN;
            end 
            if isnan(dataPoolID)
                [dataPoolIDs, dataPoolsItemCounts] = AppDataPoolMgr.get_pool_ids();
                numItems = length(dataPoolIDs);
                
                itemValuesForListMgr = dataPoolIDs;
                itemDisplayNamesForListMgr = arrayfun(...
                    @(dataPoolNum) ...
                        sprintf('%s [%d Entries]', ...
                            dataPoolIDs{dataPoolNum}, ...
                            dataPoolsItemCounts(dataPoolNum)), ...
                    (1:numItems)', ...
                    'UniformOutput', false);
                return;
            end
            [dataItems, ~, dataItemIDs] = AppDataPoolMgr.get_data_items(dataPoolID);
            if not(isempty(dataItemIDs)) && not(ischar(dataItemIDs{1}))
                dataItemIDs = cellfun(...
                    @num2str, ...
                    dataItemIDs, ...
                    'UniformOutput', false);
            end
            itemDisplayNamesForListMgr = dataItemIDs;
            itemValuesForListMgr = dataItems;
            
        end
        function [btnLoadMovies] = make_resync_button_template(adpvs)
            import FancyGUI.FancyList.FancyListMgrBtn;
            btnLoadMovies = FancyListMgrBtn('Refresh  List', @(~, ~, flm) adpvs.sync_ui_list_data());
        end
        function [btnLoadMovies] = make_view_pool_entries_button_template(adpvs)
            import FancyGUI.FancyList.FancyListMgrBtn;
            function [] = try_view_pool_entries_lists_in_tabs()
                import AppMgr.AppDataPoolViewScreen;
                flm = adpvs.DataPoolListManager;
                selectedItems = flm.get_selected_list_items();
                selectedDataPoolIDs = selectedItems(:, 2);
                numDataPools = size(selectedDataPoolIDs);
                
                if numDataPools == 0
                    return;
                end
                ts2 = adpvs.TabbedScreenNibling;
                if isempty(ts2)
                    ts = adpvs.TabbedScreen;
                    [hTabViewer2, ~] = ts.create_tab('Pool Entry Lists');
                    hPanelViewer2 = uipanel('Parent', hTabViewer2);
                    import FancyGUI.FancyTabs.TabbedScreen;
                    ts2 = TabbedScreen(hPanelViewer2);
                    adpvs.TabbedScreenNibling = ts2;
                end
                for dataPoolNum = 1:numDataPools
                     dataPoolID = selectedDataPoolIDs{dataPoolNum};
                     AppDataPoolViewScreen(ts2, dataPoolID)
                end
                
            end
            btnLoadMovies = FancyListMgrBtn('View Pool Entry Lists', @(~, ~, flm) try_view_pool_entries_lists_in_tabs());
        end
        
        function [hViewerTabHandle, hViewerPanelHandle] = make_list_viewer_panel_in_new_tab(ts, tabTitle, deleteFcn)
            [hViewerPanelHandle, ~] = ts.create_tab(tabTitle, deleteFcn);
            hViewerTabHandle = uipanel('Parent', hViewerPanelHandle);
        end
    end
end 