classdef ImportItemContext < handle
    properties (Constant)
        Version = [0, 0, 1];
    end
    properties
        AppSession

        ImportSrcPath = '';
        SrcSelectionTime

        ListableDataItems
        DataItemUpdateTime
    end
    methods
        function [iic] = ImportItemContext(srcPath, srcSelectionTime, listableDataItems, dataItemUpdateTime)
            if nargin < 2
                srcSelectionTime = [];
            end
            if isempty(srcSelectionTime)
                srcSelectionTime = clock();
            end
            if nargin < 3
                listableDataItems = [];
            end
            if isempty(listableDataItems)
                listableDataItems = cell(0, 1);
            end
            if nargin < 4
                dataItemUpdateTime = [];
            end
            import AppMgr.AppSession;
            appSession = AppSession.get_instance();
            
            iic.AppSession = appSession;
            
            iic.ImportSrcPath = srcPath;
            iic.SrcSelectionTime = srcSelectionTime;
            
            iic.ListableDataItems = listableDataItems;
            iic.DataItemUpdateTime = dataItemUpdateTime;
        end
    
        function [] = update_item(iic, listableDataItems, dataItemUpdateTime)
            if nargin < 2
                dataItemUpdateTime = clock();
            end
            iic.ListableDataItems = listableDataItems;
            iic.DataItemUpdateTime = dataItemUpdateTime;
        end
    end
end