classdef (Sealed) AppDataPoolMgr < handle
    % APPDATAPOOLMGR - Application data pool manager helps manage data
    %   pools shared by the various components of a running app
    %
    % Note: An instance of APPDATAPOOLMGR should be retrieved
    %   using get_instance since it will be defined as a persistent
    %   variable using the singleton design pattern.
    %
    % http://mathworks.com/help/matlab/ref/persistent.html
    % http://mathworks.com/help/matlab/matlab_oop/controlling-the-number-of-instances.html
    %
    % Authors:
    %   Saair Quaderi
    properties (Constant)
        Version = [0, 0, 1];
    end
    properties (Constant, Access = private)
        DefaultVerbose = false;
    end
    properties (SetAccess = private)
        DataMap %containers.Map mapping strings (data pool ids) to containers.Map which maps data item ids to data item values
        UUID % unique ID for debugging purposes
        Verbose = AppMgr.AppDataPoolMgr.DefaultVerbose
    end
    methods
        function [appDataPoolMgr] = AppDataPoolMgr(uuid, verboseTF)
            if (nargin < 1)
                uuid = [];
            end
            if (nargin < 2)
                verboseTF = [];
            end
            if isempty(uuid)
                uuid = char(java.util.UUID.randomUUID);
            end
            if isempty(verboseTF)
                verboseTF = AppMgr.AppDataPoolMgr.DefaultVerbose;
            end
            appDataPoolMgr.DataMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
            appDataPoolMgr.UUID =  uuid;
            appDataPoolMgr.Verbose = verboseTF;

            if appDataPoolMgr.Verbose
                fprintf('Generated new app data pool manager: %s\n', uuid);
            end
        end
    end
    methods (Static)
        function [dataPoolIDs, dataPoolsItemCounts] = get_pool_ids()
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            dataPoolIDs = keys(appDataMap);
            dataPoolIDs = dataPoolIDs(:);
            if nargout > 1
                dataPoolsItemCounts = cellfun(@(dataPoolID) size(appDataMap(dataPoolID), 1), dataPoolIDs);
            end
        end
        function [dataItemIDs] = get_pool_data_item_ids(dataPoolID)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            if appDataMap.isKey(dataPoolID)
                datapoolMap = appDataMap(dataPoolID);
                dataItemIDs = keys(datapoolMap);
                dataItemIDs = dataItemIDs(:);
            else
                dataItemIDs = cell(0,1);
            end
        end
        
        function [dataItems, dataItemsFoundMask, dataItemIDs] = get_data_items(dataPoolID, dataItemIDs)
            if nargin < 2
                dataItemIDs = AppDataPoolMgr.get_pool_data_item_ids(dataPoolID);
            end
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            numItems = length(dataItemIDs);
            dataItemsFoundMask = false(numItems, 1);
            dataItems = cell(numItems, 1);
            dataPoolFoundTF = appDataMap.isKey(dataPoolID);
            if not(dataPoolFoundTF)
                return;
            end
            if dataPoolFoundTF
                datapoolMap = appDataMap(dataPoolID);
                for itemNum = 1:numItems
                    dataItemID = dataItemIDs{itemNum};
                    dataItemsFoundMask(itemNum) = datapoolMap.isKey(dataItemID);
                    if dataItemsFoundMask(itemNum)
                        dataItems{itemNum} = datapoolMap(dataItemID);
                    end
                end
            end
        end
        function [dataItem, dataItemsFoundTF, dataItemID] = get_data_item(dataPoolID, dataItemID)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            [dataItems, dataItemsFound] = appDataPoolMgr.get_data_items(dataPoolID, {dataItemID});
            dataItem = dataItems{1};
            dataItemsFoundTF = dataItemsFound(1);
        end
        function [dataItemsRemoved, dataItemsRemovedMask] = remove_data_items(dataPoolID, dataItemIDsToRemove)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            dataTypeFound = appDataMap.isKey(dataPoolID);
            if not(dataTypeFound)
                error('Unknown data pool');
            end
            
            [dataItemsRemoved, dataItemsRemovedMask] = AppDataPoolMgr.get_data_items(dataPoolID, dataItemIDsToRemove);
            
            datapoolMap = appDataMap(dataPoolID);
            datapoolMap.remove(dataItemIDsToRemove(dataItemsRemovedMask));
        end
        
        function [dataItem, dataItemsRemovedTF] = remove_data_item(dataPoolID, dataItemID)
            import AppMgr.AppDataPoolMgr;
            [dataItems, dataItemsRemovedTF] = AppDataPoolMgr.remove_data_items(dataPoolID, {dataItemID});
            dataItem = dataItems{1};
        end
        
        function [] = update_data_items(dataPoolID, dataItemIDs, dataItems)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            numItems = numel(dataItemIDs);
            if length(dataItems) < numItems
                error('Mismatch is ID and item count');
            end
            appDataMap = appDataPoolMgr.DataMap;
            dataPoolFound = appDataMap.isKey(dataPoolID);
            if not(dataPoolFound)
                error('Unknown data pool');
            end
            datapoolMap = appDataMap(dataPoolID);
            for itemNum = 1:numItems
                datapoolMap(dataItemIDs{itemNum}) = dataItems{itemNum};
            end
            appDataMap(dataPoolID) = datapoolMap;
            appDataPoolMgr.DataMap = appDataMap;
        end
        function [] = update_data_item(dataPoolID, dataItemID, dataItem)
            import AppMgr.AppDataPoolMgr;
            AppDataPoolMgr.update_data_items(dataPoolID, {dataItemID}, {dataItem})
        end
        function [hasDataPool] = has_data_pool(dataPoolID)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            hasDataPool = appDataMap.isKey(dataPoolID);
        end
        
        function [] = create_new_data_pool(dataPoolID, keyType, valueType)
            if nargin < 2
                keyType = 'char';
            end
            if nargin < 3
                valueType = 'any';
            end
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            if appDataMap.isKey(dataPoolID)
                error('Data pool already exists');
            end
            appDataMap(dataPoolID) = containers.Map('KeyType', keyType, 'ValueType', valueType);
            appDataPoolMgr.DataMap = appDataMap;
        end
        function [datapoolMap] = remove_data_pool(dataPoolID)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            if appDataMap.isKey(dataPoolID)
                datapoolMap = appDataMap(dataPoolID);
            end
            appDataMap.remove(dataPoolID);
        end
        function [] = clear_data_pool(dataPoolID)
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            appDataMap = appDataPoolMgr.DataMap;
            if appDataMap.isKey(dataPoolID)
                datapoolMap = appDataMap(dataPoolID);
                dataItemIDs = keys(datapoolMap);
                dataItemIDs = dataItemIDs(:);
                datapoolMap.remove(dataItemIDs);
            end
        end
        function [] = total_reset()
            import AppMgr.AppDataPoolMgr;
            appDataPoolMgr = AppDataPoolMgr.get_instance();
            delete(appDataPoolMgr);
            AppDataPoolMgr.get_instance();
        end
        function [appDataPoolMgr] = get_instance(varargin)
            persistent persistentAppDataPoolMgr
            import AppMgr.AppDataPoolMgr;
            if isempty(persistentAppDataPoolMgr) || not(isvalid(persistentAppDataPoolMgr))
                persistentAppDataPoolMgr = AppDataPoolMgr();
                appDataPoolMgr = persistentAppDataPoolMgr;
            else
                appDataPoolMgr = persistentAppDataPoolMgr;
            end
        end
    end
end