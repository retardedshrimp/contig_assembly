classdef (Sealed) AppResourceMgr < handle
    % APPRESOURCEMGR - Application resource management class helps manage
    %   functionality related to resources available to the app
    %
    % Note: An instance of APPRESOURCEMGR should be retrieved
    %   using get_instance since it will be defined as a persistent
    %   variable using the singleton design pattern.
    %
    % http://mathworks.com/help/matlab/ref/persistent.html
    % http://mathworks.com/help/matlab/matlab_oop/controlling-the-number-of-instances.html
    %
    % Authors:
    %   Saair Quaderi
    properties (Constant)
        Version = [0, 0, 1];
        
        AppResourceMgrsDataPoolID = sprintf('%sAppResourceMgrs', AppMgr.AppDataPoolViewScreen.HideDataPoolPrefix);
    end
    properties (Constant, Access = private)
        RequiredRootPkgParentDirname = 'Packages'
    end
    properties
        AppSession
        Dirpaths = containers.Map('KeyType', 'char', 'ValueType', 'char');
    end
    
    methods (Access = private)
        function [appRsrcMgr] = AppResourceMgr(appSess)
            if nargin < 1
                appSess = [];
            end
            
            if isempty(appSess)
                import AppMgr.AppSession;
                appSess = AppSession.get_instance();
            end
            appRsrcMgr.AppSession = appSess;
            
            
            import AppMgr.AppResourceMgr;
            
            packagesDirpath = AppResourceMgr.find_root_package_parent_dirpath();
            [appDirpath, packagesDirname] = fileparts(packagesDirpath);
            if not(strcmp(packagesDirname,  AppResourceMgr.RequiredRootPkgParentDirname))
                error('Expected matlab package directory roots to have a parent directory named ''%s''', ...
                    AppResourceMgr.RequiredRootPkgParentDirname);
            end
            
            % Have to call the non-static private p_* versions because
            % calling the static versions could retrigger AppResourceMgr
            % constructor through get_app_instance which might result in an
            % infinite loop
            appRsrcMgr.p_init_dirpath('App', appDirpath);
            appRsrcMgr.p_init_dirpath('Packages', packagesDirpath);
            appRsrcMgr.p_init_dirpath('Assets', fullfile(appDirpath, 'Assets'));
            appRsrcMgr.p_init_dirpath('DataCache', fullfile(appDirpath, 'DataCache'));
            appRsrcMgr.p_init_dirpath('SettingFiles', fullfile(appDirpath, 'SettingFiles'));
            
            appRsrcMgr.p_add_all_packages();
        end
        function [] = p_add_all_packages(appRsrcMgr)
            addpath(genpath(appRsrcMgr.p_get_packages_dirpath()));
        end
        
        function [] = p_init_dirpath(appRsrcMgr, dirpathKey, dirpath)
            import FancyIO.mkdirp;
            mkdirp(dirpath);
            if not(isKey(appRsrcMgr.Dirpaths, dirpathKey))
                appRsrcMgr.p_set_dirpath(dirpathKey, dirpath);
            else
                error('Path already exists');
            end
        end
        
        function [] = p_set_dirpath(appRsrcMgr, dirpathKey, dirpath)
            appRsrcMgr.Dirpaths(dirpathKey) = dirpath;
        end
        
        function [appDirpath] = p_get_app_dirpath(appRsrcMgr)
            appDirpath = appRsrcMgr.Dirpaths('App');
        end
        
        function [packagesDirpath] = p_get_packages_dirpath(appRsrcMgr)
            packagesDirpath = appRsrcMgr.Dirpaths('Packages');
        end
        
        function [dirpath] = p_get_dirpath(appRsrcMgr, dirpathKey)
            appDirpath = appRsrcMgr.p_get_app_dirpath();
            if appRsrcMgr.Dirpaths.isKey(dirpathKey)
                dirpath = appRsrcMgr.Dirpaths(dirpathKey);
            else
                defaultDirpath = fullfile(appDirpath, dirpathKey);
                if not(exist(defaultDirpath, 'dir'))
                    defaultDirpath = appDirpath;
                end
                dirpath = uigetdir(defaultDirpath, sprintf('Select ''%s'' directory', dirpathKey));
                if not(isequal(dirpath, 0))    
                    appRsrcMgr.Dirpaths(dirpathKey) = dirpath;
                end
            end
        end
    end
    methods (Static)
        
        function [] = init_dirpath(dirpathKey, dirpath)
            import AppMgr.AppResourceMgr;
            appRsrcMgr = AppResourceMgr.get_instance();
            appRsrcMgr.p_init_dirpath(dirpathKey, dirpath);
        end
        
        function [] = set_dirpath(dirpathKey, dirpath)
            import AppMgr.AppResourceMgr;
            appRsrcMgr = AppResourceMgr.get_instance();
            appRsrcMgr.p_set_dirpath(dirpathKey, dirpath);
        end
        
        function [dirpath] = get_dirpath(dirpathKey)
            import AppMgr.AppResourceMgr;
            appRsrcMgr = AppResourceMgr.get_instance();
            dirpath = appRsrcMgr.p_get_dirpath(dirpathKey);
        end

        function [packagesDirpath] = get_packages_dirpath()
            import AppMgr.AppResourceMgr;
            appRsrcMgr = AppResourceMgr.get_instance();
            packagesDirpath = appRsrcMgr.p_get_packages_dirpath();
        end
        
        function [appDirpath] = get_app_dirpath()
            import AppMgr.AppResourceMgr;
            appRsrcMgr = AppResourceMgr.get_instance();
            appDirpath = appRsrcMgr.p_get_app_dirpath();
        end
        
        function [] = add_all_packages()
            import AppMgr.AppResourceMgr;
            appRsrcMgr = AppResourceMgr.get_instance();
            appRsrcMgr.p_add_all_packages();
        end
        
        function appRsrcMgr = get_instance(varargin)
            import AppMgr.AppSession;
            appSess = AppSession.get_instance();
            
            
            strArgMask = cellfun(@(arg) (ischar(arg) && isrow(arg)), varargin);
            strArgs = cellfun(@lower, varargin(strArgMask), 'UniformOutput', false);
            verbose = not(isempty(intersect(strArgs, {'verbose'})));
            persistent persistentLocalAppRsrcMgr
            import AppMgr.AppResourceMgr;
            if isempty(persistentLocalAppRsrcMgr) || not(isvalid(persistentLocalAppRsrcMgr))
                if verbose
                    fprintf('Generating New App Resource Manager...\n');
                end
                persistentLocalAppRsrcMgr = AppResourceMgr(appSess);
            else
                if verbose
                    fprintf('Retrieving App Resource Manager from Memory...\n');
                end
            end
            appRsrcMgr = persistentLocalAppRsrcMgr;
        end
    end
    methods (Static, Access = private)
        
        function rootPkgParentDirpath = find_root_package_parent_dirpath()
            currFilepath = mfilename('fullpath');
            [currDirpath, ~, ~] = fileparts(currFilepath);
            splitPathStrs = strsplit(fileparts(currDirpath), filesep);
            packageDirStrIdx = length(splitPathStrs);
            while ((packageDirStrIdx > 1) && ...
                    not(isempty(splitPathStrs{packageDirStrIdx})) && ...
                    (strcmp(splitPathStrs{packageDirStrIdx}(1), '+')))
                packageDirStrIdx = packageDirStrIdx - 1;
            end
            rootPkgParentDirpath = fullfile(splitPathStrs{1:packageDirStrIdx});
        end
    end
end
