function [bitmask] = get_block_bitmask(blockBoundaries, maxIndex)
	import Unused.FancyBitmasks.get_block_indices;
	import Unused.FancyBitmasks.get_bitmask_from_indices;
    bitmask = get_bitmask_from_indices(get_block_indices(blockBoundaries), maxIndex);
end