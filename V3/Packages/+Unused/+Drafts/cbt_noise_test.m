import Scripts.quick_cbt_demo;
[theoryBarcodesZscaled_pxRes, theoryBarcodesUnscaled_pxRes, seqDescriptions, seqFilepaths, seqIdxsInFile, barcodeGenSettings] = quick_cbt_demo();

hFig = figure('Name', 'CBT Noise Test');
hPanel = uipanel('Parent', hFig);
hTabgroup = uitabgroup('Parent', hPanel);

numBarcodes = length(theoryBarcodesUnscaled_pxRes);
for barcodeIdx = 1:numBarcodes
    theoryBarcodeUnscaled_pxRes = theoryBarcodesUnscaled_pxRes{barcodeIdx};
    seqDescription = seqDescriptions{barcodeIdx};
    theoryProbSignalStd = std(theoryBarcodeUnscaled_pxRes);
    fgSignalStd = 84.9;
    fgSignalStdCorrectionFactor = fgSignalStd/theoryProbSignalStd;
    fgNoiseMean = 722.4;
    fgNoiseStd = 37.1;

    fgNoiseImgSize = size(theoryBarcodeUnscaled_pxRes);
    fgNoiseAdjustmentAddend = fgNoiseMean + (randn(fgNoiseImgSize).*fgNoiseStd);
    adjustedBarcode = fgNoiseAdjustmentAddend + (theoryBarcodeUnscaled_pxRes.*fgSignalStdCorrectionFactor);
    hTab = uitab('Parent', hTabgroup, 'Title', seqDescription);
    hAxis = axes('Parent', hTab);
    plot(hAxis, adjustedBarcode);
end