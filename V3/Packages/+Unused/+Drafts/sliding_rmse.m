function [slidingRMSE] = sliding_rmse(seqRef, subseqApproxToMatch)
    %root mean square error (sliding)
    seqRef = seqRef(:);
    subseqApproxToMatch = subseqApproxToMatch(:);
    lenRef = length(seqRef);
    lenSubseq = length(subseqApproxToMatch);
    if lenRef < lenSubseq
        error('Subsequence to match must not be longer than reference sequence');
    end
    
    diffsMat = bsxfun(@minus, seqRef', subseqApproxToMatch);
    numRows = size(diffsMat, 1);
    for rowNum = 1:numRows
        diffsMat(rowNum, :) = circshift(diffsMat(rowNum, :), 1 - rowNum, 2);
    end
    slidingRMSE = sqrt(mean(diffsMat.^2));
end