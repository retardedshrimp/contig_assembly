function [featureScore] = cb_calcinfotheory_fs(curve, ratioAT, numSigma, smoothingWindowSz)
    % CB_CALCINFOTHEORY_FS = feature score for competitive binding theory barcodes
    %
    % Inputs: 
    %  curve
    %     the input barcode
    %  ratioAT
    %      the AT-concentration (#A and T/total number of bases)
    %  numSigma
    %   multiple of sigma that is to be used as an energy barrier
    %   threshold when detecting extrema for info score
    %
    % Outputs: 
    %   featureScore
    %     the feature score
    %
    % Authors:
    %   Erik Lagerstedt
    %   Saair Quaderi

    % SQ: Not sure this smoothing parameter/code is even worth
    %  keeping
    if nargin < 4
        % % historically smoothing was set to 5 but it's not
        % %  clear why it should happen, so commenting out
        % %  and setting to 1 so there is no smoothing by default
        % smoothingWindowSz = 5; 
        smoothingWindowSz = 1;
    end
    % Smooth the curve
    if smoothingWindowSz > 1
        curve = smooth(curve, smoothingWindowSz);
    end

    % Find the std in the intensity of a barcode with that AT-concentration
    import Unused.Historical.FeatureScores.get_eCISs3_std_approx;
    stdApprox = get_eCISs3_std_approx(ratioAT);

    % % historically, double the numSigma was multiplied, but that
    % %   doesn't really seem to make sense, commented out:
    % minValDistBetweenAdjExtrema = 2*numSigma*stdApprox;
    minValDistBetweenAdjExtrema = numSigma*stdApprox;


    % Reisner rescale the barcode
    curveMean = mean(curve(:));
    curveStd = std(curve(:));
    rescaledCurve = (curve-curveMean)./curveStd;

    import Unused.Historical.FeatureScores.detect_local_extrema;
    [localExtremaIdxs, ~] = detect_local_extrema(curve, minValDistBetweenAdjExtrema);
    numExtrema = numel(localExtremaIdxs);

    if numExtrema > 1
        rescaledLocalExtremaVals = rescaledCurve(localExtremaIdxs);
        featureScore = sum(diff([rescaledLocalExtremaVals(:)', rescaledLocalExtremaVals(1)]).^2);
    else
        featureScore = 0;
    end
end