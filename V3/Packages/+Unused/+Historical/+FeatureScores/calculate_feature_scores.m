function featureScoresStruct = calculate_feature_scores(theorySequences, displayNames, barcodeGenSettings)
    if nargin < 3
        import CBT.get_default_barcode_gen_settings;
        barcodeGenSettings = get_default_barcode_gen_settings();

        warning('Using default barcode generation settings:');
        disp(barcodeGenSettings);
    end

    outlierNumSigmas = 2;
    numTheories = length(theorySequences);
    import Unused.Historical.FeatureScores.cb_calcinfotheory_fs;
    import CBT.cb_netropsin_vs_yoyo1_plasmid;
    import CBT.get_AT_richness;
    import CBT.apply_point_spread_function;
    import CBT.apply_stretching;
    import CBT.convert_bpRes_to_pxRes;


    psfSigmaWidth_bp = barcodeGenSettings.psfSigmaWidth_nm/barcodeGenSettings.meanBpExt_nm;
    meanBpExt_pixels = barcodeGenSettings.meanBpExt_nm/barcodeGenSettings.pixelWidth_nm;

    sequenceLengths = zeros(numTheories, 1);
    ratiosAT = NaN(numTheories, 1);
    theoryCurves_pxRes = cell(numTheories, 1);
    for theoryNum=1:numTheories
        theorySequence = theorySequences{theoryNum};

        [probBindingYoyo1, ntBitsmart_theorySeq_uint8] = cb_netropsin_vs_yoyo1_plasmid(theorySequence, barcodeGenSettings.concNetropsin_molar, barcodeGenSettings.concYOYO1_molar, [], true);
        sequenceLength = length(ntBitsmart_theorySeq_uint8);
        ratioAT = get_AT_richness(ntBitsmart_theorySeq_uint8);
        theoryCurve_bpRes = probBindingYoyo1;

        theoryCurve_bpRes_prePSF = apply_stretching(theoryCurve_bpRes, stretchFactor);
        theoryCurve_bpRes_postPSF = apply_point_spread_function(theoryCurve_bpRes_prePSF, psfSigmaWidth_bp);
        theoryCurve_pxRes = convert_bpRes_to_pxRes(theoryCurve_bpRes_postPSF, meanBpExt_pixels);


        sequenceLengths(theoryNum) = sequenceLength;
        ratiosAT(theoryNum) = ratioAT;
        theoryCurves_pxRes(theoryNum) = theoryCurve_pxRes;
    end
    theoryBarcodes = cellfun(@zscore, theoryCurves_pxRes, 'UniformOutput', false); % Reisner-rescale (not totally related)

    featureScores = zeros(numTheories, 1);
    featureScoreDensities = zeros(numTheories, 1);
    for theoryNum = 1:numTheories
        theoryBarcode = theoryBarcodes{theoryNum};
        featureScore = cb_calcinfotheory_fs(theoryBarcode, ratioAT, outlierNumSigmas);
        featureScoreDensity = featureScore/sequenceLength;

        featureScores(theoryNum) = featureScore;
        featureScoreDensities(theoryNum) = featureScoreDensity;
    end

    featureScoresStruct.sequenceLengths = sequenceLengths;
    featureScoresStruct.featureScores = featureScores;
    featureScoresStruct.feaatureScoreDensities = featureScoreDensities;
    featureScoresStruct.displayNames = displayNames;
end