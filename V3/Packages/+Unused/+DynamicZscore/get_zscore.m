function normalizedSubseq = get_zscore(meanMat, stdMat, sequence, startIdx, endIdx)
    % zscore normalizes a sequence to a mean of 0 and a variance
    %   and standard deviation of 1
    % This is faster at computing zscore in some cases where it needs
    %   to be done repeatedly because it can reuse precomputed values
    %   (the means and standard deviations)
    % Example usage as follows:
    %  import Unused.DynamicZscore.subseq_means_and_stds;
    %  import Unused.DynamicZscore.get_zscore;
    %  [meanMat, stdMat] = subseq_means_and_stds(bigSequence);
    %  bigSequenceLen = length(bigSequence);
    %  for startIdx=1:bigSequenceLen
    %    for endIdx=startIdx+1:bigSequenceLen
    %      normalizedSubseq = get_zscore(meanMat, stdMat, bigSequence, startIdx, endIdx);
    %      %... do things!
    %    end
    %  end
    %  clear meanMat, stdDevMat; % Clear these big matrices out from memory
    
    subseq = sequence(startIdx:endIdx);
    mu = meanMat(startIdx, endIdx);
    sigma = stdMat(startIdx, endIdx);
    if (sigma == 0)
        sigma = 1;
    end
    normalizedSubseq = (subseq - mu)/sigma;
    normalizedSubseq = normalizedSubseq(:)';
end