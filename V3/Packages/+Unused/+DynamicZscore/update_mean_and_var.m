function [new_sample_mean, new_sample_var, new_sum_xb, new_sum_xxb] = update_mean_and_var(new_sample_size, old_sum_xb, old_sum_xxb, x_removed, x_added, xx_removed, xx_added)

    new_sum_xb =  old_sum_xb - sum(x_removed) + sum(x_added);
    new_sum_xxb = old_sum_xxb - sum(xx_removed) + sum(xx_added);

    new_sample_mean = new_sum_xb/new_sample_size;
    new_sample_var =  new_sum_xxb/(new_sample_size - 1) - new_sum_xb^2/(new_sample_size*(new_sample_size - 1));
end