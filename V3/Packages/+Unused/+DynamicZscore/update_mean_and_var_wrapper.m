function [new_sample_mean, new_sample_var, new_sum_xb, new_sum_xxb] = update_mean_and_var_wrapper(old_sum_xb, old_sum_xxb, old_bitmask, new_bitmask, x, xx)
    removed_bitmask = (old_bitmask == 1 + new_bitmask);
    added_bitmask = (new_bitmask == 1 + old_bitmask);

    new_sample_size = sum(new_bitmask);
    x_removed = x(removed_bitmask);
    x_added = x(added_bitmask);
    xx_removed = xx(removed_bitmask);
    xx_added = xx(added_bitmask);

    [new_sample_mean, new_sample_var, new_sum_xb, new_sum_xxb] = Unused.DynamicZscore.update_mean_and_var(new_sample_size, old_sum_xb, old_sum_xxb, x_removed, x_added, xx_removed, xx_added);
end