function [meanMat, stdMat] = subseq_means_and_stds(seq)
    % Use dynamic programming to compute means and standard deviations
    %  for all continuous ranges of values within x
    %  Useful when these need to be repeatedly computed over different
    %  subsequences of the same sequence
    n = length(seq);
    if (n == 0)
        meanMat = NaN;
        stdMat = NaN;
        return;
    elseif (n == 1)
        meanMat = seq(1);
        stdMat = 0;
        return;
    end
    meanMat = zeros(n, n);
    varMat = meanMat;
    lastSum = cumsum(seq);
    meanMat(1,:) = lastSum./(1:n);
    for ii=2:1:n
        % dynamic programming
        lastSum = lastSum(2:end) - lastSum(1);
        meanMat(ii,ii:end) = lastSum./(1:(n + 1 - ii));
    end

    for ii=1:1:n
        for jj=(ii + 1):1:n
            % note: jj-ii = length(seq(ii:jj)) - 1
            varMat(ii, jj) = sum(abs(seq(ii:jj) - meanMat(ii, jj)).^2)./(jj - ii);
        end
    end
    stdMat = sqrt(varMat);
end