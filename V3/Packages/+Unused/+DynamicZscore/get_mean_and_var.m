function [sample_mean, sample_var, sample_size, sum_xb, sum_xxb] = get_mean_and_var(x, xx, bitmask)
    % xx = x.*x; (precomputed)
    sample_size = sum(bitmask);
    sum_xb = sum(x.*bitmask);
    sum_xxb = sum(xx.*bitmask);
    sample_mean = sum_xb/sample_size;
    sample_var = sum_xxb/(sample_size - 1) - sum_xb^2/(sample_size*(sample_size - 1));
end