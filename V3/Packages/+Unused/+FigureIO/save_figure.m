function [hFig, figFilepath] = save_figure(hFig, figFilepath)
    if nargin < 1
        hFig = gcf();
    end
    if nargin < 2
        timestamp = datestr(clock(), 'yyyy-mm-dd_HH_MM_SS');
        import AppMgr.AppResourceMgr;
        appRsrcMgr = AppResourceMgr.get_instance();
        appDirpath = appRsrcMgr.get_app_dirpath();
        defaultFigureDirpath = appDirpath;
        figureName = get(hFig, 'Name');
        defaultFigureFilename = sprintf('figure_%s_%s.fig', figureName, timestamp);
        defaultFigureFilepath = fullfile(defaultFigureDirpath, defaultFigureFilename);
        figFilepath = uiputfile({'*.fig'},'Save figure', defaultFigureFilepath);
    end
    saveas(hFig, figFilepath);
end