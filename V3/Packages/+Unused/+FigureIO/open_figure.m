function [hFig, figFilepath] = open_figure(figFilepath)
    if nargin < 1
        import AppMgr.AppResourceMgr;
        appRsrcMgr = AppResourceMgr.get_instance();
        appDirpath = appRsrcMgr.get_app_dirpath();
        defaultFigureDirpath = appDirpath;
        figFilepath = uigetfile({'*.fig'}, 'Open a figure to save a screenshot of', defaultFigureDirpath);
    end
    hFig = openfig(figFilepath);
end