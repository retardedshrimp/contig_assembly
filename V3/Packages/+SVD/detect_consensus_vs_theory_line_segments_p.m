function [] = detect_consensus_vs_theory_line_segments_p(ts, consensusCurve, theoryCurve)
    % DETECT_CONSENSUS_VS_THEORY_LINE_SEGMENTS_P = Line segment detection consensus vs theory
    %
    % Creates a matrix with comparisons of small segments
    % between a consensus and a theoretical sequence, and highlights
    % similar regions.
    %
    % Authors:
    %   Erik Lagerstedt
    %   Saair Quaderi (refactoring)
    %
    % This is not a final version. There is much code that can be removed and
    % other parts that can be rewritten. It is similar to 
    % cb_linesegmentdetection, but that function is using another (and 
    % possibly worse) algorithm to create the binary matrix.

    %TODO: use tabbed screens (ts), instead of creating new figures
    %   separate display from computation, only display what's
    %   necessary, and consolidate with other version
    
    % Dynamic time warp is used to find the
    % direction the barcodes should have. Probably a bit wasteful.

    indelRelCost = 40;
    import SVD.Core.compute_dtw;
    [dtwUnnormalizedDist, ~, ~, ~] = compute_dtw(consensusCurve, theoryCurve, indelRelCost, indelRelCost);
    [dtwUnnormalizedDistFlipped, ~, ~, ~] = compute_dtw(flip(consensusCurve), theoryCurve, indelRelCost, indelRelCost);
    if dtwUnnormalizedDistFlipped < dtwUnnormalizedDist
        consensusCurve = flip(consensusCurve);
    end

    % Here the matrix is created. Each pixel is the cross correlation
    % between two small sections of the two barcodes.
    xs = zeros([numel(consensusCurve), numel(theoryCurve)]);
    fragLeng=10;
    for idxAlongConsensusCurve = 1:(length(consensusCurve)-fragLeng)
        for idxAlongTheoryCurve=1:(length(theoryCurve)-fragLeng)
            xs(idxAlongConsensusCurve,idxAlongTheoryCurve)=(sum(consensusCurve(idxAlongConsensusCurve:idxAlongConsensusCurve+fragLeng).*theoryCurve(idxAlongTheoryCurve:idxAlongTheoryCurve+fragLeng))/(norm(consensusCurve(idxAlongConsensusCurve:idxAlongConsensusCurve+fragLeng))*norm(theoryCurve(idxAlongTheoryCurve:idxAlongTheoryCurve+fragLeng))));
        end
    end
    figure
    imshow(xs,[])
    xs0=xs(1:end-fragLeng,1:end-fragLeng);
    figure
    imshow(xs0,[])

    % Each element in the matrix is set to 1 or 0, to get a black-and-white
    % image, which then can be evaluated.

    % One way of choosing the threshold
    %xs2=xs>0.9;

    % Another way of choosing the threshold
    c=5;
    valVec=sort(xs(:),'descend');
    xs2=xs>valVec(round(length(valVec)*c*sqrt(2)/min(size(xs))));

    figure();
    colormap('gray');
    imagesc(abs(xs2-1));
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    xs3 = zeros(size(xs));
    for idxAlongConsensusCurve = 1:(length(consensusCurve)-fragLeng)
        for idxAlongTheoryCurve = 1:(length(theoryCurve)-fragLeng)
            if xs2(idxAlongConsensusCurve,idxAlongTheoryCurve)
                for ijc = 0:(fragLeng - 1)
                    xs3(idxAlongConsensusCurve+ijc, idxAlongTheoryCurve+ijc) = 1;
                end
            end
        end
    end
    xs2 = xs3;
    figure();
    colormap('gray');
    imagesc(abs(xs3-1));
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);


    % Some filters are applied to enhance and detect the lines.
    sigmaTV = 0.5;
    sizeTV = 2;
    x = linspace(-sizeTV / 2, sizeTV / 2, sizeTV);
    gaussFilter = exp(-x .^ 2 / (2 * sigmaTV ^ 2));
    gaussFilter = gaussFilter / sum(gaussFilter);
    xs2 = filter(gaussFilter,1, xs2);
    xs2 = filter(gaussFilter,1, xs2');
    xs2 = xs2';
    figure();
    colormap('gray');
    imagesc(abs(xs2-1));

    import ThirdParty.RankOrderFilter.RankOrderFilter;
    xs5=fliplr(RankOrderFilter(xs2,20,30)); %TODO: ang of 45
    xs6=RankOrderFilter(fliplr(xs2),20,30); %TODO: ang of -45

    sigmaTV = 0.5;
    sizeTV = 2;
    x = linspace(-sizeTV / 2, sizeTV / 2, sizeTV);
    gaussFilter = exp(-x .^ 2 / (2 * sigmaTV ^ 2));
    gaussFilter = gaussFilter / sum(gaussFilter);
    xs5 = filter(gaussFilter,1, xs5);
    sigmaTV = 0.5;
    sizeTV = 2;
    x = linspace(-sizeTV / 2, sizeTV / 2, sizeTV);
    gaussFilter = exp(-x .^ 2 / (2 * sigmaTV ^ 2));
    gaussFilter = gaussFilter / sum(gaussFilter);
    xs6 = filter(gaussFilter,1, xs6);
    [xs7,xs7T,xs7R]=hough(xs5>0,'Theta',-45);

    xs7c=houghlines(xs5,xs7T,xs7R,houghpeaks(xs7, 20),'FillGap',10,'MinLength',50);

    figure();
    imagesc(abs((xs5>0)-1)), colormap(gray);
    hold on
    for it=1:length(xs7c)
        xy = [xs7c(it).point1; xs7c(it).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','r');
    end
    hold off
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    [xs8,xs8T,xs8R]=hough(xs6>0,'Theta',45);

    xs8c=houghlines(xs6,xs8T,xs8R,houghpeaks(xs8, 20),'FillGap',10,'MinLength',50);

    figure();
    imagesc(abs((xs6>0)-1)), colormap(gray);
    hold on
    for it=1:length(xs8c)
        xy = [xs8c(it).point1; xs8c(it).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','r');
    end
    hold off
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);

    gmcc = zeros(length(xs7c),1);
    tmcc = zeros(length(xs8c),1);

    % The detected lines are saved and drawn.
    figure();
    imagesc(abs(((xs5+xs6)>0)-1)), colormap(gray);
    hold on
    for it=1:length(xs7c)
        xy = [xs7c(it).point1; xs7c(it).point2];
        %CC for the matching fragments
        conSec=consensusCurve(xs7c(it).point1(1,2):xs7c(it).point2(1,2));
        thySec=theoryCurve(xs7c(it).point1(1,1):xs7c(it).point2(1,1));
        if length(conSec)==length(thySec)
            gmcc(it)=sum(conSec.*thySec)/length(conSec);
        else
            if length(conSec)>length(thySec)
                conSec=conSec(1:length(thySec));
            else
                thySec=thySec(1:length(conSec));
            end
            gmcc(it)=sum(conSec.*thySec)/length(conSec);
        end
        figure
        plot(conSec)
        hold on
        plot(thySec,'r')
        xlabel(gmcc(it));
        hold off
    end
    for it=1:length(xs8c)

        %CC for the matching fragments
        if xs8c(it).point2(1,1)<xs8c(it).point1(1,1)
            conSec=consensusCurve(xs8c(it).point1(1,2):xs8c(it).point2(1,2));
            thySec=fliplr(theoryCurve(xs8c(it).point2(1,1):xs8c(it).point1(1,1)));
        else
            conSec=consensusCurve(xs8c(it).point2(1,2):xs8c(it).point1(1,2));
            thySec=fliplr(theoryCurve(xs8c(it).point1(1,1):xs8c(it).point2(1,1)));            
        end
        if length(conSec)==length(thySec)
            tmcc(it)=sum(conSec.*thySec)/length(conSec);
        else
            if length(conSec)>length(thySec)
                conSec=conSec(1:length(thySec));
            else
                thySec=thySec(1:length(conSec));
            end
            tmcc(it)=sum(conSec.*thySec)/length(conSec);
        end
        figure
        plot(conSec)
        hold on
        plot(thySec,'r')
        xlabel(tmcc(it));
        hold off
    end
    hold off

    % The image is ploted, and the lines are drawn on it. The cross
    % correlation for the region under the line is displayed.
    figure();
    imagesc(abs(xs2-1));
    colormap('gray');
    hold on
    for it=1:length(xs7c)
        xy = [xs7c(it).point1; xs7c(it).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',3,'Color','g');
        text(xs7c(it).point1(1,1),xs7c(it).point1(1,2),num2str(gmcc(it)),'Color','g','FontWeight','bold')
    end
    for it=1:length(xs8c)
        xy = [xs8c(it).point1; xs8c(it).point2];
        plot(xy(:,1),xy(:,2),'LineWidth',3,'Color','r');
        text(xs8c(it).point1(1,1),xs8c(it).point1(1,2),num2str(tmcc(it)),'Color','r','FontWeight','bold')
    end
    hold off
    set(gca, ...
        'XTick', [], ...
        'YTick', []);

    % [xs5a,xs5b,xs5c]=hough(xs5);
    % [xs6a,xs6b,xs6c]=hough(xs6);
    % xs5=houghlines(xs2,xs5b,xs5c,houghpeaks(xs5a,3));
    % xs6=houghlines(xs6,xs6b,xs6c,houghpeaks(xs6a,3));
    % 
    % sigmaTV = 5;
    % sizeTV = 30;
    % x = linspace(-sizeTV / 2, sizeTV / 2, sizeTV);
    % gaussFilter = exp(-x .^ 2 / (2 * sigmaTV ^ 2));
    % gaussFilter = gaussFilter / sum(gaussFilter);
end