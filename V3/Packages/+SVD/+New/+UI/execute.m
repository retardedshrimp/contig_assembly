function [] = execute(...
        hEditSelectFile1, hEditSelectFile2, ...
        hRadioButton1, hRadioButton2, hRadioButton3, hRadioButton4, ...
        hMVMParamBox1,...
        hLCMAParamBox1, hLCMAParamBox2, hLCMAParamBox3, hLCMAParamBox4,...
        hCheckboxUseNeighbors, hEditNeighborWeight, hCheckboxUsePCC)
    %Initiation:
    import SVD.Import.import_curve_from_filepath;
    seq1 = import_curve_from_filepath(get(hEditSelectFile1, 'String'));
    seq2 = import_curve_from_filepath(get(hEditSelectFile2, 'String'));

    useNeighborhood = (hCheckboxUseNeighbors.Value == 1);
    usePCC = (hCheckboxUsePCC.Value == 1);
    if (useNeighborhood || usePCC)
        dist = round(str2double(get(hEditNeighborWeight, 'String')));
    else
        dist = 0;
    end

    useDTWMethod = (hRadioButton1.Value == 1);
    useMVMMethod = (hRadioButton2.Value == 1);
    useALCMASDMethod = (hRadioButton3.Value == 1);
    useLAPMethod = (hRadioButton4.Value == 1);
    if length(seq2) < length(seq1)
        [seqShorter, seqLonger] = deal(seq2, seq1);
    else
        [seqShorter, seqLonger] = deal(seq1, seq2);
    end
    refSeq = seqLonger;
    warpedSeq = seqShorter;

    hFigCostMat = figure('Units', 'normalized', 'Position', [0.25 0.13 0.5 0.33]);
    hParentCostMat = hFigCostMat;
    hAxisCostMat = axes('Parent', hParentCostMat);
    hFigAlignment = figure('Units', 'normalized', 'Position', [0.25 0.55 0.5 0.33]);
    hParentAlignment = hFigAlignment;
    hAxisAlignment = axes('Parent', hParentAlignment);

    %Detects and executes method:
    if useDTWMethod %DTW
        import SVD.run_dtw_method;
        run_dtw_method(warpedSeq, refSeq, useNeighborhood, usePCC, dist, hAxisCostMat, hAxisAlignment)
    elseif useMVMMethod
        mvmWinWidth = str2double(get(hMVMParamBox1, 'String'));

        import SVD.run_mvm_method;
        run_mvm_method(warpedSeq, refSeq, useNeighborhood, usePCC, dist, mvmWinWidth, hAxisCostMat, hAxisAlignment);
    elseif useALCMASDMethod %A-LCMA-SD
        bandWidth = str2double(get(hLCMAParamBox1, 'String'));
        lengthParam = str2double(get(hLCMAParamBox2, 'String'));
        lcmaThresholdZeroDist = str2double(get(hLCMAParamBox3, 'String'));
        numBestPaths = str2double(get(hLCMAParamBox4, 'String'));

        import SVD.run_A_LCMA_SD_method;
        run_A_LCMA_SD_method(warpedSeq, refSeq, useNeighborhood, usePCC, dist, bandWidth, lengthParam, lcmaThresholdZeroDist, numBestPaths, hAxisCostMat, hAxisAlignment);
    elseif useLAPMethod
        import SVD.run_lap_method;
        run_lap_method(warpedSeq, refSeq, useNeighborhood, usePCC, dist, hAxisCostMat, hAxisAlignment);
    end
end