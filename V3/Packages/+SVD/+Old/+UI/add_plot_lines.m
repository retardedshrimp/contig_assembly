function [] = add_plot_lines(hAxisA, hAxisB, pixelIdxA, pixelIdxB)
    axes(hAxisA);
    hold(hAxisA, 'on');
    line(pixelIdxA.*[1 1], get(hAxisA,'YLim'));
    hold(hAxisA, 'off');

    axes(hAxisB);
    hold(hAxisB, 'on');
    line(get(hAxisB,'XLim'), pixelIdxB.*[1 1]);
    hold(hAxisB, 'off');
end