function [] = on_tvt_curve_vs_curve_click(coordPos, hAxisAB, hAxisA, hAxisB, theoryCurveLenA, theoryCurveLenB, theorySeqLenA, theorySeqLenB)
    set(hAxisAB, 'Units', 'normalized');
    nrmAreaPosVector = get(hAxisAB, 'Position');

    import SVD.Old.UI.Helpers.get_relative_position;
    [ratioAlongAreaX, ratioAlongAreaY] = get_relative_position(nrmAreaPosVector, coordPos);
    if isnan(ratioAlongAreaX) || isnan(ratioAlongAreaY)
        return;
    end
    ratioAlongAreaY = 1 - ratioAlongAreaY;

    pixelIdxA = round(ratioAlongAreaX*theoryCurveLenA);
    pixelIdxB = round(ratioAlongAreaY*theoryCurveLenB);
    import SVD.Old.UI.add_plot_lines;
    add_plot_lines(hAxisA, hAxisB, pixelIdxA, pixelIdxB);

    approxBpIdxA = round(ratioAlongAreaX*theorySeqLenA);
    approxBpIdxB = round(ratioAlongAreaY*theorySeqLenB);
    import SVD.Old.UI.show_tvt_click_message;
    show_tvt_click_message(approxBpIdxA, approxBpIdxB);
end