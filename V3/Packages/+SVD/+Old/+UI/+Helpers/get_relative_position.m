function [ratioAlongAreaX, ratioAlongAreaY] = get_relative_position(nrmAreaPosVector, coordPos)
    areaStartX = nrmAreaPosVector(1);
    areaStartY = nrmAreaPosVector(2);
    areaRangeX = nrmAreaPosVector(3);
    areaRangeY = nrmAreaPosVector(4);
    areaEndX = areaStartX + areaRangeX;
    areaEndY = areaStartY + areaRangeY;
    coordX = coordPos(1);
    coordY = coordPos(2);
    if (coordX < areaStartX) || (coordX > areaEndX) || (coordY < areaStartY) || (coordY > areaEndY)
        ratioAlongAreaX = NaN;
        ratioAlongAreaY = NaN;
    else
        ratioAlongAreaX = (coordX - areaStartX)/areaRangeX;
        ratioAlongAreaY = (coordY - areaStartY)/areaRangeY;
    end
end