function [] = run_consensus_vs_theory_line_segment_detection_x(ts)
    import SVD.General.Import.get_consensus_curve;
    consensusCurve = get_consensus_curve();
    if isempty(consensusCurve)
        return;
    end

    [theoryCurve, ~] = SVD.Old.Import.prompt_theory_curve();
    if isempty(theoryCurve)
        return;
    end

    import SVD.detect_consensus_vs_theory_line_segments_x;
    detect_consensus_vs_theory_line_segments_x(ts, consensusCurve, theoryCurve);
end