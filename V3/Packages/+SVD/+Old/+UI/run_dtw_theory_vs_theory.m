function [] = run_dtw_theory_vs_theory(ts)

    import SVD.Old.Import.prompt_theory_curve;
    [theoryCurveA, theorySequenceA] = prompt_theory_curve();
    theorySeqLenA = length(theorySequenceA);
    theoryCurveLenA = length(theoryCurveA);
    if isempty(theoryCurveA)
        return;
    end

    [theoryCurveB, theorySequenceB] = prompt_theory_curve();
    theorySeqLenB = length(theorySequenceB);
    theoryCurveLenB = length(theoryCurveB);
    if isempty(theoryCurveB)
        return;
    end

    theoryAWasFlipped = false;

    indelRelCost = 40;
    import SVD.Core.compute_dtw;
    [dtwUnnormalizedDist, accumulatedDistMat, ~, optimalPath] = compute_dtw(theoryCurveA, theoryCurveB, indelRelCost, indelRelCost);
    [dtwUnnormalizedDistFlipped, accumulatedDistMatFlipped, ~, optimalPathFlipped] = compute_dtw(flip(theoryCurveA), theoryCurveB, indelRelCost, indelRelCost);
    if (dtwUnnormalizedDistFlipped < dtwUnnormalizedDist)
        dtwUnnormalizedDist = dtwUnnormalizedDistFlipped;
        accumulatedDistMat = accumulatedDistMatFlipped;
        optimalPath = optimalPathFlipped;
        theoryCurveA = flip(theoryCurveA);
        theoryAWasFlipped = true;
    end

    if theoryAWasFlipped
        tabTitleDTW = 'Dynamic Time Warping (Flipped Theory vs Theory)';
    else
        tabTitleDTW = 'Dynamic Time Warping (Theory vs Theory)';
    end

    fprintf('"G" (lower is better): %g\n', dtwUnnormalizedDist/length(theoryCurveA));


    hTabDTW = ts.create_tab(tabTitleDTW);
    ts.select_tab(hTabDTW);
    hPanelDTW = uipanel('Parent', hTabDTW);

    labelFactorA = theorySeqLenA/theoryCurveLenA;
    labelFactorB = theorySeqLenB/theoryCurveLenB;
    import SVD.General.UI.plot_dtw_curve_vs_curve;
    [hAxisAB, hAxisA, hAxisB] = plot_dtw_curve_vs_curve(hPanelDTW, accumulatedDistMat, optimalPath, theoryCurveA, theoryCurveB, labelFactorA, labelFactorB);


    import SVD.Old.UI.on_tvt_curve_vs_curve_click;
    hFig = ancestor(hPanelDTW, 'figure');
    iptaddcallback(hFig, ...
        'WindowButtonDownFcn', @(hObject, ~) ...
            on_tvt_curve_vs_curve_click(get(hObject, 'CurrentPoint'), hAxisAB, hAxisA, hAxisB, theoryCurveLenA, theoryCurveLenB, theorySeqLenA, theorySeqLenB));
    
    import SVD.Core.calculate_step_costs;
    [stepCosts] = calculate_step_costs(accumulatedDistMat, optimalPath);

    tabTitleStepCosts = 'Step costs';
    hTabeStepCosts = ts.create_tab(tabTitleStepCosts);
    ts.select_tab(hTabeStepCosts);
    hPanelStepCosts = uipanel('Parent', hTabeStepCosts);
    
    hAxisStepCosts = axes('Parent', hPanelStepCosts);
    import SVD.Old.UI.plot_step_costs;
    plot_step_costs(hAxisStepCosts, stepCosts);
end