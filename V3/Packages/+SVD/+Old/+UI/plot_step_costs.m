function [] = plot_step_costs(hAxisStepCosts, stepCosts)
    plot(hAxisStepCosts, stepCosts);
    xlabel(hAxisStepCosts, 'Step along path');
    ylabel(hAxisStepCosts, 'Cost for step');
end