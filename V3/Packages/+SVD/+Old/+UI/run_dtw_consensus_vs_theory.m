function [] = run_dtw_consensus_vs_theory(ts)
    import SVD.Core.compute_dtw;
    import SVD.Core.calculate_step_costs;

    import SVD.General.Import.get_consensus_curve;
    consensusCurve = get_consensus_curve();
    consensusCurveLen = length(consensusCurve);
    if isempty(consensusCurve)
        return;
    end

    [theoryCurve, theorySequence] = SVD.Old.Import.prompt_theory_curve();
    theorySeqLen = length(theorySequence);
    theoryCurveLen = length(theoryCurve);
    if isempty(theoryCurve)
        return;
    end

    consensusWasFlipped = false;

    indelRelCost = 40;
    [dtwUnnormalizedDist, accumulatedDistMat, ~, optimalPath] = compute_dtw(consensusCurve, theoryCurve, indelRelCost, indelRelCost);
    [dtwUnnormalizedDistFlipped, accumulatedDistMatFlipped, ~, optimalPathFlipped] = compute_dtw(flip(consensusCurve), theoryCurve, indelRelCost, indelRelCost);
    if (dtwUnnormalizedDistFlipped < dtwUnnormalizedDist)
        dtwUnnormalizedDist = dtwUnnormalizedDistFlipped;
        accumulatedDistMat = accumulatedDistMatFlipped;
        optimalPath = optimalPathFlipped;
        consensusCurve = flip(consensusCurve);
        consensusWasFlipped = true;
    end

    if consensusWasFlipped
        tabTitleDTW = 'Dynamic Time Warping (Flipped Consensus vs Theory)';
    else
        tabTitleDTW = 'Dynamic Time Warping (Consensus vs Theory)';
    end

    fprintf('"G" (lower is better): %g\n', dtwUnnormalizedDist/length(consensusCurve));


    hTabDTW = ts.create_tab(tabTitleDTW);
    ts.select_tab(hTabDTW);
    hPanelDTW = uipanel('Parent', hTabDTW);

    labelFactorA = 1;
    labelFactorB = 1;
    import SVD.General.UI.plot_dtw_curve_vs_curve;
    [axisAB, axisA, axisB] = plot_dtw_curve_vs_curve(hPanelDTW, accumulatedDistMat, optimalPath, consensusCurve, theoryCurve, labelFactorA, labelFactorB);


    import SVD.Old.UI.on_cvt_curve_vs_curve_click;
    hFig = ancestor(hPanelDTW, 'figure');
    iptaddcallback(hFig, ...
        'WindowButtonDownFcn', @(hObject, ~) ...
            on_cvt_curve_vs_curve_click(get(hObject, 'CurrentPoint'), axisAB, axisA, axisB, consensusCurveLen, theoryCurveLen));
    [stepCosts] = calculate_step_costs(accumulatedDistMat, optimalPath);

    tabTitleStepCosts = 'Step costs';
    hTabeStepCosts = ts.create_tab(tabTitleStepCosts);
    ts.select_tab(hTabeStepCosts);
    hPanelStepCosts = uipanel('Parent', hTabeStepCosts);
    
    hAxisStepCosts = axes('Parent', hPanelStepCosts);
    
    import SVD.Old.UI.plot_step_costs;
    plot_step_costs(hAxisStepCosts, stepCosts);
end
