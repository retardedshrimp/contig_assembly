function [theoryCurve, ntSeq, ntSeqFilepath] = prompt_theory_curve()
    theoryCurve = [];
    ntSeq = [];
    ntSeqFilepath = [];

    import NtSeq.Import.UI.try_prompt_nt_seq_filepaths;
    [aborted, ntSeqFilepaths] = try_prompt_nt_seq_filepaths([], false, false);
    if (aborted) || isempty(ntSeqFilepaths)
        return;
    end
    ntSeqFilepath = ntSeqFilepaths{1};

    import NtSeq.Import.import_fasta_nt_seqs;
    ntSeqs = import_fasta_nt_seqs(ntSeqFilepaths);
    ntSeq = ntSeqs{1};

    import CBT.get_default_barcode_gen_settings;
    defaultBarcodeGenSettings = get_default_barcode_gen_settings();
    barcodeGenSettings = defaultBarcodeGenSettings;

    import CBT.gen_zscaled_cbt_barcode;
    theoryCurve = gen_zscaled_cbt_barcode(ntSeq, barcodeGenSettings);
end