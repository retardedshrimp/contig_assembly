function [bindingsMat_bpRes] = generate_k_theory_bindings_bpRes(theoryCurveProbs_bpRes, k)
    
    bindingsMat_bpRes = 1.*bsxfun(@ge, theoryCurveProbs_bpRes(:)', rand([k, length(theoryCurveProbs_bpRes)]));
% import CBT.cb_netropsin_vs_yoyo1_plasmid;
% [probBindingYoyo1, ntBitsmart_theorySeq_uint8] = cb_netropsin_vs_yoyo1_plasmid(ntSeq, concNetropsin_molar, concYOYO1_molar, [], true);
      
end