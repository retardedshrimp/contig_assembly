function sOut = hashify_struct(sIn)
    % HASHIFY_STRUCT - helper function that hashifies struct fields so
    %   they can be diffed for changes more easily
    %
    % Inputs
    %  sIn
    %    the scalar (1x1) input struct array
    %
    % Outputs
    %  sOut
    %   a new version of the struct with fields of hashes labeling the
    %   hashes computed for fields
    %
    % Note that the struct would need to contain only DataHash supported
    %   data to capture all differences. Certain classes such as
    %   containers.Map aren't currently supported by DataHash, but for
    %   simple things like structs of numeric values, logicals, characters,
    %   and cell arrays, it should work
    import FancyUtils.data_hash;
    
    sOut = struct;
    fnames = fieldnames(sIn);
    numFields = numel(fnames);
    for fieldNum=1:numFields
        fname = fnames{fieldNum};
        tmp = sIn.(fname);
        if isstruct(tmp)
            tmp = hashify_struct(tmp);
        end
        sOut.(fname) = tmp;
        sOut.(['hSx_', data_hash(sIn.(fname))]) = fname;
    end
end