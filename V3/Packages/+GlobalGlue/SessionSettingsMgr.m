classdef SessionSettingsMgr < handle
    
    properties
        SettingsDataMap = containers.Map()
    end
    
    methods
        function [ssm] = SessionSettingsMgr()
        end
        function [] = set_answer(ssm, infoKey, answer)
            ssm.SettingsDataMap(infoKey) = answer;
        end
    end
    
    methods(Static)
        function appDirpath = get_app_dir()
            appDirpath = fileparts(fileparts(fileparts(mfilename('fullpath'))));
        end
        function answer = find_answer(infoKey)
            switch infoKey
                case 'Prompt for bitmasking params?'
                    try
                        import AppMgr.AppResourceMgr;
                        appRsrcMgr = AppResourceMgr.get_instance();
                        appDirpath = appRsrcMgr.get_app_dirpath();
                        settingsDirpath = fullfile(appDirpath, 'SettingFiles');
                        settingsFilename = 'DBM.ini';
                        settingsFilepath = fullfile(settingsDirpath, settingsFilename);
                        
                        import GlobalGlue.SessionSettingsMgr;
                        dbmSettingsStruct = SessionSettingsMgr.read_ini_settings_to_struct(settingsFilepath);
                        bitmaskSettings = dbmSettingsStruct.bitmasks;
                        promptForBitmaskingParams = bitmaskSettings.promptForBitmaskingParams;
                        answer = logical(promptForBitmaskingParams);
                    catch
                        promptForBitmaskingParams = false;
                    end
            end
            
        end
        function answer = get_answer(infoKey)
            import GlobalGlue.SessionSettingsMgr;
        end
        function outStruct = read_ini_settings_to_struct( settingsIniFilepath )
            % READ_INI_SETTINGS_TO_STRUCT - Reads and parses an INI setting file
            %
            % Returns a structure with section names and keys as fields.
            %
            % See FancyIO.ini2struct for details
            
            import FancyIO.ini2struct;
            outStruct = ini2struct(settingsIniFilepath);
        end
        
    end
end