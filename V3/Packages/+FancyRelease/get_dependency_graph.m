function [depFilePaths, depProductsTable, depSparseMatrix, depGraph] = get_dependency_graph(startEntryCommand)
    % GET_DEPENDENCY_GRAPH - builds a directed acyclic graph of
    %  dependencies for a command
    %
    % Inputs:
    %   startEntryCommand
    %     the initial command
    %
    % Outputs:
    %   depFilePaths
    %     cell array of dependency filepaths
    %   depProductsTable
    %     table containing matlab product toolbox dependencies
    %   depSparseMatrix
    %     sparse matrix containing filepath dependency data
    %   depGraph
    %     Note: requires Matlab digraph functionality
    
    % Todo: fix issue where dependencies aren't being detected from
    % filepaths; it only seems to with commands for some reasons and since
    % the dependencies are found as filepaths, only shallow dependencies
    % seem to be detected reliably from the command
    entryIndicesMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
    startEntryPath = which(startEntryCommand);
    entryIndicesMap(startEntryPath) = {1, []};
    oldNodePaths = keys(entryIndicesMap);
    numOldEntries = numel(oldNodePaths);
    nodeIdx = 0;
    totalEdges = 0;
    depProductsTable = [];
    while nodeIdx < numOldEntries
        nodeIdx = nodeIdx + 1;
        nodePath = oldNodePaths{nodeIdx};
        [depEntries, pList] = matlab.codetools.requiredFilesAndProducts(nodePath, 'toponly');
        depProductsTable = [depProductsTable; arrayfun(@(s) setfield(s, 'code', nodePath), pList(:))];
        depEntries = depEntries(:);
        numDepEntries = numel(depEntries);
        totalEdges = totalEdges + numDepEntries;
        [newEntries, newEntryDiffIdxs] = setdiff(depEntries, oldNodePaths);
        newEntryMask = false(numDepEntries, 1);
        newEntryMask(newEntryDiffIdxs) = true;
        oldEntryDeps = depEntries(~newEntryMask);
        numNewEntries = numel(newEntries);
        newEntryIndices = numOldEntries + (1:numNewEntries)';
        for newEntryNum=1:numNewEntries
            entryIndicesMap(newEntries{newEntryNum}) = {newEntryIndices(newEntryNum), []};
        end
        currEntryVal = entryIndicesMap(nodePath);
        [~, oldEntryIndices] = intersect(oldNodePaths, oldEntryDeps);
        edgeIdxs = [oldEntryIndices(:); newEntryIndices];
        currEntryVal{2} = edgeIdxs;
        entryIndicesMap(nodePath) = currEntryVal;
        oldNodePaths = keys(entryIndicesMap);
        numOldEntries = numel(oldNodePaths);
    end
    depProductsTable = unique(sortrows(struct2table(depProductsTable), 1));
    dependenciesSparseMat = zeros(totalEdges, 2);
    depFilePaths = keys(entryIndicesMap);
    depFilePaths = depFilePaths(:);
    numEntries = numel(depFilePaths);
    lastEdgeNum = 0;
    for entryNum=1:numEntries
        nodeEdgesOut = entryIndicesMap(depFilePaths{entryNum});
        nodeIdx = nodeEdgesOut{1};
        edgeIdxs = nodeEdgesOut{2};
        numNewEdges = size(edgeIdxs, 1);
        dependenciesSparseMat(lastEdgeNum + (1:numNewEdges), :) = [repmat(nodeIdx, [numNewEdges, 1]), edgeIdxs];
        lastEdgeNum = lastEdgeNum + numNewEdges;
    end
    dependenciesSparseMat = dependenciesSparseMat(1:lastEdgeNum,1:2);
    depSparseMatrix = sparse(dependenciesSparseMat(:,1), dependenciesSparseMat(:,2), 1, numEntries, numEntries);
    depGraph = [];
    if exist('digraph','class')
        depGraph = digraph(depSparseMatrix, depFilePaths);
    elseif nargout > 3
        warning('digraph functionality was unavailable so depGraph is empty');
    end
end