function img = shifter( img, shiftAmt )
%SHIFTER Summary of this function goes here
%   Detailed explanation goes here
    shiftAmt(shiftAmt > 0) = floor(shiftAmt(shiftAmt > 0));
    shiftAmt(shiftAmt < 0) = ceil(shiftAmt(shiftAmt < 0));
    numRows = size(img, 1);
    for rowNum=1:numRows
        img(rowNum,:) = circshift(img(rowNum,:), shiftAmt(rowNum), 2);
    end

end

