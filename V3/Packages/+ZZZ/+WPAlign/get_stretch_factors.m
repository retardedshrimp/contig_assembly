function [ xCoordsMat ] = get_stretch_factors(featureMat, targetRow)
%GET_STRETCH_FACTORS Summary of this function goes here
%   Detailed explanation goes here)
    numRows = size(featureMat, 1);
    numCols = size(featureMat, 2);
    numFeatures = max(featureMat(:));
    xCoordsMat = ones(numRows, numCols);

    if prod(diff(targetRow(~targetRow))) ~= 1
        error('bad input targetFeatureMat');
    end
    currTargetRowGaps = diff(find(diff(targetRow) < 0)) - 1;
    if length(currTargetRowGaps) ~= (numFeatures - 1) || any(currTargetRowGaps < 0)
        error('bad input targetFeatureMat');
    end
    for rowNum=1:numRows
        currRow = featureMat(rowNum, :);
        if prod(diff(currRow(~currRow))) ~= 1
            error('bad input featureMat');
        end
        currRowGaps = diff(find(diff(currRow) < 0)) - 1;
        if (length(currRowGaps) ~= (numFeatures - 1)) || any(currRowGaps < 0)
            error('bad input featureMat');
        end
        
        featureIdxs = find(currRow > 0);
        featureIdxFirst = featureIdxs(1);
        featureIdxLast = featureIdxs(end);
        featureRangeLen = featureIdxLast - featureIdxFirst;
        
        
        targetFeatureIdxs = find(targetRow > 0);
        targetFeatureIdxFirst = targetFeatureIdxs(1);
        targetFeatureIdxLast = targetFeatureIdxs(end);
        targetFeatureRangeLen = targetFeatureIdxLast - targetFeatureIdxFirst;
        
        targetFeatureNormXIdx = (targetFeatureIdxs - targetFeatureIdxFirst)./(targetFeatureRangeLen);
        currXPosNormRelTarget = cell2mat(...
            arrayfun(@(x, y, z) (repmat(x/z, [1, z]).*colon(1, z)) + y,...
                diff(targetFeatureNormXIdx),...
                [0, targetFeatureNormXIdx(1:(end - 1))],...
                diff(featureIdxs),...
                'UniformOutput', false));

        meanStretching = targetFeatureRangeLen/featureRangeLen;
        
        xCoordsRow = [colon(-meanStretching*(featureIdxFirst - 1), meanStretching, -meanStretching), currXPosNormRelTarget, colon(meanStretching, meanStretching, meanStretching*(numCols - featureIdxLast))];
        xCoordsMat(rowNum,:) = xCoordsRow;
    end
end

