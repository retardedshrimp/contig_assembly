function alignedKymo = wp_align(unalignedKymo)
    % WP_ALIGN - Apply Weighted Path Alignment algorithm on input image
    %
    % Inputs: 
    %	unalignedKymo
    %     an unaligned kymograph where values in rows represent intensities
    %      at spacial positions within a timeframe (ordered by row number)
    % Outputs: 
    %	alignedKymo
    %     the aligned kymograph is output in the last step

    wpalignSettings = get_default_wpalign_settings();

    TYPICAL_FEATURE_HALFWIDTH = wpalignSettings.TYPICAL_FEATURE_HALFWIDTH;
    MAX_PATH_DIST = wpalignSettings.MAX_PATH_DIST;
    MIN_COL_RANGE_LEN = wpalignSettings.MIN_COL_RANGE_LEN;
    MAX_MOVEMENT_PX = wpalignSettings.MAX_MOVEMENT_PX;
    MYSTERIOUS_NUMBER_A = 2;
    MYSTERIOUS_NUMBER_B = 2;
    fnGaussianFilter = wpalignSettings.fn.G_FILTER;
    featureFilterSettings = wpalignSettings.featureFilterSettings;
    kymoSize = size(unalignedKymo);
    alignedKymo = unalignedKymo;
    alignedKymoSmooth = fnGaussianFilter(alignedKymo);

    numRows = kymoSize(1);
    numCols = kymoSize(2);

    rangeContraction = [1, -1]*TYPICAL_FEATURE_HALFWIDTH;
    
    alignmentRangeQueue = [1, numCols];
    while not(isempty(alignmentRangeQueue))
        
        rangeRegular = alignmentRangeQueue(1,:);
        alignmentRangeQueue(1,:) = [];
        rangeContracted = rangeRegular + rangeContraction;
        

        rangeContractedColIdxs = rangeContracted(1):rangeContracted(2);
        rangeContractedLen = length(rangeContractedColIdxs);

        if rangeContractedLen < MIN_COL_RANGE_LEN
            continue;
        end

        alignedImgSmoothCropped = alignedKymoSmooth(:,rangeContractedColIdxs);

        mysteriousColRatio = rangeContractedLen / (numRows * MAX_MOVEMENT_PX);
        numColWindows = ceil(mysteriousColRatio) * MYSTERIOUS_NUMBER_A - 1;
        colWindowDelta = ceil(ceil(rangeContractedLen / ceil(mysteriousColRatio))/MYSTERIOUS_NUMBER_B);

        [featurePath, featurePathDist] = find_next_feature(alignedImgSmoothCropped, MAX_MOVEMENT_PX, numColWindows, colWindowDelta, featureFilterSettings);
        featurePath = featurePath + (rangeContracted(1) - 1);
        featureMeanIdx = round(mean(featurePath));
        if (featurePathDist == inf)
            continue;
        end
        if (featurePathDist > MAX_PATH_DIST)
            continue;
        end

        rangeRegularLeftIdx = rangeRegular(1);
        rangeRegularRightIdx = rangeRegular(2);
        rangeRegularColIdxs = rangeRegularLeftIdx:rangeRegularRightIdx;
        rangeRegularLen = length(rangeRegularColIdxs);
        pxHorizStretchFactors = get_px_horiz_stretchfactors(1 + (featurePath - rangeRegularLeftIdx), [numRows, rangeRegularLen]);

        % Align by a feature
        alignedKymoSmooth(:, rangeRegularColIdxs) = stretch_image_horiz(alignedKymoSmooth(:, rangeRegularColIdxs), pxHorizStretchFactors);
        alignedKymo(:, rangeRegularColIdxs) = stretch_image_horiz(alignedKymo(:, rangeRegularColIdxs), pxHorizStretchFactors);

        leftAlignRange = [rangeRegularLeftIdx, featureMeanIdx - TYPICAL_FEATURE_HALFWIDTH];
        rightAlignRange = [featureMeanIdx + TYPICAL_FEATURE_HALFWIDTH, rangeRegularRightIdx];

        alignmentRangeQueue = [...
            alignmentRangeQueue;...
            leftAlignRange;...
            rightAlignRange...
            ]; %#ok<AGROW>
    end
end
