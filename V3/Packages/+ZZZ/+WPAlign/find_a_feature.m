function [featurePath, featurePathDist] = find_a_feature(prefilteredImg, maxMovementPx)
    % FIND_A_FEATURE - detects a single "feature" in the WPAlign
    %	algorithm (see the WPAlign paper for details)
    %
    % Inputs:
    %  prefilteredImg
    %    the image to detect the feature in (pre-filtered to express
    %    features and to have values between -1 and 1)
    %  maxMovementPx
    %    the number of pixels to the left/right that the
    %      feature can jump in each step
    %
    % Outputs: 
    %	featurePath
    %    the position of the best feature
    %	featurePathDist
    %    the path distance (cost) of the best feature
    % 
    % Authors:
    %   Charleston Noble
    %   Saair Quaderi (2015-11): refactoring
    validateattributes(prefilteredImg, {'numeric'}, {'<=', 1, '>=' -1}, 1);
    validateattributes(maxMovementPx, {'numeric'}, {'scalar', 'nonnan', 'real', 'nonnegative'}, 2);
    maxMovementPx = floor(maxMovementPx);
    
    imgSize = size(prefilteredImg);

    imgUp = inf(imgSize);
    imgDown = inf(imgSize);
    
    upMask = prefilteredImg > 0;
    downMask = prefilteredImg < 0;
    
    imgUp(upMask) = 1 - prefilteredImg(upMask);
    imgDown(downMask) = 1 + prefilteredImg(downMask);

    weightImgs = {imgUp; imgDown};
    
    numRows = imgSize(1);
    numCols = imgSize(2);
    numWeightImgs = length(weightImgs);
    featurePathLens = inf(1, numWeightImgs);
    featuresPosByRow = zeros(numRows, numWeightImgs);
    for weightImgNum = 1:numWeightImgs
        weightImg = weightImgs{weightImgNum};

        idxs = zeros(1,maxMovementPx);
        idxs(1) = 1;

        P = maxMovementPx-1;
        M = numRows*(numCols+P) + 2;    

        % Creating I with padding connections
        iPart1 = ones(1,numCols);    
        iPart2 = 2:M-1-numCols-P;    
        x = [2:P/2+1, (2+numCols+P/2):(numCols+P+1)];
        xRep = repmat(x, numRows-1, 1);
        jumpMat = cumsum((numCols + P) * ones(numRows-1, P)) - (numCols+P);
        removeIdxs = xRep + jumpMat - 1;    
        iPart2(removeIdxs(:)) = [];    
        iPart2 = iPart2(cumsum(repmat(idxs,1,length(iPart2))));    
        iPart3 = M-numCols-P/2:M-1-P/2;    
        I = [iPart1, iPart2, iPart3];

        % Creating J with padding connections
        jPart1 = P/2+2:numCols+P/2+1;
        x = (2+P + numCols) : (numCols+P) : (2+(numRows-1)*(numCols+P));
        xRep = repmat(x, numCols, 1);
        jumpMat = cumsum(ones(numCols, length(x))) - 1;
        idx = xRep+jumpMat;
        idx = idx(:)';
        idxRep = repmat(idx, maxMovementPx, 1);
        jumpMat = cumsum(ones(maxMovementPx, length(idxRep))) - 1;
        jPart2 = idxRep + jumpMat;
        jPart2 = jPart2(:)';
        jPart3 = M * ones(1,numCols);
        J = [jPart1, jPart2, jPart3];

        paddedImg = zeros(size(weightImg,1), size(weightImg,2)+P);
        paddedImg(:,P/2+1:end-P/2) = weightImg;
        paddedImg(:,[1:P/2, end-P/2+1:end]) = -1;
        paddedImg = paddedImg';
        paddedImg = paddedImg(:);
        S = [paddedImg([jPart1, jPart2] - 1); ones(numCols, 1)]';

        removeIdxs = find(S == -1);
        I(removeIdxs) = [];
        J(removeIdxs) = [];
        S(removeIdxs) = [];

        %TODO: use graphallshortestpaths and filter out things which are too close to speed up the process
        [currFeaturePathLen, currFeaturePath, ~] = graphshortestpath(sparse(I, J, S, M, M), 1, M, 'method', 'acyclic');

        featurePathLens(weightImgNum) = currFeaturePathLen;
        featuresPosByRow(:, weightImgNum) = (mod(currFeaturePath(2:numRows+1)-1, numCols+P) - P/2)';
    end

    [featurePathDist, bestFeatureIdx] = min(featurePathLens);
    featurePath = featuresPosByRow(:, bestFeatureIdx);
end