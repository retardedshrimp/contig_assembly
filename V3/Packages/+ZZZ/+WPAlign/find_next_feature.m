function [featurePath, featurePathDist] = find_next_feature(inputImg, maxMovementPx, numColWindows, colWindowDelta, featureFilterSettings)
    numRows = size(inputImg, 1);
    numCols = size(inputImg, 2);
    
    if numColWindows < 2
        filteredImg = feature_filter(inputImg, featureFilterSettings);
        [featurePath, featurePathDist] = find_a_feature(filteredImg, maxMovementPx);
    else
        featuresPosByRow = zeros(numRows, numColWindows);
        featurePathLens = zeros(1,numColWindows);

        colWindowStarts = (0:(numColWindows - 1)).*colWindowDelta + 1;
         %SQ Question: does the fact that the last window may be less wide
         % bias the path distance to be expeced for it?
        colWindowEnds = min((1:numColWindows).*colWindowDelta + 1, numCols);
        
        for colWindowNum = 1:numColWindows
            colWindowStart = colWindowStarts(colWindowNum);
            colWindowEnd = colWindowEnds(colWindowNum);
            windowColIdxRange = colWindowStart:colWindowEnd;
            filteredImg = feature_filter(inputImg(:,windowColIdxRange), featureFilterSettings);
            [featurePath, featurePathDist] = find_a_feature(filteredImg, maxMovementPx);
            featurePath = featurePath + colWindowStart;
            featuresPosByRow(:,colWindowNum) = featurePath;
            featurePathLens(colWindowNum) = featurePathDist;     
        end
        [featurePathDist, bestFeatureIdx] = min(featurePathLens);
        featurePath = featuresPosByRow(:, bestFeatureIdx);
    end
end