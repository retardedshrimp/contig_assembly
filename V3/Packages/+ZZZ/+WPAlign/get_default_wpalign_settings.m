function wpalignSettings = get_default_wpalign_settings()

    gFilterSettings.filterSize = [10, 10];
    gFilterSettings.gaussianKernelSigma = 2;
    gFilterSettings.useEdgeExtension = true;

    wpalignSettings.fn.G_FILTER = gen_filter_fn('gaussian',...
        gFilterSettings.filterSize,...
        gFilterSettings.gaussianKernelSigma,...
        gFilterSettings.useEdgeExtension);
    wpalignSettings.G_FILTER_SETTINGS = gFilterSettings;
    wpalignSettings.TYPICAL_FEATURE_HALFWIDTH = 5;
    wpalignSettings.MAX_PATH_DIST = 10000;
    wpalignSettings.MIN_COL_RANGE_LEN = 26;
    wpalignSettings.MAX_MOVEMENT_PX = 3;

    logFilterSettings.filterSize = [2, 6];
    logFilterSettings.gaussianKernelSigma = 2;
    logFilterSettings.useEdgeExtension = true;
    featureFilterSettings.LoG_filter = logFilterSettings;
    featureFilterSettings.fn.LoG_FILTER = gen_filter_fn('log',...
        logFilterSettings.filterSize,...
        logFilterSettings.gaussianKernelSigma,...
        logFilterSettings.useEdgeExtension);
    wpalignSettings.featureFilterSettings = featureFilterSettings;
end