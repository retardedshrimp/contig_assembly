function [ filteredImg ] = feature_filter( inputImg, featureFilterSettings)
% FEATURE_FILTER
    filterFn_LoG = featureFilterSettings.fn.LoG_FILTER;
    filteredImg = filterFn_LoG(inputImg);
    posValsMask = filteredImg > 0;
    negValsMask = filteredImg < 0;
    posVals = filteredImg(posValsMask);
    negVals = filteredImg(negValsMask);
    if not(isempty(posVals))
        filteredImg(posValsMask) = posVals ./ max(posVals);
    end
    if not(isempty(negVals))
        filteredImg(negValsMask) = negVals ./ abs(min(negVals));
    end
end

