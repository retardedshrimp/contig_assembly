function [shiftedKymo, shiftedKymoFeatures, featurePathMat, offsetClusters, mainOffsetCluster, Z, alignmentFeatureNum, featurePathOffsets] = process_kymo( kymoImg, maxNumFeatures )
%FIND_OFFSET_CLUSTERS Summary of this function goes here
%   Detailed explanation goes here
    
    if nargin < 2
        maxNumFeatures = inf;
    end
    
    [featurePathMat, featurePathMeans, featurePathOffsets, featurePathDists] = feature_finder(kymoImg, maxNumFeatures);
    
    numFeatures = size(featurePathOffsets, 1);
    dissimilarityMat = inf(numFeatures);
    for featureNumA=1:numFeatures
        featureA = featurePathOffsets(featureNumA, :);
        for featureNumB=1:numFeatures
            featureB = featurePathOffsets(featureNumB,:);
            dissimilarityMat(featureNumA, featureNumB) = norm(featureA - featureB);
        end
    end
    dissimilarityVarianceFactor = 1.0;
    dissimilarityCutoff = sqrt(size(featurePathOffsets, 2) * median(var(dissimilarityVarianceFactor*featurePathOffsets')));
    Z = linkage(dissimilarityMat,'complete');
    offsetClusters = cluster(Z,'cutoff',dissimilarityCutoff,'criterion','distance');
    mainOffsetCluster = mode(offsetClusters);
    mainClusterFeatureNums = find(offsetClusters == mainOffsetCluster);
    mainFeatureOffsets = featurePathOffsets(mainClusterFeatureNums,:);
    shiftAmt = median(mainFeatureOffsets, 1);
%     bestCorr = -inf;
%     alignmentFeatureNum = 1;
%     numFeatureCandidates = length(mainClusterFeatureNums);
%     for featureCandidateNum=1:numFeatureCandidates
%         featureNum = mainClusterFeatureNums(featureCandidateNum);
%         feature = featurePathOffsets(featureNum, :);
%         corr = xcorr(feature(:), shiftAmt(:), 0, 'coeff');
%         if corr > bestCorr
%             bestCorr = corr;
%             alignmentFeatureNum = featureNum;
%         end
%     end
%     shiftAmt = featurePathOffsets(alignmentFeatureNum, :);
    shiftAmt = -shiftAmt;
    padding = [0, max(abs(shiftAmt))];
    shiftedKymo = shifter(padarray(kymoImg, padding, NaN), shiftAmt);
    shiftedKymoFeatures = featurePathMat;
    shiftedKymoFeatures(shiftedKymoFeatures == 0) = -1;
    shiftedKymoFeatures = shifter(padarray(shiftedKymoFeatures, padding, 0), shiftAmt) + 1;
end

