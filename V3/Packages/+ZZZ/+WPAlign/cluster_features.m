function [offsetClusters, featurePathMatClustered] = cluster_features( featurePathMat, featurePathOffsets, dissimilarityVarianceFactor)
%CLUSTER_FEATURES Summary of this function goes here
%   Detailed explanation goes here
    numFeatures = size(featurePathOffsets, 1);
    featureLen = size(featurePathOffsets, 2);
    dissimilarityMat = inf(numFeatures);
    for featureNumA=1:numFeatures
        featureA = featurePathOffsets(featureNumA, :);
        for featureNumB=1:numFeatures
            featureB = featurePathOffsets(featureNumB,:);
            dissimilarityMat(featureNumA, featureNumB) = norm(featureA - featureB);
        end
    end
    Z = linkage(dissimilarityMat, 'complete');
    dissimilarityCutoff = sqrt(featureLen * median(var(dissimilarityVarianceFactor * featurePathOffsets')));
    offsetClusters = cluster(Z, 'cutoff', dissimilarityCutoff, 'criterion', 'distance');
    featurePathMatClustered = featurePathMat;
    for featureNum=1:numFeatures
        featurePathMatClustered(featurePathMatClustered == featureNum) = offsetClusters(featureNum);
    end
    figure, imshow(label2rgb(featurePathMat))
    figure, imshow(label2rgb(featurePathMatClustered))
end

