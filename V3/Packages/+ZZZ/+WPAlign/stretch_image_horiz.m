function stretchedImg = stretch_image_horiz(inputImg, pxHorizStretchFactors)
    % STRETCH_IMAGE_HORIZ - applies local stretching on the rows of an
    % image
    % 
    % spline interpolation is used to created the new image after values at
    %  pixels are repositioned horizontally for the spline based on the
    %  horizontal stretch factors provided for each pixels in each row 
    %
    % Inputs:
    %   inputImg
    %    the image to stretch
    %   pxHorizStretchFactors
    %    how much each pixel should be stretched horizontally
    %
    % Outputs:
    %   stretchedImg
    %    the stretched image
    % 
    % Authors:
    %   Saair Quaderi (2015-11): refactored out of code by
    %    Charleston Noble (OptMap.KymoAlignment.stretch_image_old)

    imgSize = size(inputImg);
    numRows = imgSize(1);
    numCols = imgSize(2);
    queryPos = 1:numCols;
    stretchedImg = zeros(size(inputImg));
    for rowNum = 1:numRows
        stretchedImg(rowNum,:) = interp1(cumsum(pxHorizStretchFactors(rowNum,:)), inputImg(rowNum,:), queryPos, 'spline');
    end
end
