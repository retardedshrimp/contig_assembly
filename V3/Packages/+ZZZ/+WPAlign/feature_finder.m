function [featurePathMat, featurePathMeans, featurePathOffsets, featurePathDists] = feature_finder(unalignedKymo, numFeatures)
    % FEATURE_FINDER - find features in an image
    %
    % Inputs: 
    %	unalignedKymo
    %     an unaligned kymograph where values in rows represent intensities
    %      at spacial positions within a timeframe (ordered by row number)
    %   numFeatures
    %      number of features we wish to find
    % Outputs: 
    %	featurePaths
    %     the feature paths

    wpalignSettings = get_default_wpalign_settings();

    TYPICAL_FEATURE_HALFWIDTH = wpalignSettings.TYPICAL_FEATURE_HALFWIDTH;
    MAX_MOVEMENT_PX = wpalignSettings.MAX_MOVEMENT_PX;
    fnGaussianFilter = wpalignSettings.fn.G_FILTER;
    featureFilterSettings = wpalignSettings.featureFilterSettings;
    kymoSize = size(unalignedKymo);
    alignedKymo = unalignedKymo;
    alignedKymoSmooth = fnGaussianFilter(alignedKymo);

    numRows = kymoSize(1);
    numCols = kymoSize(2);

    rangeContraction = [1, -1]*TYPICAL_FEATURE_HALFWIDTH;

    rangeRegular = [1, numCols];
    rangeContracted = rangeRegular + rangeContraction;


    rangeContractedColIdxs = rangeContracted(1):rangeContracted(2);

    alignedImgSmoothCropped = alignedKymoSmooth(:, rangeContractedColIdxs);

    filteredImg = feature_filter(alignedImgSmoothCropped, featureFilterSettings);
    foundFeatures = find_k_features(filteredImg, MAX_MOVEMENT_PX, TYPICAL_FEATURE_HALFWIDTH, numFeatures);
    foundFeatures(:,1) = cellfun(@(ffPath) ffPath  + (rangeContracted(1) - 1), foundFeatures(:, 1), 'UniformOutput', false);
    featurePathDists = [foundFeatures{:, 2}];
    numFoundFeatures = size(foundFeatures,1);
    featurePathMat = zeros(kymoSize);
    featurePathMeans = nan(numFoundFeatures, 1);
    featurePathOffsets = zeros(numFoundFeatures, numRows);
    for featuresFoundNum=1:numFoundFeatures
        featurePath = foundFeatures{featuresFoundNum,1};
        featurePathMean = ceil(mean(featurePath));
        featurePathMat(sub2ind(kymoSize, (1:numRows)', featurePath)) = featuresFoundNum;
        featurePathMeans(featuresFoundNum) = featurePathMean;
        featurePathOffsets(featuresFoundNum, :) = featurePath - featurePathMeans(featuresFoundNum);
    end
end
