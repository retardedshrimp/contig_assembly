function pxHorizStretchFactors = get_px_horiz_stretchfactors(featureColIdxLeft, imgSize)
    numRows = imgSize(1);
    numCols = imgSize(2);
    colsToLeft = mean(featureColIdxLeft);
    colsToRight = numCols - colsToLeft;
    pxHorizStretchFactors = ones(imgSize);
    for rowNum = 1:numRows
        rowLeftColCount = featureColIdxLeft(rowNum);
        rowRightColCount = numCols - rowLeftColCount;

        leftStretchFactor = colsToLeft / rowLeftColCount;
        rightStretchFactor = colsToRight / rowRightColCount;

        pxHorizStretchFactors(rowNum,:) = [...
            repmat(leftStretchFactor, [1, rowLeftColCount]),...
            repmat(rightStretchFactor, [1, rowRightColCount])];
    end
end