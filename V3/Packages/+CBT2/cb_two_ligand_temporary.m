function [ barcodeBP ] = cb_two_ligand_temporary(ntSeq, settings)
%CB_TWO_LIGAND method

import CBT.get_bitsmart_ACGT; %? not needed

% Author: Albertas Dvirnas

% convert leters to digits
ntIntSeq = nt2int(ntSeq);
nn = size(ntIntSeq,2);

% This will be our output vector
barcodeBP = zeros(1,nn);

%[probBinding, ~, ntBitsmart_theorySeq_uint8] = calculate_competitive_binding_probs(paddedNtSeq, competitors, skipProbCalcCompetitors);

%ntBitsmart_theorySeq_uint8 = get_bitsmart_ACGT(ntIntSeq);

% First, based on sequence ntIntSeq, we want to calculate which binding are
% possible for each ligand. YOYO1 can bind always, while Netrospin is more
% probable to bind if there are G's or C's

ntNetrospinSeq = zeros(1,size(ntIntSeq,2));

was = 0;
for i=1:size(ntIntSeq,2)
    if ntIntSeq(i) == 2 || ntIntSeq(i) == 3 || was > 0
        ntNetrospinSeq(i) = 1;
        if was == 3
            was = 0;
        else
            was = was + 1;
        end
    else
       ntNetrospinSeq(i) = 2;
    end       
end

% transfer matrix
transferMat =  [       1 0 0 0 1 0 0 0 1;
                       1 0 0 0 1 0 0 0 1;
                       0 1 0 0 0 0 0 0 0;
                       0 0 1 0 0 0 0 0 0;
                       0 0 0 settings.NETROPSINconc 0 0 0 0 0;
                       1 0 0 0 1 0 0 0 1;
                       0 0 0 0 0 1 0 0 0;
                       0 0 0 0 0 0 1 0 0;
                       0 0 0 0 0 0 0 settings.YOYO1conc*settings.yoyo1BindingConstant 0];


                        
yoyoConst = settings.YOYO1conc*settings.yoyo1BindingConstant;               
                   

%full(transferMat)

% two choices for Netropsin, based on if there are G and C letters in a
% given quadromer or not
choice = [transferMat(5,4)*settings.netropsinBindingConstant(1) transferMat(5,4)*settings.netropsinBindingConstant(2)];


leftVec = zeros(nn+1,9);
rightVec = zeros(9,nn+1);
maxEltLeft = zeros(1, nn);
maxEltRight = zeros(1, nn);

leftVec(1,:) = [1 0 0 0 1 0 0 0 1];
leftVec(1,:) = leftVec(1,:)./norm(leftVec(1,:));
%leftVec
rightVec(:,nn+1) = transpose([1 0 0 0 0 0 0 0 0]);
rightVec(:,nn+1)  = rightVec(:,nn+1) ./norm( rightVec(:,nn+1) );
%rightVec
for i=1:nn
     transferMat(5,4)= choice(ntNetrospinSeq(i));
     
     leftVecPrev = leftVec(i,:);
     leftVecNext = [leftVecPrev(1)+leftVecPrev(2)+leftVecPrev(6),  leftVecPrev(3),  leftVecPrev(4),leftVecPrev(5)*choice(ntNetrospinSeq(i)),leftVec(1)+leftVecPrev(2)+leftVecPrev(6),leftVecPrev(7),leftVecPrev(8),  leftVecPrev(9)*yoyoConst,leftVecPrev(1)+leftVecPrev(2)+leftVecPrev(6)];
     
     leftVec(i+1,:) = leftVec(i,:)*transferMat;
     transferMat
     leftVecNext
     leftVec(i+1,:)
     transferMat(5,4)= choice(ntNetrospinSeq(nn-i+1)) ;
     %rightVec(:,nn+1-i) = transferMat*rightVec(:,nn-i+2);
     
     rightVecPrev = rightVec(:,nn-i+2);
     rightVecNext = [rightVecPrev(1)+rightVecPrev(5)+rightVecPrev(9);
                            rightVecPrev(1)+rightVecPrev(5)+rightVecPrev(9);
                            rightVecPrev(2);
                            rightVecPrev(3);
                            rightVecPrev(4)*choice(ntNetrospinSeq(nn-i+1));
                            rightVecPrev(1)+rightVecPrev(5)+rightVecPrev(9);
                            rightVecPrev(6);
                            rightVecPrev(7);
                            rightVecPrev(8)*yoyoConst];

     maxEltLeft(i) = norm(leftVecNext);
     
     maxEltRight(nn+1-i) =norm(rightVecNext);

     leftVec(i+1,:) = leftVecNext./maxEltLeft(i);
     rightVec(:,nn+1-i) = rightVecNext./maxEltRight(nn+1-i);
end

maxVecDiv =  zeros(1,nn);
maxVecDiv(1) = maxEltLeft(1)/maxEltRight(1);

for i=2:nn
    maxVecDiv(i) = maxVecDiv(i-1)*maxEltLeft(i)/maxEltRight(i);
end


oMat = diag([0,0,0,0,0,1,1,1,1]);


denominator = leftVec(1,:)*rightVec(:,1);


barcodeBP(1) = leftVec(1,:)*oMat*rightVec(:,1)/denominator;

for i=2:nn
    barcodeBP(i) = leftVec(i,:)*oMat*rightVec(:,i)*maxVecDiv(i-1)/denominator;
end
%barcodeBP
%figure, plot(barcodeBP)

 
