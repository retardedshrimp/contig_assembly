classdef TabbedScreen < handle
    % TABBEDSCREEN 
    
    properties
        TabGroupHandle = gobjects(0, 1);
        TabHandles = gobjects(0, 1);
    end
    
    methods
        function [ts] = TabbedScreen(hPanel)
            ts.TabGroupHandle = uitabgroup(hPanel);
        end
        function [hTab, tabNum] = create_tab(ts, tabTitle, deleteFcn)
            if nargin < 3
                deleteFcn = [];
            end
            import FancyGUI.FancyTabs.add_a_new_fancy_tab;
            if not(isvalid(ts.TabGroupHandle))
                warning('Tab group was closed, creating new tab figure');
                hFig = figure('Name', tabTitle);
                hPanel = uipanel('Parent', hFig);
                import FancyGUI.FancyTabs.TabbedScreen;
                ts = TabbedScreen(hPanel);
            end
            
            hTab = add_a_new_fancy_tab(ts.TabGroupHandle, tabTitle);
            if not(isempty(deleteFcn)) && isa(deleteFcn, 'function_handle')
                set(hTab, 'DeleteFcn', deleteFcn)
            end
            tabNum = size(ts.TabHandles, 1) + 1;
            ts.TabHandles(tabNum) = hTab;
            
        end
        function [hTabs, tabNums] = create_tabs(ts, tabTitles, deleteFcns)
            if nargin < 3
                deleteFcns = [];
            end
            if isempty(deleteFcns) || isscalar(deleteFcns)
                deleteFcns = repmat({deleteFcns}, size(ts));
            end
            [hTabs, tabNums] = cellfun(...
                @(tabTitle, deleteFcn) ...
                    ts.create_tab(tabTitle, deleteFcn), ...
                    tabTitles(:), deleteFcns(:));
        end
        function [hTab] = get_tab_handle(ts, tabNum)
            hTab = ts.TabHandles(tabNum);
        end
        function select_tab(ts, tabID)
            if isa(tabID, 'matlab.ui.container.Tab')
                hTab = tabID;
                hTabgroup = get(hTab, 'Parent');
            elseif isnumeric(tabID)
                hTabgroup = ts.TabGroupHandle;
                tabNum = tabID;
                hTab = ts.get_tab_handle(tabNum);
            end
            if isempty(hTabgroup) || not(isvalid(hTabgroup))
                warning('Tabgroup could not be found');
            end
            set(hTabgroup, 'SelectedTab', hTab);
        end
    end
    methods (Static)
        function [ts, hFig] = make_tabbed_screen_in_new_fig(figTitle)
            hFig = figure(...
                'Name', figTitle, ...
                'Units', 'normalized', ...
                'OuterPosition', [0 0.05 1 0.95], ...
                'MenuBar', 'none', ...
                'ToolBar', 'none');
            
            hPanel = uipanel('Parent', hFig);
            
            import FancyGUI.FancyTabs.TabbedScreen;
            ts = TabbedScreen(hPanel);
        end
    end
end