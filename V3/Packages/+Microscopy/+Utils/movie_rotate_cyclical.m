function [rotM] = movie_rotate_cyclical(M, rotationAngle, rotationSamplingMethod)
    if nargin < 3
        rotationSamplingMethod = 'bilinear';
    end
    
    szI = size(M);
    rotM = repmat(M, [2 2]);
    for dimIdx = 1:2
        rotM = circshift(rotM, floor(szI(dimIdx)/2), dimIdx);
    end
    rotM = imrotate(rotM, rotationAngle, rotationSamplingMethod, 'crop');
    for dimIdx = 1:length(szI)
        rotM = circshift(rotM, -floor(szI(dimIdx)/2), dimIdx);
    end
    c = repmat({':'}, [1, length(szI)]);
    c{1} = 1:szI(1);
    c{2} = 1:szI(2);
    rotM = rotM(c{:});
end