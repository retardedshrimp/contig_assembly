function [movieAmplified] = amplify_molecules(movieNrm, amplificationKernel)
    if any(mod(size(amplificationKernel), 2) == 0)
        error('Convolution array for amplification kernel must have an odd-length in each dimension');
    end
    for dimNum=1:3
        if not(isequal(amplificationKernel, flip(amplificationKernel, dimNum)))
            error('Convolution array for amplification kernel must be symmetrical in each dimension');
        end
    end

    paddingLens = ((size(amplificationKernel) - 1)/2);
    paddedMovie = padarray(movieNrm, paddingLens + 1, 'symmetric', 'both'); % + 1 because we will be removing the symmetrical repetition of the border
    paddedMovie([1 + paddingLens(1), end - paddingLens(1)],:,:) = []; % remove repetition of top/bottom row borders from row padding
    paddedMovie(:,[1 + paddingLens(2), end - paddingLens(2)],:) = []; % remove repetition of left/right col borders from col padding
    paddedMovie(:,:,[1 + paddingLens(3), end - paddingLens(3)]) = []; % remove repetition of first/last frame borders from frame padding
    paddedMovieAmplified = convn(paddedMovie, amplificationKernel, 'same'); % convolve against kernel
    movieAmplified = paddedMovieAmplified(...
        (1 + paddingLens(1)):(end - paddingLens(1)),...
        (1 + paddingLens(2)):(end - paddingLens(2)),...
        (1 + paddingLens(3)):(end - paddingLens(3))); % remove all padding from result
    
    % normalize to min of 0 and max of 1
    movieAmplified = movieAmplified - min(movieAmplified(:));
    movieAmplified = movieAmplified./max(movieAmplified(:));
    
    % warp values to increase contrast
    movieAmplified = movieAmplified.^2;
end