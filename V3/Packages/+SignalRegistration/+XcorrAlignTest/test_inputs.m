function [] = test_inputs(seqA, seqB, bitmaskA, bitmaskB, circularA, circularB, allowExtraCropping, topN, targetAIndices, targetBIndices, targetBestFlipped, targetBestCircShift)
    % import SignalRegistration.XcorrAlign.find_best_alignment_params;
    import SignalRegistration.XcorrAlign.get_top_scores;
    import SignalRegistration.XcorrAlign.compute_alignment_params
    import SignalRegistration.XcorrAlignTest.plot_computed_values;
    import SignalRegistration.XcorrAlignTest.plot_best_alignments;

    lenA = length(seqA);
    lenB = length(seqB);
    
    % [aIndicesAtBestN, bIndicesAtBestN,...
    %     scoresAtBestN, xcorrAtBestN, coverageLenAtBestN,...
    %     scores, xcorrs, coverageLens,...
    %     firstOffset, maxPossibleCoverageLen] = find_best_alignment_params(...
    %         seqA, seqB, bitmaskA, bitmaskB, circularA, circularB, allowExtraCropping, topN);

    [scoresAtBestN, xcorrAtBestN, coverageLenAtBestN, flipAtBestN, circShiftAtBestN, offsetAtBestN, scores, xcorrs, coverageLens, firstOffset, maxPossibleCoverageLen] = get_top_scores(seqA, seqB, bitmaskA, bitmaskB, circularA, circularB, allowExtraCropping, topN);
    [aIndicesAtBestN, bIndicesAtBestN] = compute_alignment_params(lenA, lenB, circularA, circularB, flipAtBestN, circShiftAtBestN, offsetAtBestN);

    plot_computed_values(scores, xcorrs, coverageLens, maxPossibleCoverageLen);
    plot_best_alignments(seqA, seqB, {targetAIndices}, {targetBIndices});
    plot_best_alignments(seqA, seqB, aIndicesAtBestN, bIndicesAtBestN);

    % % Ugly piece of code that helps with debugging
    % myVarList=who;
    % for indVar = 1:length(myVarList)
    %     assignin('base',myVarList{indVar},eval(myVarList{indVar}))
    % end
end