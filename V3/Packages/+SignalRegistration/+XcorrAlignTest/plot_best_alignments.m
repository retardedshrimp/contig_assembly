function [] = plot_best_alignments(seqA, seqB, aIndicesAtBestN, bIndicesAtBestN)
    hFig = figure();
    numPlots = length(aIndicesAtBestN);
    for plotNum=1:numPlots
        subplot(round(sqrt(numPlots)), ceil(numPlots/round(sqrt(numPlots))), plotNum);
        indicesA = aIndicesAtBestN{plotNum};
        indicesB = bIndicesAtBestN{plotNum};
        title(['Sequence A (red) vs Sequence B (blue) - Alignment #', num2str(plotNum)]);
        x = 1:length(indicesA);
        plot(x, seqA(indicesA), 'r-');
        hold on;
        plot(x, seqB(indicesB), 'b-');
    end
end