function [refSeqsFailedImport] = import_fastas(plasmidFastaDirpath, maxNumImports)
    import FancyIO.try_write_file;
    import NCBI.get_latest_complete_bacterial_plasmid_version_info;
    import NCBI.get_complete_bacterial_plasmid_fastas;
    import NCBI.try_prompt_plasmid_fastas_dirpath;
    
    if (nargin < 1) || isempty(plasmidFastaDirpath)
        [aborted, plasmidFastaDirpath] = try_prompt_plasmid_fastas_dirpath();
    else
        aborted = false;
    end
    if aborted
        return;
    end
    if nargin < 2
        maxNumImports = inf;
    end
    
    dirFastaFiles = feval(@(fs) {fs.name}, dir([plasmidFastaDirpath, filesep(), '*.fasta']))';
    [~, refSeqsImported] = cellfun(@fileparts, dirFastaFiles, 'UniformOutput', false);

    [refSeqs, refSeqsAccessionNumber, refSeqsAccessionVersionNumber, plasmidsDataTable, originalColHeaders] = get_latest_complete_bacterial_plasmid_version_info();

    refSeqsToImport = setdiff(refSeqs, refSeqsImported);

    refSeqsToImport = refSeqsToImport(1:min(maxNumImports, end));

    refSeqsFailedImport = cell(0, 1);
    if plasmidFastaDirpath ~= 0
        fn_on_read_success = @(refSeq, fastaStr) try_write_file(fullfile(plasmidFastaDirpath, [refSeq, '.fasta']), fastaStr);
        [successStatuses] = get_complete_bacterial_plasmid_fastas(refSeqsToImport, fn_on_read_success);
        refSeqsFailedImport = refSeqsToImport(not(successStatuses));
    end
end