function [posEndCounts, posEndCountsPreCut] = count_consensus_cuts(clusterConsensusData, tsvOutputFilepath, hAxis)
    if nargin < 1
        [consensusFilename, consensusDirpath] = uigetfile('*.mat', 'Load Consensus Cluster Data');
        if isequal(consensusDirpath, 0)
            posEndCounts = [];
            posEndCountsPreCut = [];
            return;
        end
        consensusFilepath = fullfile(consensusDirpath, consensusFilename);
        clusterConsensusData = feval(@(tmp) tmp.clusterConsensusData, load(consensusFilepath, 'clusterConsensusData'));
    end
    clusterResultStruct = clusterConsensusData.clusterResultStruct;
    barcodeLens = cellfun(@length, clusterResultStruct.barcodes);
    barcodeFlipTFs = clusterResultStruct.flipTFs;
    barcodeCircShifts = clusterResultStruct.circShifts;
    alignedPosVectCells = arrayfun(...
        @(barcodeLen, flipTF, circShift) ...
            flip(circshift(linspace(0, 1, barcodeLen), circShift, 2), flipTF + 1),...
        barcodeLens,...
        barcodeFlipTFs,...
        barcodeCircShifts,...
        'UniformOutput', false);
    alignedPosMat = cell2mat(alignedPosVectCells);
    endsMat = (alignedPosMat == 0) | (alignedPosMat == 1);
    posEndCounts = sum(endsMat)';
    posEndCountsPreCut = sum(circshift(endsMat, -1, 2) & endsMat)';
    
    
    if nargin < 2
        defaultDirpath = pwd();
        defaultTsvOutputFilename = 'consensus_ends.tsv';
        defaultTsvOutputFilepath = fullfile(defaultDirpath, defaultTsvOutputFilename);
        [tsvOutputFilename, dirpath] = uiputfile({'*.tsv'}, 'Save consensus end counts as', defaultTsvOutputFilepath);
        if isequal(dirpath, 0)
            tsvOutputFilepath = '';
        else
            tsvOutputFilepath = fullfile(dirpath, tsvOutputFilename);
        end
    end
    if not(isempty(tsvOutputFilepath))
        dlmwrite(tsvOutputFilepath, posEndCounts, 'delimiter', '\t');
    end
    
    
    if nargin < 3
        hFig = figure('Name', 'Consensus Cut Counts');
        hPanel = uipanel('Parent', hFig);
        hAxis = axes('Parent', hPanel);
    end
    if not(isempty(hAxis))
        bar(hAxis, posEndCounts);
    end
end