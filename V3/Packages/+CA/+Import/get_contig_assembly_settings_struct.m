function [settingsStructCAT] = get_contig_assembly_settings_struct()

    settingsStructCAT.minValidShortestSeq = 4 * barcodeGenSettings.psfSigmaWidth_nm / barcodeGenSettings.meanBpExt_nm;

    settingsStructCAT.qMax = round(5*10^5);
    settingsStructCAT.overlapLim = 2;
    settingsStructCAT.allowOverlap = (settingsStructCAT.overlapLim > 0);
    settingsStructCAT.forcePlace = false;
    settingsStructCAT.pThreshold = 0.501;
    settingsStructCAT.data = 'Unknown';
    settingsStructCAT.numRandBarcodes = 1000; %number of PR barcodes
    settingsStructCAT.flipAllowed = true;
    settingsStructCAT.shouldFormatNamesTF = true;


    % isDefault = true;
    % if not(isDefault)
    %     prompts = {...
    %         'Name of sample:', ...
    %         'Allowed overlap (px):', ...
    %         'Force place contigs:', ...
    %         'Shortest seq (bp):', ...
    %         'Format names:' ...
    %         };
    %     defaultVals = { ...
    %         'Unknown', ...
    %         num2str(2), ...
    %         'No', ...
    %         num2str(7000), ...
    %         'Yes' ...
    %         };
    %     dlg_title = 'Input settings';
    %     num_lines = 1;
    %     answer = inputdlg(prompts, dlg_title, num_lines, defaultVals);
    % 
    %     %---Input settings dialog---
    %     settingsStructCAT.data = answer{1};
    %     settingsStructCAT.overlapLim = str2double(answer{2});
    %     settingsStructCAT.forcePlace = not(strcmpi(answer{3}(1), 'N'));
    %     settingsStructCAT.minValidShortestSeq = round(max(minValidShortestSeq, str2double(answer{4})));
    %     settingsStructCAT.shouldFormatNamesTF = strcmpi(answer{5}(1),'Y');
    % end

end
