function [richnessAT] = get_AT_richness(ntBitsmart_theorySeq_uint8)
    % GET_AT_RICHNESS = Calculate how rich a sequence is in A & T basepairs
    %
    % Inputs:
    %   ntBitsmart_theorySeq_uint8
    %     DNA sequence with custom bitsmart encoding
    %     (You can use CBT.get_bitsmart_ACGT to achieve this from regular
    %     DNA encoding)
    %
    % Outputs:
    %   richnessAT
    %    double from 0 to 1 representing AT-richness
    %    the value is the portion of sequence
    %    known to be (A, T, or either A or T) over the portion of
    %    basepairs known to be either (A, T, or either A or T) OR
    %    alternatively (C, G, or either C or G) -- i.e. ignores
    %    basepairs that cannot be classified as AT (W) or CG (S)
    %
    % Authors:
    %   Saair Quaderi

    import CBT.get_bitsmart_ACGT;

    ntsOfInterest = {'A'; 'C'; 'G'; 'T'; 'W'; 'S'};
    numNtsOfInterest = length(ntsOfInterest);
    for ntsOfInterestNum = 1:numNtsOfInterest
        ntOfInterest = ntsOfInterest{ntsOfInterestNum};
        ntBitsmart_uint8.(ntOfInterest) = get_bitsmart_ACGT(ntOfInterest);
    end
    countATW = sum(...
        (ntBitsmart_uint8.A == ntBitsmart_theorySeq_uint8) +...
        (ntBitsmart_uint8.T == ntBitsmart_theorySeq_uint8) +...
        (ntBitsmart_uint8.W == ntBitsmart_theorySeq_uint8));
    countCGS = sum(...
        (ntBitsmart_uint8.C == ntBitsmart_theorySeq_uint8) +...
        (ntBitsmart_uint8.G == ntBitsmart_theorySeq_uint8) +...
        (ntBitsmart_uint8.S == ntBitsmart_theorySeq_uint8));
    richnessAT = countATW / (countATW + countCGS);
end