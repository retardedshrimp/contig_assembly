function [] = add_ete_menu(hMenuParent, tsETE)
    % Pre-generate FFT for extreme distrubution parameters
    hMenuFFTGenerate = uimenu(hMenuParent, 'Label','Pregenerate FFT Zero Models');
    import CBT.RandBarcodeGen.PhaseRandomization.pregen_zero_model_fft_from_prompted_nt_seqs;
    uimenu(hMenuFFTGenerate,'Label', 'From sequences', 'Callback', @(~, ~) pregen_zero_model_fft_from_prompted_nt_seqs());
    import CBT.RandBarcodeGen.PhaseRandomization.pregen_zero_model_fft_from_prompted_consensuses;
    uimenu(hMenuFFTGenerate,'Label', 'From prompted consensus barcodes', 'Callback', @(~, ~) pregen_zero_model_fft_from_prompted_consensuses());

    % Experiment to experiment comparison
    hMenuExpToExp = uimenu(hMenuParent, 'Label','Comparison');
    import CBT.ExpComparison.UI.run_consensus_vs_consensus_similarity_analysis;
    uimenu(hMenuExpToExp,'Label', 'Analyze Consensus vs Concensus Similarity', 'Callback', @(~, ~) run_consensus_vs_consensus_similarity_analysis(tsETE));
end