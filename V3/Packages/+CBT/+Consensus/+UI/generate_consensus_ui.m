function [] = generate_consensus_ui(ts, consensusStruct)
    import CBT.Consensus.UI.make_save_cluster_consensus_ui;
    make_save_cluster_consensus_ui(ts, consensusStruct);

    [hTabConsensusClusters, tabNumConsensusClusters] = ts.create_tab('Consensus Clusters');
    ts.select_tab(tabNumConsensusClusters);
    hPanelConsensusClusters = uipanel(hTabConsensusClusters);
    import CBT.Consensus.UI.plot_consensus_clusters;
    plot_consensus_clusters(consensusStruct, hPanelConsensusClusters);

    [hTabPairwiseConsensusHistory, tabNumPairwiseConsensusHistory] = ts.create_tab('Pairwise Consensus History');
    ts.select_tab(tabNumPairwiseConsensusHistory);
    hPanelPairwiseConsensusHistory = uipanel(hTabPairwiseConsensusHistory);
    import CBT.Consensus.UI.plot_pairwise_consensus_history;
    plot_pairwise_consensus_history(consensusStruct, hPanelPairwiseConsensusHistory);

    [hTabConsensusDendros, tabNumConsensusDendros] = ts.create_tab('Consensus Dendrograms');
    ts.select_tab(tabNumConsensusDendros);
    hPanelConsensusDendros = uipanel(hTabConsensusDendros);
    import CBT.Consensus.UI.plot_consensus_dendrograms;
    plot_consensus_dendrograms(consensusStruct, hPanelConsensusDendros);

    ts.select_tab(tabNumConsensusClusters);
end