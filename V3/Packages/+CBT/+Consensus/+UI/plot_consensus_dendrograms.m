function [] = plot_consensus_dendrograms(consensusStruct, hPanelConsensusDendros)
    if nargin < 2
        hFig = figure('Name', 'Consensus Dendrograms');
        hPanelConsensusDendros = uipanel('Parent', hFig);
    end

    import FancyGUI.FancyTabs.TabbedScreen;
    ts = TabbedScreen(hPanelConsensusDendros);

    barcodeAliases = consensusStruct.inputs.barcodeAliases;
    numBarcodes = length(consensusStruct.inputs.barcodes);
    commonLength = length(consensusStruct.inputs.barcodes{1});
    clusterThresholdScore = consensusStruct.inputs.clusterThresholdScore;
    bestPossibleScore = sqrt(commonLength);
    clusterThresholdScoreNormalized = clusterThresholdScore/bestPossibleScore;
    consensusMergingTree = consensusStruct.consensusMergingTree;
    clusterAssignmentsMatrix = consensusStruct.clusterAssignmentsMatrix;

    
    dendroDefaultOrderTitleStr = 'Consensus Dendrogram (Default Order)';
    [hDendroDefaultOrderTab, dendroDefaultOrderTabNum] = ts.create_tab(dendroDefaultOrderTitleStr);
    ts.select_tab(dendroDefaultOrderTabNum);
    
    import CBT.Consensus.UI.plot_dendrogram;
    plot_dendrogram(hDendroDefaultOrderTab, numBarcodes, consensusMergingTree, clusterThresholdScoreNormalized, [], barcodeAliases, commonLength);

    
    dendroReorderedTitleStr = 'Consensus Dendrogram (Reordered)';
    [hDendroReorderedTab, dendroReorderedTabNum] = ts.create_tab(dendroReorderedTitleStr);
    ts.select_tab(dendroReorderedTabNum);
    [~, leafReordering] = sort(clusterAssignmentsMatrix(:));
    leafReordering = leafReordering';

    import CBT.Consensus.UI.plot_dendrogram;
    plot_dendrogram(hDendroReorderedTab, numBarcodes, consensusMergingTree, clusterThresholdScoreNormalized,  leafReordering, barcodeAliases, commonLength);
end