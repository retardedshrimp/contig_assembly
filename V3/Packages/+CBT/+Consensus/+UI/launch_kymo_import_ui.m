function [lm] = launch_kymo_import_ui(tsCBC)
    % launch_kymo_import_ui -
    %   adds a tab with list management UI/functionality  for
    %    kymographs

    tabTitle = 'Kymographs for Consensus';

    [hTabKymoImport, tabNumKymoImport] = tsCBC.create_tab(tabTitle);
    hPanelKymoImport = uipanel(hTabKymoImport);
    tsCBC.select_tab(tabNumKymoImport);

    import FancyGUI.FancyList.FancyListMgr;
    lm = FancyListMgr();
    lm.set_ui_parent(hPanelKymoImport);
    lm.make_ui_items_listbox();
    
    import FancyGUI.FancyList.FancyListMgrBtnSet;
    flmbs1 = FancyListMgrBtnSet();
    flmbs1.NUM_BUTTON_COLS = 2;
    flmbs1.add_button(FancyListMgr.make_select_all_button_template());
    flmbs1.add_button(FancyListMgr.make_deselect_all_button_template());
    flmbs1.add_button(make_add_kymos_btn());
    flmbs1.add_button(make_remove_kymos_btn());

    lm.add_button_sets(flmbs1);
    function [btnAddKymos] = make_add_kymos_btn()
        import FancyGUI.FancyList.FancyListMgrBtn;
        btnAddKymos = FancyListMgrBtn(...
            'Add kymographs from  DBM sessions', ...
            @(~, ~, lm) on_add_kymos(lm));

        function [aborted, kymoNames, kymoStructs] = prompt_kymos_from_DBM_session()
            % prompt_kymos_from_DBM_session - get kymo structs
            %   extracted from formatted DBM session .mat files provided
            %   by a prompt to the user

            import CBT.Consensus.Import.prompt_dbm_session_filepath;
            [aborted, sessionFilepath] = prompt_dbm_session_filepath();

            if aborted
                kymoStructs = cell(0, 1);
                kymoNames = cell(0, 1);
                return;
            end

            import OldDBM.General.Import.try_loading_from_session_file;
            [dbmODW, dbmOSW] = try_loading_from_session_file(sessionFilepath);
            

            import OldDBM.General.Export.DataExporter;
            dbmDE = DataExporter(dbmODW, dbmOSW);
            kymoStructs = dbmDE.extract_kymo_structs();
            kymoNames = cellfun(@(x) x.displayName, kymoStructs, 'UniformOutput', false);
            
            
            % TODO: replace with different kymo viewing functionality
            %  downstream independent of DataWrapper
            hFigMainDBM = figure('Name', 'Kymos from DBM session');
            hPanelShowKymos = uipanel('Parent', hFigMainDBM);
            import OldDBM.General.UI.show_home_screen;
            show_home_screen(dbmODW, hPanelShowKymos);
        end
        function [] = on_add_kymos(lm)
            [aborted, kymoNames, kymoStructs] = prompt_kymos_from_DBM_session();
            if aborted
                return;
            end

            lm.add_list_items(kymoNames, kymoStructs);
        end            
    end

    function [btnRemoveKymos] = make_remove_kymos_btn()
        import FancyGUI.FancyList.FancyListMgrBtn;
        btnRemoveKymos = FancyListMgrBtn(...
            'Remove selected kymographs', ...
            @(~, ~, lm) on_remove_selected_kymos(lm));
        function [] = on_remove_selected_kymos(lm)
            lm.remove_selected_items();
        end
    end
end