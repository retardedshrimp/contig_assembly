function [] = plot_pairwise_consensus_history(consensusStruct, hParent)
    if nargin < 2
        hParent = figure('Name', 'Pairwise Consensus History');
    end
    hPanel = uipanel('Parent', hParent);

    import FancyGUI.FancyTabs.TabbedScreen;
    ts = TabbedScreen(hPanel);
    
    import CBT.Consensus.UI.plot_pairwise_consensus_history_helper;
    plot_pairwise_consensus_history_helper(ts, consensusStruct.finalConsensusKey, consensusStruct.barcodeStructsMap);
end