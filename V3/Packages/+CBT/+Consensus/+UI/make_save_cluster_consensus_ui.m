function [] = make_save_cluster_consensus_ui(ts, consensusStruct)
    clusterKeys = consensusStruct.clusterKeys;
    numClusterKeys = length(clusterKeys);
    clusterValues = cell(numClusterKeys, 1);
    import CBT.Consensus.Helper.extract_cluster_deliverables;
    for clusterKeyNum=1:numClusterKeys
        currClusterKey = clusterKeys{clusterKeyNum};
        currClusterConsensusData.clusterKey = currClusterKey;
        [...
            currClusterConsensusData.barcode,...
            currClusterConsensusData.bitmask,...
            currClusterConsensusData.stdErrOfTheMean,...
            currClusterConsensusData.indexWeights,...
            currClusterConsensusData.clusterResultStruct,...
            ~...
        ] = extract_cluster_deliverables(consensusStruct, currClusterKey);
        currClusterConsensusData.details.consensusStruct = consensusStruct;
        clusterValues{clusterKeyNum} = currClusterConsensusData;
    end

    import CBT.Consensus.UI.launch_export_ui;
    launch_export_ui(ts, clusterKeys, clusterValues)
end