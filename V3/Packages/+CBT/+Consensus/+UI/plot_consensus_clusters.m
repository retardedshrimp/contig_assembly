function [] = plot_consensus_clusters(consensusStruct, hParent)
    if nargin < 2
        hParent = figure('Name', 'Consensus Cluster Plots');
    end
    hPanel = uipanel('Parent', hParent);

    import FancyGUI.FancyTabs.TabbedScreen;
    ts = TabbedScreen(hPanel);

    clusterKeys = consensusStruct.clusterKeys;
    barcodeStructsMap = consensusStruct.barcodeStructsMap;
    clusterResultStructs = consensusStruct.clusterResultStructs;

    numClusters = size(clusterKeys, 1);
    if numClusters == 0
        return;
    end
    tabNums = zeros(numClusters, 1);
    import CBT.Consensus.UI.plot_cluster;
    for clusterNum=1:numClusters
        clusterKey = clusterKeys{clusterNum};
        clusterStruct = barcodeStructsMap(clusterKey);
        clusterResultStruct = clusterResultStructs{clusterNum};
        [hTab, tabNums(clusterNum)] = ts.create_tab(clusterKey);
        ts.select_tab(tabNums(clusterNum));
        hAxis = axes('Units', 'normal', 'Position', [0 0 1 1], 'Parent', hTab);
        plot_cluster(hAxis, clusterKey, clusterStruct, clusterResultStruct, barcodeStructsMap);
    end
    ts.select_tab(tabNums(1));
end