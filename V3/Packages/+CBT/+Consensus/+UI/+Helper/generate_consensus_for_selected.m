function [consensusStruct, cache] = generate_consensus_for_selected(lm, cache)
    import CBT.Consensus.Import.get_default_consensus_settings;
    defaultConsensusSettings = get_default_consensus_settings();

    import CBT.Consensus.Import.get_cluster_threshold;
    [clusterScoreThresholdNormalized, quitConsensus] = get_cluster_threshold(defaultConsensusSettings);
    aborted = quitConsensus;
    if aborted
        fprintf('Aborting consensus input generation\n');
        consensusStruct = [];
        return;
    end

    import CBT.Consensus.Import.Helper.generate_barcodes_for_selected_kymos;
    [kymoStructs] = generate_barcodes_for_selected_kymos(lm, true, true);


    import CBT.Consensus.Import.Helper.check_kymo_structs_for_consensus_inputs;
    [aborted, displayNames, rawBarcodes, bpsPerPx_original] = check_kymo_structs_for_consensus_inputs(kymoStructs);
    if aborted
        fprintf('Aborting consensus input generation\n');
        consensusStruct = [];
        return;
    end

    rawBarcodeLens = cellfun(@length, rawBarcodes);
    commonLength = ceil(mean(rawBarcodeLens));

    import CBT.Consensus.Import.confirm_stretching_is_ok;
    [notOK, commonLength] = confirm_stretching_is_ok(commonLength, rawBarcodeLens);
    aborted = notOK;
    if aborted
        fprintf('Aborting consensus input generation\n');
        consensusStruct = [];
        return;
    end

    import CBT.Consensus.Import.Helper.gen_consensus_inputs_struct;
    consensusInputs = gen_consensus_inputs_struct(...
        displayNames, ...
        rawBarcodes, ...
        bpsPerPx_original, ...
        clusterScoreThresholdNormalized, ...
        commonLength);

    import CBT.Consensus.Import.confirm_consensus_generation;
    aborted = confirm_consensus_generation(consensusInputs);
    if aborted
        fprintf('Aborting consensus input generation\n');
        consensusStruct = [];
        return;
    end

    import CBT.Consensus.Core.make_consensus_as_struct;
    [consensusStruct, cache] = make_consensus_as_struct( ...
        consensusInputs.barcodes, ...
        consensusInputs.bitmasks, ...
        consensusInputs.displayNames,...
        consensusInputs.otherBarcodeData, ...
        consensusInputs.clusterScoreThresholdNormalized, ...
        cache ...
    );
end