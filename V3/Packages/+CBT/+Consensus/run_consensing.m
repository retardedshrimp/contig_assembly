function [] = run_consensing(tsCBC)
    import CBT.Consensus.UI.launch_kymo_import_ui;
    lm = launch_kymo_import_ui(tsCBC);

    import CBT.Consensus.UI.add_consensus_btns_to_kymo_list_mgr;
    add_consensus_btns_to_kymo_list_mgr(lm, tsCBC);
end