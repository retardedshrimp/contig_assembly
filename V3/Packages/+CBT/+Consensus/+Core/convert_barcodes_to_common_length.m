function [barcodes] = convert_barcodes_to_common_length(rawBarcodes, commonLength)
    fprintf('Converting barcodes to the same length...\n');
    barcodes = cellfun(@(rawBarcode) interp1(rawBarcode, linspace(1, length(rawBarcode), commonLength)), rawBarcodes, 'UniformOutput', false);
end