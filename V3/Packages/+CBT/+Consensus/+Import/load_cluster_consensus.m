function [clusterConsensusData, consensusFilepath] = load_cluster_consensus(consensusFilepath)
    if nargin < 1
        consensusFilepath = [];
    end
    clusterConsensusData = [];
    while isempty(clusterConsensusData)
        import OptMap.DataImport.try_prompt_consensus_filepaths;
        [~, consensusFilepaths] = try_prompt_consensus_filepaths([], false);
        consensusFilepath = consensusFilepaths{1};
        if isempty(consensusFilepath)
            continue;
        end
        consensusStruct = load(consensusFilepath, 'clusterConsensusData');
        if isfield(consensusStruct, 'clusterConsensusData')
            clusterConsensusData = consensusStruct.clusterConsensusData;
        end
    end
end