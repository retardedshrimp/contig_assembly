function [lm] = ensure_alignment_for_selected_kymos(lm)
    selectedIndices = lm.get_selected_indices();

    lenKymos = length(selectedIndices);
    if lenKymos < 1
        questdlg('You must select some kymographs first!', 'Not Yet!', 'OK', 'OK');
        return;
    end

    confirmContinuePrompt = sprintf('Align %d kymographs?', lenKymos);
    confirmContinueChoice = questdlg(confirmContinuePrompt, 'Kymograph Alignment Confirmation', 'Continue', 'Continue');
    quitAlignment = not(strcmp(confirmContinueChoice, 'Continue'));
    if quitAlignment
        fprintf('Kymograph alignments aborted\n');
        return;
    end
    trueValueList = lm.get_true_value_list();
    import CBT.Consensus.Import.Helper.ensure_alignment_at_index;
    for kymoIndex = selectedIndices
        [~, ~, trueValueList] = ensure_alignment_at_index(kymoIndex, lm, trueValueList, true);
    end
    lm.set_list_items(lm.get_diplay_names(selectedIndices), trueValueList);
    fprintf('Kymograph alignments are complete!\n');
end