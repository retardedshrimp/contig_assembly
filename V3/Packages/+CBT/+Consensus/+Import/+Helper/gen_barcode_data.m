function [barcodeGenData] = gen_barcode_data(alignedKymo)
    import OptMap.MoleculeDetection.EdgeDetection.get_default_edge_detection_settings;
    edgeDetectionSettings = get_default_edge_detection_settings(false);

    import OptMap.MoleculeDetection.EdgeDetection.approx_main_kymo_molecule_edges;
    [leftEdgeIdx, rightEdgeIdx] = approx_main_kymo_molecule_edges(alignedKymo, edgeDetectionSettings);


    % Determine indices for rotated barcode with background cropped out
    adjustedIndices = leftEdgeIdx:rightEdgeIdx;
    rawBarcode = mean(alignedKymo);
    rawBarcode = rawBarcode(adjustedIndices);
    % rawBarcode = zscore(rawBarcode);

    barcodeGenData.rawBarcode = rawBarcode;
    barcodeGenData.rawBarcodeLeftEdgeIndex = leftEdgeIdx;
    barcodeGenData.rawBarcodeRightEdgeIndex = rightEdgeIdx;
end