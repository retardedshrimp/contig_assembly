function [consensusInputs] = gen_consensus_inputs_struct(displayNames, rawBarcodes, bpsPerPx_original, clusterScoreThresholdNormalized, commonLength)
    import CBT.Bitmasking.calculate_untrusted_edge_len_pixels;

    import CBT.Consensus.Core.convert_barcodes_to_common_length;
    [barcodes] = convert_barcodes_to_common_length(rawBarcodes, commonLength);
    barcodes = cellfun(@zscore, barcodes, 'UniformOutput', false);

    import OldDBM.General.SettingsWrapper;
    [dbmSettingsStruct] = SettingsWrapper.read_DBM_settings();
    bitmaskSettings = dbmSettingsStruct.bitmasks;
    skipOverridePrompt = isfield(bitmaskSettings, 'promptForBitmaskingParams') && (bitmaskSettings.promptForBitmaskingParams == 0);
    import FancyUtils.extract_fields;
    [psfSigmaWidth_nm, deltaCut, prestretchPixelWidth_nm] = extract_fields(bitmaskSettings,...
        {'PSF_width', 'DeltaCut', 'nmPerPixel'});

    [ ...
        prestretchUntrustedEdgeLenUnrounded_pixels, ...
        ~, ...
        ~, ...
        prestretchPixelWidth_nm ...
        ] = get_raw_px_edge_length(...
            psfSigmaWidth_nm, ...
            deltaCut, ...
            prestretchPixelWidth_nm, ...
            skipOverridePrompt);


    function [edgeLength_px_unrounded, psfSigmaWidth_nm, deltaCut, pixelWidth_nm] = get_raw_px_edge_length(default_psfSigmaWidth_nm, default_deltaCut, default_pixelWidth_nm, skipOverridePrompt)
        if nargin < 4
            skipOverridePrompt = true;
        end
        psfSigmaWidth_nm = default_psfSigmaWidth_nm;
        deltaCut = default_deltaCut;
        pixelWidth_nm = default_pixelWidth_nm;
        if skipOverridePrompt
            edgeLength_px_unrounded = deltaCut * psfSigmaWidth_nm / pixelWidth_nm;
           return;
        end
        % Set a threshold for grouping barcodes.
        answer = inputdlg(...
            { ...
                'Point Spread Function Sigma Width (nm)', ...
                'Pixel width (nm)', ...
                'Delta Cut (edge length as multiple of PSF width in pixels))' ...
            }, ... % prompt
            'Bitmask Generation Parameters', ... % dialog title
            1, ... % number of lines
            { ...
                num2str(psfSigmaWidth_nm), ...
                num2str(pixelWidth_nm), ...
                num2str(deltaCut) ...
            }... % default value
        );
        if isempty(answer)
            edgeLength_px_unrounded = deltaCut * psfSigmaWidth_nm / pixelWidth_nm;
            return;
        else
            psfSigmaWidth_nm = str2double(answer{1});
            pixelWidth_nm = str2double(answer{2});
            deltaCut = str2double(answer{3});
        end
        isAcceptable = struct;
        isAcceptable.psfSigmaWidth_nm = (psfSigmaWidth_nm >= 0);
        isAcceptable.pixelWidth_nm = (pixelWidth_nm > 0);
        isAcceptable.deltaCut = (deltaCut >= 0);

        if all([isAcceptable.psfSigmaWidth_nm, isAcceptable.pixelWidth_nm, isAcceptable.deltaCut])
            edgeLength_px_unrounded = deltaCut * psfSigmaWidth_nm / pixelWidth_nm;
            return;
        end
        if not(isAcceptable.psfSigmaWidth_nm)
            psfSigmaWidth_nm = default_psfSigmaWidth_nm;
            warning('Bad input for PSF width! Try again!');
        end
        if not(isAcceptable.pixelWidth_nm)
            pixelWidth_nm = default_pixelWidth_nm;
            warning('Bad input pixel width! Try again!');
        end
        if not(isAcceptable.deltaCut)
            deltaCut = default_deltaCut;
            warning('Bad input for Delta Cut! Try again!');
        end
        edgeLength_px_unrounded = deltaCut * psfSigmaWidth_nm / pixelWidth_nm;
    end


    stretchFactors = cellfun(@length, rawBarcodes)/commonLength;

    import CBT.Bitmasking.generate_zero_edged_bitmask_row;
    bitmasks = arrayfun( ...
        @(stretchFactor) ...
            generate_zero_edged_bitmask_row(commonLength, round(stretchFactor * prestretchUntrustedEdgeLenUnrounded_pixels)), ...
        stretchFactors(:), ...
        'UniformOutput', false);

    bpsPerPx_stretched = bpsPerPx_original;
    bpsPerPx_stretched(bpsPerPx_stretched > 0) = bpsPerPx_stretched(bpsPerPx_stretched > 0).*bpsPerPx_original(bpsPerPx_stretched > 0);
    numBarcodes = length(barcodes);
    prestretchPixelWidth_nm = prestretchPixelWidth_nm*ones(numBarcodes, 1);
    postStretchPixelWidth_nm = prestretchPixelWidth_nm./stretchFactors;
    otherBarcodeData = cell(numBarcodes, 1);
    for barcodeNum=1:numBarcodes
        currBarcodeData = struct;
        currBarcodeData.stretchFactor = stretchFactors(barcodeNum);
        currBarcodeData.bpsPerPx_original = bpsPerPx_original(barcodeNum);
        currBarcodeData.bpsPerPx_stretched = bpsPerPx_stretched(barcodeNum);
        currBarcodeData.nmPerPx_original = prestretchPixelWidth_nm(barcodeNum);
        currBarcodeData.nmPerPx_stretched = postStretchPixelWidth_nm(barcodeNum);
        otherBarcodeData{barcodeNum} = currBarcodeData;
    end
    consensusInputs.otherBarcodeData = otherBarcodeData;
    consensusInputs.barcodes = barcodes;
    consensusInputs.bitmasks = bitmasks;
    consensusInputs.displayNames = displayNames;
    consensusInputs.clusterScoreThresholdNormalized = clusterScoreThresholdNormalized;
end