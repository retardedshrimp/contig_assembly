function [quitConsensus] = confirm_consensus_generation(consensusInputs)
    continueGenPrompt = sprintf('Generate consensus for %d barcodes?', length(consensusInputs.barcodes));
    continueGenChoice = questdlg(continueGenPrompt, 'Consensus Generation Confirmation', 'Continue', 'Continue');
    quitConsensus = not(strcmp(continueGenChoice, 'Continue'));
end