function [consensusSettings] = get_default_consensus_settings()
    import OldDBM.General.SettingsWrapper;
    [dbmSettingsStruct] = SettingsWrapper.read_DBM_settings();
    consensusSettings = dbmSettingsStruct.consensus;
end