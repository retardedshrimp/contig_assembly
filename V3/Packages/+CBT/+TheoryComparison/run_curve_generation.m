function theoryStruct = run_curve_generation(concNetropsin_molar, concYOYO1_molar, saveToCache, theoryStruct, curveNum, numCurves, cacheExists, cacheSubfolderPath)
    import FancyIO.mkdirp;
    import CBT.TheoryComparison.is_cached;
    import CBT.TheoryComparison.save_to_cache;
    import CBT.cb_netropsin_vs_yoyo1_plasmid;
    import FancyUtils.data_hash;

    cacheFilepath = '';
    envConstantsStruct = struct('NETROPSINconc', concNetropsin_molar, 'YOYO1conc', concYOYO1_molar);
    if saveToCache
        if isempty(cacheSubfolderPath)
            pathname = uigetdir('OutputFiles\Cache\IntensityCurves', 'Select the IntensityCurves Cache folder');
            if isequal(pathname, 0)
               disp('Cache folder was not selected');
               saveToCache = false;
            end

            if (pathname(end) ~= filesep)
                pathname = [pathname, filesep];
            end
            paramHash = data_hash(envConstantsStruct);
            cacheSubfolderPath = [pathname, paramHash, filesep];
        end
        mkdirp(cacheSubfolderPath);
    end

    displayName = theoryStruct.displayName;
    theorySequence = theoryStruct.sequenceData;
    sequenceDataHash = theoryStruct.dataHash;
    wasFoundInCache = false;
    if (cacheExists)
        [wasFoundInCache, cachedVal, cacheFilepath] = is_cached(cacheSubfolderPath, sequenceDataHash, displayName, concNetropsin_molar, concYOYO1_molar);
    end
    if (wasFoundInCache)
        % fprintf('Found curve in cache (%d/%d): %s\n', curveNum, numCurves, displayName);
        theoryCurve_bpRes_prePSF = cachedVal;
    else
        fprintf('Generating intensity curve (%d/%d): %s\n', curveNum, numCurves, displayName);
        theoryCurve_bpRes_prePSF = cb_netropsin_vs_yoyo1_plasmid(theorySequence, concNetropsin_molar, concYOYO1_molar, [], true);
        if saveToCache
            cacheFilepath = [cacheSubfolderPath, sequenceDataHash, '.mat'];
            theoryStruct.cacheFilepath = cacheFilepath;
            save_to_cache(cacheFilepath, concNetropsin_molar, concYOYO1_molar, sequenceDataHash, displayName, theoryCurve_bpRes_prePSF);
        end
    end
    if exist(cacheFilepath, 'file')
        theoryStruct.cacheFilepath = cacheFilepath;
    end
    theoryStruct.theoryCurve_bpRes_prePSF = theoryCurve_bpRes_prePSF;
end