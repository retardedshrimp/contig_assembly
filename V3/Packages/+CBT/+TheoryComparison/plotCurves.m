function dataStruct = plotCurves(curveA, curveB, indicesA, indicesB, alignedBitmaskA, alignedBitmaskB)

   % Plot everything
   % A bit complicated because of the "gap" in the shorter barcode

    function plotComponent(indices, curve, compLeftIndex, compRightIndex, strLineSpec, lineWidth)
        compIndices = compLeftIndex:compRightIndex;
        plot(indices(compIndices), curve(compIndices), strLineSpec, 'LineWidth', lineWidth);
        hold on;
    end

    function plotComponents(indices, curve, compEdges, strLineSpec, lineWidth)
        for compNum = 1:size(compEdges, 2)
            plotComponent(indices, curve, compEdges(1, compNum), compEdges(2, compNum), strLineSpec, lineWidth);
        end
    end

    function plotEm(indices, curve, asComponents, strLineSpec, lineWidth)
        if asComponents
            compEdges = [0, find(abs(diff(indices))~=1), length(indices)];
            compEdges = [compEdges(1:(end-1)) + 1; compEdges(2:end)];
            plotComponents(indices, curve, compEdges, strLineSpec, lineWidth);
        else
            plot(indices, curve, strLineSpec, 'LineWidth', lineWidth);
            hold on;
        end
    end

    indicesA = indicesA(:);
    alignedBitmaskA = alignedBitmaskA(:);
    curveA = curveA(:);
    aa = 1:length(indicesA);

    a = curveA(indicesA);
    plotEm(aa(alignedBitmaskA), a(alignedBitmaskA), true, 'r-', 1);
    aOut = a;
    aOut(~alignedBitmaskA) = NaN;

    indicesB = indicesB(:);
    alignedBitmaskB = alignedBitmaskB(:);
    curveB = curveB(:);
    bb = 1:length(indicesB);

    b = curveB(indicesB);
    plotEm(bb(alignedBitmaskB), b(alignedBitmaskB), true, 'b-', 1);
    hold off;
    bOut = b;
    bOut(~alignedBitmaskB) = NaN;

    dataStruct = struct;
    aOut = num2cell(aOut);
    bOut = num2cell(bOut);
    aOut = cellfun(@g, aOut, 'UniformOutput', false);
    bOut = cellfun(@g, bOut, 'UniformOutput', false);
    dataStruct.curveA = aOut;
    dataStruct.curveB = bOut;
    function x = g(x)
        if isnan(x)
            x = [];
        end
    end
end