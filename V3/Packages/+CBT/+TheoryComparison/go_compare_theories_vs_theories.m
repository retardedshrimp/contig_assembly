function go_compare_theories_vs_theories(theoriesFromDisplayName, theoryNamesA, theoryNamesB, constSettings, cacheResultsSubfolderPath)
    import CBT.TheoryComparison.get_length_ordering_for_theories;
    import CBT.TheoryComparison.compare_theories_to_theories;
    import CBT.TheoryComparison.extract_scalarMat;
    import CBT.TheoryComparison.plot_heat_maps;

    numTheoriesA = length(theoryNamesA);
    theoriesA = cell(numTheoriesA, 1);
    for theoryNumA=1:numTheoriesA
        theoryDisplayName = theoryNamesA{theoryNumA};
        theoryDisplayNameStruct = theoriesFromDisplayName(theoryDisplayName);
        theoriesA{theoryNumA} = theoryDisplayNameStruct;
    end
    [~, orderedTheoryIndicesA] = get_length_ordering_for_theories(theoriesA, constSettings.nmPerBp);
    theoryNamesA = theoryNamesA(orderedTheoryIndicesA);
    theoriesA = theoriesA(orderedTheoryIndicesA);
    assignin('base', 'theoryNamesA', theoryNamesA);
    assignin('base', 'theoryStructsA', theoriesA);

    numTheoriesB = length(theoryNamesB);
    theoriesB = cell(numTheoriesB, 1);
    for theoryNumB=1:numTheoriesB
        theoryDisplayName = theoryNamesB{theoryNumB};
        theoryDisplayNameStruct = theoriesFromDisplayName(theoryDisplayName);
        theoriesB{theoryNumB} = theoryDisplayNameStruct;
    end
    [~, orderedTheoryIndicesB] = get_length_ordering_for_theories(theoriesB, constSettings.nmPerBp);
    theoryNamesB = theoryNamesB(orderedTheoryIndicesB);
    theoriesB = theoriesB(orderedTheoryIndicesB);
    assignin('base', 'theoryNamesB', theoryNamesB);
    assignin('base', 'theoryStructsB', theoriesB);

    comparisonResults = compare_theories_to_theories(theoriesA, theoriesB, constSettings, cacheResultsSubfolderPath);
    assignin('base', 'theoryVsTheoryResults', comparisonResults);

    comparisonResultsByField = struct;
    scalarNumberFields = {'bestCC', 'meanCC', 'stdCC', 'bestStretchFactor', 'numStretchFactors'};
    for theoryNumA=1:length(scalarNumberFields)
        fieldName = scalarNumberFields{theoryNumA};
        comparisonResultsByField.(fieldName) = extract_scalarMat(comparisonResults, fieldName, NaN);
    end
    assignin('base', 'theoryVsTheoryResultsByField', comparisonResultsByField);

    plot_heat_maps(comparisonResultsByField);
    disp('Theory vs. Theory Calculations Complete');
    disp('Check workspace variables for results!');
end