function expCurveBitmask = get_std_experiment_bitmask(curveLen, deltaCut, psfSigmaWidth_nm, pixelWidth_nm)
    edgeLen = round(deltaCut*psfSigmaWidth_nm/pixelWidth_nm);
    if curveLen == 0
        warning('Empty curve');
    end
    expCurveBitmask = true(1, curveLen);
    edgeIndices = [(1:min(curveLen, edgeLen)), ((max(curveLen, edgeLen) - edgeLen + 1):curveLen)];
    expCurveBitmask(edgeIndices) = false;
    if not(any(expCurveBitmask))
        warning('Edge region takes up entire curve');
    end
end