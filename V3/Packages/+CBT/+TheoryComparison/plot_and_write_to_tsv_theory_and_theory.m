function plot_and_write_to_tsv_theory_and_theory(thyStructA, thyStructB, stretchFactor, settingsParams, columnLabels)
    import FancyIO.FancyTSV.write_tsv;
    import CBT.TheoryComparison.plot_theory_and_theory;

    defaultColumnLabels = {'curveA', 'curveB'};
    if nargin < 5
        columnLabels = defaultColumnLabels;
    end
    [tsvFilename, tsvDirpath] = uiputfile({'.tsv'});

    if isequal(tsvDirpath, 0)
        return;
    end

    dataStruct = plot_theory_and_theory(thyStructA, thyStructB, stretchFactor, settingsParams);
    dataStruct2 = struct;
    numDefaultColumnLabels = length(defaultColumnLabels);
    for defaultColumnLabelNum=1:numDefaultColumnLabels
        defaultColumnLabel = defaultColumnLabels{defaultColumnLabelNum};
        columnLabel = defaultColumnLabel;
        if length(columnLabels) >= defaultColumnLabelNum
            columnLabel = columnLabels{defaultColumnLabelNum};
        end
        dataStruct2.(columnLabel) = dataStruct.(defaultColumnLabel);
    end
    write_tsv([tsvDirpath, tsvFilename], dataStruct2, columnLabels);
end