function [bestCCsRaw, theoryDataHashesRaw, theoryNamesRaw, theoryLengths_bpRaw] = extract_tvt_results_data(resultsStructTvT)
    import FancyUtils.extract_fields;
    [bestCCsRaw, theoryDataHashesRaw, theoryNamesRaw, theoryLengths_bpRaw] = extract_fields(resultsStructTvT,...
        {'bestCC'; 'theoryDataHashes'; 'theoryNames'; 'theoryLengths_bp'});
end