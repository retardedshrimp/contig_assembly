function dataStruct = plot_theory_and_theory(thyStructA, thyStructB, stretchFactor, settingsParams)
    import CBT.TheoryComparison.generate_alignment_for_theory_and_theory;
    import CBT.TheoryComparison.plotCurves;

    [~, ~, alignedCurveABitmask, alignedCurveBBitmask, alignedCurveAIndices, alignedCurveBIndices, curveA_pxRes, curveB_pxRes] = generate_alignment_for_theory_and_theory(thyStructA, thyStructB, stretchFactor, settingsParams);
    dataStruct = plotCurves(curveA_pxRes, curveB_pxRes, alignedCurveAIndices, alignedCurveBIndices, alignedCurveABitmask, alignedCurveBBitmask);
end