function [curve_pxRes, curveBitmask] = get_pxRes_other_curve_and_bitmask_from_struct(curveStruct, isExperimentStruct, constantSettingsStruct)
    import FancyUtils.extract_fields;
    import CBT.TheoryComparison.PValue.convert_theory_curves_to_pxRes;
    import CBT.TheoryComparison.PValue.get_theory_bitmask;

    if isExperimentStruct
        [...
            deltaCut,...
            psfSigmaWidth_nm,...
            pixelWidth_nm...
            ] = extract_fields(constantSettingsStruct, {...
                'deltaCut',...
                'psfWidth_nm',...
                'nmPerPixel'
                });
        curve_pxRes = CBT_TheoryComparison.get_struct_experiment_curve_pxRes(curveStruct);
        curve_pxRes = zscore(curve_pxRes);
        curveBitmask = CBT_TheoryComparison.get_experiment_bitmask(curveStruct, deltaCut, psfSigmaWidth_nm, pixelWidth_nm);
    else
        [...
            deltaCut,...
            psfSigmaWidth_nm,...
            psfSigmaWidth_bp,...
            pixelWidth_nm,...
            meanBpExt_pixels...
            ] = extract_fields(constantSettingsStruct, {...
                'deltaCut',...
                'psfWidth_nm',...
                'psfWidth_bp',...
                'nmPerPixel',...
                'pixelsPerBp'...
                });

        theoryCurves_bpRes = {CBT_TheoryComparison.get_struct_theory_curve_bpRes(curveStruct)};
        theoryCurves_pxRes = convert_theory_curves_to_pxRes(theoryCurves_bpRes, psfSigmaWidth_bp, meanBpExt_pixels, 1);
        curve_pxRes = theoryCurves_pxRes{1};

        asIfExperiment = true;
        curveBitmask = get_theory_bitmask(curve_pxRes, asIfExperiment, deltaCut, psfSigmaWidth_nm, pixelWidth_nm); % get bitmask as if experiment
    end
end