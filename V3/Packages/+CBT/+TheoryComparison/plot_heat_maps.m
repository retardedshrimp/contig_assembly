function plot_heat_maps(comparisonResultsByField)
    hFig = figure();
    hAxisBestCC = subplot(2, 2, 1);
    hAxisMeanCC = subplot(2, 2, 2);
    hAxisStdCC = subplot(2, 2, 3);
    
    colormap(hAxisBestCC, gray());
    axes(hAxisBestCC);
    imagesc(comparisonResultsByField.bestCC, [-1, 1]);
    title(hAxisBestCC, 'Max Pearson Cross-correlation Coefficient');

    colormap(hAxisMeanCC, gray());
    axes(hAxisMeanCC);
    imagesc(comparisonResultsByField.meanCC, [-1, 1]);
    title(hAxisMeanCC, 'Pearson Cross-correlation Coefficient Mean');

    colormap(hAxisStdCC, gray());
    axes(hAxisStdCC);
    imagesc(comparisonResultsByField.stdCC, [0, 1]);
    title(hAxisStdCC, 'Pearson Cross-correlation Coefficient Standard Deviation');
end