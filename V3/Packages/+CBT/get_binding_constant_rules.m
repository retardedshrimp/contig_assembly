function [ bindingConstantRules ] = get_binding_constant_rules( ligandName )
    % GET_BINDING_CONSTANT_RULES - Returns the binding constant rules for a
    %  particular ligand
    %
    % Inputs:
    %   ligandName
    %     the name of the ligand
    %
    % Outputs:
    %   bindingConstantRules
    %     a struct specifying the binding constant rules for the ligand
    %     (these can be decompressed by CBT.gen_binding_constants into a
    %     matrix for all the possibilities)
    %
    % Authors:
    %   Saair Quaderi
    
    % TODO: Stop hardcoding this and retrieve it from some settings file or
    %   database or something
    switch ligandName
        case 'Netropsin'
            bindingConstantRules = {...
                'NNNN', 5e+05; ...
                'WWWW', 1e+08; ...
            };
        case 'Yoyo1'
            bindingConstantRules = {...
                'NNNN', 1e+11; ...
             };
        otherwise
            bindingConstantRules = struct;
            warning('Ligand binding constant rules not found');
    end

end

