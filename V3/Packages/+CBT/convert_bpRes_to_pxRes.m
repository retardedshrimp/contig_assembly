function [thyCurve_pxRes] = convert_bpRes_to_pxRes(thyCurve_bpRes, meanBpExt_pixels)
    % Convert from bp resolution to pixel resolution

    % TODO: sampling moving average with window might be superior to this
    meanPixelWidth_bps = 1/meanBpExt_pixels;
    thyCurve_pxRes = thyCurve_bpRes(1:round(meanPixelWidth_bps):end);
end