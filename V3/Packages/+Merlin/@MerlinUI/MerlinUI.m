classdef MerlinUI < handle
    %MERLINUI
    properties (Access = private)
    end
    properties
        FigureHandle
        RootPanelHandle
        RootPanelTabgroupHandle
        HomeTabHandle
        HomeTabPanelHandle
        
        HomeScreenObj
    end
    methods (Static)
        function settingsStruct = load_settings(settingsType)
            switch settingsType
                case 'Figure'
                    settingsStruct = struct( ...
                        'Name', 'merlin', ...
                        'NumberTitle', 'off', ...
                        'Toolbar', 'none', ...
                        'MenuBar', 'none' ...
                    );
                case 'HomeTab'
                    settingsStruct = struct( ...
                        'Title', 'home' ...
                    );
                case 'HomeTabPanel'
                    settingsStruct = struct( ...
                        'BackgroundColor', [0.2, 0.2, 0.2],...
                        'ForegroundColor', [0, 0, 0]...
                    );
                otherwise
                    settingsStruct = struct( ...
                    );
            end
        end
    end
    methods (Access = private)
        function init_figure(mui)
            import FancyGUI.FancyPositioning.maximize_figure_or_make_big;
            import FancyUtils.feval_with_structified_args;
            
            figureSettings = Merlin.MerlinUI.load_settings('Figure');
            mui.FigureHandle = feval_with_structified_args(...
                @figure, ...
                figureSettings ...
            );
            [~] = maximize_figure_or_make_big(mui.FigureHandle);
            
        end
        
        function init_root_panel_with_tabgroup(mui)
            import FancyUtils.feval_with_structified_args;
            
            rootPanelSettings = Merlin.MerlinUI.load_settings('RootPanel');
            rootPanelSettings.Parent = mui.FigureHandle;
            mui.RootPanelHandle = feval_with_structified_args(...
                @uipanel, ...
                rootPanelSettings ...
            );
        
            rootPanelTabgroupSettings = Merlin.MerlinUI.load_settings('RootPanelTabgroup');
            rootPanelTabgroupSettings.Parent = mui.RootPanelHandle;
            mui.RootPanelTabgroupHandle = feval_with_structified_args(...
                @uitabgroup, ...
                rootPanelTabgroupSettings ...
            );
        end
        
        function init_home_tab_with_panel(mui)
            import FancyUtils.feval_with_structified_args;
            
            homeTabSettings = Merlin.MerlinUI.load_settings('HomeTab');
            homeTabSettings.Parent = mui.RootPanelTabgroupHandle;
            mui.HomeTabHandle = feval_with_structified_args(...
                @uitab, ...
                homeTabSettings ...
            );
            
            homeTabPanelSettings = Merlin.MerlinUI.load_settings('HomeTabPanel');
            homeTabPanelSettings.Parent = mui.HomeTabHandle;
            mui.HomeTabPanelHandle = feval_with_structified_args(...
                @uipanel, ...
                homeTabPanelSettings ...
            );
        end
        
        function init_home_screen(mui)
            import Merlin.HomeScreen;
            mui.HomeScreenObj = HomeScreen(mui.HomeTabPanelHandle);
        end
    end
    methods
        function [mui] = MerlinUI()
            mui.init_figure();
            mui.init_root_panel_with_tabgroup();
            mui.init_home_tab_with_panel();
            mui.init_home_screen();
        end
    end
end