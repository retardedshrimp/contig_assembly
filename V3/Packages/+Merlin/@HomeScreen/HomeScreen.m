classdef HomeScreen < handle
    %HOMESCREEN
    properties
        ButtonHandles
    end
    
    methods (Static, Access = private)
        function [fgRGB, fgAlpha, bgRGB, bgAlpha] = preprocess_image_data(fgRGB, fgAlpha, bgRGB, bgAlpha)
            import FancyGUI.ImageGen.pad_image;

            bgRGB = [bgRGB; bgRGB];
            bgAlpha = [bgAlpha; bgAlpha];
            fgRGB = pad_image(fgRGB, 0, 0, 0, size(fgRGB, 1), 0);
            fgAlpha = pad_image(fgAlpha, 0, 0, 0, size(fgAlpha, 1), 0);
        end
        
        function buttonStrings = get_button_strings()
            buttonStrings = {...
                'Settings'; ...
                'Competitive Binding'; ...
                'DNA Sequences'; ...
                'Movies'; ...
                'DNA Barcodes'; ...
                'Consensus'; ...
                'Identification' ...
                };
        end
        
        function iconPath = get_button_icon_path(buttonString)
            import AppMgr.AppResourceMgr;
            assetRootPath = AppResourceMgr.get_dirpath('Assets');
            
            switch buttonString
                case 'Settings'
                    iconName = 'SettingsGear';
                case 'Competitive Binding'
                    iconName = 'TestTubeQuantities';
                case 'DNA Sequences'
                    iconName = 'DoubleHelix_2';
                case 'Movies'
                    iconName = 'FilmStrip';
                case 'DNA Barcodes'
                    iconName = 'Barcode';
                case 'Consensus'
                    iconName = 'concentric';
                case 'Identification'
                    iconName = 'Fingerprint';
                otherwise
                    iconName = '';
            end
            iconPath = '';
            if not(isempty(iconName))
                iconSize = 32;
                iconExt = '.png';
                assetRelSubpathDir = {'ThirdParty', 'NounProject', iconExt(2:end), num2str(iconSize)};
                iconPath = fullfile(assetRootPath, assetRelSubpathDir{:}, [iconName, iconExt]);
            end
        end
        
        function [hButtons] = create_buttons(hParent, buttonStrings, iconPaths)
            import FancyGUI.generate_ui_elems;
            import FancyGUI.FancyPositioning.FancyGrid.position_ui_elems_in_grid;
            import FancyGUI.ImageGen.load_png_as_cdata;
            import FancyUtils.pass_through;

            defaultArgStruct = struct( ...
                'Parent', hParent, ...
                'Visible', 'off', ...
                'Units', 'Normalized', ...
                'BackgroundColor', [0, 0.5, 0.5], ...
                'ForegroundColor', [1, 1, 1], ...
                'FontSize', 16 ...
                );
            
            
            overrideArgStructs = cellfun(@(buttonString, cdata) struct('String', ['<html><br/>', buttonString, '</html>']), buttonStrings, 'UniformOutput', false);

            [hButtons] = generate_ui_elems(@uicontrol, overrideArgStructs, defaultArgStruct);
            maxNumControlItemsPerRow = 3;
            paddingNrm = struct('top', 0.005, 'bottom', 0.005, 'left', 0.005, 'right', 0.005);
            position_ui_elems_in_grid(hButtons, maxNumControlItemsPerRow, paddingNrm);
            
            fn_preprocess = @Merlin.HomeScreen.preprocess_image_data;
            fn_postprocess = @pass_through;
            cellfun(@(hControl, srcFilepathPNG) load_png_as_cdata(hControl, srcFilepathPNG, fn_preprocess, fn_postprocess),...
                arrayfun(@(x) x, hButtons, 'UniformOutput', false),...
                iconPaths,...
                'UniformOutput', false);
            
            % make controls visible
            arrayfun(@(gObj) set(gObj, 'Visible', 'on'), hButtons, 'UniformOutput', false);
        end
    end
    methods
        function [hs] = HomeScreen(hParent)
            buttonStrings = Merlin.HomeScreen.get_button_strings();
            buttonIconPaths = cellfun(@Merlin.HomeScreen.get_button_icon_path, buttonStrings, 'UniformOutput', false);
            hs.ButtonHandles = Merlin.HomeScreen.create_buttons(hParent, buttonStrings, buttonIconPaths);
        end
    end
    
end

