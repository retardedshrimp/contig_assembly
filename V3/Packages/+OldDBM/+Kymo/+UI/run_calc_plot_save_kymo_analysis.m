function [] = run_calc_plot_save_kymo_analysis(dbmODW, skipDoubleTanhAdjustmentTF, shouldSavePngTF)
    import OldDBM.Kymo.UI.run_kymo_analysis;
    [~] = run_kymo_analysis(dbmODW, skipDoubleTanhAdjustmentTF);


    % % TODO: complete and uncomment
    % defaultStatsOutputDirpath =  dbmOSW.get_default_export_dirpath('raw_kymo_stats');
    % 
    % defaultStatsOutputFilepath = fullfile(defaultStatsOutputDirpath, 'stats.txt');
    % [statsOutputTsvFilename, statsOutputTsvDirpath, ~] = uiputfile('*.txt', 'Save Molecule Stats As', defaultStatsOutputFilepath);
    % 
    % if isequal(statsOutputTsvDirpath, 0)
    %    return; 
    % end
    % 
    % statsOutputTsvFilepath = fullfile(statsOutputTsvDirpath, statsOutputTsvFilename);
    % 
    % % TODO: Write frame-wise length estimates, intensity means, and
    % % intensity stds for all kymographs


    if shouldSavePngTF
        [fileIdxs, fileMoleculeIdxs] = dbmODW.get_molecule_idxs();

        [moleculeStatuses] = dbmODW.get_molecule_statuses(fileIdxs, fileMoleculeIdxs);
        selectionMask = moleculeStatuses.hasRawKymo & moleculeStatuses.passesFilters;

        fileIdxs = fileIdxs(selectionMask);
        fileMoleculeIdxs = fileMoleculeIdxs(selectionMask);

        hFig = figure('Name', 'Raw Kymo Edge Detection');
        hPanel = uipanel('Parent', hFig);
        import OldDBM.Kymo.UI.plot_detected_raw_kymo_edges;
        hAxisPlot = plot_detected_raw_kymo_edges(dbmODW, fileIdxs, fileMoleculeIdxs, hPanel);


        import OldDBM.Kymo.Helper.get_raw_kymo_edge_plot_png_output_filepaths;
        pngOutputFilepaths = get_raw_kymo_edge_plot_png_output_filepaths(dbmODW, fileIdxs, fileMoleculeIdxs);

        % Save images of the axes
        import OldDBM.General.Export.export_axis_image_as_png;
        cellfun(@export_axis_image_as_png, hAxisPlot, pngOutputFilepaths);
    end
end