function [] = show_kymos(kymos, hAxesKymos, headerTexts)
    import OldDBM.General.UI.set_centered_header_text;
    
    numKymos = numel(kymos);
    for kymoNum = 1:numKymos
        kymo = kymos{kymoNum};
        headerText = headerTexts{kymoNum};
        hAxisKymo = hAxesKymos(kymoNum);
        
        axes(hAxisKymo); %#ok<LAXES>
        imagesc(kymo);
        colormap(hAxisKymo, gray());
        set(hAxisKymo, ...
            'XTick', [], ...
            'YTick', []);
        box(hAxisKymo, 'on');

        set_centered_header_text(hAxisKymo, headerText);
    end
end