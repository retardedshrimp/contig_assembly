function [] = show_aligned_kymos(dbmODW, hParent)
    [alignedKymos, alignedKymoFileIdxs, alignedKymoFileMoleculeIdxs] = dbmODW.get_all_existing_aligned_kymos();
    [kymoSrcFilenames] = dbmODW.get_molecule_src_filenames(alignedKymoFileIdxs);
    
    numAxes = numel(alignedKymos);
    import FancyGUI.FancyPositioning.FancyGrid.generate_axes_grid;
    hAxesAlignedKymos = generate_axes_grid(hParent, numAxes);
    
    import OldDBM.General.UI.Helper.get_header_texts;
    headerTexts = get_header_texts(alignedKymoFileIdxs, alignedKymoFileMoleculeIdxs, kymoSrcFilenames);
    
    import OldDBM.Kymo.UI.show_kymos;
    show_kymos(alignedKymos, hAxesAlignedKymos, headerTexts);
end