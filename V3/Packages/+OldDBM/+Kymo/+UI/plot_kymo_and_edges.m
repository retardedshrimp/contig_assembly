function [] = plot_kymo_and_edges(hAxis, rawKymo, leftEndIdxs, rightEndIdxs, headerText)
    import OldDBM.General.UI.set_centered_header_text;

    axes(hAxis);
    imagesc(rawKymo);
    colormap(hAxis, gray);
    set(hAxis, 'YTick', []);
    set(hAxis, 'XTick', []);
    hold(hAxis, 'on');
    box('on');

    % Plot the kymograph's edges
    plot(hAxis, leftEndIdxs, 1:length(leftEndIdxs), 'm-', 'Linewidth', 2);
    plot(hAxis, rightEndIdxs, 1:length(rightEndIdxs), 'c-', 'Linewidth', 2);
    set_centered_header_text(hAxis, headerText);
    hold(hAxis, 'off');
end