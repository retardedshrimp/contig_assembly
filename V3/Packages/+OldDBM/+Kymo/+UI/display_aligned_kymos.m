function [] = display_aligned_kymos(dbmODW, hParent)
    % DISPLAY_ALIGNED_KYMOS - Aligns kymographs and displays them
    % 
    % Authors:
    %   Saair Quaderi
    %   Charleston Noble (previous version)


    dbmODW.verify_thresholds();
    
    import OldDBM.Kymo.DataBridge.create_and_set_all_missing_aligned_kymos;
    create_and_set_all_missing_aligned_kymos(dbmODW);
    
    import OldDBM.Kymo.UI.show_aligned_kymos;
    show_aligned_kymos(dbmODW, hParent);
end