function [hAxesPlots] = plot_detected_raw_kymo_edges(dbmODW, fileIdxs, fileMoleculeIdxs, hParent)
    import OldDBM.Kymo.UI.plot_kymo_and_edges;
    import OldDBM.General.UI.Helper.get_header_texts;
    import OldDBM.General.UI.create_tabs_panels_axes;

    rawKymos = dbmODW.get_raw_kymos(fileIdxs, fileMoleculeIdxs);

    srcFilenames = dbmODW.get_molecule_src_filenames(fileIdxs);
    headerTexts = get_header_texts(fileIdxs, fileMoleculeIdxs, srcFilenames);

    [kymosMoleculeLeftEdgeIdxs, kymosMoleculeRightEdgeIdxs] = dbmODW.get_raw_kymos_molecules_edge_idxs(fileIdxs, fileMoleculeIdxs);

    tabgroupHandle = uitabgroup('Parent', hParent);
    hAxesPlots = create_tabs_panels_axes(tabgroupHandle, headerTexts);

    % Show the raw kymographs with labeled edges and header text on
    %  their alloted axis handles
    cellfun(@plot_kymo_and_edges, ...
        arrayfun(@(x) x, hAxesPlots, 'UniformOutput', false), ...
        rawKymos, ...
        kymosMoleculeLeftEdgeIdxs, ...
        kymosMoleculeRightEdgeIdxs, ...
        headerTexts);
end