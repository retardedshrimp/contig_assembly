function [endOverStartIntensityRatio] = calc_end_over_start_intensity_ratio(rawKymo, moleculeMask, largeDropNumKymoEndAreaFrames)
    rawKymoFgWithNaN = rawKymo;
    rawKymoFgWithNaN(~moleculeMask) = NaN;
    bgIntensityApprox = kymoStatsStruct.meanNonMainMoleculePixelIntensity;
    numFrames = size(rawKymoFgWithNaN, 1);
    if numFrames > largeDropNumKymoEndAreaFrames
        meanIntensityAboveBgStartFrames = nanmean(rawKymoFgWithNaN(1:largeDropNumKymoEndAreaFrames, :)) - bgIntensityApprox;
        meanIntAboveBgEndFrames = nanmean(rawKymoFgWithNaN(end + 1 - (1:largeDropNumKymoEndAreaFrames), :)) - bgIntensityApprox;
        endOverStartIntensityRatio = meanIntAboveBgEndFrames / meanIntensityAboveBgStartFrames;
    end
end