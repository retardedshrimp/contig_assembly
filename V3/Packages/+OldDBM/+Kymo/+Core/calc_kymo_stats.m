function [kymoStatsStruct] = calc_kymo_stats(rawKymo, moleculeMask, moleculeLeftEdgeIdxs, moleculeRightEdgeIdxs)
    rawKymoFgWithNaN = rawKymo;
    rawKymoFgWithNaN(~moleculeMask) = NaN;

    meanFramewiseMoleculeIntensity = nanmean(rawKymoFgWithNaN, 2);
    stdFramewiseMoleculeIntensity = nanstd(rawKymoFgWithNaN, 0, 2);
    framewiseMoleculeExts = sum(moleculeMask, 2);


    meanOfFramewiseMoleculeExts = mean(framewiseMoleculeExts);
    stdOfFramewiseMoleculeExts = std(framewiseMoleculeExts);
    medianOfFramewiseMoleculeExts = median(framewiseMoleculeExts);
    madEstStdOfFramewiseMoleculeExts = 1.4826 * mad(framewiseMoleculeExts,1);

    unroundedMoleculeCenterOfMassIdxs = arrayfun(...
        @(moleculeLeftEdgeIdx, moleculeRightEdgeIdx) ...
            feval(...
                @(nrmCurveMainMolecule)...
                    sum((1:length(nrmCurveMainMolecule)) .* nrmCurveMainMolecule) / (sum(nrmCurveMainMolecule) * length(nrmCurveMainMolecule)),...
            nrmCurve(moleculeLeftEdgeIdx:moleculeRightEdgeIdx)), ...
            moleculeLeftEdgeIdxs, moleculeRightEdgeIdxs);
    meanUnroundedCenterOfMassIdx = mean(unroundedMoleculeCenterOfMassIdxs);


    kymoStatsStruct.moleculeLeftEdgeIdxs = moleculeLeftEdgeIdxs;
    kymoStatsStruct.moleculeRightEdgeIdxs = moleculeRightEdgeIdxs;
    kymoStatsStruct.meanOfFramewiseMoleculeExts = meanOfFramewiseMoleculeExts;
    kymoStatsStruct.stdOfFramewiseMoleculeExts = stdOfFramewiseMoleculeExts;
    kymoStatsStruct.medianOfFramewiseMoleculeExts = medianOfFramewiseMoleculeExts;
    kymoStatsStruct.madEstStdOfFramewiseMoleculeExts = madEstStdOfFramewiseMoleculeExts;
    kymoStatsStruct.framewiseMoleculeExts = framewiseMoleculeExts;
    kymoStatsStruct.meanUnroundedCenterOfMassIdx = meanUnroundedCenterOfMassIdx;
    kymoStatsStruct.meanFramewiseMoleculeIntensity = meanFramewiseMoleculeIntensity;
    kymoStatsStruct.stdFramewiseMoleculeIntensity = stdFramewiseMoleculeIntensity;
end