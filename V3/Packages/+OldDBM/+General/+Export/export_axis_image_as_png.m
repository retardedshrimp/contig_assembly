function [] = export_axis_image_as_png(hAxis, pngOutputFilepath)
    axisImg = frame2im(getframe(hAxis));
    imwrite(axisImg, pngOutputFilepath);
end