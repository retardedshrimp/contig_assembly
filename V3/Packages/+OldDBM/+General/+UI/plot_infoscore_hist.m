function [] = plot_infoscore_hist(dbmODW, hParent)
    % PLOT_INFOSCORE_HIST - Plots the distribution of information
    %   scores for the various molecules in a histogram
    %
    % Authors:
    %  Saair Quaderi
    %  Charleston Noble
    if nargin < 2
        hFig = figure('Name', 'Information Score Distribution');
        hParent = hFig;
    end

    [fileIdxs, fileMoleculeIdxs] = dbmODW.get_molecule_idxs();
    infoScores = dbmODW.get_info_scores(fileIdxs, fileMoleculeIdxs);
    infoScores = infoScores(not(isnan(infoScores)));
    if isempty(infoScores)
        disp('There were no information scores to plot');
        return;
    end


    hAxis = axes('Parent', hParent);
    hist(hAxis, infoScores);
    xlabel(hAxis, 'Information Score');
    ylabel(hAxis, 'Molecules');
    set(get(hAxis,'child'),...
        'FaceColor', 'k',...
        'EdgeColor', 'k');
end