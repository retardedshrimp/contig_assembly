function [] = plot_molecule_lengths_hist(dbmODW, hParent)
    % PLOT_MOLECULE_LENGTHS_HIST - Plots the distribution of molecule lengths
    %   in a histogram
    %
    % Authors:
    %  Saair Quaderi
    %  Charleston Noble
    %
    if nargin < 2
        hFig = figure('Name', 'Molecule Length Distribution');
        hParent = hFig;
    end

    [fileIdxs, fileMoleculeIdxs] = dbmODW.get_molecule_idxs();
    moleculeLengths = dbmODW.get_molecule_lengths(fileIdxs, fileMoleculeIdxs);
    moleculeLengths = moleculeLengths(not(isnan(moleculeLengths)));
    if isempty(moleculeLengths)
        disp('There were no molecule lengths to plot');
        return;
    end

    hAxis = axes('Parent', hParent);
    hist(hAxis, moleculeLengths);
    set(get(hAxis,'child'), 'FaceColor', 'k', 'EdgeColor', 'k');
    xlabel(hAxis, 'Length (pixels)')
    ylabel(hAxis, 'Molecules')
end