function [rawKymoFileStructs, fileMoleculeStructs, pixelsWidths_bps] = import_raw_kymos(defaultRawKymoDirpath)
    % IMPORT_RAW_KYMOS - here the user can import data straight from kymographs 
    %	(assumed unaligned), rather than from tif files (importing from tif 
    %	files is handled in importdata() above).
    % 
    % Authors:
    %   Charleston Noble
    %   Tobias Ambjörnsson

    % Get the file

    [rawKymoFilenames, rawKymoDirpath] = uigetfile({'*.tif;'},'Select raw kymo file(s) to import', defaultRawKymoDirpath, 'MultiSelect','on');

    if isequal(rawKymoDirpath, 0)
        rawKymoFileStructs = cell(0,1);
        fileMoleculeStructs = cell(0, 1);
        pixelsWidths_bps = zeros(0, 1) - 1;
        return;
    end

    if not(iscell(rawKymoFilenames))
        rawKymoFilenames = {rawKymoFilenames};
    end
    rawKymoFilenames = rawKymoFilenames(:);
    rawKymoFilepaths = fullfile(rawKymoDirpath, rawKymoFilenames);
    numFiles = length(rawKymoFilenames);


    rawKymoFileStructs = cell(numFiles,1);
    fileMoleculeStructs = cell(numFiles, 1);

    % Go through each of the files.
    if numFiles > 0
        fprintf('Importing data from:\n');
    end
    for fileNum = 1:numFiles

        rawKymoFilename = rawKymoFilenames{fileNum};
        rawKymoFilepath = rawKymoFilepaths{fileNum};

        fprintf('    %s\n', rawKymoFilename);
        rawKymo = double(imread(rawKymoFilepath));

        % Detect all the molecules in the file, returned as cell of struct
        rawKymoFileStruct = struct();
        rawKymoFileStruct.locs = [];
        rawKymoFileStruct.regions = [];
        rawKymoFileStruct.averagedImg = rawKymo';
        rawKymoFileStruct.fileName = rawKymoFilepath;

        moleculeStruct = struct();
        moleculeStruct.frames = [];
        moleculeStruct.kymograph = rawKymo;
        moleculeStruct.information = [];
        moleculeStruct.passesFilters = true;

        rawKymoFileStructs{fileNum} = rawKymoFileStruct;
        fileMoleculeStructs{fileNum}{1} = moleculeStruct;
    end

    fprintf('All kymograph(s) imported\n');

    hFigBpsPerPixel = figure('Name', 'Input Basepairs/Pixel');
    import FancyGUI.FancyPositioning.try_maximize_figure;
    try_maximize_figure(hFigBpsPerPixel);
    hPanelBpsPerPixel = uipanel(hFigBpsPerPixel);
    import OldDBM.General.Import.prompt_files_bps_per_pixel;
    [pixelsWidths_bps, errorMsg] = prompt_files_bps_per_pixel(rawKymoFilenames, hPanelBpsPerPixel);
    close(hFigBpsPerPixel);
    if not(isempty(errorMsg))
        disp(errorMsg);
        return;
    end
    pixelsWidths_bps = pixelsWidths_bps(:);

end