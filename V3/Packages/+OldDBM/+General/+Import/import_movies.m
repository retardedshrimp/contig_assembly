function [fileCells, fileMoleculeCells, pixelsWidths_bps] = import_movies(averagingWindowWidth)
    % IMPORT_MOVIES - Loads multipage tiff files ("movies"), 
    %	detects molecules in the movies, and stores the results
    %    in a few cell arrays
    %
    % Authors:
    %   Charleston Noble
    %   Tobias Ambjörnsson
    %   Saair Quaderi

    averagingWindowSideExtensionWidth = floor((averagingWindowWidth - 1)/2);

    % Get the filesile(dirpath, movieFilenames);

    import OptMap.DataImport.try_prompt_movie_filepaths;
    [aborted, movieFilepaths] = try_prompt_movie_filepaths();
    [~, movieFilenamesSansExt, movieFileExts] = cellfun(@fileparts, movieFilepaths, 'UniformOutput', false);
    if aborted
        fileCells = cell(0, 1);
        fileMoleculeCells = cell(0, 1);
        pixelsWidths_bps = zeros(0, 1) - 1;
        return;
    end
    movieFilenames = strcat(movieFilenamesSansExt, movieFileExts);
    
    numFiles = numel(movieFilepaths);

    % Get ready to save the molecule information.

    import OldDBM.General.Import.try_prompt_signal_threshold;
    % Ask user for a signal to noise ratio for accepting a detected molecule
    DEFAULT_SIGNAL_THRESHOLD = 10;
    [signalThreshold, errorMsg] = try_prompt_signal_threshold(DEFAULT_SIGNAL_THRESHOLD);
    if not(isempty(errorMsg))
        fileCells = cell(0, 1);
        fileMoleculeCells = cell(0, 1);
        pixelsWidths_bps = zeros(0, 1) - 1;
        disp(errorMsg);
        return;
    end


    %  Ask user for experimentally determined (using reference molecule)
    %  value for the number of basepairs per pixel for each file

    hFigBpsPerPixel = figure('Name', 'Input Basepairs/Pixel');
    import FancyGUI.FancyPositioning.try_maximize_figure;
    try_maximize_figure(hFigBpsPerPixel);
    hPanelBpsPerPixel = uipanel(hFigBpsPerPixel);
    import OldDBM.General.Import.prompt_files_bps_per_pixel;
    [pixelsWidths_bps, errorMsg] = prompt_files_bps_per_pixel(movieFilepaths, hPanelBpsPerPixel);
    pixelsWidths_bps = pixelsWidths_bps(:);
    close(hFigBpsPerPixel);
    if not(isempty(errorMsg))
        fileCells = cell(0, 1);
        fileMoleculeCells = cell(0, 1);
        pixelsWidths_bps = zeros(0, 1) - 1;
        disp(errorMsg);
        return;
    end

    import Microscopy.Import.import_grayscale_tiff_video;
    import OldDBM.MoleculeDetection.detect_molecules;
    % Go through each of the files.
    fileCells = cell(numFiles, 1);
    fileMoleculeCells = cell(numFiles, 1);
    for fileNum = 1:numFiles
        filename = movieFilenames{fileNum};
        srcTiffFilepath = movieFilepaths{fileNum};

        fprintf('Importing data from: %s\n', filename);

        % Detect all the molecules in the file, returned as cell of struct.

        [grayscaleVideo, ~, ~, ~] = import_grayscale_tiff_video(srcTiffFilepath);
        grayscaleVideo = permute(grayscaleVideo, [1 2 4 3]);
        [fileMoleculeCells{fileNum}, fileCells{fileNum}] = detect_molecules(grayscaleVideo, averagingWindowSideExtensionWidth, signalThreshold);
    end

    fprintf('All file(s) imported and molecules detected\n');
end