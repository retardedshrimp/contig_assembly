function [imgFgMask] = get_foreground_mask(rotatedAmplifiedMovie, numThresholds, minThresholdsForegroundMustPass)
    % First: segment the image.
    theshFrame = mean(rotatedAmplifiedMovie);
    thresholds = multithresh(theshFrame(:), numThresholds);
    theshFrame = mean(rotatedAmplifiedMovie, 3);
    imgQuantizedRegions = imquantize(theshFrame, thresholds);
    imgFgMask = imgQuantizedRegions > 1 + minThresholdsForegroundMustPass;

    % % TODO: remove
    % figure, imshow(meanRotatedMovieFrame)
    % figure, imshow(theshFrame)
    % figure, imshow(label2rgb(imgQuantizedRegions))
    % figure, imshow(imgFgMask)


    % Save only the largest connected components.
    cc = bwconncomp(imgFgMask);
    numPixels = cellfun(@numel, cc.PixelIdxList);
    idxs = find(numPixels < 50);
    for detectedChannelNum = 1:length(idxs)
        imgFgMask(cc.PixelIdxList{idxs(detectedChannelNum)}) = false;
    end
end