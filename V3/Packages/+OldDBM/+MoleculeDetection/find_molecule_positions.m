function [rowEdgeIdxs, colCenterIdxs] = find_molecule_positions(rotatedMovie, imgFgMask, signalThreshold)
    meanRotatedMovieFrame = mean(mean(rotatedMovie, 4), 3);

    % Set the background to zero and smooth for peak finding.
    filteredImgIntensity = meanRotatedMovieFrame;
    filteredImgIntensity = filteredImgIntensity .* imgFgMask;
    
    import OldDBM.MoleculeDetection.apply_gaussian_blur;
    smoothingWindowHsize = [min(50, size(filteredImgIntensity, 1)), min(3, size(filteredImgIntensity, 2))];
    filteredImgIntensity = apply_gaussian_blur(filteredImgIntensity, smoothingWindowHsize);

    % Find the channel coordinates.
    sumProfileForChannelDetection = sum(filteredImgIntensity,1);
    [ ~, detectedChannelCenterPosIdxs ] = findpeaks(sumProfileForChannelDetection);

    % Save the beginning and ending column coordinates for each molecule.

    numDetectedChannels = length(detectedChannelCenterPosIdxs);
    channelsRowEdgeIdxs = cell(numDetectedChannels, 1);
    channelsColCenterIdxs = cell(numDetectedChannels, 1);
    import OldDBM.MoleculeDetection.find_molecules_in_channel;
    for detectedChannelNum = 1:numDetectedChannels
        % Extract the channel intensity profile along the channel
        channelIntensityCurve = filteredImgIntensity(:, detectedChannelCenterPosIdxs(detectedChannelNum));

        % Locate the individual molecules
        [channelRowEdgeIdxs, curveRegionLabels] = find_molecules_in_channel(channelIntensityCurve, signalThreshold);

        channelsRowEdgeIdxs{detectedChannelNum} = channelRowEdgeIdxs;
        channelsColCenterIdxs{detectedChannelNum} = detectedChannelCenterPosIdxs(detectedChannelNum) + zeros(size(channelRowEdgeIdxs, 1));
    end
    rowEdgeIdxs = vertcat(channelsRowEdgeIdxs{:});
    colCenterIdxs = vertcat(channelsColCenterIdxs{:});
    

end