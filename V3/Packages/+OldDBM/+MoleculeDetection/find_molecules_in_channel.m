function [moleculeEdgeIdxs, channelMoleculeLabeling] = find_molecules_in_channel(channelIntensityCurve, signalThreshold, filterEdgeMolecules)
    % FIND_MOLECULES_IN_CHANNEL - given a 1D intensity curve (curve), and a threshold
    %	(signalThreshold), detects a molecule (assumed to be a continuous bright
    %	region which is brighter 'on average' than the signalTreshold.
    %
    % Inputs:
    %   channelIntensityCurve
    %     vector containing the intensity profile along the channel
    %   signalThreshold
    %     the intensity threshold for a region to be considered a
    %     molecule
    %   filterEdgeMolecules
    %     true if molecules found at the very start or very end should
    %     be filtered out and false otherwise
    %
    % Outputs: 
    %	moleculeEdgeIdxs
    %     Nx2 matrix where N is the number of molecules found and not
    %     filtered out and first column provides the start indices
    %     for the molecules and the second column provides the end
    %     indices for the molecules
    %	channelMoleculeLabeling
    %	  
    %
    % Authors:
    %   Charleston Noble
    %   Saair Quaderi

    if nargin < 3
        filterEdgeMolecules = true;
    end

    % Save the original curve we receive as input.
    moleculeEdgeIdxs = [];
    channelMoleculeLabeling = zeros(size(channelIntensityCurve));

    curveLabelNum = 1;

    channelIntensityCurve = max(0, channelIntensityCurve);
    posCurve = channelIntensityCurve ~= 0;
    curveLen = length(posCurve);

    % Disqualify molecules that run out of the field of view
    if filterEdgeMolecules
        if posCurve(1)
            idx = find(~posCurve, 1, 'first');
            clearRange = 1:idx;
            channelIntensityCurve(clearRange) = 0;
            posCurve(clearRange) = false;
        end
        if posCurve(end)
            idx = find(~posCurve, 1, 'last');
            clearRange = idx:curveLen;
            channelIntensityCurve(clearRange) = 0;
            posCurve(clearRange) = false;
        end
    end

    % If the discounted molecule was the only in the channel, continue.
    while any(posCurve)
        % Set everything to zero except the very first molecule.
        startPoint = find(posCurve, 1, 'first');
        for idx = startPoint:1:curveLen
            if not(posCurve(idx))
                clearRange = idx:curveLen;
                channelIntensityCurve(clearRange) = 0;
                posCurve(clearRange) = false;
            end
        end

        % Make a guess as to where the region starts and ends.
        b0 = find(posCurve, 1, 'first');
        c0 = find(posCurve, 1, 'last');

        % Starting point for the model fitting.
        p0 = [0, b0, c0, 0];
        aFittype = fittype('a + f * (tanh((x - b) ) - tanh((x - c) ))');
        fitObject = fit((1:length(channelIntensityCurve))', channelIntensityCurve, aFittype, 'StartPoint', p0);    

        % Get the coordinates back from the fitted model.
        moleculeEdgeIdxs = [moleculeEdgeIdxs; sort([ceil(fitObject.b), floor(fitObject.c)])]; %#ok<AGROW>
        signalAboveNoise = fitObject.f;

        % x = 1:1:length(curve);
        % fittedFunction = fitObject.a + fitObject.f * (tanh( (x - fitObject.b) ) - tanh( (x - fitObject.c) ));
        % figure;
        % plot(x, curve); hold on;
        % plot(x, fittedFunction, '--');

        % If there are inconsistencies in the region finding,
        % or the signal is too small then terminate 
        % the molecule detection method

        badRegionStart = moleculeEdgeIdxs(end, 1) < 1;
        badRegionEnd = moleculeEdgeIdxs(end, 2) > length(channelIntensityCurve);
        thresholdDeficit = max(0, signalThreshold - signalAboveNoise);
        if badRegionStart || badRegionEnd || (thresholdDeficit > 0)
            moleculeEdgeIdxs = moleculeEdgeIdxs(1:(end - 1), :);
            clearRange = 1:length(channelIntensityCurve);
            channelIntensityCurve(clearRange) = 0;
            posCurve(clearRange) = false;
        else
            % Extend the coordinates forward and backward until we hit a zero
            while (moleculeEdgeIdxs(end, 1) > 1) && posCurve(moleculeEdgeIdxs(end, 1) - 1)
                moleculeEdgeIdxs(end, 1) = moleculeEdgeIdxs(1) - 1;
            end
            while (moleculeEdgeIdxs(end, 2) < length(channelIntensityCurve)) && posCurve(moleculeEdgeIdxs(end, 2) + 1)
                moleculeEdgeIdxs(end, 2) = moleculeEdgeIdxs(end, 2) + 1;
            end

            moleculeEdgeIdxs(end,:) = [max(1, moleculeEdgeIdxs(end, 1)), min(length(channelIntensityCurve), moleculeEdgeIdxs(end, 2))];

            % Flatten this part of the curve so we don't find the same molecule
            %  again
            labelRange = moleculeEdgeIdxs(end, 1):moleculeEdgeIdxs(end, 2);
            if not(isempty(labelRange))
                channelMoleculeLabeling(labelRange) = curveLabelNum;
                clearRange = labelRange;
                channelIntensityCurve(clearRange) = 0;
                posCurve(clearRange) = false;
                curveLabelNum = curveLabelNum + 1;
            end
        end
    end
end