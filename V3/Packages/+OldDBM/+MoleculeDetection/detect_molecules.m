function [moleculeStructs, fileStruct] = detect_molecules(grayscaleVideo, averagingWindowSideExtensionWidth, signalThreshold)
    % DETECT_MOLECULES - takes a tif video file (with path fname), an averaging window
    %	width (typically 3 pixels), and a noise threshold (signalAboveNoise),
    %	and finds molecules in the movies. It returns a struct for each molecule.
    %
    % Inputs:
    %   grayscaleVideo
    %   averagingWindowSideExtensionWidth
    %   signalThreshold
    %
    % Outputs:
    %	moleculeStructs
    %     a cell with struct entries, one for each molecule detected
    %	rtnfileStruct
    %    a struct holding data at the file-level (rather than the
    %     molecule level)
    %
    % Authors:
    %   Charleston Noble
    %
    minVal = min(grayscaleVideo(:));
    maxVal = max(grayscaleVideo(:));
    grayscaleVideoRescaled = (grayscaleVideo - minVal)./(maxVal - minVal);
    function [amplificationFilterKernel] = get_amplification_filter_kernel()
        % square without center
        amplificationFilterKernel = ones(3,3);
        amplificationFilterKernel(floor((end +1)/2),floor((end +1)/2)) = 0;
        amplificationFilterKernel = amplificationFilterKernel./sum(amplificationFilterKernel(:));
    end
    amplificationFilterKernel = get_amplification_filter_kernel();

    amplifiedGrayscaleMovie = convn(grayscaleVideoRescaled, amplificationFilterKernel, 'same').*grayscaleVideoRescaled;

    import OldDBM.MoleculeDetection.get_angle;
    rotationAngle = get_angle(amplifiedGrayscaleMovie);


    rotatedMovie = grayscaleVideoRescaled;
    rotatedAmplifiedMovie = amplifiedGrayscaleMovie;
    ninetyDegRotations = round(rotationAngle/90);
    finetunedRotation = rotationAngle - ninetyDegRotations*90;
    % ninetyDegRotations = ninetyDegRotations + 1; % Orient Channels Vertically
    ninetyDegRotations = mod(ninetyDegRotations, 4);

    rotatedMovie = rot90(rotatedMovie, ninetyDegRotations);
    rotatedAmplifiedMovie = rot90(rotatedAmplifiedMovie, ninetyDegRotations);

    if finetunedRotation ~= 0
        warning('Movie data is being rotated via bilinear interpolation');
        rotatedMovie = imrotate(rotatedMovie, finetunedRotation, 'bilinear', 'crop');
        rotatedAmplifiedMovie = imrotate(rotatedAmplifiedMovie, finetunedRotation, 'bilinear', 'crop');
    end


    numThresholds = 2;
    minThresholdsForegroundMustPass = 1;
    import OldDBM.MoleculeDetection.get_foreground_mask;
    imgFgMask = get_foreground_mask(rotatedAmplifiedMovie, numThresholds, minThresholdsForegroundMustPass);


    disp('Detecting molecules...')

    import OldDBM.MoleculeDetection.find_molecule_positions;
    [rowEdgeIdxs, colCenterIdxs] = find_molecule_positions(rotatedMovie, imgFgMask, signalThreshold);


    numMoleculesDetected = size(rowEdgeIdxs, 1);

    fprintf('Detected a total of %d molecules.\n\n', numMoleculesDetected);

    % Extend the row regions for two reasons:
    %  1) so there is background space in the generated kymograph
    %  2) because we are detecting the molecules locations for the
    %     mean frame of the movie it is possible that some of the
    %     frames have the molecule further along the channel than
    %     was detected by the algorithm above
    rowSidePadding = 100;
    colSidePadding = averagingWindowSideExtensionWidth;
    rotatedMovieSz = size(rotatedMovie);

    import OldDBM.MoleculeDetection.get_molecule_movie_coords;
    [miniRotatedMoviesCoords] = get_molecule_movie_coords(rowEdgeIdxs, colCenterIdxs, rotatedMovieSz, rowSidePadding, colSidePadding);



    miniRotatedMovies = cellfun(@(miniMovieCoords) rotatedMovie(...
            miniMovieCoords(1,1):miniMovieCoords(1,2),...
            miniMovieCoords(2,1):miniMovieCoords(2,2),...
            miniMovieCoords(3,1):miniMovieCoords(3,2)...
            ), ...
            miniRotatedMoviesCoords, ...
            'UniformOutput', false);


    % rawFlatKymos = cellfun(@(miniMovie) permute(mean(miniMovie, 2), [3 1 2]), miniMovies, 'UniformOutput', false);

    import FancyUtils.merge_structs;
    defaultOldMoleculeStruct = struct( ...
            'frames', [], ...
            'kymograph', [], ...
            'information', [], ...
            'passesFilters', true ...
        );
    moleculeStructs = cellfun(...
        @(miniMovie) ...
        merge_structs( ...
            defaultOldMoleculeStruct, ...
            struct(...
                'frames', miniMovie, ...
                'kymograph', permute(mean(miniMovie, 2), [3 1 2]) ... % raw flat kymographs
                ) ...
            ), ...
        miniRotatedMovies, ...
        'UniformOutput', false);


    % Create a struct holding information for the overall file.
    fileStruct = struct( ...
        'locs', colCenterIdxs, ...
        'regions', rowEdgeIdxs, ...
        'averagedImg', meanRotatedMovieFrame, ...
        'fileName', srcTiffFilepath ...
    );
end