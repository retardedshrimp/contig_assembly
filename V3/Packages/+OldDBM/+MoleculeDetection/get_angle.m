function angle = get_angle(grayFrames)
    % GET_ANGLE
    %
    % Inputs:
    %	grayFrames
    %
    % Outputs:
    %	angle -the angle of the edges in the movie
    %
    % By: Charleston Noble
    % (Edited by Saair Quaderi)


    % Average the intensities
    averagedImg = mean(grayFrames,3);

    % Correct for uneven illumination.
    se = strel('disk', 12);
    averagedImg = imtophat(averagedImg, se);

    % Edge detection
    edgeAveragedImg = edge(averagedImg);

    % Hough transform.
    [H, theta, ~] = hough(edgeAveragedImg,'theta',-90:.01:89.99);

    % Find the peak pt in the Hough transform.
    peak = houghpeaks(H);

    % Optimal angle obtained from Hough peaks
    angle = mod(theta(peak(2)), 360);
end
