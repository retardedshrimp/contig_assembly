function [] = add_dbm_menu(hMenuParent, tsDBM)
    import OldDBM.General.DataWrapper;
    dbmODW = DataWrapper();

    import OldDBM.General.SettingsWrapper;
    dbmOSW = SettingsWrapper.import_dbm_settings_from_ini();

    hMenuDBM = uimenu( ...
        'Parent', hMenuParent, ...
        'Label', 'DBM');

    % Import menu
    hMenuImport = uimenu( ...
        'Parent', hMenuDBM, ...
        'Label','&Import');
    uimenu( ...
        'Parent', hMenuImport, ...
        'Label', 'Load Session Data', ...
        'Callback', @(~, ~) on_load_sess_data(dbmODW, dbmOSW, tsDBM));
    uimenu( ...
        'Parent', hMenuImport, ...
        'Label', 'Load Movie(s) (tif-format)', ...
        'Callback', @(~, ~) on_load_movies(dbmODW, tsDBM), 'Accelerator', 'L');
    uimenu( ...
        'Parent', hMenuImport, ...
        'Label', 'Load Raw Kymograph(s)', ...
        'Callback', @(~, ~) on_load_raw_kymos(dbmODW, dbmOSW, tsDBM));

    % Export menu
    import OldDBM.General.Export.DataExporter;
    dbmDE = DataExporter(dbmODW, dbmOSW);
    hMenuExport = uimenu( ...
        'Parent', hMenuDBM, ...
        'Label', '&Export');
    uimenu( ...
        'Parent', hMenuExport, ...
        'Label', 'Save Session Data', ...
        'Callback', {@(~, ~) dbmDE.export_dbm_session_struct_mat()});
    uimenu( ...
        'Parent', hMenuExport, ...
        'Label', 'Raw Kymographs', ...
        'Callback', @(~, ~) dbmDE.export_raw_kymos());
    uimenu( ...
        'Parent', hMenuExport, ...
        'Label', 'Aligned Kymographs', ...
        'Callback', @(~, ~) dbmDE.export_aligned_kymos());
    uimenu( ...
        'Parent', hMenuExport, ...
        'Label', 'Time Averages', ...
        'Callback', ...
        @(~, ~) dbmDE.export_aligned_kymo_time_avs());
    % uimenu( ...
    %     'Parent', hMenuExport, ...
    %     'Label', 'Molecule Statistics', ...
    %     'Callback', @(~, ~) DBM_Gui.export_molecule_analyses(dbmODW, dbmOSW));

    % Settings menu
    hMenuSettings = uimenu( ...
        'Parent', hMenuDBM, ...
        'Label','&Change Settings');
    uimenu( ...
        'Parent', hMenuSettings, ...
        'Label', 'Update Filter Settings', ...
        'Callback', {@(~, ~) dbmODW.update_filter_settings()});


    % Kymographs menu
    hMenuKymographs = uimenu( ...
        'Parent', hMenuDBM, ...
        'Label','&Kymographs');

    uimenu( ...
        'Parent', hMenuKymographs, ...
        'Label', 'Display &Raw Kymographs', ...
        'Callback', @(~, ~) on_display_raw_kymos(dbmODW, tsDBM), ...
        'Accelerator', 'R');
    uimenu( ...
        'Parent', hMenuKymographs, ...
        'Label', 'Display &Aligned Kymographs', ...
        'Callback', @(~, ~) on_display_aligned_kymos(dbmODW, tsDBM), ...
        'Accelerator', 'A');
    uimenu( ...
        'Parent', hMenuKymographs, ...
        'Label', 'Plot Time Averages', ...
        'Callback', @(~, ~) on_plot_kymo_time_averages(dbmODW, tsDBM), ...
        'Accelerator', 'T');

    % Statistics menu
    hMenuStatistics = uimenu( ...
        'Parent', hMenuDBM, ...
        'Label', '&Statistics');
    uimenu( ...
        'Parent', hMenuStatistics, ...
        'Label', 'Calculate molecule lengths and intensities', ...
        'Callback', @(~, ~) on_calc_molecule_lengths_and_intensity(dbmODW));

    import OldDBM.Kymo.UI.disp_raw_kymos_centers_of_mass;
    uimenu( ...
        'Parent', hMenuStatistics, ...
        'Label', 'Calculate Raw Kymo Centers of Mass', ...
        'Callback', @(~, ~) disp_raw_kymos_centers_of_mass(dbmODW, dbmOSW));

    import OldDBM.General.UI.plot_molecule_lengths_hist;
    uimenu( ...
        'Parent', hMenuStatistics, ...
        'Label','Plot Molecule Lengths', ...
        'Callback', @(~, ~) plot_molecule_lengths_hist(dbmODW));

    import OldDBM.General.UI.plot_infoscore_hist;
    uimenu( ...
        'Parent', hMenuStatistics, ...
        'Label','Plot InfoScore Hists', ...
        'Callback', @(~, ~) plot_infoscore_hist(dbmODW));



    function [] = on_update_home_screen(dbmODW, tsDBM)
        persistent hPanelHomescreen hTabHomescreen;
        if isempty(hPanelHomescreen) || not(isvalid(hPanelHomescreen))
            hTabHomescreen = tsDBM.create_tab('HomeScreen');
            hPanelHomescreen = uipanel('Parent', hTabHomescreen);
        end
        tsDBM.select_tab(hTabHomescreen);

        import OldDBM.General.UI.show_home_screen;
        show_home_screen(dbmODW, hPanelHomescreen);
    end

    function [] = on_display_raw_kymos(dbmODW, tsDBM)
        persistent hPanelRawKymos hTabRawKymos;
        if isempty(hPanelRawKymos) || not(isvalid(hPanelRawKymos))
            hTabRawKymos = tsDBM.create_tab('Raw Kymographs');
            hPanelRawKymos = uipanel('Parent', hTabRawKymos);
        end
        tsDBM.select_tab(hTabRawKymos);

        import OldDBM.Kymo.UI.display_raw_kymos;
        display_raw_kymos(dbmODW, hPanelRawKymos);
    end

    function [] = on_display_aligned_kymos(dbmODW, tsDBM)
        persistent hPanelAlignedKymos hTabAlignedKymos;
        if isempty(hPanelAlignedKymos) || not(isvalid(hPanelAlignedKymos))
            hTabAlignedKymos = tsDBM.create_tab('Aligned Kymographs');
            hPanelAlignedKymos = uipanel('Parent', hTabAlignedKymos);
        end
        tsDBM.select_tab(hTabAlignedKymos);

        import OldDBM.Kymo.UI.display_aligned_kymos;
        display_aligned_kymos(dbmODW, hPanelAlignedKymos);
    end

    function [] = on_plot_kymo_time_averages(dbmODW, tsDBM)
        persistent hPanelAlignedKymoTimeTraces hTabAlignedKymoTimeTraces;
        if isempty(hPanelAlignedKymoTimeTraces) || not(isvalid(hPanelAlignedKymoTimeTraces))
            hTabAlignedKymoTimeTraces = tsDBM.create_tab('Aligned Kymograph Time Traces');
            hPanelAlignedKymoTimeTraces = uipanel('Parent', hTabAlignedKymoTimeTraces);
        end
        tsDBM.select_tab(hTabAlignedKymoTimeTraces);

        import OldDBM.Kymo.UI.plot_kymo_time_averages;
        plot_kymo_time_averages(dbmODW, hPanelAlignedKymoTimeTraces);
    end

    function [] = on_load_sess_data(dbmODW, dbmOSW, tsDBM)
        defaultSessionDirpath = dbmOSW.get_default_import_dirpath('session');

        import OldDBM.General.Import.try_prompt_single_session_filepath;
        sessionFilepath = try_prompt_single_session_filepath(defaultSessionDirpath);

        if isempty(sessionFilepath)
            return;
        end

        import OldDBM.General.Import.try_loading_from_session_file;
        dbmODW2 = try_loading_from_session_file(sessionFilepath);

        dbmODW.update_data(dbmODW2);

        import OldDBM.General.SettingsWrapper;
        dbmOSW2 = SettingsWrapper.import_dbm_settings_from_session_path(sessionFilepath);
        dbmOSW.update_settings(dbmOSW2);

        on_update_home_screen(dbmODW, tsDBM);
    end

    function [] = on_load_movies(dbmODW, tsDBM)
        averagingWindowWidth = dbmODW.get_averaging_window_width();

        import OldDBM.General.Import.import_movies;
        [fileCells, fileMoleculeCells, pixelsWidths_bps] = import_movies(averagingWindowWidth);

        dbmODW.DBMMainstruct.fileCell = fileCells;
        dbmODW.DBMMainstruct.fileMoleculeCell = fileMoleculeCells;

        numFiles = length(fileCells);
        fileIdxs = (1:numFiles)';
        dbmODW.set_molecule_src_pixel_widths_in_bps(fileIdxs, pixelsWidths_bps);
        on_update_home_screen(dbmODW, tsDBM);
    end

    function [] = on_load_raw_kymos(dbmODW, dbmOSW, tsDBM)
        defaultRawKymoDirpath = dbmOSW.get_default_import_dirpath('raw_kymo');

        import OldDBM.General.Import.import_raw_kymos;
        [rawKymoFileStructs, fileMoleculeStructs, pixelsWidths_bps] = import_raw_kymos(defaultRawKymoDirpath);
        numFiles = length(rawKymoFileStructs);
        fileIdxs = (1:numFiles)';
        dbmODW.DBMMainstruct.fileCell = rawKymoFileStructs;
        dbmODW.DBMMainstruct.fileMoleculeCell = fileMoleculeStructs;
        dbmODW.verify_thresholds();
        dbmODW.set_molecule_src_pixel_widths_in_bps(fileIdxs, pixelsWidths_bps);
        on_update_home_screen(dbmODW, tsDBM);
    end

    function [] = on_calc_molecule_lengths_and_intensity(dbmODW)

        import OldDBM.Kymo.UI.prompt_kymo_analysis_method;
        [kymoAnalysisMethod, shouldSaveTF] = prompt_kymo_analysis_method();
        switch kymoAnalysisMethod
            case 'basic_otsu_edge_detection'
                skipDoubleTanhAdjustmentTF = true;
            case 'double_tanh_edge_detection'
                skipDoubleTanhAdjustmentTF = false;
            otherwise
                return;
        end
        import OldDBM.Kymo.UI.run_calc_plot_save_kymo_analysis;
        run_calc_plot_save_kymo_analysis(dbmODW, skipDoubleTanhAdjustmentTF, shouldSaveTF)
    end

end
