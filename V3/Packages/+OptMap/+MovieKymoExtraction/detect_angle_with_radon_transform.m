function [angle] = detect_angle_with_radon_transform(img, numAngleCandidates)
    % RADON_ROTATE - find the angle of parallel lines in the image
    % 
    % Inputs:
    %   img
    %     the image in which to find parallel lines
    %   numAngleCandidates
    %     the number of angle candidates at which to check for parallel
    %     lines in the image (spread equally spaced in range [-90, 90) )
    %
    % Outputs:
    %   angle
    %     the angle of the detected parallel lines in degrees
    %     will be in range [-90, 90)
    %
    % Authors:
    %  Saair Quaderi
    
    
    theta = -90 + 180*(0:(numAngleCandidates - 1))./numAngleCandidates;
    R = radon(img, theta);
    
    % [R, xp] = radon(img, theta);
    % figure
    % imagesc(theta, xp, R);
    % colormap(hot);
    % xlabel('\theta (degrees)'); ylabel('x\prime');
    % title('R_{\theta} (x\prime)');
    % colorbar
    
    varR = var(R);
    varRFirstDeriv = diff([varR(end), varR]);
    varRSecondDeriv = diff([varRFirstDeriv(end), varRFirstDeriv]);
    
    % figure
    % plot(theta, [zscore(varR(:)), zscore(varRFirstDeriv(:)), zscore(varRSecondDeriv(:))]);
    % xlabel('\theta (degrees)'); ylabel('x\prime');
    
    [~, thetaIdx] = max(varRSecondDeriv);
    thetaVal = theta(thetaIdx);
    
%     figure
%     imshow(imrotate(img, -thetaVal))
    % figure
    % plot(xp, R(:, thetaIdx))
    
    angle = -thetaVal;
end