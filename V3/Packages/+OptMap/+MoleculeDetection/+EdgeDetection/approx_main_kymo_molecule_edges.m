function [moleculeStartEdgeIdxs, moleculeEndEdgeIdxs, mainKymoMoleculeMask] = approx_main_kymo_molecule_edges(kymo, edgeDetectionSettings)
    % APPROX_MAIN_KYMO_MOLECULE_EDGES - Attempts to find the start and
    %  end indices for the main molecule in the kymograph
    %
    % For more details see:
    %  See basic_otsu_approx_main_kymo_molecule_edges
    %  See adjust_kymo_edge_detection
    %
    % Inputs:
    %   kymo
    %   kymoEdgeDetectionSettings
    %   
    % Outputs:
    %   moleculeStartEdgeIdxsApprox
    %   moleculeEndEdgeIdxsApprox
    %   mainKymoMoleculeMaskApprox
    %
    % Authors:
    %   Saair Quaderi
    %     (refactoring)
    %   Charleston Noble
    %     (original version, algorithm)
    
    import OptMap.MoleculeDetection.EdgeDetection.basic_otsu_approx_main_kymo_molecule_edges;
    import OptMap.MoleculeDetection.EdgeDetection.DoubleTanh.adjust_kymo_edge_detection;

    otsuApproxSettings = edgeDetectionSettings.otsuApproxSettings;
    [moleculeStartEdgeIdxsFirstApprox, moleculeEndEdgeIdxsFirstApprox, mainKymoMoleculeMaskFirstApprox] = basic_otsu_approx_main_kymo_molecule_edges(...
        kymo, ...
        otsuApproxSettings.smoothingWindowLen, ...
        otsuApproxSettings.imcloseHalfGapLen, ...
        otsuApproxSettings.numThresholds, ...
        otsuApproxSettings.minNumThresholdsFgShouldPass ...
    );

    if edgeDetectionSettings.skipDoubleTanhAdjustment
        moleculeStartEdgeIdxs = moleculeStartEdgeIdxsFirstApprox;
        moleculeEndEdgeIdxs = moleculeEndEdgeIdxsFirstApprox;
        mainKymoMoleculeMask = mainKymoMoleculeMaskFirstApprox;
    else
        tanhSettings = edgeDetectionSettings.tanhSettings;
        [moleculeStartEdgeIdxs, moleculeEndEdgeIdxs, mainKymoMoleculeMask] = adjust_kymo_edge_detection(...
            kymo, ...
            moleculeStartEdgeIdxsFirstApprox, ...
            moleculeEndEdgeIdxsFirstApprox, ...
            tanhSettings ...
        );
    end
end