function [moleculeStartEdgeIdxsApprox, moleculeEndEdgeIdxsApprox, mainKymoMoleculeMaskApprox] = basic_otsu_approx_main_kymo_molecule_edges(kymo, smoothingWindowLen, imcloseHalfGapLen, numThresholds, minNumThresholdsFgShouldPass)
    % BASIC_OTSU_APPROX_MAIN_KYMO_MOLECULE_EDGES - Attempts to find the start and
    %  end indices for the main molecule in the kymograph
    %
    %  Uses Otsu's method,
    %  some morphological operations, and component analysis,
    %  to try to separate the foreground from the background and find the
    %  "main" (i.e. largest) contiguous foreground component in each row
    %  (i.e. time frame) represented in the provided kymograph
    %
    % Inputs:
    %   kymo
    %   smoothingWindowLen (optional, defaults to 1)
    %   imcloseHalfGapLen (optional, defaults to 0)
    %   numThresholds (optional, defaults to 1)
    %   minNumThresholdsFgShouldPass (optional, defaults to 1)
    %   
    % Outputs:
    %   moleculeStartEdgeIdxsApprox
    %   moleculeEndEdgeIdxsApprox
    %   mainKymoMoleculeMaskApprox
    %
    % Authors:
    %   Saair Quaderi
    %     (refactoring)
    %   Charleston Noble
    %     (original, algorithm)


    if nargin < 2
        smoothingWindowLen = 1;
    end

    if nargin < 3
        imcloseHalfGapLen = 0;
    end

    if nargin < 4
        numThresholds = 1;
    end

    if nargin < 5
        minNumThresholdsFgShouldPass = 1;
    end

    imcloseNhood = true(1 + 2*imcloseHalfGapLen, 1);
    numFrames = size(kymo, 1);
    mainKymoMoleculeMaskApprox = false(size(kymo));
    for frameNum = 1:numFrames
        kymoFrameRow = kymo(frameNum, :);

        kymoFrameRowVer2 = kymoFrameRow;
        if smoothingWindowLen > 1
            kymoFrameRowVer2 = smooth(kymoFrameRowVer2, smoothingWindowLen);
        end
        
        thresholdsArr = multithresh(kymoFrameRowVer2, numThresholds);
        nonoverflowingFgMaskApproxCurve = imquantize(kymoFrameRowVer2, thresholdsArr) >= (1 + minNumThresholdsFgShouldPass);

        %% plot(kymoFrameRowVer2), hold on, plot(nonoverflowingFgMaskApproxCurve.*max(kymoFrameRowVer2))
        % First, set to zero molecules that run out of the field of view.
        if nonoverflowingFgMaskApproxCurve(1)
            idx = find(~nonoverflowingFgMaskApproxCurve, 1, 'first');
            nonoverflowingFgMaskApproxCurve(1:idx) = false;
        end

        if nonoverflowingFgMaskApproxCurve(end)
            idx = find(~nonoverflowingFgMaskApproxCurve, 1, 'last');
            nonoverflowingFgMaskApproxCurve(idx:end) = false; 
        end

        % Remove small gaps.
        if imcloseHalfGapLen > 0
            nonoverflowingFgMaskApproxCurve = imclose(nonoverflowingFgMaskApproxCurve, imcloseNhood);
        end
        
        % Get the largest molecule.
        labeledFgMaskApproxCurve = bwlabel(nonoverflowingFgMaskApproxCurve);
        ccStats = regionprops(nonoverflowingFgMaskApproxCurve);
        [~, largestRegionLabel] = max([ccStats.Area]);
        mainMoleculeCurveMaskApprox = (labeledFgMaskApproxCurve == largestRegionLabel);

        mainKymoMoleculeMaskApprox(frameNum, :) = mainMoleculeCurveMaskApprox;
    end
    frameNums = (1:numFrames)';
    
    moleculeStartEdgeIdxsApprox = arrayfun(@(frameNum) find(mainKymoMoleculeMaskApprox(frameNum, :), 1, 'first'), frameNums);
    moleculeEndEdgeIdxsApprox = arrayfun(@(frameNum) find(mainKymoMoleculeMaskApprox(frameNum, :), 1, 'last'), frameNums);
end