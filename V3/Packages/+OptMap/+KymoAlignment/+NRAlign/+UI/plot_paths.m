function [] = plot_paths(hAxis, inpImg, paths)
    % PLOT_PATHS - Debugging function for plotting paths (features) on top of a kymograph.
    %
    % Inputs:
    %  hAxis
    %   handle for axis in which to plot paths
    %  inpImg
    %   normalized kymograph, preferably the one in which the features have
    %   been found.
    %  paths
    %   the output alignXVals from the function findsinglefeature of find_k_features, 
    %   which contain the  paths for each feature to be straightened.
    %
    % Outputs:
    %  none
    % 
    % Authors: Henrik Nordanger

    %normalize so it's all visible
    inpImg = inpImg - min(inpImg(:));
    inpImg = inpImg./max(inpImg(:));
    
    %Each pixel in inpImg that is crossed by a path, is set to 2.
    for rowIdx = 1:size(paths,1)
        for colIdx = 1:size(paths,2)  
            inpImg(rowIdx, paths(rowIdx,colIdx)) = 2;
        end  
    end

    %An image is shown, where the paths are twice as bright as the
    % maximm of the original kymograph.
    axes(hAxis);
    imshow(inpImg);
end