function outImg = shift_rows(inpImg, shifts)  
    % SHIFT_ROWS - Shifts the rows in a kymograph without stretching it.
    %
    % Inputs:
    %  inpImg
    %   raw kymograph
    %  shifts
    %   array containing the shift to be performed of each row
    %
    % Outputs:
    %  outImg
    %   kymograph with rows shifted
    %
    % Authors:
    %  Henrik Nordanger


    rows = size(inpImg,1);
    cols = size(inpImg,2);
    cols_two = cols * 2;

    %Check if the two input arguments are compatible
    if size(shifts)~=size(inpImg,1)-1
        disp('The number of specified row shifts do not match the number of rows');
    elseif (max(shifts) > cols) || (min(shifts) < -cols)
        disp('Row shift values are outside acceptable range');
    else
        % NaN values are added at the sides of the kymograph, to make
        %  room for any shifted row
        inpImg = padarray(inpImg, [0, cols], NaN);

        % Each row (except the first) is investigated.
        for row = 2:rows
            % The present row is temporarily and separately saved
            tempRow = inpImg(row, (cols + 1):cols_two);

            % The whole row is set to NaN
            inpImg(row,cols+1:cols_two) = NaN(1, cols);

            % The temporarily saved row is placed where specified by
            %  therelevant element of 'shifts'
            inpImg(row, (cols + 1 + shifts(row - 1)):(cols_two + shifts(row - 1))) = tempRow;
        end

        %The relevant part of the resulting kymograph is returned
        outImg = inpImg(:,(min(shifts) + cols + 1):(max(shifts) + cols_two));
    end
end