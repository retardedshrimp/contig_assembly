function [shifts] = get_shift_alignments(img)
    % GET_SHIFT_ALIGNMENTS - Calculates the optimal shifting of each
    %   row for aligning a kymograph, without stretching it
    %
    % Inputs:
    %  img
    %   input image to perform shift alignment on (e.g.  a
    %    kymograph with each row representing a timeframe)
    %
    % Outputs:
    %   shifts
    %    the optimal shifting of each row relative to row 1
    %    (with positive values meaning shifting right)
    %
    % 
    % Authors:
    %   Henriks Nordanger

    % The maximum shifting of each row
    maxShift = 3;

    numRows = size(img,1);
    numCols = size(img,2);

    % The mean intensity is subtracted from each pixel
    img = img - mean(img(:));

    % Array for containing the horizontal position of each row, is
    % pre-allocated. The postion of the first row equals 'cols'
    horPosition = zeros(1, numRows);
    horPosition(1) = numCols;

    % Each row (except the first) is investigated
    for rowIdx = 2:numRows

        % The cross-correlation between row 1 and the present row is
        %  calculated
        xCorrs = xcorr(img(1,:), img(rowIdx,:));

        % The positioning of the present row, relative to previous row,
        %  is optimized to fit the first row
        [~,horPosition(rowIdx)] = max(xCorrs(horPosition(rowIdx-1)-maxShift:horPosition(rowIdx-1)+maxShift));

        % The absolute positioning of the present row is found
        horPosition(rowIdx) = horPosition(rowIdx) + horPosition(rowIdx-1) - (maxShift - 1);

    end

    % The shifting of row 2 and down are calculated
    shifts = horPosition - numCols;
    shifts = shifts(2:end);
end