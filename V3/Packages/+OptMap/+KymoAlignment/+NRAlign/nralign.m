function [alignedKymo, stretchFactorsMat] = nralign(unalignedKymo, leftColIdx, rightColIdx)
    % NRALIGN - non-recursively aligns a kymograph, using a modified
    %  wpalign algorithm.
    %
    % Inputs:
    %	unalignedKymo
    %	  the unaligned kymograph
    %	leftColIdx
    %	  the index of the left boundary for alignment
    %	rightColIdx
    %	  the index of the right boundary for alignment
    %
    % Outputs:
    %	alignedKymo
    %	  the aligned kymograph is output in the last step
    %   stretchFactorsMat
    %	  array of the stretch factor of each pixel in the kymograph
    %
    % Authors:
    %	Henrik Nordanger
    %   Saair Quaderi (minor refactoring)

    % The number of pixels to ignore at the edges of the kymograph
    unstretchedSIdeWidth = 10;
    
    % The number of pixels around each feature on each side (not including
    % the pixel the feature is on) on which there may not be another
    % feature
    W_trim = 5;
    typicalFeatureWidth = (2 * W_trim) + 1;

    % The maximum number of pixels a feature can move from frame to next
    % frame
    maxFeatureMovementPx = 3;


    % The edges of the region in which features are to be found are
    %  specified, unless already done in the call to the function.
    if nargin < 2
        leftColIdx = 1 + unstretchedSIdeWidth;
        rightColIdx = size(unalignedKymo, 2) - unstretchedSIdeWidth;
    end


    % The (maximum) number of features to look for "k"
    maxNumFeaturesSoughtK = ceil((rightColIdx - leftColIdx + 1) / ((2 * W_trim) + 1));

    % The kymograph is smoothed out in order to reduce noise
    squareSmoothingWindowLen_pixels = 10;
    blurSigmaWidth_pixels = 2;
    import OptMap.KymoAlignment.apply_gaussian_blur;
    smoothImg = apply_gaussian_blur(unalignedKymo(:,leftColIdx:rightColIdx), squareSmoothingWindowLen_pixels, blurSigmaWidth_pixels);


    %The kymograph is "prealigned" using shift-based alignment
    import OptMap.KymoAlignment.NRAlign.get_shift_alignments;
    shifts = get_shift_alignments(smoothImg);
    
    import OptMap.KymoAlignment.NRAlign.shift_rows;
    unalignedKymo = shift_rows(unalignedKymo,shifts);
    
    %The prealigned kymograph is smoothed out
    smoothImg = apply_gaussian_blur(unalignedKymo, squareSmoothingWindowLen_pixels, blurSigmaWidth_pixels);


    %The laplacian of gaussian is obtained and normalized
    import OptMap.KymoAlignment.apply_laplacian_of_gaussian_filter;
    smoothImg = apply_laplacian_of_gaussian_filter(smoothImg, [2, 6], 2);
    smoothImg = smoothImg ./ max(abs(smoothImg(:)));

    
    %The k shortest paths through the (prealigned) kymograph are found
    import OptMap.KymoAlignment.find_k_features;
    alignXVals = find_k_features(smoothImg(:, (leftColIdx:rightColIdx)), maxFeatureMovementPx, typicalFeatureWidth, maxNumFeaturesSoughtK); 
    alignXVals = horzcat(alignXVals{:, 1});


    %All features are stretched, finishing the alignment.
    import OptMap.KymoAlignment.stretch_image;
    [alignedKymo, stretchFactorsMat] = stretch_image(unalignedKymo, alignXVals + leftColIdx);


    % % For debugging reasons, the paths can be drawn in the (unaligned)
    % % kymograph.
    % hFig = figure();
    % hPanel = uipanel('Parent', hFig);
    % hAxis = axes('Parent', hPanel);
    % import OptMap.KymoAlignment.NRAlign.UI.plot_paths;
    % plot_paths(hAxis, unalignedKymo, alignXVals+unstretchedSIdeWidth);
end
