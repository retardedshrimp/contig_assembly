function [alignedKymo, alignedBlurredKymo, featureCountOut] = wpalign_helper(...
        kymo, ...
        blurredKymo, ...
        featureCountIn, ...
        maxFeatureMovementPerRow_pixels, ...
        leftX, ...
        rightX, ...
        unstretchedSideWidth, ...
        featureHalfWidth, ...
        barrierVal ...
    )
    % wpalign_helper
    %
    % Inputs:
    %	kymo
    %	  the kymograph
    %	blurredKymo
    %	  the filtered version of the kymograph
    %	featureCountIn
    %	  the number of features already found
    %	maxFeatureMovementPerRow_pixels
    %	  the maximum n
    %	leftX
    %	  the left boundary for alignment
    %	rightX
    %	  the right boundary for alignment
    %	unstretchedSideWidth
    %	  how many pixels from leftX and rightX are not included in
    %      the stretching process
    %  featureHalfWidth
    %	  how many pixels from a found feature the left/right
    %      boundaries in subsequent recursive calls to this function
    %  barrierVal
    %    how high a feature path cost/dist is permissible (before it is
    %    ignored)
    % 
    % Outputs:
    %   alignedKymo
    %	  the kymograph after the round of alignment
    %   blurredKymo
    %	  the blurred kymograph after the round of alignment
    %   featureCount
    % 	  the number of features found after the round of alignment
    % 
    % TODO: switch from recursive function call approach to iterative method
    %  using iterative loop with a queue of alignment ranges
    %
    % See ZZZ.WPAlign.wp_align
    %
    % Authors: 
    %	Charleston Noble
    %   Saair Quaderi (clean up, removal of globals)


    if (rightX - leftX + 1) <= 25
        alignedKymo = kymo;
        alignedBlurredKymo = blurredKymo;
        featureCountOut = featureCountIn;
        return;
    end

    partialBlurredKymo = blurredKymo(:, leftX:rightX);

    [numPartialRows, numPartialCols] = size(partialBlurredKymo);

    maxFeatureMovementTotal_pixels = maxFeatureMovementPerRow_pixels * numPartialRows;
    if numPartialCols < maxFeatureMovementTotal_pixels
        import OptMap.KymoAlignment.WPAlign.find_single_feature;
        [featurePathsVect, lowestFeaturePathCost] = find_single_feature(partialBlurredKymo, maxFeatureMovementPerRow_pixels, barrierVal);

    else
        numWindows = ceil(numPartialCols / maxFeatureMovementTotal_pixels) * 2 - 1;
        nbr = ceil(numPartialCols / maxFeatureMovementTotal_pixels);
        windowLen = round(ceil(numPartialCols / nbr)/2);

        featurePathsMat = zeros(numPartialRows,numWindows);
        featurePathCosts = zeros(1,numWindows);

        partialsPartialBlurredKymo = cell(numWindows,1);
        windowStartArr = 1 + (((1:numWindows) - 1) * windowLen);
        windowEndArr =  max(numPartialCols, 1 + ((1:numWindows) * windowLen));


        for windowNum = 1:numWindows
            windowStartCol = windowStartArr(windowNum);
            windowEndCol = windowEndArr(windowNum);

            partialsPartialBlurredKymo{windowNum} = partialBlurredKymo(:, windowStartCol:windowEndCol);
        end

        import OptMap.KymoAlignment.WPAlign.find_single_feature;
        for windowNum = 1:numWindows
            windowStartCol = windowStartArr(windowNum);

            [featurePathsVect, dist] = find_single_feature(partialsPartialBlurredKymo{windowNum}, maxFeatureMovementPerRow_pixels, barrierVal);
            featurePathsVect = featurePathsVect + windowStartCol;
            featurePathsMat(:, windowNum) = featurePathsVect;
            featurePathCosts(windowNum) = dist;     
        end

        [featurePathCosts, sortOrder] = sort(featurePathCosts);
        featurePathsMat = featurePathsMat(:, sortOrder);

        featurePathsVect = featurePathsMat(:, 1);
        lowestFeaturePathCost = featurePathCosts(1);
    end
    
    alignedKymo = kymo;
    alignedBlurredKymo = blurredKymo;
    featureCountOut = featureCountIn;
    if lowestFeaturePathCost >= barrierVal
        return;
    end

    import OptMap.KymoAlignment.stretch_image;
    colIdxs = (leftX - unstretchedSideWidth):(rightX + unstretchedSideWidth);
    stretchMat = featurePathsVect + unstretchedSideWidth;
    featureCountOut = featureCountOut + 1;
    alignedKymo(:, colIdxs) = stretch_image(alignedKymo(:, colIdxs), stretchMat);
    alignedBlurredKymo(:, colIdxs) = stretch_image(alignedBlurredKymo(:, colIdxs), stretchMat);

    featurePathCol = round(mean(featurePathsVect)) + leftX - 1;

    % Recursive calls
    import OptMap.KymoAlignment.WPAlign.wpalign_helper;
    [alignedKymo, alignedBlurredKymo, featureCountOut] = wpalign_helper(alignedKymo, alignedBlurredKymo, featureCountOut, maxFeatureMovementPerRow_pixels, leftX, (featurePathCol - featureHalfWidth), unstretchedSideWidth, featureHalfWidth, barrierVal);
    [alignedKymo, alignedBlurredKymo, featureCountOut] = wpalign_helper(alignedKymo, alignedBlurredKymo, featureCountOut, maxFeatureMovementPerRow_pixels, (featurePathCol + featureHalfWidth), rightX, unstretchedSideWidth, featureHalfWidth, barrierVal);
end