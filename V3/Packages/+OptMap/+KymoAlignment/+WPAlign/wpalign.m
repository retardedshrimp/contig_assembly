function [alignedKymo] = wpalign(unalignedKymo)
    % WPALIGN - recursively aligns a kymograph (see the WPAlign paper)
    %
    % Inputs:
    %	unalignedKymo
    %	  the unaligned kymograph (the only input to the initial fcn call)
    %
    % Outputs: 
    %	alignedKymo
    %	  the aligned kymograph is output in the last step
    %
    % Authors: 
    %	Charleston Noble
    %   Saair Quaderi (clean up, removal of globals)

    
    featureCount = [];
    
    
    unstretchedSideWidth = 5;
    featureHalfWidth = 5;
    barrierVal = 10000;
    maxFeatureMovementPerRow_pixels = 3;

    smoothWindow = 10;
    import OptMap.KymoAlignment.apply_gaussian_blur;
    alignedKymo = unalignedKymo;
    blurredKymo = apply_gaussian_blur(unalignedKymo, smoothWindow);
    
    
    leftX = 1 + unstretchedSideWidth;
    rightX = size(unalignedKymo, 2) - unstretchedSideWidth;
    import OptMap.KymoAlignment.WPAlign.wpalign_helper;
    % TODO: switch from recursive function call approach to iterative method
    %  using iterative loop with a queue of alignment ranges
    %
    % See ZZZ.WPAlign.wp_align
    [alignedKymo, ~, ~] = wpalign_helper(...
        alignedKymo, ...
        blurredKymo, ...
        featureCount, ...
        maxFeatureMovementPerRow_pixels, ...
        leftX, ...
        rightX, ...
        unstretchedSideWidth, ...
        featureHalfWidth, ...
        barrierVal ...
        );
    
end