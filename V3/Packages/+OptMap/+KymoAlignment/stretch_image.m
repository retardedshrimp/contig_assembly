function [imgStretched, stretchFactorsMat] = stretch_image(img, stretchMat)
    % STRETCH_IMAGE - interpolates/stretches an image to align features feature
    %
    % Inputs:
    %   img
    %     the image to stretch
    % 
    %   stretchMat
    %     the output alignXVals from the function findsinglefeature of
    %      find_k_features, which contain the  paths for each feature
    %      to be straightened
    %
    % Outputs:
    %   imgStretched
    %     the stretched image
    %   stretchFactorsMat
    %     matrix containing the stretch factor of each pixel
    % 
    % Authors:
    %   Saair Quaderi (2016- 11): Improved NaN handling
    %   Henrik Nordanger (2016-10): Generalized to stretch more than one
    %     path
    %   Charleston Noble
    %

    numRows = size(img, 1);
    numCols = size(img, 2);


    %The paths are sorted.
    stretchMat = sortrows(stretchMat');
    stretchMat = stretchMat';

    %The number of features is obtained.
    numFeatures = size(stretchMat, 2);

    %The mean position of each feature is determined.
    xMean = zeros(1, numFeatures);
    for featureNum = 1:numFeatures
        xMean(featureNum) = mean(stretchMat(:, featureNum));
    end

    %Array for containing the stretch factor for each "gap" between
    %features is pre-allocated.
    stretchFactors = zeros(numRows,numFeatures+1);
    stretchFactorsMat = ones(numRows,numCols);

    for rowNum = 1:numRows

        stretchRow = stretchMat(rowNum, :);                  
        %cmpXValDiffs = ones(1,cols);


        %The first stretch factor for the current row is determined, and 
        %assigned to the relevant elements of 'cmpXValDiffs'
        stretchFactors(rowNum, 1) = xMean(1) / stretchRow(1);
        stretchFactorsMat(rowNum, (1:stretchRow(1))) = stretchFactors(rowNum, 1);

        %The stretch factors between all features are determined, and 
        %assigned to the relevant elements of 'cmpXValDiffs'
        for featureNum = 2:numFeatures
            stretchFactors(rowNum,featureNum) = (xMean(featureNum) - xMean(featureNum - 1)) / (stretchRow(featureNum) - stretchRow(featureNum - 1));
            stretchFactorsMat(rowNum, (stretchRow(featureNum-1) + 1):stretchRow(featureNum)) = stretchFactors(rowNum, featureNum);    
        end

        %The last stretch factor for the current row is determined, and 
        %assigned to the relevant elements of 'cmpXValDiffs'
        stretchFactors(rowNum, end) = (numCols - xMean(end)) / (numCols - stretchRow(end));
        stretchFactorsMat(rowNum, (stretchRow(end) + 1):end) = stretchFactors(rowNum, end);

        cmpXVals = cumsum(stretchFactorsMat(rowNum, :));

        %The aligned row is created, by linear "stretching" using the 
        %cumuative sum of the stretch factors of each pixel
        imgRow = img(rowNum,:);
        oldNonnanMask = not(isnan(imgRow));
        nonnanVals = imgRow(oldNonnanMask);
        tmp = 1:length(imgRow);
        imgRowFilled = mean([...
            interp1(tmp(oldNonnanMask), nonnanVals, tmp, 'nearest', 'extrap'); ...
            fliplr(interp1(tmp(fliplr(oldNonnanMask)), fliplr(nonnanVals), tmp, 'nearest', 'extrap'))], ...
            1); % fill NaNs with the nearest nonnan value (average of nearest nonnan values if nearness is a tie)
        cmpIntensityVals = interp1(cmpXVals, imgRowFilled, 1:numCols, 'spline');
        newNanMask = interp1(cmpXVals, double(oldNonnanMask), 1:numCols, 'nearest') < 1;
        cmpIntensityVals(newNanMask) = NaN;

        img(rowNum, :) = cmpIntensityVals;
    end

    imgStretched = img;
end