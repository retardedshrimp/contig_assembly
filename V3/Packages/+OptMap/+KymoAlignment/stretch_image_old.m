function [imgStretched] = stretch_image_old(img, numColsLeftVect)
    % STRETCH_IMAGE_OLD - interpolates/stretches an image to align a single feature
    %
    % Inputs:
    %   imgMat
    %     the image to stretch
    %   numColsLeftVect
    %     the output alignXVals from the function
    %
    % Outputs:
    %   imgMat
    %     the stretched image
    % 
    % Authors:
    %   Charleston Noble
    %   Ssaair Quaderi (refactoring)

    numRows = size(img, 1);
    numCols = size(img, 2);

    revisedNumColsLeft = mean(numColsLeftVect);
    revisedNumColsRight = numCols - revisedNumColsLeft;

    imgStretched = img;
    for rowNum = 1:numRows
        numColsLeft = numColsLeftVect(rowNum);
        numColsRight = numCols - numColsLeft;

        colCoordsX = cumsum([ ...
            repmat(revisedNumColsLeft / numColsLeft, 1, numColsLeft), ...
            repmat(revisedNumColsRight / numColsRight, 1, numColsRight) ...
            ]);

        imgRowOld = img(rowNum, :);
        imgRowNew = interp1(colCoordsX, imgRowOld, 1:numCols, 'spline');
        imgStretched(rowNum,:) = imgRowNew;
    end
end