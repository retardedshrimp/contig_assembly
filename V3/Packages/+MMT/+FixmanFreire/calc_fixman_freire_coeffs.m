function [vectA, vectB] = calc_fixman_freire_coeffs()
    import MMT.FixmanFreire.prompt_num_iterations;
    import MMT.FixmanFreire.prompt_num_expansion_terms;
    import MMT.FixmanFreire.prompt_loop_exponent;
    import MMT.FixmanFreire.prompt_n_vect;
    import MMT.FixmanFreire.prompt_should_continue_fitting;
    import MMT.FixmanFreire.improve_fit;


    [aborted, numIterations] = prompt_num_iterations();
    if aborted
        return;
    end
    
    [aborted, loopExponent] = prompt_loop_exponent();
    if aborted
        return;
    end
    
    [aborted, numExpansionTerms] = prompt_num_expansion_terms();
    if aborted
        return;
    end
    
    [aborted, vectN] = prompt_n_vect(numExpansionTerms);
    if aborted
        return;
    end
    
    
    shouldContinue = true;
    vectA = zeros(numExpansionTerms, 1);
    vectB = zeros(numExpansionTerms, 1);
    strIdxVect = arrayfun(@num2str, (1:numExpansionTerms)', 'UniformOutput', false);
    
    
    while shouldContinue
        for iterationNum = 1:numIterations
            [vectA, vectB] = improve_fit(vectA, vectB, vectN, loopExponent);
        end
        strVectA = arrayfun(@(a) sprintf('%.20f', a), vectA, 'UniformOutput', false);
        strVectB = arrayfun(@(b) sprintf('%.20f', b), vectB, 'UniformOutput', false);
        tableAB = table(strVectA, strVectB, 'VariableNames', {'a', 'b'}, 'RowNames', strIdxVect);
        disp(tableAB);
        shouldContinue = prompt_should_continue_fitting();
    end
end