function [bindingExpectedMask, numberOfBindings] = find_sequence_matches(bindingNtSequence, ntSequence)
    bindingSequenceRC = seqrcomplement(bindingNtSequence);
    bindingExpectedMask = false(size(ntSequence));
    bindingSeqLen = length(bindingNtSequence);
    numberOfBindings = 0;
    bpLastStartIdx = length(ntSequence) - bindingSeqLen;
    bpStartIdx = 1;
    while bpStartIdx <= bpLastStartIdx
        idxRange = ((1:bindingSeqLen) - 1) + bpStartIdx;
        if (all(ntSequence(idxRange) == bindingNtSequence) ...
            || all(ntSequence(idxRange) == bindingSequenceRC))
            bindingExpectedMask(idxRange) = true;
            numberOfBindings = numberOfBindings + 1;
            bpStartIdx = bpStartIdx + bindingSeqLen;
        else
            bpStartIdx = bpStartIdx + 1;
        end
    end
end