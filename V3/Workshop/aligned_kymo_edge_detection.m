[kymoFilenames, dirpath] = uigetfile('*.tif', 'Select Kymo Tiff File', 'Multiselect', 'on');
aborted = isequal(dirpath, 0);
if aborted
    return;
end
if not(iscell(kymoFilenames))
    kymoFilenames = {kymoFilenames};
end
kymoFilepaths = fullfile(dirpath, kymoFilenames);
kymoImgs = cellfun(@(kymoFilepath) double(imread(kymoFilepath)), kymoFilepaths, 'UniformOutput', false);
import OptMap.KymoAlignment.NRAlign.nralign;
alignedKymoImgs = cellfun(@(kymo) nralign(kymo), kymoImgs, 'UniformOutput', false);
[~, kymoNames, ~] = cellfun(@fileparts, kymoFilepaths, 'UniformOutput', false);
alignedKymo = alignedKymoImgs{1};
alignedKymoAvg = mean(alignedKymo, 1);
fn_nrm_img = @(img) (img  - min(img(:)))./(max(img(:)) - min(img(:)));
import OptMap.MoleculeDetection.EdgeDetection.get_default_edge_detection_settings;
edgeDetectionSettings = get_default_edge_detection_settings(true);
import OptMap.MoleculeDetection.EdgeDetection.approx_main_kymo_molecule_edges;
[leftEdgeIdxs, rightEdgeIdxs, moleculeMask] = approx_main_kymo_molecule_edges(alignedKymo, edgeDetectionSettings);
leftEdgeIdx = floor(median(leftEdgeIdxs));
rightEdgeIdx = ceil(median(rightEdgeIdxs));
moleculeMask2 = false(size(moleculeMask));
moleculeMask2(:,leftEdgeIdx:rightEdgeIdx) = true;
alignedKymo2 = alignedKymo;
alignedKymo2(not(moleculeMask2)) = NaN;
barcodeApproxNan = nanmean(alignedKymo2, 1);

barcode = barcodeApproxNan;



img = fn_nrm_img(repmat(alignedKymoAvg(:)', [floor(max(alignedKymoAvg(:))/10) , 1]));
img = cat(3,img, img, img);
for idx = 1:length(alignedKymoAvg)
    img((floor(alignedKymoAvg(idx)/10):end), idx, 1:3) = NaN;
end
img = flipud(img);
mask = isnan(img);
mask2 = false(size(mask));
mask3 = mask2;
mask2(:, isnan(barcodeApproxNan), 1) = mask(:, isnan(barcodeApproxNan), 1);
mask3(:, not(isnan(barcodeApproxNan)), 2) = mask(:, not(isnan(barcodeApproxNan)), 1);
img(mask) = 1;
img(mask2) = 0;
img(mask3) = 0;

figure
imshow(img)

import CBT.get_default_barcode_gen_settings;
barcodeGenSettings = get_default_barcode_gen_settings();
deltaCut = 3;
edgeLen = round(deltaCut*barcodeGenSettings.psfSigmaWidth_nm/barcodeGenSettings.pixelWidth_nm);
fprintf(' deltaCut: %g\n edgeLen: %d\n', deltaCut, edgeLen);



tmpBg = alignedKymoAvg;
tmpFgFull = alignedKymoAvg;
tmpFgTrusted = alignedKymoAvg;
tmpFgFull(isnan(barcodeApproxNan)) = NaN;
tmpFgTrusted(imdilate(isnan(barcodeApproxNan), ones(1, edgeLen*2 + 1))) = NaN;
tmpBg(not(imdilate(isnan(barcodeApproxNan), ones(1, 1*2 + 1)))) = NaN;
hFig = figure('Name', 'Aligned Kymo Mean Barcode Extraction');
hPanel = uipanel('Parent', hFig);
hParent = hPanel;
hAxis = axes('Parent', hParent);
hold(hAxis, 'on');
plot(hAxis, tmpFgFull, 'b-');
plot(hAxis, tmpFgTrusted, 'g-');
plot(hAxis, tmpBg, 'r-');
xlim(hAxis, [1, length(alignedKymoAvg)]);
ylim(hAxis, [0, 10^ceil(log10(max(alignedKymoAvg(:))))]);
grid(hAxis, 'on');
grid(hAxis, 'minor');
hold(hAxis, 'off');