function [] = generate_molecule_detection_filter(meanGrayscaleFrameRot, psfSigmaWidth_pixels)
    if nargin < 2
        psfSigmaWidth_nm = 300;
        pixelSigmaWidth_nm = 159.2;
        arbFactor = 1;
        psfSigmaWidth_pixels = arbFactor.*psfSigmaWidth_nm./pixelSigmaWidth_nm;
    end
    import Microscopy.Utils.sobel_radon_edge_fg_method;
    gradmag = sobel_radon_edge_fg_method(meanGrayscaleFrameRot);
    import OptMap.MovieKymoExtraction.detect_angle_with_radon_transform;
    [angleTmp] = detect_angle_with_radon_transform(gradmag, 1024);
    rotationAngle = 90 - angleTmp;
    detectionAmpImage = imclose(gradmag./max(gradmag(:)), ones(3, 3));
    detectionAmpImage = detectionAmpImage./max(detectionAmpImage(:));
    detectionAmpImage = detectionAmpImage.*meanGrayscaleFrameRot;
    figure, imshow([meanGrayscaleFrameRot, gradmag; detectionAmpImage, detectionAmpImage > graythresh(detectionAmpImage(:))]);
    
%     n = 31;
%     m = 15;
%     k = zeros(n, n);
%     k(:,(end + 1)/2 + (0:1)) = 1;
%     k = imgaussfilt(k, psfSigmaWidth_pixels);
%     k = imrotate(k, 90 + rotationAngle, 'bilinear', 'crop');
%     idxs = ((n - m)/2) + (1:m);
%     k = k(idxs, idxs);
%     [gradmagFilter] = sobel_radon_edge_fg_method(k);
%     gradmagFilter = gradmagFilter./sum(gradmagFilter(:));
%     detectionAmpImage = imfilter(gradmag, gradmagFilter, 'replicate');m
%     gradmag2 = gradmag - min(gradmag(:));
%     gradmag2 = 1 - (gradmag2./max(gradmag(:)));
%     detectionAmpImage2 = detectionAmpImage - min(detectionAmpImage(:));
%     detectionAmpImage2 = detectionAmpImage2./max(detectionAmpImage2(:));
%     figure, imshow([meanGrayscaleFrameRot, gradmag; detectionAmpImage, gradmag2.*detectionAmpImage2]);
end