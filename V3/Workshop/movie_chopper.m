function [s] = movie_chopper(gsMovObj)
    if nargin < 1
        % temporary code because I am lazy. todo: remove
        import AppMgr.AppDataPoolMgr;
        [dataItems, dataItemsFoundMask, dataItemIDs] = AppDataPoolMgr.get_data_items('LoadedMovies');
        if isempty(dataItems)
            figTitle = 'Movies';
            import FancyGUI.FancyTabs.TabbedScreen;
            [ts, hFig] = TabbedScreen.make_tabbed_screen_in_new_fig(figTitle);
            import Microscopy.UI.MovieImportScreen;
            MovieImportScreen(ts);
            uiwait(hFig);
            s = movie_chopper();
            return;
        end
        gsMovObj = dataItems{end};
    end
    [~, movieIn] = gsMovObj.try_get_bounded_data(Microscopy.GrayscaleMovieStorageMode.Normalized);

    meanGsMovieFrame = nanmean(movieIn, 4);
    import Microscopy.Utils.sobel_radon_edge_fg_method;
    gradmag = sobel_radon_edge_fg_method(meanGsMovieFrame);
    import OptMap.MovieKymoExtraction.detect_angle_with_radon_transform;
    [angleTmp] = detect_angle_with_radon_transform(gradmag, 1024);
    rotationAngle = 90 - angleTmp;


    rotationAngle = mod(rotationAngle, 360);

    
    import Microscopy.Utils.movie_rotate_cyclical;
    movieIn_rotFine = movie_rotate_cyclical(movieIn, rotationAngle, 'bilinear');
    
    import Microscopy.generate_amplification_kernel;
    neighborhoodCutoffDistByDim = [2 2 1]; % distance cutoffs in each dimension
    distWarpingPowers = [2 2 1]; % how much the distances should be warped
    amplificationKernel = generate_amplification_kernel(neighborhoodCutoffDistByDim, distWarpingPowers);

    import Microscopy.amplify_molecules;
    movieIn_rotFineAmp = permute(amplify_molecules(permute(movieIn_rotFine, [1 2 4 3]), amplificationKernel), [1 2 4 3]);
    tmp_findEdgeMat = movieIn_rotFineAmp;
    

    % tmp_findEdgeMat = movieIn_rotFine;
    
    tmp_sobelKernel = fspecial('sobel');
    tmp_padSz = zeros(1, 4);
    tmp_padSz(1:2) = floor(size(tmp_sobelKernel)./2);
    tmp_EdgeMat = convn(padarray(tmp_findEdgeMat, tmp_padSz, 'replicate', 'both'), tmp_sobelKernel, 'valid');
    upperEdgeMag = max(tmp_EdgeMat, 0);
    lowerEdgeMag = max(-tmp_EdgeMat, 0);
    tmp_maxUpperMag = max(upperEdgeMag(:));
    tmp_maxLowerMag = max(lowerEdgeMag(:));
    tmp_maxMagVal = max(tmp_maxUpperMag, tmp_maxLowerMag);
    upperEdgeMag = upperEdgeMag./tmp_maxMagVal;
    lowerEdgeMag = lowerEdgeMag./tmp_maxMagVal;
    mainMolWidth = 3;  % MAGIC NUMBER
    tmp_upperNrmDilDown = imdilate(upperEdgeMag, [zeros(mainMolWidth - 1, 1); ones(mainMolWidth, 1)]);
    tmp_lowerNrmDilUp = imdilate(lowerEdgeMag, [ones(mainMolWidth, 1); zeros(mainMolWidth - 1, 1)]);
    tmp_overlapM = (tmp_upperNrmDilDown + tmp_lowerNrmDilUp)/2; %sqrt(upperNrmDilDown.*lowerNrmDilUp);
    tmp_overlapM = imopen(tmp_overlapM, ones(mainMolWidth, mainMolWidth));
    gapCloseDist = 8; % MAGIC NUMBER
    tmp_moleculeScoreInit = imclose(tmp_overlapM, ones(1, 1 + 2*gapCloseDist));
    tmp_threshM2a =  graythresh(tmp_moleculeScoreInit(:));
    tmp_moleculeMaskApproxRotInit = (tmp_moleculeScoreInit >= tmp_threshM2a);
    tmp_bgValsA = tmp_moleculeScoreInit(~tmp_moleculeMaskApproxRotInit);
    tmp_threshM2b = min(tmp_threshM2a, mean(tmp_bgValsA) + 3*std(tmp_bgValsA));
    moleculeMaskApproxRot = (tmp_moleculeScoreInit >= tmp_threshM2b);
    moleculeMaskApproxRot = imopen(moleculeMaskApproxRot, ones(3, 3));
    ccStructRot = bwconncomp(moleculeMaskApproxRot);
    tmp_szRot = size(moleculeMaskApproxRot);
    [~, ~, ~, firstFrameIdxs] = ind2sub(tmp_szRot, cellfun(@(pixelLinIdxs) pixelLinIdxs(1), ccStructRot.PixelIdxList));
    [~, ~, ~, lastFrameIdxs] = ind2sub(tmp_szRot, cellfun(@(pixelLinIdxs) pixelLinIdxs(end), ccStructRot.PixelIdxList));
    numFramesSpanned = 1 + lastFrameIdxs - firstFrameIdxs;
    

    minFramePresence = 3; % MAGIC NUMBER

    includeCcMask = (numFramesSpanned >= minFramePresence);
    ccStructRot.NumObjects = sum(includeCcMask);
    ccStructRot.PixelIdxList = ccStructRot.PixelIdxList(includeCcMask);

    labeledConnCompsMat = labelmatrix(ccStructRot);
    moleculeMaskApproxRot = labeledConnCompsMat > 0;


    % Rotate results back to original movie
    import Microscopy.Utils.movie_rotate_cyclical;
    movieInMolMaskApprox = movie_rotate_cyclical(double(moleculeMaskApproxRot), -rotationAngle, 'bilinear') > 0;

    % bgVals = movieIn(~movieInMolMaskApprox);
    % bgMean = mean(bgVals);
    % bgStd = std(bgVals);

    import FancyUtils.var2struct;
    vars_to_save = feval(@(allvars) allvars(~strncmp('tmp_', allvars, 4)), who());
    s = eval(['var2struct(', strjoin(vars_to_save, ', '),');']);
end