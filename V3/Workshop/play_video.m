function [] =  play_video(vidMat, clim)
    if nargin < 2
        minVal =  min(vidMat(:));
        maxVal =  max(vidMat(:));
        clim = [minVal, maxVal];
    end
    
    
    vidMatSz = size(vidMat);
    vidMatSz = [vidMatSz, ones(1, 4 - length(vidMatSz))];
    
    if (vidMatSz(4) == 1) && (vidMatSz(3) ~= 1) && (vidMatSz(3) ~= 3)
        vidMat = permute(vidMat, [1 2 4 3]);
        vidMatSz = size(vidMat);
        vidMatSz = [vidMatSz, ones(1, 4 - length(vidMatSz))];
    end
    if (vidMatSz(3) == 1)
        vidMat = repmat(vidMat, [1 1 3 1]);
        vidMatSz(3) = 3;
    end
    
    
    numFrames = vidMatSz(4);
    frameSz = vidMatSz(1:2);
    hFig = figure(...
        'Name', 'Video Player', ...
        'NumberTitle', 'off', ...
        'Menubar', 'none', ...
        'Toolbar', 'none');
    hPanel = uipanel('Parent', hFig);
    hSlider = uicontrol( ...
        'Parent', hPanel, ...
        'Units', 'normalized', ...
        'Position', [0 0.15 1 0.05],...
        'Style', 'slider', ...
        'Min', 1, ...
        'Max', numFrames, ...
        'Value', 1, ...
        'SliderStep', [1/numFrames, max(.1, 1/numFrames)] ...
    ); 
    set(hSlider, 'Callback', @(~, ~) update_frame(round(get(hSlider, 'Value'))));
    hAxis = axes( ...
        'Parent', hPanel, ...
        'Units', 'normalized', ...
        'Position', [0 0.25 1 0.7], ...
        'XTick', [], ...
        'YTick', [], ...
        'Visible', 'off' ...
    );
    axis(hAxis, 'equal');
    hTitle = title(hAxis, '-');
    set(hTitle, 'visible', 'on')
    update_frame(1);
    
    function update_frame(frameIdx)
        set(hSlider, 'Value', frameIdx);
        frameRGB = vidMat(:, :, :, frameIdx);
        imagesc(hAxis, 'CData', frameRGB);
        title(hAxis, sprintf('Frame %d/%d', frameIdx, numFrames));
    end

    %TODO: add play/pause buttons
end