function [] = SVD_Gui()
    % SVD_GUI - Structural Variation Detection (SVD) GUI for
    %  detecting structural variations in barcodes
    hFig = figure(...
        'Name', 'Structural Variation Detection', ...
        'Units', 'normalized', ...
        'OuterPosition', [0 0 1 1], ...
        'NumberTitle', 'off', ...
        'MenuBar', 'none' ...
        );
    hMenuParent = hFig;
    
    hPanel = uipanel('Parent', hFig);
    import FancyGUI.FancyTabs.TabbedScreen;
    ts = TabbedScreen(hPanel);
    
    hTabSVDOld = ts.create_tab('SVD (old)');
    ts.select_tab(hTabSVDOld);
    hPanelSVDOld = uipanel('Parent', hTabSVDOld);
    tsSVDOld = TabbedScreen(hPanelSVDOld);

    import SVD.Old.UI.add_svd_old_menu;
    add_svd_old_menu(hMenuParent, tsSVDOld);
end