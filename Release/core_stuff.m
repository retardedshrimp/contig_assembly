
% load contig file & consensus file
% load 'dataDA15000 contigs.mat'; % 
% load 'plasmid_puuh.mat';


init
% load settings file, files, etc.

% or load something called sequences from a .fasta file

% generate theoretical barcodes for contigs

%theoreticalBarcodes = ZEROMODEL.generate_barcodes_for_contigs({sequences{1}},settings);
theoreticalBarcodes = CBT.gen_zscaled_cbt_barcodes({sequences{1}},settings);


% select method to generate random barcodes
% 1) phase randomisation
% 2) predefined Gaussian autocorrelation
% 3) spectral analysis (should have precomputed average over all)


% compute maximum correlation coefficients for theoreticalBarcodes vs.
% consensus barcode

COMPARISON.compute_correlation_coefficients(theoreticalBarcodes, refCurve, settings )
% select a statistical zero model to compare theoretical barcodes to
% a consensus barcode



