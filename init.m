
% We will need to run commands from these folders
addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']), genpath([pwd '/Results' ]));

% load settings file
settings = SETTINGS.settings(); % 


saveFolder ='Results/Autocor';


% defaults settings for the test
settings.contigSize = 70;
settings.nRandom = 1000;
settings.uncReg = 0;
m = 100;
alpha = 5;
