
	tic
	load 'thyDatabase.mat';
	
	settings = SETTINGS.settings(); % 

	expkbpPerPixel = settings.kbpPerPixel;
	ZMkbpPerPixel = kbpPerPixel;
	ZMfft = meanFFT;
	
	refLen = 373;

    N = 1000;



    % Stretch the ZM FFT to have correct kbp/pixel
    if expkbpPerPixel ~= ZMkbpPerPixel
		stretchZM = true;
		kbpRatio = ZMkbpPerPixel/expkbpPerPixel;
	else
        stretchZM = false;
        kbpRatio = 1;
    end
            
    newLen = round(refLen/kbpRatio);
    ZMfftEven = newLen/2 == floor(newLen/2);
	if round(length(ZMfft)*kbpRatio) ~= refLen
		% Interpolate
		if floor(length(ZMfft)/2) == length(ZMfft)/2
			intplfft = interp1(ZMfft(2:length(ZMfft)/2),linspace(1,length(ZMfft)/2-1,floor((newLen-1)/2)));
		else
			intplfft = interp1(ZMfft(2:length(ZMfft)/2+0.5),linspace(1,length(ZMfft)/2-0.5,floor((newLen-1)/2)));
		end
		
		% "Fold" negative frequences over positive
		if newLen/2 == floor(newLen/2)
			ZMfft = [ZMfft(1) intplfft ZMfft(floor(length(ZMfft)/2)+1) fliplr(intplfft)];
		else
			ZMfft = [ZMfft(1) intplfft fliplr(intplfft)];
		end
		norm = sqrt(sum(ZMfft.^2)/((newLen-1)*newLen));
		ZMfft = ZMfft/norm;
	end
	
	% Generate the random barcodes and calculate cross-correlation
	randomBarcodes = cell(1,N);
	
	for iNew = 1:N
		fi = rand(1,floor((length(ZMfft)-1)/2));
		PR1 = exp(2i*pi*fi);
		PR2 = fliplr(exp(2i*pi*(-fi)));
		if ZMfftEven
			fftRand = ZMfft.*[1 PR1 1 PR2];
		else
			fftRand = ZMfft.*[1 PR1 PR2];
		end
		randomBarcode = ifft(fftRand);
		
		% Stretch the random barcode to same length as exp to
		% obtain correct kbp/pixel
		if stretchZM
			randomBarcode = interp1(randomBarcode,linspace(1,length(ZMfft),refLen));
		end
		
		randomBarcodes{iNew} = randomBarcode;
		
		% % Test figures to see how the random barcodes look
		% if iNew < 3
		% figure
		% plot(randomBarcode)
		% title(['Random barcode ' num2str(iNew)])
		% end
		
		
	end
	
	[autoC,~] = COMPARISON.cross_correlation_coefficients_fft(randomBarcodes{1},randomBarcodes{1});
	[autoC2,~] = COMPARISON.cross_correlation_coefficients_fft(randomBarcodes{2},randomBarcodes{2});
	[autoC3,~] = COMPARISON.cross_correlation_coefficients_fft(randomBarcodes{3},randomBarcodes{3});
	toc
