function [theoreticalSequence, nameSequence] = read_fasta(file)
    % updated 21/09/16 
    % reads data from fasta file named file, which should be
    % in the working directory

    FASTAData = fastaread(file);

    theoreticalSequence = cell(1,size(FASTAData,1));
    nameSequence = cell(1,size(FASTAData,1));
    for i=1:size(FASTAData,1)
        theoreticalSequence{i} =  FASTAData(i).Sequence;
        nameSequence{i} = FASTAData(i).Header;
    end
end

