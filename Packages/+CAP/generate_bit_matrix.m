function [pValMat] = generate_bit_matrix(contigBarcodes,consensusBarcode, evdParameters,settings, method)
    % 01/10/16 Albertas Dvirnas
    % includes transformation
    % Includes also the distribution for the reverse of the barcode, which
    % was not done before
    %Generates a cost list for all possible positions that the
    %contig can be placed on the reference barcode (refCurve).
    
    if nargin <  5
        method = 'tan';
    end
    import CAP.*;

    m = size(consensusBarcode,2);
    
    
    pValMat = zeros(size(contigBarcodes,2), 2*m );
 
    for i=1:size(contigBarcodes,2)
         pTemp = ones(1,2*m);

         if ~isempty(contigBarcodes{i})
            curBar = contigBarcodes{i}(settings.uncReg+1:end-settings.uncReg);
            curBar = (curBar-mean(curBar))./std(curBar);
            [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(curBar,consensusBarcode);
            %[cc1,cc2,flip] = CMN_HelperFunctions.ccorr_all(contigBarcodes{i},consensusBarcode,true,true);
      


            if isequal(method, 'tan')
                cc1 = tan((pi/2)*cc1);
                cc2 = tan((pi/2)*cc2);
            else if isequal(method, 'fisher')
                    cc1 = (1/2)*log((1+cc1)./(1-cc1));
                    cc2 = (1/2)*log((1+cc2)./(1-cc2));
                end
            end
            pTemp(1:m) = CMN_HelperFunctions.p_val_gumbel(cc1,evdParameters{i});
            pTemp((1:m)+m) = CMN_HelperFunctions.p_val_gumbel(cc2,evdParameters{i});
            pTemp(pTemp == 0) = 10^(-100); %Remove possibility of pTemp
         end
         
         pValMat(i,:) = pTemp;

        

    end
    %max(max(cc1,cc2))
end