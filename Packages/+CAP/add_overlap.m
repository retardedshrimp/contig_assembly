function [bidMat1, nLengths ] = add_overlap( bidMat, contigLengths, interlapingItems)
% modifies the bidmat data to have interlaps


[n,m] = size(bidMat);


nLengths = zeros(1,n);

for i=1:n
   prevLength = contigLengths(i);
   newLength = prevLength - 2*interlapingItems;
   if newLength < 0
       bidMat1(i,:) = zeros(1,m);
       nLengths(i) = -1;
   else
       bidMat1(i,:) = circshift(bidMat(i,:),[0,-interlapingItems]);
       nLengths(i) = newLength;
   end
end



end

