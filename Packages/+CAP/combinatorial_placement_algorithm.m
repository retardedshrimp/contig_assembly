function [ output_args ] = combinatorial_placement_algorithm(consensusBarcode, contigBarcodes, pValMat, settings)
    % edited 21/09/16 by Albertas Dvirnas
    % can be used to place contigs around circular (linear) sequence. 
    
    % has been moved from dnaproject, so needs to be testsed thoroughly and
    % checked that everything is working!

    m = size(consensusBarcode,2);
    n = size(pValMat,1);
    %contigLengths = zeros(1,n);

    pValueThresh = 0.01;

    for i=1:n
        settings.contigLengths(i) = size(contigBarcodes{i},2)-1;
    end

    % redefine that pVal would be only those values < 1 ? 
    [ pVal, pValBi] = CAP.create_best_value_p_value_matrix(pValMat, m, n);

    % create a bidMat, also takes into account that the bid is for the first
    % item and not the last
    [bidMat, pValNew, contigLengths2, n, II, xInd ] = CAP.convert_first_item_to_last_item(pVal,settings.pValueThresh, settings.contigLengths, m,n);



    [bidMat1, nLengths ] = CAP.add_overlap( bidMat, settings.contigLengths, settings.allowedOverlap);


    tic
    [opt2, bestValues, opIndex2] = CAP.contig_placement_optimal_value(bidMat1, nLengths, m, n);
    toc
    
    opt2

    [optBid] = CAP.contig_placement_optimal_bid( bestValues, opIndex2, bidMat1, nLengths, n, m );

    optimalBid = sortrows(optBid,3);

    uniqueItems = unique(II);
    uniqueFormer = unique(xInd);

    for i=1:size(optBid,1)
       optimalBid(i,1) = uniqueFormer(find(uniqueItems==optimalBid(i,1)));
    end

    %optimalBid

    optimalBid2 = CAP.return_to_overlap(optimalBid, m, allowedOverlap );

    optimalBid2

    CAP.plot_comparison(consensusBarcode,optimalBid2, pValBi, contigBarcodes,m,'Combinatorial auction, pUUH with 6 contigs, p-value < 0.01, with overlap');


end

