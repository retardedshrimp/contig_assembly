function [optimalBid ] = return_to_overlap(optimalBid,m, overlap )
% 13/09/16 turns optimalBid back to bids with overlaps
% simplify this to be include in the optimal bid selection function in the
% future

    for i=1:size(optimalBid,1)
        startBarcode = optimalBid(i,2)-overlap;
        stopBarcode = optimalBid(i,3)+overlap;
        
        if startBarcode < 1
            optimalBid(i,2) = m + startBarcode;
        else
             optimalBid(i,2) = startBarcode;
        end
        if stopBarcode > m
            optimalBid(i,3) = stopBarcode - m;
        else
            optimalBid(i,3) = stopBarcode;
        end
    end


end

