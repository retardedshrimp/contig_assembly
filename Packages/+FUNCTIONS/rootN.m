function [ F ] = rootN(x,cc )
% function for maximum likelihood stuff
    m = size(cc,1);
    
    C1 = gamma((x-1)/2)./(sqrt(pi)*gamma((x-2)/2));
    
    C2 = 2.*sum(log(C1.*cc.*hypergeom([1/2, (4-x)/2 ],3/2,cc.^2 )+1/2));
    
    F = -m./C2; 

end

