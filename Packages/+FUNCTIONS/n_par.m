function [F,xx]=n_par(corCoef)

	F = [];
	xx = 5:0.2:20;
	
	for x=5:0.2:20

		F = [F FUNCTIONS.n_fun_test(x, corCoef(:))];
	end

	figure, plot(xx,F)
end
