function [ interpolatedData ] = interpolate_in_fourier_space( data, newLength )
    % 01/12/16
    % Interpolated data  to newLength + keeps normalization
    
    oldLength = length(data);
    halfL = floor(newLength/2+1);

    % Define vector of points to interpolate at
    intV = linspace(1,round(length(data)/2)-1,halfL-1);

    tempData = data(2:round(length(data)/2));

        % interpolate
    intVal = [interp1(tempData, intV )];

    % recreate fft for all frequences. Add an extra value from the original seq. in case maxL is even
    if mod(newLength,2)==0
        intVal = [data(1)*newLength/oldLength intVal(1:end) fliplr(intVal(1:end-1))];
    else
        intVal = [data(1)*newLength/oldLength intVal fliplr(intVal(1:end))];
    end

    % renormalise. Make sure that first element is the same
    %intVal(1) = prFFT(1); % this gives the mean of a sequence in the real space

    % and the Parseval's identity also needs to be satisfied
    len1 = length(data)*(length(data)-1);
    len2 = length(intVal)*(length(intVal)-1);


    % the sums a and b have to be the same
    a = sum(data.^2)/(len1);
    b = sum(intVal.^2)/(len2);


    % if they are not the same, we renormalise intVal values 2:end by
    % constant konst
    konst=sqrt((len2*a-intVal(1).^2)/(len2*b-intVal(1).^2));


    % and define intValNorm
    interpolatedData = [intVal(1) intVal(2:end).*konst];


end

