function [ rescaledBarcode ] = rescale_barc(theoreticBarcode, settings )

    barLen = size(theoreticBarcode,1);
    newRes = round((1/(settings.bpPerNm*settings.camRes))*(barLen-1)); %could stretch more precisely, so that kbp/pixel ratio would be 0.57
    rescaledBarcode = interp1(theoreticBarcode, 1:(barLen-1)/newRes:barLen);
    rescaledBarcode = (rescaledBarcode-mean(rescaledBarcode))/std(rescaledBarcode);
end

