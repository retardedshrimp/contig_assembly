function [ interpolatedBarcode ] = interpolate_fourier_data( barcode, outputLen )
    % 21/09/16 by Albertas Dvirnas
    % interpolates data, this still needs to be tested to see how accurate
    % it is/ include posibility of different inter/extra-polation methods

    % only the first half of the set is needed
    N = floor(outputLen/2); 

    % set of points to interpolate to
    xSp = floor(linspace(1,floor(size(barcode,2)/2-1), N-1));
    % interpolation, using matlab in-build interp1 method
    

    xx = interp1(barcode(2:floor(size(barcode,2)/2)), xSp);

    
    % differentiate between even and odd cases
    if N == outputLen/2
        interpolatedBarcode = [ barcode(1) xx 0 fliplr(xx)];
    else
        interpolatedBarcode = [ barcode(1) xx fliplr(xx)];
    end

end

