function [ autocorEstTilde,fftEst] = estimate_autocorr_no_interp( probVec, settings, method )
    % 30/11/16
    % There are many methods to estimate autocorr function, here we
    % implement a few of them
    if nargin < 3
        method = 'prob';
    end
    
    psfSigmaWidth_bp = 1.1157e+03;
    
   
    
    % Now the probabilistic component
    
    ssum = zeros(settings.maxL,1);
    nn = length(probVec);
    halfL = floor(settings.maxL/2+1);


    for iInd = 1:1;
        iInd
        probCurrent = repmat(probVec{iInd}, ceil(settings.maxL/length(probVec{iInd})),1);
        probV = probCurrent(1:settings.maxL);
        % first compute the absolute value of fft
        prFFT = abs(fft(probV));
%         
%         % Define vector of points to interpolate at
%         intV = linspace(1,round(length(prFFT)/2)-1,halfL-1);
% 
%         % Take first half of frequencies of prFFT
%         tempfft = prFFT(2:round(length(prFFT)/2));
% 
%         % interpolate
%         intVal = [interp1(tempfft, intV )];
% 
%         % recreate fft for all frequences
%         intVal = [prFFT(1) intVal fliplr(intVal(1:end))];
%         
%         % renormalise. Make sure that first element is the same
%         %intVal(1) = prFFT(1);
% 
%         % and the Parseval's identity also needs to be satisfied
%         len1 = length(prFFT)*(length(prFFT)-1);
%         len2 = length(intVal)*(length(intVal)-1);
% 
%         % the sums a and b have to be the same
%         a = sum(prFFT.^2)/(len1);
%         b = sum(intVal.^2)/(len2);
%         
%         
%         % if they are not the same, we renormalise intVal values 2:end by
%         % constant konst
%         konst=sqrt((len2*a-intVal(1).^2)/(len2*b-intVal(1).^2));
%         
%         
%         
%         % and define intValNorm
%         intValNorm=[intVal(1) intVal(2:end).*konst];
%         
%         %b = sum(intValNorm.^2)/(len2);
%        
%       
%         % thus defined, intValNorm will satisfy b=a
%         
        % now sum thse
        ssum = ssum + (prFFT/(sqrt(nn))).^2;
    end
    
    % and divide by the number of summands
    fftEst1 = ssum;
    
    switch method
        case 'prob'   
            % Deal with non-probabilistic component
            % Gaussian kernel to be used here
            
            % this also needs to be stretched the same way
            ker = ZEROMODEL.gaussian_kernel(settings.maxL,psfSigmaWidth_bp);

            % compute square of the absolute value of kernel
            fftEst2 = (abs(fft(ker))).^2;
            
            % Multiply component-wise
            fftEst = fftEst1.*transpose(fftEst2);
            
            autocorEstTilde =ifft(fftEst);
            
        case 'seq'
            autocorEstTilde =ifft(fftEst1);
            fftEst = fftEst1;
        otherwise
            autocorEstTilde = [];
            fftEst = [];
    end

    %autocorEst = autocorEstTilde - mean(autocorEstTilde);
    %autocorEst = autocorEstTilde;

    %autocorEstNormalised = autocorEst/autocorEst(1);

    %figure,plot(autocorEst)




end

