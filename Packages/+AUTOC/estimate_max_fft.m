function [ autocorEstTilde,fftEst, fftEst1,meanFFT] = estimate_max_fft( probVec, settings, method )
    % 30/11/16
    % There are many methods to estimate autocorr function, here we
    % implement a few of them
    
    if nargin < 3
        method = 'prob';
    end
    
    psfSigmaWidth_bp = 1.1157e+03;
    % this should be in the settings
   
    
    barcodeLens = cellfun(@length, probVec);
    [maxSize,minInd] = max(barcodeLens);
    % Now the probabilistic component
    
    ssum = zeros(1,maxSize);
    nn = length(probVec);
    %nn = 2;

    halfL = ceil((maxSize-1)/2);
    
   % bitWeight = zeros(1,maxSize);

    for iInd = 1:2;
        iInd
        
        % first compute the absolute value of fft
        prFFT = abs(fft(probVec{iInd}));

        fftPr = prFFT(1:floor((end+3)/2));

        f1 = [0:length(1:(maxSize+1)/2)-1]*(1/maxSize);
        f2 = [0:length(fftPr)-1]*(1/length(prFFT));

        % and the Parseval's identity also needs to be satisfied
        len1 = length(prFFT)*(length(prFFT)-1);
        len2 = maxSize*(maxSize-1);

        newB = zeros(1, maxSize);
        newB(1) = prFFT(1)*maxSize/length(prFFT);     

        newf = interp1(f2,fftPr,f1);

        newB(2:length(newf)) = newf(2:end);
        newB(maxSize-(2:length(newf))+2) =  newf(2:end);

        % the sums a and b have to be the same
        a = sum(prFFT.^2)/(len1); b = sum(newB.^2)/(len2);

        % if they are not the same, we renormalise intVal values 2:end by
        % constant konst
        konst=sqrt((len2*a-newB(1).^2)/(len2*b-newB(1).^2));

        % and define intValNorm
        intValNorm=[newB(1) newB(2:end).*konst];

        ssum = ssum + (intValNorm/(sqrt(nn))).^2;
    end
    
%     st = (ssum.*nn)./bitWeight;
%     figure,plot(bitWeight(1:100))
    % and divide by the number of summands
    fftEst1 = ssum;
    
    switch method
        case 'prob'   
            % Deal with non-probabilistic component
            % Gaussian kernel to be used here
            
            % this also needs to be stretched???
            
            ker = ZEROMODEL.gaussian_kernel(maxSize,psfSigmaWidth_bp);

            % compute square of the absolute value of kernel
            fftEst2 = (abs(fft(ker))).^2;
            
            % Multiply component-wise
            fftEst = fftEst1.*fftEst2;
            
            autocorEstTilde =ifft(fftEst);
            
            meanFFT = sqrt(fftEst);
            
        case 'seq'
            autocorEstTilde =ifft(fftEst1);
            fftEst = fftEst1;
        otherwise
            autocorEstTilde = [];
            fftEst = [];
    end

    %autocorEst = autocorEstTilde - mean(autocorEstTilde);
    %autocorEst = autocorEstTilde;

    %autocorEstNormalised = autocorEst/autocorEst(1);

    %figure,plot(autocorEst)




end

