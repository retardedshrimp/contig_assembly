function [ autocorEstTilde,fftEst, fftEst1,bitWeight] = estimate_autocorr_exact_frequencies( probVec, settings, method )
    % 30/11/16
    % There are many methods to estimate autocorr function, here we
    % implement a few of them
    
    if nargin < 3
        method = 'prob';
    end
    
    psfSigmaWidth_bp = 1.1157e+03;
    % this should be in the settings
   
    
    barcodeLens = cellfun(@length, probVec);
    maxSize = round(max(barcodeLens));
    maxSize
    % Now the probabilistic component
    
    ssum = zeros(1,maxSize);
    nn = length(probVec);
    %nn = 1;

    halfL = ceil((maxSize-1)/2);
    
    bitWeight = zeros(1,maxSize);

    for iInd = 1:1;
        iInd
        
        % first compute the absolute value of fft
        prFFT = abs(fft(probVec{iInd}));
        
        nSeq = length(prFFT);
        % nSeq
        nMax = ceil(maxSize/nSeq);
        
        nOld = length(prFFT);

        % on this side want to compute for all values
        %nMin =  floor(maxSize/(nSeq/ceil((nSeq-1)/2)));
       
       % k = nMax:1:min(nMin,halfL);
        k = nMax:1:halfL;
        
        %k(1)
        %halfL-k(end)
        % interp points
        xx = (k.*nOld)./maxSize;
        
        % nOld./maxSize
        
        %xx(end-20) - length(2:round((length(prFFT)+1)/2))
        
        % Take first half of frequencies of prFFT
        tempfft = prFFT(2:round((length(prFFT)+1)/2));
 
        % this interpolates to the right frequencies
        intVal = [interp1(tempfft, xx )];
        
        %intVal(end)
        
       % figure,plot(intVal)
        
       % intVal(end:end)
        %sum(intVal.^2./length(intVal))
       % figure,plot(intVal)
        
        intSeq = zeros(1,maxSize);
        intSeq(k) = intVal;
        intSeq(maxSize-k) = intVal;
        
      

        bitWeight(k)=bitWeight(k)+1;
        bitWeight(maxSize-k)=bitWeight(maxSize-k)+1;
        
        if k(end)==halfL && maxSize-k(end)==halfL
             bitWeight(halfL) = bitWeight(halfL)-1;
        end
        
      

        intSeq(1) = prFFT(1)*maxSize/length(prFFT);
        
        %figure, plot(intSeq)
        
        % recreate fft for all frequences. Add an extra value from the original seq. in case maxL is even
%         if mod(maxSize,2)==0
% 			intVal = [prFFT(1)*maxSize/length(prFFT) intVal(1:end) fliplr(intVal(1:end-1))];
% 		else
% 			intVal = [prFFT(1)*maxSize/length(prFFT) intVal fliplr(intVal(1:end))];
%         end

        %figure,plot(intVal)

        % re normalise. Make sure that first element is the same
        %intVal(1) = prFFT(1); % this gives the mean of a sequence in the real space

        % and the Parseval's identity also needs to be satisfied
        len1 = length(prFFT)*(length(prFFT)-1);
        len2 = length(intSeq)*(length(intSeq)-1);

        
        % the sums a and b have to be the same
        a = sum(prFFT.^2)/(len1);
        b = sum(intSeq.^2)/(len2);
        
        
        % if they are not the same, we renormalise intVal values 2:end by
        % constant konst
        konst=sqrt((len2*a-intSeq(1).^2)/(len2*b-intSeq(1).^2));
        
        
        
        % and define intValNorm
        intValNorm=[intSeq(1) intSeq(2:end).*konst];
        
        %b = sum(intValNorm.^2)/(len2);
       
      
        % thus defined, intValNorm will satisfy b=a
        
        % now sum thse
        ssum = ssum + (intValNorm/(sqrt(nn))).^2;
    end
    
    st = (ssum.*nn)./bitWeight;
    figure,plot(bitWeight(1:100))
    % and divide by the number of summands
    fftEst1 = st;
    
    switch method
        case 'prob'   
            % Deal with non-probabilistic component
            % Gaussian kernel to be used here
            
            % this also needs to be stretched???
            
            ker = ZEROMODEL.gaussian_kernel(maxSize,psfSigmaWidth_bp);

            % compute square of the absolute value of kernel
            fftEst2 = (abs(fft(ker))).^2;
            
            % Multiply component-wise
            fftEst = fftEst1.*fftEst2;
            
            autocorEstTilde =ifft(fftEst);
            
        case 'seq'
            autocorEstTilde =ifft(fftEst1);
            fftEst = fftEst1;
        otherwise
            autocorEstTilde = [];
            fftEst = [];
    end

    %autocorEst = autocorEstTilde - mean(autocorEstTilde);
    %autocorEst = autocorEstTilde;

    %autocorEstNormalised = autocorEst/autocorEst(1);

    %figure,plot(autocorEst)




end

