% This test compares the correlation coefficients based on the lengths of
% contigs

figure
for kk = 1:2
    kk
    sizeRand = kk*30;
    sizeRand2 = kk*15;

    intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

    barLength = ceil(size(intZeroModel,2)/(settings.bpPerNm*settings.camRes));

    randBarcode = zeros(1,barLength);   

    tic % just to check how fast it is, can be commented out
    for iNew = 1:1
        barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
        randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
        randBarcode = (randBarcode-mean(randBarcode))/std(randBarcode);
    end

    randBarcode = barcode(1:30*kk);
    [gumbelFit, gevParams, corCoef, corCoefRev, corCoefVec] = COMPARISON.evd_with_shorter_barcode_randomised( randBarcode, sizeRand2, settings );

    normFit = fitdist(corCoefVec(:),'Normal'); % since this includes gumbel, results should be similar

    cc = linspace(-1,1,1001);
    gev = normpdf(cc, normFit.mu, normFit.sigma);

    [f,x]=  hist(corCoefVec(:),30);
        
    subplot(3,3,kk)
    bar(x,f/trapz(x,f));hold on
    plot(cc, gev, 'r', 'linewidth',3)
    ylabel('PDF')
    xlabel('CC')
    %legend({'Correlation coefficient hist', 'Normal distribution'})
    
end