% this plots a heat map for random barcodes vrs random barcodes
% http://stackoverflow.com/questions/15676363/matlab-how-does-one-plot-a-heatmap-from-nxn-matrix

% edited 27/10/16

addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));
settings = SETTINGS.settings(); % 

%load ZERO.mat; % load zero model
%load ZERO_markov.mat; % load zero model
%settings.zeroModel = zeroModel;

%distToPlot = 'Gumbel'; 

tryNum = 60;

%figure;
rSqE = zeros(1, tryNum);
rSqG = zeros(1, tryNum);
rSqGEV = zeros(1, tryNum);
%RsqrdNormal = zeros(tryNum, tryNum);

%pValGumbel =  zeros(tryNum, tryNum);
%pValGEV = zeros(tryNum, tryNum);

randBarcodes = ZEROMODEL.generate_random_sequences(100,1000,'phaserand',barcode,6);
xVals = [];
nVals = [];

for kk = 1:tryNum
    kk
    sizeRand = 30+kk;
    settings.contigSize = sizeRand;
    
    randBar = cell(1,1000);
    for ii=1:1000
        randBar{ii} = randBarcodes{ii}(1:sizeRand);
    end
    [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, barcode, settings );

    f = @(y) FUNCTIONS.n_fun_test(y,corCoef(:));
	x0 = [10];
	x2 = fsolve(f,x0,optimoptions('fsolve','Display','off'));
	N2 = FUNCTIONS.rootN_numeric(x2,corCoef(:));
    
    xVals =[xVals x2];
	nVals = [nVals N2];

    rSqE(kk) = COMPARISON.compute_r_squared( corCoef(:), [x2,N2], 'exactfull' );

	evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
	rSqG(kk) = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );

	evdParGEV =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
	%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )
	
	%COMPARISON.compare_distribution_to_data( corCoef(:), [x2,N2], 'exactfull' )

	rSqGEV(kk) = COMPARISON.compute_r_squared(corCoef(:), evdParGEV, 'gev' );

end

rSqE(rSqE<0) = 0;
rSqG(rSqG<0) = 0;
 rSqGEV(rSqGEV<0) = 0;

 colormap('hot')
 figure
h1 = imagesc(rSqE);
colorbar
 saveas(h1,'RsqrdEx.jpg') ;
 
 figure
h2 = imagesc(rSqG);
colorbar
 saveas(h2,'RsqrdGumbel.jpg') 



 figure
h3 = imagesc(rSqGEV);
colorbar
 saveas(h3,'RsqrdGEV.jpg') 
 
xx = 31:30+tryNum;
figure, hold on;plot(xx,rSqE); 
plot(xx,rSqGEV),plot(xx,rSqG)
legend({'exact','gev','gumbel'});
xlabel('Pixel length of a contig');
ylabel('R-squared');

