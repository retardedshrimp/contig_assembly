
settings = SETTINGS.settings(); % 

contigs = IMPORT.read_fasta('DA15302 complete contigs.fasta');

barc = ZEROMODEL.generate_contig_barcode(contigs{29},settings );

settings.contigSize = size(barc,2);

size(barc,2)

barcode2 = ZEROMODEL.generate_autocorr_rand_seq(size(barcode,2)-1,1 ,'gaussian2' );
barcode2 = barcode2{1};

settings.nRandom = 1;
[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients({barc}, barcode2, settings );


settings.nRandom = 20000;
randBar = ZEROMODEL.generate_autocorr_rand_seq(settings.contigSize-1,settings.nRandom ,'gaussian2' );
[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, barcode2, settings );


f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
x0 = [30];
x = fsolve(f,x0,optimoptions('fsolve','Display','off'));

N = FUNCTIONS.rootN_numeric(x,corCoef(:));


rSquared = COMPARISON.compute_r_squared( corCoef(:), [x,N], 'exactfull' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
rSquared2 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )

rSquared3 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gev' );


COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )
COMPARISON.compare_distribution_to_data( corCoef(:), [x, N], 'exactfull' )
