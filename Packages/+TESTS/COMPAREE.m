
settings = SETTINGS.settings(); % 
seq = IMPORT.read_fasta('DA1500 all both 13.fasta');

tic % ~ takes rougly 2 - 3 minutes
theoreticalBarcodes = ZEROMODEL.generate_barcodes_for_contigs(seq,settings);
toc

load '1000rand.mat';

%index = 29;

% settings.contigSize = size(theoreticalBarcodes{index},2);
settings.contigSize = 100;
%settings.nRandom = size(barLL,2);
settings.nRandom = 1000;
settings.uncReg = 5;
settings.method = 'fisher';

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(barLL, barcode, settings );
[ transformedCorCoef ] = COMPARISON.transform_coeficients(corCoef,settings.method);
[ transformedCorCoefAll ] = COMPARISON.transform_coeficients(corCoefAll(:),settings.method);


parNormal = COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
COMPARISON.compare_distribution_to_data( corCoefAll(:), parNormal, 'normal' )

NN = settings.contigSize-2*settings.uncReg;
cc = linspace(-1,1,1000);
yy = (1/sqrt(pi)) * (gamma((NN-1)/2 )/gamma((NN-2)/2)) * (1-cc.^2).^((NN-4)/2);
figure, plot(yy);

evdPar =  COMPARISON.compute_distribution_parameters(transformedCorCoef,'gumbel');
evdPar2 =  COMPARISON.compute_distribution_parameters(transformedCorCoef,'gev');
parNormal = COMPARISON.compute_distribution_parameters(transformedCorCoefAll,'normal');



COMPARISON.compare_distribution_to_data( transformedCorCoefAll, parNormal, 'normal' )
COMPARISON.compare_distribution_to_data( transformedCorCoef, evdPar, 'gumbel' )
COMPARISON.compare_distribution_to_data( transformedCorCoef, evdPar2, 'gev' )

rSquared = COMPARISON.compute_r_squared( transformedCorCoef, evdPar, 'gumbel' );


