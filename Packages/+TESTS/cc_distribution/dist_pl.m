%%%%%%%%%%%%%%%%% EXACT CDF

xx = linspace(-1,1,1000);

% n even
n = 20 ;
A1 = 1./sqrt(pi) *gamma((n-1)/2)/gamma((n-2)/2);
I = (n-4)/2;
sum = 0;
r = 0.9;
for k = 0:I
    sum = sum + (-1)^k*nchoosek(I,k)*( xx.^(2*k+1)/(2*k+1) + 1/(2*k+1));
end
sum = sum*A1;
figure, plot(xx,sum)
exactcdf = sum.^N;
figure, plot(xx,exactcdf)

exactpdf = N*sum.^(N-1).*A1.*(1-xx.^2).^I;
figure, plot(xx,exactpdf)


%n odd
n = 19;
J = n-1;
ss = 0;
r = 0.9;

xx = linspace(-1,1,1000);

for k=0:J
    ss = ss + NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1)*(1-xx.^2).^(k+1/2);
end
nmS = ss;
ss = 1./pi*(asin(xx) +ss.*xx-asin(-1));
figure, plot(xx,ss)

N= 40;

exactcdf = ss.^N;
figure, plot(xx,exactcdf)

ds = 0;
for k=0:J
    ds =ds + (k+1/2).*(NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1)*(1-xx.^2).^(k-1/2));
end

exactpdf =  N*ss.^(N-1).*(1./(sqrt(1-xx.^2))+nmS-2*xx.^2.*ds );

figure, plot(xx,exactpdf)


