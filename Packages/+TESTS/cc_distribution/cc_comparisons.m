
settings = SETTINGS.settings(); % 
seq = IMPORT.read_fasta('DA1500 all both 13.fasta');

tic % ~ takes rougly 2 - 3 minutes
theoreticalBarcodes = ZEROMODEL.generate_barcodes_for_contigs(seq,settings);
toc
load '1000rand.mat';

ll = [];
for i=1:1000
    ll = [ll barLL{i}(6:360)];
end

%index = 29;
% settings.contigSize = size(theoreticalBarcodes{index},2);
mult = 60;
settings.contigSize = mult+10;
%settings.nRandom = size(barLL,2);
settings.nRandom = 1000;
settings.uncReg = 1;
NN = settings.contigSize - 2*settings.uncReg-72;
NN = mult/5;
%NN = 4/5;

settings.method = 'fisher';

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(barLL, barcode, settings );
% [ transformedCorCoef ] = COMPARISON.transform_coeficients(corCoef,settings.method);
% [ transformedCorCoefAll ] = COMPARISON.transform_coeficients(corCoefAll(:),settings.method);


parNormal = COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
%COMPARISON.compare_distribution_to_data( corCoefAll(:), parNormal, 'normal' )

COMPARISON.compare_distribution_to_data( corCoefAll(:), NN, 'cc' )

COMPARISON.compare_distribution_to_data(  corCoefAll(:), NN, 'ccfisher' )


evdPar =  COMPARISON.compute_distribution_parameters(transformedCorCoef,'gumbel');
evdPar2 =  COMPARISON.compute_distribution_parameters(transformedCorCoef,'gev');
parNormal = COMPARISON.compute_distribution_parameters(transformedCorCoefAll,'normal');



COMPARISON.compare_distribution_to_data( transformedCorCoefAll, parNormal, 'normal' )
COMPARISON.compare_distribution_to_data( transformedCorCoef, evdPar, 'gumbel' )
COMPARISON.compare_distribution_to_data( transformedCorCoef, evdPar2, 'gev' )
COMPARISON.compare_distribution_to_data( corCoef(:), [NN+8,746], 'exact' )


rSquared = COMPARISON.compute_r_squared( transformedCorCoef, evdPar, 'gumbel' );


