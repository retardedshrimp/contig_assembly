N = 20 ;
n = 20 ;

x = linspace(-1,1,100);

hypergeom([(N-2)/2,1/2], N/2, 1-x.^2); 

xx = 0.99;

C1 = (1./beta(1/2,(n-2)/2)).^(N);
C2 = N*(n-2)/2;

cumDist = C1*(-1/(n-2))^N*(1-xx^2).^C2 * (hypergeom([(n-2)/2,1/2],n/2,1-xx.^2)).^N;

xx = linspace(-0.5,0.5,10);
fval = -2*xx.*C1*(1-xx.^2).^(C2-1)*(hypergeom([(n-2)/2 1/2], n/2, 1-xx.^2)).^(N-1)*(C2*hypergeom([(n-2)/2, 1/2],n/2,1-xx.^2 )+N*(1-xx.^2)*(n-2)/(2*n)*hypergeom([n/2, 3/2], (n+2)/2,1-xx.^2));

%%%%%%%%%%%%%%%%% EXACT CDF
% n even
n = 20 ;
A1 = 2./sqrt(pi) *gamma((n-1)/2)/gamma((n-2)/2);
I = (n-4)/2;
sum = 0;
r = 0.9;
for k = 0:I
    sum = sum + (-1)^k*nchoosek(I,k)* abs(r).^(2*k+1)/(2*k+1 );
end
sum = sum*A1;

%n odd
n = 19;
J = n-1;
ss = 0;
r = 0.9;


for k=0:J
    ss = ss + NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1)*(1-r.^2).^(k+1/2);
end
ss = 2./pi*(asin(abs(r)) +ss*abs(r));



