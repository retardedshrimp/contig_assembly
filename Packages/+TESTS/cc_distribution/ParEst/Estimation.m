settings = SETTINGS.settings(); % 

settings.contigSize = 20;

tic
nNum = 1;
evdPar = zeros(nNum,2);
rSquared2 = zeros(nNum,1);

for s=1:nNum
    settings.nRandom = 100+100*s;
    settings.uncReg = 0;
    m = 30;

    fBarcode = normrnd(0,1,1,m);

    randBar = cell(1,settings.nRandom);
    for i=1:settings.nRandom
        randBar{i} = normrnd(0,1,1,settings.contigSize);
    end

    [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );

    % 
    n0 = settings.contigSize ;
    evdPar(s,:) = COMPARISON.exact_par(n0,corCoef,corCoefAll );
    rSquared2(s) = COMPARISON.compute_r_squared( corCoef(:), [evdPar(s,1),2*evdPar(s,2)], 'exactfull' );

end
toc

%rSquared2(s) = COMPARISON.compute_r_squared( corCoef(:), [evdPar(s,1),2*30], 'exactfull' );

% 
% parNormal = COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
% COMPARISON.compare_distribution_to_data( corCoefAll(:), parNormal, 'normal' )

COMPARISON.compare_distribution_to_data( corCoefAll(:), settings.contigSize, 'cc' )
COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exact' )
COMPARISON.compare_distribution_to_data( corCoef(:), [round(evdPar(1,1)),2*round(evdPar(1,2))], 'exactfull' )

% rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exact' );
% rSquared2 = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exactfull' );
% 
% COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exactfull' )
% cval = 0.7;
% CMN_HelperFunctions.p_val_exact(cval,[settings.contigSize,2*m])
% hold on
% line([cval cval], [0,5]),