
settings = SETTINGS.settings(); % 


settings.contigSize = 70;
settings.nRandom = 1000;
settings.uncReg = 0;
m = 100;

%fBarcode = normrnd(0,1,1,m);
bar = ZEROMODEL.generate_autocorr_rand_seq(m,1 ,'gaussian' );
fBarcode = bar{1};

randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end

randBar = ZEROMODEL.generate_autocorr_rand_seq(settings.contigSize,settings.nRandom ,'gaussian' );

%size(randBar)

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );


f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
x0 = [30];
x = fsolve(f,x0,optimoptions('fsolve','Display','off'));

%m =size(corCoefAll(:),1);

%x = 20;
N = FUNCTIONS.rootN_numeric(x,corCoef(:));

%N2 = FUNCTIONS.rootN(x,corCoef(:));

rSquared = COMPARISON.compute_r_squared( corCoef(:), [x,N], 'exactfull' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
rSquared2 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )

rSquared3 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gev' );
% 
% evdPar =  COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
% COMPARISON.compare_distribution_to_data( corCoefAll(:),evdPar,  'normal' )

COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )
COMPARISON.compare_distribution_to_data( corCoef(:), [x, N], 'exactfull' )

%rSquaredT = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exactfull' );


%COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exact' )
%COMPARISON.compare_distribution_to_data( corCoef(:), [x, 2*N], 'exactfull' )

%rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exact' );
%rSquared2 = COMPARISON.compute_r_squared( corCoef(:), [x,2*N], 'exactfull' );


%evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
%rSquared = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );
%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gumbel' )

% 