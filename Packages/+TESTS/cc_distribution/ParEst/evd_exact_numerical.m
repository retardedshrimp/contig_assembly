
settings = SETTINGS.settings(); % 


settings.contigSize = 20;
settings.nRandom = 100;
settings.uncReg = 0;
m = 25;

fBarcode = normrnd(0,1,1,m);
% bar = ZEROMODEL.rand_seq_with_same_autocorr(barcode(1:m), m, 1);
% fBarcode = bar{1};

randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end

% randBar = ZEROMODEL.rand_seq_with_same_autocorr(barcode(1:20),settings.contigSize,settings.nRandom);

%size(randBar)

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );

% f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
% x0 = [20];
% x = fsolve(f,x0);

%m =size(corCoefAll(:),1);
%x = 20;
%x = 20;
%N = FUNCTIONS.rootN_numeric(x,corCoef(:));

%N2 = FUNCTIONS.rootN(x,corCoef(:));


COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exact' )
COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exactfull' )

rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exact' );
rSquared2 = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exactfull' );

f = @(y) COMPARISON.ev_find_n(y,corCoef(:));
x0 = [30];
x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
N = FUNCTIONS.rootN_numeric(x,corCoef(:));

%rSquared2 = COMPARISON.compute_r_squared( corCoef(:), [x,2*N], 'exactfull' );
%COMPARISON.compare_distribution_to_data( corCoef(:), [round(x),round(2*N)], 'exactfull' )

% 
% Nn = [];
% for kk=1:1
%     
% 
%     settings.contigSize = 20;
%     settings.nRandom = 10000;
%     settings.uncReg = 0;
%     m = 30;
% 
%     fBarcode = normrnd(0,1,1,m);
% 
% 
%     randBar = cell(1,settings.nRandom);
%     for i=1:settings.nRandom
%         randBar{i} = normrnd(0,1,1,settings.contigSize);
%     end
% 
%     [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );
% 
% 
%     %f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
%     f = @(y) COMPARISON.ev_find_n(y,corCoefAll(:));
% 
%     x0 = [20];
%     x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
% 
%     m =size(corCoefAll(:),1);
%     %x = 20;
%     %x = 20;
%     N = FUNCTIONS.rootN_numeric(x,corCoef(:));
%     Nn = [Nn N];
% end
