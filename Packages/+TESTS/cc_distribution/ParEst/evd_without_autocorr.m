settings = SETTINGS.settings(); % 

tic
NNN = [];
settings.contigSize = 30;
settings.nRandom = 1000; % number of random sequences
settings.uncReg = 0;
m = 60;

fBarcode = normrnd(0,1,1,m);

randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );


f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
x0 = [20];
x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
%x = 20;
N = FUNCTIONS.rootN_numeric(x,corCoef(:), 'betainc');


rSquared = COMPARISON.compute_r_squared( corCoef(:), [x,N], 'exactfull' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
rSquared2 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )

rSquared3 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gev' );

rSquaredT = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exactfull' );

COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )
COMPARISON.compare_distribution_to_data( corCoef(:), [x, N], 'exactfull' )


f = @(y) COMPARISON.ev_find_n(y,corCoef(:));
x0 = [30];
x = fsolve(f,x0,optimoptions('fsolve','Display','off'));


%1+betainc(corCoef(:).^2,1/2,x/2-1))

N = FUNCTIONS.rootN_numeric(x,corCoef(:));


rSquared2 = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exactfull' );
rSquared3 = COMPARISON.compute_r_squared( corCoef(:), [x,2*N], 'exactfull' );
toc

COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exactfull' )
COMPARISON.compare_distribution_to_data( corCoef(:), [x,N], 'exactfull' )

%N2 = FUNCTIONS.rootN(x,corCoef(:));


% COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exact' )
% COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exactfull' )

%rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exact' );
% 
%     NNN = [];
%     xx = [];
% for ss=1:100
%     settings.contigSize = 30;
%     settings.nRandom = 1000; % number of random sequences
%     settings.uncReg = 0;
%     m = 60;
% 
%     fBarcode = normrnd(0,1,1,m);
% 
%     randBar = cell(1,settings.nRandom);
%     for i=1:settings.nRandom
%         randBar{i} = normrnd(0,1,1,settings.contigSize);
%     end
%     [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );
% 
% 
%     f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
%     x0 = [20];
%     x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
%     %x = 20;
%     N = FUNCTIONS.rootN_numeric(x,corCoef(:));
%     NNN = [NNN N];
%     xx = [xx x];
% end
% 
