settings = SETTINGS.settings(); % 

tic
NNN = [];
for ss=1:100
    settings.contigSize = 20;
    settings.nRandom = 100; % number of random sequences
    settings.uncReg = 0;
    m = 1000;

    fBarcode = normrnd(0,1,1,m);

    randBar = cell(1,settings.nRandom);
    for i=1:settings.nRandom
        randBar{i} = normrnd(0,1,1,settings.contigSize);
    end
    [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );


    f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
    x0 = [20];
    %x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
    x = 20;
    N = FUNCTIONS.rootN_numeric(x,corCoef(:));
    NNN=[NNN N];
end

rSquared2 = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exactfull' );
rSquared3 = COMPARISON.compute_r_squared( corCoef(:), [x,2*N], 'exactfull' );
toc

COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exactfull' )
COMPARISON.compare_distribution_to_data( corCoef(:), [x,2*N], 'exactfull' )

%N2 = FUNCTIONS.rootN(x,corCoef(:));


% COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exact' )
% COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m], 'exactfull' )

%rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exact' );


% 
% f = @(y) COMPARISON.ev_find_n(y,corCoef(:));
% x0 = [30];
% x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
% N = FUNCTIONS.rootN_numeric(x,corCoef(:));
