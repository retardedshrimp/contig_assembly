
corLength = 7;
[c,b] = COMPARISON.decorrelated_cor_coef(corCoefAll, corLength );

xx = [];
NN = [];
r1 = [];
r2 = [];
r3 = [];
for i=1:corLength

    f = @(y) COMPARISON.rootFunction(y,b{i}(:));
    x0 = [30];
    x = fsolve(f,x0,optimoptions('fsolve','Display','off'));

    %m =size(corCoefAll(:),1);

    %x = 20;
    N = FUNCTIONS.rootN_numeric(x,c{i}(:));
    xx = [xx x];
    NN = [NN N];

    rSquared = COMPARISON.compute_r_squared( c{i}(:), [x,N], 'exactfull' );
    r1 = [r1 rSquared];
    
    
    
    evdPar =  COMPARISON.compute_distribution_parameters(c{i}(:),'gumbel');
    rSquared2 = COMPARISON.compute_r_squared(c{i}(:), evdPar, 'gumbel' );
    
    r2 = [r2 rSquared2];
    
    evdPar =  COMPARISON.compute_distribution_parameters(c{i}(:),'gev');
%     %COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )
% 
    rSquared3 = COMPARISON.compute_r_squared(c{i}(:), evdPar, 'gev' );
    
    r3 = [r3 rSquared3];
    

	COMPARISON.compare_distribution_to_data(  c{i}(:), evdPar, 'gev' )
    COMPARISON.compare_distribution_to_data( c{i}(:), [x, N], 'exactfull' )

end

NN
size(b{1})

mean(r1)
mean(r2)
mean(r3)