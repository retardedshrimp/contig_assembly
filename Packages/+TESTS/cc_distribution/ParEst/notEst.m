%% Simulation of Gaussian Processes (1 dim): 
%% Wood and Chan: Simulation of Stationary Gaussian Processes in [0,1]^d
clc
tic

n=100; % Length of gaussian sequence

t = (0:n)'; % time steps
m = 2*n; % m = 2^g >= 2n, choose smallest g

alpha = 2;

% Correlation functions as input: <x(t)x(t-n)> = cov(n)
% Here Rice parameter: phi = cov(t=1)/cov(0)
%% Exponential
%c_fun = alpha.^t;

%% Power-Law 
c_fun = 1./(t.^(alpha)+1);

x = 1:373;
[autoCor] = autocorr(barcode, 372);
uu = polyfit(x,autoCor,20);
cVa = polyval(uu,x);
c_fun= transpose(cVa(1:n+1));

%% Algorithm starts here

% First row of the circulant matrix C, Eq. (2.2) (p. 412)
C1 = [c_fun; c_fun(length(t)-1:-1:2)];

% Eigenvalues lambda of C1 via FFT, Eq. (2.4) (p. 413)
lambda = real(fft(C1));

% Check that eigenvalues are non-negative
if any(lambda < 0)       
    disp('Try new lengths n and m')
end

tic
% Inverse FFT 
hist = 1000; % number of realizations = 2*hist
pos1 = zeros(n,1); % vector to hold mean position
pos2 = zeros(n,1); % vector to hold mean squared position
cov_fun = zeros(n,1); % vector to hold simulated covariance function
for dum1 = 1:hist    
    % Steps S1, S2 and S3 (p. 413)
    % W_j = sqrt(lambda)*Q*Z; Q*Z = randn(m,1)+irandn(m,1) Eq. (3.21) (p. 417)
    U = randn(m,1) + sqrt(-1)*randn(m,1);
    x = ifft(sqrt(lambda).*U)*sqrt(m); %sqrt(1/m) in Eq. (2.5) ???
                                       %Because of MATLAB def of FFT ???
    out1 = real(x(1:n));
    out2 = imag(x(1:n));

    % compute covariance function of simulated process
    cov_fun = cov_fun + out1(1)*out1 + out2(1)*out2;
    
    % compute first and second moment
    pos1 = pos1 + out1 + out2; %should be close to zero
    pos2 = pos2 + out1.^2 + out2.^2; %should be close to c_fun(t=0)
end
%normalization
cov_fun = cov_fun/(2*hist); 
pos1 = pos1/(2*hist);
pos2 = pos2/(2*hist);
% 
% figure(1)
% clf
% plot(0:n-1,c_fun(1:n),'-o','LineWidth',3) 
% hold on
% plot(0:n-1,cov_fun,'o','LineWidth',3)
% xlabel('steps $n$','interpreter','latex','FontSize',17)
% ylabel('covariance $\gamma(n)$','interpreter','latex','FontSize',17)
% legend({'Input covariance','Simulated covariance'},...
%     'interpreter','latex','FontSize',17,'Location','northeast')
% 
% figure(2)
% plot(1:n,pos1,'-o','LineWidth',3) 
% xlabel('steps $n$','interpreter','latex','FontSize',17)
% ylabel('first moment','interpreter','latex','FontSize',17)
% ylim([-0.02 0.02])
% 
% figure(3)
% plot(1:n,pos2-pos1.^2,'-o','LineWidth',3) 
% xlabel('steps $n$','interpreter','latex','FontSize',17)
% ylabel('second moment','interpreter','latex','FontSize',17)
% ylim([c_fun(1)-0.02 c_fun(1)+0.02])
toc

us = pos2-pos1.^2;




