
settings = SETTINGS.settings(); % 

settings.contigSize = 4;
settings.nRandom = 1000;
settings.uncReg = 0;
m = 50;

fBarcode = interp1(normrnd(0,1,1,m), linspace(1,m,5*m));


randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = interp1(normrnd(0,1,1,settings.contigSize),linspace(1,settings.contigSize,5*settings.contigSize));
end
settings.contigSize = 5*settings.contigSize;

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );

parNormal = COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
COMPARISON.compare_distribution_to_data( corCoefAll(:), parNormal, 'normal' );
COMPARISON.compare_distribution_to_data( corCoefAll(:), settings.contigSize/2, 'cc' );

%figure, plot(corCoefAll(:) )


settings.method = 'fisher';

[ transformedCorCoef ] = COMPARISON.transform_coeficients(corCoef,settings.method);
[ transformedCorCoefAll ] = COMPARISON.transform_coeficients(corCoefAll(:),settings.method);



% 
% COMPARISON.compare_distribution_to_data(  corCoefAll(:), NN, 'ccfisher' )
% 

% evdPar =  COMPARISON.compute_distribution_parameters(transformedCorCoef,'gumbel');
% evdPar2 =  COMPARISON.compute_distribution_parameters(transformedCorCoef,'gev');
% parNormal = COMPARISON.compute_distribution_parameters(transformedCorCoefAll,'normal');
% 
% 
% 
% COMPARISON.compare_distribution_to_data( transformedCorCoefAll, parNormal, 'normal' )
% COMPARISON.compare_distribution_to_data( transformedCorCoef, evdPar, 'gumbel' )
% COMPARISON.compare_distribution_to_data( transformedCorCoef, evdPar2, 'gev' )
% 
% rSquared = COMPARISON.compute_r_squared( transformedCorCoef, evdPar, 'gumbel' );
% 

bBarcode = interp1(fBarcode, linspace(1, m,2*m));
figure, plot(bBarcode)
hold on
plot(fBarcode)


transition_probabilities = [0.1 0.4 0.5;0.6 0.2 0.2;0.1 0.8 0.1]; starting_value = 1; chain_length = 1000;

    chain = zeros(1,chain_length);
    chain(1)=starting_value;

    for i=2:chain_length
        this_step_distribution = transition_probabilities(chain(i-1),:);
        cumulative_distribution = cumsum(this_step_distribution);

        r = rand();

        chain(i) = find(cumulative_distribution>r,1);
    end
   