load 'dataDA15000 contigs.mat';
load 'plasmid_puuh.mat';

settings = SETTINGS.settings(); % 
contigData = seq;

seq = IMPORT.read_fasta('DA1500 all both 13.fasta');
%load puuh_barcodes.mat;
%If we want to calculate the barcodes
barC = cell(1,size(seq,2));
barProb = cell(1,size(seq,2));
for i=1:size(seq,2)
    i
    if ~isempty(seq{i})
        barProb{i} = CBT2.cb_two_ligand(seq{i},settings);
        if size(barProb{i},2) > 8995
            barcodeSeqPSF = CBT.apply_point_spread_function(barProb{i}, settings.psfWidth*settings.bpPerNm, false); %false if circular
            barC{i} = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);
        end
    end
end


barL = cell(1,size(seq,2));
barProbL = cell(1,size(seq,2));
parfor i=1:size(seq,2)
    if ~isempty(seq{i})
        barProbL{i} = CBT2.cb_two_ligand(seq{i},settings);
        if size(barProbL{i},2) > 8995
            barcodeSeqPSF = CBT.apply_point_spread_function(barProb{i}, settings.psfWidth*settings.bpPerNm, true); %false if circular
            barL{i} = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);
        end
    end
end

% If we want to do uncertainty region analysis
% 
% for i=1:10
%     uncertainLength = i;
% 
%     k =figure('Visible','off'); plot(barC{1});
%     hold on
%     plot((barL{1}-mean(barL{1}(uncertainLength:end-uncertainLength)))/(std(barL{1}(uncertainLength:end-uncertainLength) ) ));
%     ylim([-4 4])
%     ylabel('intensity')
%     xlabel('Position (pixel)')
%     legend({'Circular barcode',strcat( 'Linear barcode with uncertain region = ',num2str(uncertainLength))},'Position',[0.6 0.8 0.1 0.1])
%     saveName = sprintf('uncertaintyLength%d.jpg',uncertainLength);
%     saveas(k, saveName); 
% end

load Consensus_pUUH.mat
load ZERO_markov.mat; % load zero model
settings.zeroModel = zeroModel;

sizeRand = size(refCurve,2);

intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

barLength = 25*sizeRand;

randBarcodes = zeros(settings.nRandom,barLength);   

tic % just to check how fast it is, can be commented out
for iNew = 1:settings.nRandom
    barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
    randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
    randBarcodes(iNew,:) = (randBarcode-mean(randBarcode))/std(randBarcode);
end
toc
    

corCoef = zeros(settings.nRandom,1);
corCoefRev = zeros(settings.nRandom,1);
corCoefVec = zeros(settings.nRandom,2*size(randBarcode,2));
uncertainLength = 10; % adjust based on PSF width, should be equal to PSF in pixels

gevPar = cell(1, size(randBarcode,2));
gumPar = cell(1, size(randBarcode,2));

for i=13
    %i
    %barCur = barL{i};
    barCur = randBarcode;
    
    if ~isempty(barCur)
        barCur = barCur(uncertainLength:end-uncertainLength);
        barCur = (barCur - mean(barCur))/std(barCur);
        
        for iNew = 1:1000
            [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barCur,randBarcodes(iNew,:));
            
            corCoef(iNew) = max(cc1);
            
            corCoefRev(iNew) = max(cc2);
            
            corCoefVec(iNew,:) = [cc1 cc2];
            
        end
        allCof = [corCoef corCoefRev];
        gumbelFit = evfit(-allCof(:)); % originally evfits fits for minima, so we have to be a bit careful
        gFitTrunc = COMPARISON.evfit_max(allCof(:) ) ;
        gevFit = fitdist(allCof(:),'GeneralizedExtremeValue'); % since this includes gumbel, results should be similar
        gevFit22 = fitdist(allCof(:),'ev'); % since this includes gumbel, results should be similar

        gevParams = [gevFit.k, gevFit.sigma, gevFit.mu];
        %gevPar{i} = gevParams;
        %gumPar{i} = gumbelFit;
        
    end
end

[f,x]=  hist([corCoef, corCoefRev],100);

cc = linspace(-0.1,1,1001);

gev = evpdf(-cc, gumbelFit(1), gumbelFit(2));
gev2 = gevpdf(cc, gevParams(1), gevParams(2),gevParams(3));
gfit = evpdf2(cc, gFitTrunc(1), gFitTrunc(2));
gfit3 = evpdf2(cc, gevFit22.mu, gevFit22.sigma);

figure
bar(x,f/trapz(x,f));hold on
plot(cc, gev, 'r', 'linewidth',1)     ;
plot(cc, gev2, 'black', 'linewidth',2) ;    
plot(cc, gfit, 'green', 'linewidth',1) ; 
plot(cc, gfit3, 'blue', 'linewidth',1) ; 

xlabel('Maximal correlation coefficient')
ylabel('PDF')
legend({'Histogram', 'Gumbel', 'GEV','Truncated Gumbel'},'Position',[0.3 0.7 0.1 0.1])

gev = evpdf(-x, gumbelFit(1), gumbelFit(2));
gev2 = gevpdf(x, gevParams(1), gevParams(2),gevParams(3));
gfit = evpdf2(x, gFitTrunc(1), gFitTrunc(2));
gfit3 = evpdf(x, gevFit22.mu, gevFit22.sigma);

SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
SSres = sum((f/trapz(x,f)-gev).^2);
SSres2 = sum((f/trapz(x,f)-gev2).^2);
SSres3 = sum((f/trapz(x,f)-gfit).^2);
SSres4 = sum((f/trapz(x,f)-gfit3).^2);

RsqrdGumbel= 1-SSres/SSTot;

RsqrdGEV = 1-SSres2/SSTot;

RsqrdTrunc = 1-SSres3/SSTot;
RsqrdTrunc2 = 1-SSres4/SSTot;

