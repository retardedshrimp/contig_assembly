
settings = SETTINGS.settings(); % 


settings.contigSize = 30;
settings.nRandom = 5000;
settings.uncReg = 0;
m = 60;

bar = normrnd(0,1,m,1);

randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,settings.contigSize,1);
end

tic
r = zeros(settings.nRandom, m);

for i=1:settings.nRandom
    for j=1:m
        r(i,j) = corr(randBar{i},bar(1:settings.contigSize),'type','Spearman');
        bar = circshift(bar,[-1,0]);
    end
end
toc


corCoef = max(transpose(r));

evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )

rSquared3 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gev' );

evdPar2 =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
rSquared2 = COMPARISON.compute_r_squared(corCoef(:), evdPar2, 'gumbel' );
COMPARISON.compare_distribution_to_data( corCoef(:), evdPar2, 'gumbel' )
