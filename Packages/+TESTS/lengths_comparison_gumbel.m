% This test compares how close distribution is to Gumbel
% based on lengths of contigs

addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));
settings = SETTINGS.settings(); % 


%load ZERO.mat; % load zero model
load ZERO_markov.mat; % load zero model
settings.zeroModel = zeroModel;

distToPlot = 'Gumbel'; 

binNum = 30;

figure;
Rsqrd = [];
for kk = 1:9
    kk
    sizeRand = kk*15;
    sizeRand2 = kk*15;

    intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

    barLength = ceil(size(intZeroModel,2)/(settings.bpPerNm*settings.camRes));

    randBarcode = zeros(1,barLength);   

    tic % just to check how fast it is, can be commented out
    for iNew = 1:1
        barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
        randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
        randBarcode = (randBarcode-mean(randBarcode))/std(randBarcode);
    end
    %randBarcode = barcode(1:30*kk);

    %randBarcode = barcode(1:30*kk);
    [gumbelFit, gevParams, corCoef, corCoefRev, corCoefVec] = COMPARISON.evd_with_shorter_barcode_randomised( randBarcode, sizeRand2, settings );

    %normFit = fitdist(corCoefVec(:),'Normal'); % since this includes gumbel, results should be similar
    cc = linspace(-1,1,binNum);
    subplot(3,3,kk)
    if isequal(distToPlot,'Gumbel')

        [f,x]=  hist([corCoef, corCoefRev],binNum);

        bar(x,f/trapz(x,f));hold on
        gev = evpdf(-x, gumbelFit(1), gumbelFit(2));

        plot(x, gev, 'r', 'linewidth',3);
        xlabel('max(C)')
        
        SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
        SSres = sum((f/trapz(x,f)-gev).^2);
        R = 1-SSres/SSTot;
        Rsqrd = [Rsqrd R];

    else
                
        normFit = fitdist(corCoefVec(:),'Normal'); % since this includes gumbel, results should be similar
    
        gev = normpdf(cc, normFit.mu, normFit.sigma);
    
        [f,x]=  hist(corCoefVec(:),30);
            
        bar(x,f/trapz(x,f));hold on
        plot(cc, gev, 'r', 'linewidth',3)     
        xlabel('CC')
    end
    ylabel('PDF')

    %legend({'Correlation coefficient hist', 'Normal distribution'})
    
end