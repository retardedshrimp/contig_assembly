
settings.contigSize = 70;
settings.nRandom =100;

randBar = ZEROMODEL.generate_autocorr_rand_seq(settings.contigSize,settings.nRandom ,'circular',barcode);
%randBar = ZEROMODEL.generate_autocorr_rand_seq(settings.contigSize,1 ,'sample',barcode);

% autoC = autocorr(randBar{1},size(randBar{1},2)-1);
% figure, plot(autoC)


tt = 0;
for i=1:settings.nRandom
    tt = tt+randBar{i};
    
end
tt = tt/ settings.nRandom;

figure, plot(tt)

rr = 0;


for i=1:settings.nRandom
    [autoC,~] = COMPARISON.cross_correlation_coefficients_fft(randBar{i},randBar{i});
    %autoC = autocorr(randBar{i},size(randBar{i},2)-1);
	rr = rr +autoC;
end
	rr =rr / settings.nRandom;
	
figure,plot(rr) 