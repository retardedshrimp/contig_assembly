% In this file, some experiments with contigs.
% Only for testing purposes...

load 'dataDA15000 contigs.mat';
load 'plasmid_puuh.mat';

settings = SETTINGS.settings(); % 

contigData = seq;



contigNum = 1;
maxBitLength = 2;

averageProb = cell(1,contigNum);
for i=2
   contigSeq = contigData{i};
   %averageProb{i} = ZEROMODEL.zero_model_markov({contigSeq});
end

barcodeSeq = CBT2.cb_two_ligand(contigSeq,settings);
barcodeSeqPSF = CBT.apply_point_spread_function(barcodeSeq, settings.psfWidth*settings.bpPerNm, false); %false if circular
barC = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);

theorBarcodeSeq = CBT2.cb_two_ligand(plasmid, settings);
barcodetheorPSF = CBT.apply_point_spread_function(theorBarcodeSeq, settings.psfWidth*settings.bpPerNm, false); %false if circular
barT = NUMERICAL.rescale_barc(transpose(barcodetheorPSF), settings);
  
for bitCut=0:1:maxBitLength    
   contigSeqCut = (barC(1+bitCut:end-bitCut)-mean(barC(1+bitCut:end-bitCut)))/std(barC(1+bitCut:end-bitCut));
   [ ccForward, ccBackward] = COMPARISON.cross_correlation_coefficients_fft(contigSeqCut, barT);
end
max(max(ccForward,ccBackward))