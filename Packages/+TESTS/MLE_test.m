


% function
myfun = @(sigma,x0) mean(x0)-sigma-sum(x0.* exp(-x0 ./ sigma)+exp(-1/sigma))/(sum( exp(-x0 ./ sigma)+exp(-1/sigma)) );  % parameterized function

% additional parameter
x0 = corCoef(:);

% close to solution
w0 = (sqrt(6)*std(x0))/pi;

fun = @(x) myfun(x,x0);    % function of x alone

sigma = fzero(fun,w0);

mu = sigma*(log(n)-log(sum( exp(-x0 ./ sigma)+exp(-1/sigma)) ) );
