load 'dataDA15000 contigs.mat';
load 'plasmid_puuh.mat';

settings = SETTINGS.settings(); % 
contigData = seq;

seq = IMPORT.read_fasta('DA1500 all both 13.fasta');
%load puuh_barcodes.mat;
load 'DA1500_barcodes.mat';

figure, plot(barL{29})
ylabel('intensity')
 ax = gca;
ticks = 1:5:size(barL{29},2);
ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
ax.XTick = [ticks];
ax.XTickLabel = [ticksx ];
xlabel('Position (kbp)')

barCur = barL{29};
uncertainLength = 5;
barCur = barCur(uncertainLength:end-uncertainLength);
barCur = (barCur - mean(barCur))/std(barCur);


figure, plot(barCur)
ylabel('intensity')
 ax = gca;
ticks = 1:5:size(barCur,2);
ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
ax.XTick = [ticks];
ax.XTickLabel = [floor(uncertainLength*settings.bpPerNm*settings.camRes/1000)+ticksx];
xlabel('Position (kbp)')
%figure, plot(1.5,-2,'o')
%legend({'Circular barcode',strcat( 'Linear barcode with uncertain region = ',num2str(uncertainLength))},'Position',[0.6 0.8 0.1 0.1])

%If we want to calculate the barcodes
% barC = cell(1,size(seq,2));
% barProb = cell(1,size(seq,2));
% for i=1:size(seq,2)
%     i
%     if ~isempty(seq{i})
%         barProb{i} = CBT2.cb_two_ligand(seq{i},settings);
%         if size(barProb{i},2) > 8995
%             barcodeSeqPSF = CBT.apply_point_spread_function(barProb{i}, settings.psfWidth*settings.bpPerNm, false); %false if circular
%             barC{i} = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);
%         end
%     end
% end
% 
% 
% barL = cell(1,size(seq,2));
% barProbL = cell(1,size(seq,2));
% parfor i=1:size(seq,2)
%     if ~isempty(seq{i})
%         barProbL{i} = CBT2.cb_two_ligand(seq{i},settings);
%         if size(barProbL{i},2) > 8995
%             barcodeSeqPSF = CBT.apply_point_spread_function(barProb{i}, settings.psfWidth*settings.bpPerNm, true); %false if circular
%             barL{i} = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);
%         end
%     end
% end

% If we want to do uncertainty region analysis
% % 
% for i=1:10
%     uncertainLength = i;
% 
%     k =figure('Visible','off'); plot(barC{1});
%     hold on
%     plot((barL{1}-mean(barL{1}(uncertainLength:end-uncertainLength)))/(std(barL{1}(uncertainLength:end-uncertainLength) ) ));
%     ylim([-4 4])
%     ylabel('intensity')
%     xlabel('Position (pixel)')
%     legend({'Circular barcode',strcat( 'Linear barcode with uncertain region = ',num2str(uncertainLength))},'Position',[0.6 0.8 0.1 0.1])
%     saveName = sprintf('uncertaintyLength%d.jpg',uncertainLength);
%     saveas(k, saveName); 
% end

% barLen = size(zeroModel,2);
% newRes = round((1/(settings.bpPerNm*1000))*(barLen-1)); %could stretch more precisely, so that kbp/pixel ratio would be 0.57
% rescaledBarcode = interp1(zeroModel, 1:(barLen-1)/newRes:barLen);
% rescaledBarcode = (rescaledBarcode-mean(rescaledBarcode))/std(rescaledBarcode);

% take
settings.nRandom = 100;
barCur = barC{29};
uncertainLength = 5;
barCur = barCur(uncertainLength+1:end-uncertainLength);
barCur = (barCur - mean(barCur))/std(barCur);

load ZERO_markov.mat;
settings.zeroModel = zeroModel;
sizeRand = size(barC{29},2);

intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

barLength = sizeRand-2*uncertainLength;

randBarcodes = zeros(settings.nRandom,barLength);   

tic % just to check how fast it is, can be commented out
for iNew = 1:settings.nRandom
    barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
    randBarcode = interp1(barc,linspace(1,size(barc,2),sizeRand));
    randBarcode = randBarcode(uncertainLength+1:end-uncertainLength);
    randBarcodes(iNew,:) =(randBarcode-mean(randBarcode))/std(randBarcode);
end
toc

 
corCoef = zeros(settings.nRandom,1);
corCoefRev = zeros(settings.nRandom,1);
corCoefVec = zeros(settings.nRandom,2*size(refCurve,2));
for iNew = 1:settings.nRandom
    [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(randBarcodes(iNew,:),refCurve);

    corCoef(iNew) = max(cc1);

    corCoefRev(iNew) = max(cc2);

    corCoefVec(iNew,:) = [cc1 cc2];

end
allCof = [corCoef corCoefRev];
gumbelFit = evfit(-allCof(:)); % originally evfits fits for minima, so we have to be a bit careful
gevFit = fitdist(allCof(:),'GeneralizedExtremeValue'); % since this includes gumbel, results should be similar
gevParams = [gevFit.k, gevFit.sigma, gevFit.mu];
[f,x]=  hist(allCof(:),50);

[cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barCur,refCurve);
% max(cc1)
% max(cc2) 
gevFit.mu-gevFit.sigma/gevFit.k

gev = evpdf(-x, gumbelFit(1), gumbelFit(2));
gev2 = gevpdf(x, gevParams(1), gevParams(2),gevParams(3));

SSres = sum((f/trapz(x,f)-gev).^2);
SSres2 = sum((f/trapz(x,f)-gev2).^2);
SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);


RsqrdGumbel= 1-SSres/SSTot;

RsqrdGEV = 1-SSres2/SSTot;

%CMN_HelperFunctions.p_val_gumbel(max(max(cc1,cc2)),gev)
CMN_HelperFunctions.p_calc_GEV(max(max(cc1,cc2)),gevParams)

ccc = [];
ccb = [];
for i=1:size(cc1,2)
    ccc = [ccc CMN_HelperFunctions.p_calc_GEV(cc1(i),gevParams)];
    ccb = [ccb CMN_HelperFunctions.p_calc_GEV(cc2(i),gevParams)];
end
% 
% ccc(ccc<0.01)
% ccb(ccb<0.01)

% x = x(30:end);
% gev = gev(30:end);
% f = f(1,30:end);
% SSres = sum((f/trapz(x,f)-gev).^2);
% SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
% RsqrdGumbel= 1-SSres/SSTot;

