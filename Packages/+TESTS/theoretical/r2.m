
settings = SETTINGS.settings(); % 

settings.contigSize = 20;
settings.nRandom = 2000;
settings.uncReg = 0;
m = 30;

fBarcode = normrnd(0,1,1,m);


randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end

xx = floor(linspace(100, settings.nRandom, 100));
rr  = [];
for i=1:100
    settings.nRandom = xx(i);
    [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );
    rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m], 'exact' );
    rr = [rr rSquared];
end

figure, plot(xx,rr)
ylabel('R^2')
xlabel('Number of random sequences')
legend({'R^2'})