load barcode.mat;

settings = SETTINGS.settings(); % 

% we can choose number of contigs we want.
numContigs = 5;
lenSeq = size(barcode,2);

% cut a sequence at numContigs random places
seqEnd = sort(randi(lenSeq,1, numContigs));

% assume there are no gaps.
seqLengths = [lenSeq-seqEnd(end)+seqEnd(1) diff(seqEnd, [], 2)];

contigData = cell(1,numContigs);
for i=1:numContigs % calculate barcodes
    if i == 1
        contigData{1} = [barcode(seqEnd(end):end) barcode(1:seqEnd(1))];
    else
        contigData{i} = [barcode(seqEnd(i-1)+1:seqEnd(i))];
    end
    contigData{i} =  (contigData{i}-mean( contigData{i}))/std(contigData{i});
    [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(contigData{i}, barcode);
    figure, plot(cc1)
end



