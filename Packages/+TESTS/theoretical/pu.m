load 'plasmid_puuh.mat';
settings = SETTINGS.settings(); % 


% we can choose number of contigs we want.
untReg = 5;
numContigs = 5;
seqLengths = floor(size(plasmid,2)/numContigs)*ones(1,numContigs);

contigData = cell(1,numContigs);

for i=1:numContigs % calculate barcodes
    contigData{i} = [plasmid((i-1)*seqLengths(1)+1:(i)*seqLengths(1))];
end

contigBarcodes = ZEROMODEL.generate_barcodes_for_contigs(contigData,settings);

contigsNew = cell(1,numContigs);
for i=1:numContigs
    contigsNew{i} = interp1(contigBarcodes{i}, linspace(untReg+1,size(contigBarcodes{1},2)-untReg,round(size(contigBarcodes{1},2)/untReg)));
end

barcodeNew = interp1(barcode,linspace(1, size(barcode,2),round(size(barcode,2)/untReg)));


settings.contigSize = size(contigsNew{1},2)+10;
settings.nRandom = 1600000;
settings.uncReg = 5;
m = size(barcodeNew,2);

randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, barcodeNew, settings );

parNormal = COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
COMPARISON.compare_distribution_to_data( corCoefAll(:), parNormal, 'normal' )

COMPARISON.compare_distribution_to_data( corCoefAll(:), settings.contigSize-2*settings.uncReg, 'cc' )
COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize-2*settings.uncReg,2*m+1-39], 'exact' )
rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize-2*settings.uncReg,2*m+1-40], 'exact' );



settings.contigSize = size(contigBarcodes{1},2)+10;
settings.nRandom = 1000;
settings.uncReg = 5;
m = size(barcode,2);

randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end
interp1

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, barcodeNew, settings );

parNormal = COMPARISON.compute_distribution_parameters(corCoefAll(:),'normal');
COMPARISON.compare_distribution_to_data( corCoefAll(:), parNormal, 'normal' )

COMPARISON.compare_distribution_to_data( corCoefAll(:), settings.contigSize, 'cc' )
COMPARISON.compare_distribution_to_data( corCoef(:), [settings.contigSize,2*m+1], 'exact' )
rSquared = COMPARISON.compute_r_squared( corCoef(:), [settings.contigSize,2*m+1], 'exact' );

    