% this plots a heat map for random barcodes vrs random barcodes
% http://stackoverflow.com/questions/15676363/matlab-how-does-one-plot-a-heatmap-from-nxn-matrix

addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));
settings = SETTINGS.settings(); % 

%load ZERO.mat; % load zero model
%load ZERO_markov.mat; % load zero model
%settings.zeroModel = zeroModel;

%distToPlot = 'Gumbel'; 

tryNum = 10;

%figure;
RsqrdExact = zeros(tryNum, tryNum);
RsqrdGumbel = zeros(tryNum, tryNum);
RsqrdGEV = zeros(tryNum, tryNum);
%RsqrdNormal = zeros(tryNum, tryNum);

%pValGumbel =  zeros(tryNum, tryNum);
%pValGEV = zeros(tryNum, tryNum);

for kk = 1:tryNum
    binNum = floor((10+kk)/4);

    tic
    for ll=kk:tryNum
        sizeRand = 30+ll;
        sizeRand2 = 30+kk;

        %intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

        %barLength = ceil(size(intZeroModel,2)/(settings.bpPerNm*settings.camRes));

        %randBarcode = zeros(1,barLength);   

        for iNew = 1:1
            barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
            randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
            randBarcode = (randBarcode-mean(randBarcode))/std(randBarcode);
        end
        %randBarcode = barcode(1:ll*10);

        [gumbelFit, gevParams, corCoef, corCoefRev, corCoefVec] = COMPARISON.evd_with_shorter_barcode_randomised( randBarcode, sizeRand2, settings );



        [f,x]=  hist([corCoef, corCoefRev],binNum);

        gev = evpdf(-x, gumbelFit(1), gumbelFit(2));
        gev2 = gevpdf(x, gevParams(1), gevParams(2),gevParams(3));


        SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
        SSres = sum((f/trapz(x,f)-gev).^2);
        SSres2 = sum((f/trapz(x,f)-gev2).^2);
        RsqrdGumbel(ll,kk) = 1-SSres/SSTot;

        RsqrdGEV(ll,kk) = 1-SSres2/SSTot;

        
        normFit = fitdist(corCoefVec(:),'Normal'); % since this includes gumbel, results should be similar
        
        [f,x]=  hist(corCoefVec(:), binNum);
        normDist = normpdf(x, normFit.mu, normFit.sigma);
        
        SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
        SSres = sum((f/trapz(x,f)-normDist).^2);
        
        RsqrdNormal(ll,kk) = 1-SSres/SSTot;
        
        pValGumbel(ll,kk) = CMN_HelperFunctions.p_val_gumbel(1,gumbelFit);
        pValGEV(ll,kk) = CMN_HelperFunctions.p_calc_GEV(1,gevParams);

        %legend({'Correlation coefficient hist', 'Normal distribution'})
    end
    toc
    
end

RsqrdGEV(RsqrdGEV<0) = 0;
RsqrdGumbel(RsqrdGumbel<0) = 0;
 RsqrdNormal(RsqrdNormal<0) = 0;

 colormap('hot')
 figure
h1 = imagesc(RsqrdGEV);
colorbar
 saveas(h1,'RsqrdGEV3.jpg') ;
 
 figure
h2 = imagesc(RsqrdGumbel);
colorbar
 saveas(h2,'RsqrdGumbel3.jpg') 



 figure
h3 = imagesc(RsqrdNormal);
colorbar
 saveas(h3,'RsqrdNormal3.jpg') 

 figure
h4 = imagesc(pValGumbel);
colorbar
 saveas(h4,'pValGumbel3.jpg') 

 figure
h5 = imagesc(pValGEV);
colorbar
 saveas(h5,'pValGEV3.jpg') 
