% This is Gaussian embedding test to see recreate the plot from the article

% embedding for l>=1/sqrt(pi) and m>=sqrt(pi) l^2

m = ceil(sqrt(pi)*l^2);

nMax = 20;
mMax = 100;

tt = zeros(1,nMax);
for l=1:nMax
    for m=2:mMax
        t = 0:m;

        autoCorr = exp(-(t./l).^2 );

        C1 = [autoCorr fliplr(autoCorr(2:end-1))]; % first row of the circulant matrix

        sHat = real(fft(C1)); % fft of C1, always real


        if all(real(sHat) >=  -0.0000000000001)     % eigevalues have to be nonnegative
            tt(l) = m;
            break;
        end
    end
end

figure, plot(tt, 'o')
legend({'minimum m value for nonegative embedding'})
xlabel('l')
ylabel('m')