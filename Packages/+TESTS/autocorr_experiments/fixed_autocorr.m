% 18/10/16 by Albertas Dvirnas

settings = SETTINGS.settings(); % always load a settings file and put the parameter list there

% here we modify some stuff from the settings
settings.contigSize = 20; % the size of a smaller sequence
settings.nRandom = 100; % number of random sequences to generate
settings.uncReg = 0; % uncertain region at the start and end of the sequence
settings.longerSize = 25; % the size of a longer sequence
m = 25; % length of a longer sequence

[ Y ] = ZEROMODEL.simulated_autoregr( settings, 'AR(1)' );
tic
%[ randomSequences ] = ZEROMODEL.rand_seq_with_same_autocorr(barcode(1:20),20,100,'exponential' );


[ randomSequences ] = ZEROMODEL.rand_seq_with_same_autocorr(barcode(1:20),20,1000,'sample' );

[ randomSequences ] = ZEROMODEL.generate_autocorr_rand_seq(barcode(1:20),20,1000,'predefined' );

% this computes all the correlatino coefficients. However, data is
% correlated!

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randomSequences, barcode(1:20), settings );

% This find the fitting parameter n_eff
f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
x0 = [20];
x = fsolve(f,x0);

% make to function
ss = [];
for i=1:10
    
    [ randomSequences ] = ZEROMODEL.rand_seq_with_same_autocorr(barcode(1:20),20,1000,'sample' );

    % this computes all the correlatino coefficients. However, data is
    % correlated!

    [ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randomSequences, barcode(1:20), settings );

    % This find the fitting parameter n_eff
    f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
    x0 = [20];
    x = fsolve(f,x0);
    ss = [ss x];
end
    






