% 21/09/16
% Script to generate markov barcodes based on markovProbab

% TODO: put this into a function

addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));
settings = SETTINGS.settings(); % 

nBarcodes = 1000;
sizeBarcode = 400000;

barcodes = cell(1,nBarcodes);
parfor i=1:nBarcodes
    barc = ZEROMODEL.generate_markov_barcodes(markovProbab, 1, sizeBarcode);
    barcodes{i} = ZEROMODEL.generate_barcode(barc{1},settings);
end
    
