% 01/10/16 another test...

settings = SETTINGS.settings(); % 
seq = IMPORT.read_fasta('DA1500 all both 13.fasta');

tic % ~ takes rougly 2 - 3 minutes
theoreticalBarcodes = ZEROMODEL.generate_barcodes_for_contigs(seq,settings);
toc

load markov_prob.mat;
sizeR = 220824;
nn = 1000;

barLL = cell(1,nn);
parfor i=1:nn
    markovb = ZEROMODEL.generate_markov_barcodes(markovProbab, 1,sizeR );
    barLL{i} = ZEROMODEL.generate_barcodes_for_contigs(markovb{1},settings);
end

settings.nRandom = size(barLL,2);

uncReg = 5;
evdPar = cell(1,size(theoreticalBarcodes,2));

        
for i=1:size(theoreticalBarcodes,2)
    if ~isempty(theoreticalBarcodes{i})
        sizC = size(theoreticalBarcodes{i},2);
        corCoef = zeros(settings.nRandom,1);
        corCoefRev = zeros(settings.nRandom,1);

        %corCoefVec = zeros(settings.nRandom,);
        for iNew = 1:settings.nRandom
            curRand = barLL{iNew}(1+uncReg:sizC-uncReg);
            curRand = (curRand-mean(curRand))./std(curRand);
            [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(curRand,refCurve);
            corCoef(iNew) = max(cc1);
            corCoefRev(iNew) = max(cc2);
            %corCoefVec(iNew,:) = [cc1 cc2];
        end
        allCof = [max(corCoef,corCoefRev)];
        evdPar{i} = COMPARISON.evd_parameters(allCof(:), 'fisher');
    end
end

pValMat = CAP.generate_bit_matrix(theoreticalBarcodes, refCurve, evdPar, settings,'fisher');

%refCurve = clusterConsensusData.barcode;

fix28 = ZEROMODEL.generate_barcodes_for_contigs(newContig,settings);

 

theoreticalBarcodes{29} = fix28;
%theoreticalBarcodes{i} = aa;
for i=29
    if ~isempty(theoreticalBarcodes{i})
        sizC = size(theoreticalBarcodes{i},2);
        corCoef = zeros(settings.nRandom,1);
        corCoefRev = zeros(settings.nRandom,1);

        %corCoefVec = zeros(settings.nRandom,);
        for iNew = 1:settings.nRandom
            curRand = barLL{iNew}(1+uncReg:sizC-uncReg);
            curRand = (curRand-mean(curRand))./std(curRand);
            [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(curRand,refCurve);
            corCoef(iNew) = max(cc1);
            corCoefRev(iNew) = max(cc2);
            %corCoefVec(iNew,:) = [cc1 cc2];
        end
        allCof = max(corCoef,corCoefRev);
        evdPar{i} = COMPARISON.evd_parameters(allCof(:), 'tan');
    end
end

curBar = theoreticalBarcodes{i}(settings.uncReg+1:end-settings.uncReg);

%curBar = plb(settings.uncReg+1:end-settings.uncReg);

curBar = (curBar-mean(curBar))./std(curBar);
[cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(curBar,refCurve);

figure, plot(refCurve)
hold on
plot(271:271+size(curBar,2)-1,curBar)
%plot(ind, refCurve(ind),'s','markersize', 10);
%plot(inda, curBar(inda),'s','markersize', 10);
legend({'theoretical sequence barcode','contig barcode','CTX-M-15 on theor.s.','CTX-M-15 on contig' } );
ax = gca;
ticks = 1:50:373;
ticksx = round(ticks*592/1000);
ax.XTick = [ticks];
ax.XTickLabel = [ticksx];
xlabel('position (kbp)')


figure,subplot(3,1,1);
plot(cc2), hold on

ylabel('Correlation coefficient')
ax = gca;
ticks = 1:50:373;
ticksx = floor(ticks*592/1000);
ax.XTick = [ticks];
ax.XTickLabel = [ticksx];
xlabel('Starting placement position (kbp),flipped')%[cc1,cc2,flip] = CMN_HelperFunctions.ccorr_all(contigBarcodes{i},consensusBarcode,true,true);
ylim([0.5 1])

subplot(3,1,2), plot(refCurve)
xlabel('consensus barcode')%[cc1,cc2,flip] = CMN_HelperFunctions.ccorr_all(contigBarcodes{i},consensusBarcode,true,true);

subplot(3,1,3), plot(cc2)

figure, plot(fix28)
%ylabel('Correlation coefficient')
ax = gca;
ticks = 1:2:373;
ticksx = round(ticks*592/1000);
ax.XTick = [ticks];
ax.XTickLabel = [ticksx];
xlabel('Starting placement position (kbp)')%[cc1,cc2,flip] = CMN_HelperFunctions.ccorr_all(contigBarcodes{i},consensusBarcode,true,true);


%ylim([-1 1])

%PUUH_pUUH2392p0208
S = getgenbank('NC_016966','SequenceOnly',true);
plb = plasmid(185489:215165 );
pla = seqrcomplement(seq{29});

ind = round(findstr(seqCTX1,plb)/(settings.bpPerNm*settings.camRes));
inda = round(findstr(seqCTX1,pla)/(settings.bpPerNm*settings.camRes));

plbB = ZEROMODEL.generate_barcodes_for_contigs(plb,settings);
curB = ZEROMODEL.generate_barcodes_for_contigs(seqrcomplement(seq{29}),settings);

figure, plot(plbB(1:end))
hold on
% 
% mm = mean(plbB(5:size(curB,2)-5));
% stdd = std(plbB(5:size(curB,2)-5));
% gg = curB(5:end-5);
% gg = (gg-mean(gg))./std(gg);
% gg = gg*stdd+mm;
plot(5:size(curB,2)-5,curB(5:end-5))
plot(ind, plbB(ind),'s','markersize', 10);
plot(inda, curB(inda),'s','markersize', 10);
legend({'theoretical sequence barcode','contig barcode','CTX-M-15 on theor.s.','CTX-M-15 on contig' } );
ax = gca;
ticks = 1:10:373;
ticksx = round(ticks*592/1000);
ax.XTick = [ticks];
ax.XTickLabel = [ticksx];
xlabel('position (kbp)')

%[cc1,cc2,flip] = CMN_HelperFunctions.ccorr_all(contigBarcodes{i},consensusBarcode,true,true);

% pValMat = CAP.generate_bit_matrix(theoreticalBarcodes, refCurve, evdPar, settings,'tan');

%
%isequal(S(202858:203571+3423-1),seqrcomplement(contigD{29}(1:4136)))
%[Score, Alignment] = nwalign(S(199708:202858),seqrcomplement(contigD{29}(4137:7287)))
% so 1:4136 matches exactly, but then?