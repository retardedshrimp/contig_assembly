%30/09/16

load markov_prob.mat;
settings = SETTINGS.settings(); % 


sizeR = size(seq{29},2);
seqR = seqrcomplement(contigD{29});
% 
% barProb = CBT2.cb_two_ligand(seqR,settings);
% if size(barProb,2) > 8995
%     barcodeSeqPSF = CBT.apply_point_spread_function(barProb, settings.psfWidth*settings.bpPerNm, true); %false if circular
%     barCur = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);
% end
barCur = barC{29};
uncertainLength = 5;
barCur = barCur(uncertainLength+1:end-uncertainLength);
barCur = (barCur - mean(barCur))/std(barCur);

markovb = ZEROMODEL.generate_markov_barcodes(markovProbab, 1000,sizeR );


barLL = cell(1,size(markovb,2));
barProbL = cell(1,size(markovb,2));
parfor i=1:size(markovb,2)
    if ~isempty(markovb{i})
        barProbL{i} = CBT2.cb_two_ligand(markovb{i},settings);
        if size(barProbL{i},2) > 8995
            barcodeSeqPSF = CBT.apply_point_spread_function(barProbL{i}, settings.psfWidth*settings.bpPerNm, true); %false if circular
            barLL{i} = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);
        end
    end
end


uncertainLength = 5;
barLength = size(barLL{1},2)-2*uncertainLength;
randBarcodes = zeros(settings.nRandom,barLength);   
parfor i=1:size(markovb,2)
    randBarcode = barLL{i}(uncertainLength+1:end-uncertainLength)
    randBarcodes(i,:) =(randBarcode-mean(randBarcode))/std(randBarcode);
end


corCoef = zeros(settings.nRandom,1);
corCoefRev = zeros(settings.nRandom,1);
corCoefVec = zeros(settings.nRandom,2*size(refCurve,2));
for iNew = 1:settings.nRandom
    [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(randBarcodes(iNew,:),refCurve);
    corCoef(iNew) = max(cc1);
    corCoefRev(iNew) = max(cc2);
    corCoefVec(iNew,:) = [cc1 cc2];
end
allCof = [corCoef corCoefRev];

newCorCoef = tan(3.14159/2*corCoefVec(:));
newCof =tan(3.14159/2*allCof(:));

gevFit = fitdist(newCof,'GeneralizedExtremeValue'); % since this includes gumbel, results should be similar
gevFit.mu-gevFit.sigma/gevFit.k
gumbelFit = evfit(-newCof); % originally evfits fits for minima, so we have to be a bit careful

[cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barCur,refCurve);

kk = CMN_HelperFunctions.p_val_gumbel(tan(3.14159/2*max(max(cc1,cc2))),gumbelFit)
kk = CMN_HelperFunctions.p_val_gumbel(tan(3.14159/2*max(cc2)),gumbelFit)
kk = CMN_HelperFunctions.p_val_gumbel(tan(3.14159/2*max(cc1)),gumbelFit)

pCof = [];
for i=1:size(cc1,2)
    pCof = [ pCof CMN_HelperFunctions.p_val_gumbel(tan(3.14159/2*cc1(i)),gumbelFit)];
end

pCofrev = [];
for i=1:size(cc1,2)
    pCofrev = [ pCofrev CMN_HelperFunctions.p_val_gumbel(tan(3.14159/2*cc2(i)),gumbelFit)];
end

% 
% figure, plot(barCur)
% ylabel('intensity')
% ax = gca;
% ticks = 1:5:size(barCur,2);
% ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
% ax.XTick = [ticks];
% ax.XTickLabel = [floor(uncertainLength*settings.bpPerNm*settings.camRes/1000)+ticksx];
% xlabel('Position (kbp)')
% hold on
% plot(2, -1.5065, 's','MarkerSize',10, 'MarkerEdgeColor','b','MarkerFaceColor',[0.5,0.5,0.5])
% legend({'contig nr.28','CTX-M-15' } )
% 
% figure, plot(refCurve)
% ylabel('intensity')
% ax = gca;
% ticks = 1:50:size(refCurve,2);
% ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
% ax.XTick = [ticks];
% ax.XTickLabel = [ticksx];
% xlabel('Position (kbp)')
% 
% 
% barCur2 = barL{13};
% uncertainLength = 5;
% barCur2 = barCur2(uncertainLength+1:end-uncertainLength);
% barCur2 = (barCur2 - mean(barCur2))/std(barCur2);
% [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barCur2,refCurve);
% max(cc1)
% max(cc2)
% figure, plot(barCur2)
% ylabel('intensity')
% ax = gca;
% ticks = 1:5:size(barCur2,2);
% ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
% ax.XTick = [ticks];
% ax.XTickLabel = [floor(uncertainLength*settings.bpPerNm*settings.camRes/1000)+ticksx];
% xlabel('Position (kbp)')
% hold on
% plot(5,  -1.5263, 's','MarkerSize',10, 'MarkerEdgeColor','b','MarkerFaceColor',[0.5,0.5,0.5])
% legend({'contig nr.13a',' RepA2 ' } )
% 
% x = [1:373];
% figure,plot(x,refCurve)
% hold on;
% plot([2:28],barCur,'linewidth',2)
% legend({'consensus barcode',' contig nr.28 ' } )
% ylabel('intensity')
% ax = gca;
% ticks = 1:50:size(refCurve,2);
% ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
% ax.XTick = [ticks];
% ax.XTickLabel = [ticksx];
% xlabel('Position (kbp)')
% 
% 
% 
% x = [1:373];
% figure,plot(x,refCurve)
% hold on;
% plot([183:183+size(barCur2,2)-1],barCur2,'linewidth',2)
% legend({'consensus barcode',' contig nr.13a ' } )
% ylabel('intensity')
% ax = gca;
% ticks = 1:50:size(refCurve,2);
% ticksx = floor(ticks*settings.bpPerNm*settings.camRes/1000);
% ax.XTick = [ticks];
% ax.XTickLabel = [ticksx];
% xlabel('Position (kbp)')