% dnaSeqInv = IMPORT.read_fasta('Original.fa');
% dnaSeq = IMPORT.read_fasta('puuh_inv.fasta');
S = getgenbank('NC_016966','SequenceOnly',true);
load plasmid_puuh.mat;

seqdotplot(dnaSeq{1}(1:10000), dnaSeqInv{1}(1:10000),11,7)

k = 0;
for i=202036:220824
    if ~isequal(S(i),dnaSeq{1}(i))
        k=i;
        break;
    end
end

% here they are the same: 1:184608  184608-202035

ab = seqrcomplement( S(184609:202035));
ab

newContig = [seqrcomplement(contigD{29}(1:4136))  S(202032:202857) seqrcomplement(contigD{29}(4137:end))];


%findstr(seqrcomplement(S(202033:202853)),plasmid)

%newContig = [seqrcomplement(seq{29}(1:4136))  S(202032:202857) seqrcomplement(seq{29}(4137:end))];