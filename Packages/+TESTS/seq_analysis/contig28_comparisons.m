

load plasmid_puuh.mat;
settings = SETTINGS.settings(); % 

cutPieces = 20;

contigData = cell(1,cutPieces);

% contigLength = size(plasmid,2);

seqCTX1 = 'TGCCGAATTAGAGCGGCAGT';
seqCTX2 = 'ACTGCCGCTCTAATTCGGCA';

seqRepA1 = 'GATTACACTGAGTTTAAACG';
seqRepA2 = 'CGTTTAAACTCAGTGTAATC';

contigD = IMPORT.read_fasta('DA1500 all both 13.fasta');

% find contigs with Cas9
contigN = [];
posN = [];
flipN = [];
for i=1:size(contigD,2)
    posCas9 = findstr(seqCTX1,contigD{i});
    if ~isempty(posCas9)
        contigN =[contigN i];
        posN = [posN posCas9];
        flipN = [flipN 0];
    end
    posCas9 = findstr(seqCTX2,contigD{i});
    if ~isempty(posCas9)
        contigN =[contigN i];
        posN = [posN posCas9];
        flipN = [flipN 1];
    end
end

%isequal(S(202858:203571+3423-1),seqrcomplement(contigD{29}(1:4136)))


nextSeq1 = contigD{29}(4135:4155);
nextSeq = seqrcomplement(contigD{29}(4135:4155));


kk = 1;
for i=1:17000
    if isequal(S(202011-i:202011+20),seqrcomplement(contigD{29}(4135:4155+i)))
       kk = kk+1; 
    end
end

 isequal(S(202011-i:202011+20),seqrcomplement(contigD{29}(4135:4155+i))

 Sseq = [S(202031-17443+1:202029) S(202858:203571+3423-1) ];
  Sseq2 = [S(202031-17443-4000+1:202029) S(202858:203571+3423-1+4000) ];

  tSeq =[ S(202032:202857)];
 contNew = [seqrcomplement(contigD{29}(1:4136)) tSeq seqrcomplement(contigD{29}(4137:end))];
 contB = ZEROMODEL.generate_barcodes_for_contigs(contNew,settings);

 plbB = ZEROMODEL.generate_barcodes_for_contigs(Sseq,settings);
 
 plbB = ZEROMODEL.generate_barcodes_for_contigs(Sseq2,settings);
 figure, plot(plbB)
