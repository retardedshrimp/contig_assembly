
figure, hist(corCoefVec(:),50);
xlabel('Correlation coefficient')
ylabel('PDF')

newCorCoef = tan(3.14159/2*corCoefVec(:));

figure, hist(newCorCoef,50)
newCof =tan(3.14159/2*allCof(:));

figure, hist(tan(3.14159/2*allCof(:)),100)
gevFit = fitdist(newCof,'GeneralizedExtremeValue'); % since this includes gumbel, results should be similar
gevFit.mu-gevFit.sigma/gevFit.k
gumbelFit = evfit(-newCof); % originally evfits fits for minima, so we have to be a bit careful
