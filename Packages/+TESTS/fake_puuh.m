load plasmid_puuh.mat;

cutPieces = 20;

contigData = cell(1,cutPieces);

contigLength = size(plasmid,2);

seqCTX1 = 'TGCCGAATTAGAGCGGCAGT';
seqCTX2 = 'ACTGCCGCTCTAATTCGGCA';

seqRepA1 = 'GATTACACTGAGTTTAAACG';
seqRepA2 = 'CGTTTAAACTCAGTGTAATC';

contigD = IMPORT.read_fasta('DA1500 all both 13.fasta');