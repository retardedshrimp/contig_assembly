
%contig = barC{1};

load ex.mat;

% Comparing corr_all with our function
tic
%[cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(zscore(contig),zscore(barcode));
[cc1,cc2] = COMPARISON.cc_fft(zscore(contig),zscore(barcode));
toc

tic
[betterDirCCs, worseDirCCs, flip, shiftOfARelToB] = CBT.ExpComparison.Core.GrossCcorr.ccorr_all(zscore(barcode), zscore(contig), true, true);
toc

figure, plot(cc2)
hold on
plot(worseDirCCs)
legend({'my method','package'})



figure, plot(cc2)
hold on
plot(worseDirCCs)
legend({'my method','package'})

longVec = barcode;
longLength = 373;
shortLength = 124;

movMean = conv([longVec,longVec],ones(1,shortLength))./shortLength;
movMean = movMean(shortLength:longLength+shortLength-1);

movStd = conv([longVec.^2,longVec.^2],ones(1,shortLength));
movStd = movStd(shortLength:longLength+shortLength-1);

mmd = (movStd-movMean.^2)./(shortLength-1);
sqrt(mmd(1:5))

std(longVec(1:124))
%%%
sqrt((sum((longVec(1:124)-mean(longVec(1:124))).^2))/(shortLength-1 ))

sqrt((sum((longVec(1:124)).^2)-shortLength*mean(longVec(1:124)).^2)/(shortLength-1 ))

sqrt((movStd(1)-shortLength*movMean(1)^2)/(shortLength-1 ))
