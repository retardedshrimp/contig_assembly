load plasmid_puuh.mat;
settings = SETTINGS.settings(); % 

cutPieces = 20;

contigData = cell(1,cutPieces);

% contigLength = size(plasmid,2);

seqCTX1 = 'TGCCGAATTAGAGCGGCAGT';
seqCTX2 = 'ACTGCCGCTCTAATTCGGCA';

seqRepA1 = 'GATTACACTGAGTTTAAACG';
seqRepA2 = 'CGTTTAAACTCAGTGTAATC';

contigD = IMPORT.read_fasta('DA1500 all both 13.fasta');

barProb = CBT2.cb_two_ligand(plasmid,settings);
barcodeSeqPSF = CBT.apply_point_spread_function(barProb, settings.psfWidth*settings.bpPerNm, false); %false if circular
barC = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);

posCAS9 = findstr(seqCTX1,plasmid)/(settings.bpPerNm*settings.camRes);
posRepA2= findstr(seqRepA2,plasmid)/(settings.bpPerNm*settings.camRes);

% find contigs with Cas9
contigN = [];
posN = [];
flipN = [];
for i=1:size(contigD,2)
    posCas9 = findstr(seqCTX1,contigD{i});
    if ~isempty(posCas9)
        contigN =[contigN i];
        posN = [posN posCas9];
        flipN = [flipN 0];
    end
    posCas9 = findstr(seqCTX2,contigD{i});
    if ~isempty(posCas9)
        contigN =[contigN i];
        posN = [posN posCas9];
        flipN = [flipN 1];
    end
end


% find contigs with Cas9
contigN = [];
posN = [];
flipN = [];
for i=1:size(contigD,2)
    posCas9 = findstr(seqRepA1,contigD{i});
    if ~isempty(posCas9)
        contigN =[contigN i];
        posN = [posN posCas9];
        flipN = [flipN 0];
    end
    posCas9 = findstr(seqRepA2,contigD{i});
    if ~isempty(posCas9)
        contigN =[contigN i];
        posN = [posN posCas9];
        flipN = [flipN 1];
    end
end
% Find contig 29 reversed includes 

cont = contigD{29};

barProb = CBT2.cb_two_ligand(cont,settings);
barcodeSeqPSF = CBT.apply_point_spread_function(barProb, settings.psfWidth*settings.bpPerNm, true); %false if circular
barCas9 = NUMERICAL.rescale_barc(transpose(barcodeSeqPSF), settings);


uncertainLength = 4; % adjust based on PSF width, should be equal to PSF in pixels
barCur = barCas9(uncertainLength+1:end-uncertainLength);
barCur = (barCur - mean(barCur))/std(barCur);

load ZERO_markov.mat; % load zero model
settings.zeroModel = zeroModel;
intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

sizeRand = size(barcode,2);


barLength = sizeRand;

randBarcodes = zeros(settings.nRandom,barLength);   

 % just to check how fast it is, can be commented out
for iNew = 1:settings.nRandom
    barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
    randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
    randBarcodes(iNew,:) = (randBarcode-mean(randBarcode))/std(randBarcode);
end



corCoef = zeros(settings.nRandom,1);
corCoefRev = zeros(settings.nRandom,1);
corCoefVec = zeros(settings.nRandom,2*size(barcode,2));

for iNew = 1:1000
        [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barCur,randBarcodes(iNew,:));

        corCoef(iNew) = max(cc1);
        corCoefRev(iNew) = max(cc2);
        corCoefVec(iNew,:) = [cc1 cc2];

  end
allCof = [corCoef corCoefRev];
gumbelFit = evfit(-allCof(:)); % originally evfits fits for minima, so we have to be a bit careful
gFitTrunc = COMPARISON.evfit_max(allCof(:) ) ;
gevFit = fitdist(allCof(:),'GeneralizedExtremeValue'); % since this includes gumbel, results should be similar
gevFit22 = fitdist(allCof(:),'ev'); % since this includes gumbel, results should be similar

gevParams = [gevFit.k, gevFit.sigma, gevFit.mu];
    %gevPar{i} = gevParams;

[f,x]=  hist([corCoef, corCoefRev],100);

cc = linspace(-0.1,1,1001);

gev = evpdf(-cc, gumbelFit(1), gumbelFit(2));
gev2 = gevpdf(cc, gevParams(1), gevParams(2),gevParams(3));
gfit = COMPARISON.evpdf2(cc, gFitTrunc(1), gFitTrunc(2));
gfit3 = COMPARISON.evpdf2(cc, gevFit22.mu, gevFit22.sigma);

%     figure
%     bar(x,f/trapz(x,f));hold on
%     plot(cc, gev, 'r', 'linewidth',1)     ;
%     plot(cc, gev2, 'black', 'linewidth',2) ;    
%     plot(cc, gfit, 'green', 'linewidth',1) ; 
%     plot(cc, gfit3, 'blue', 'linewidth',1) ; 
% 
%     xlabel('Maximal correlation coefficient')
%     ylabel('PDF')
%     legend({'Histogram', 'Gumbel', 'GEV','Truncated Gumbel'},'Position',[0.3 0.7 0.1 0.1])

gev = evpdf(-x, gumbelFit(1), gumbelFit(2));
gev2 = gevpdf(x, gevParams(1), gevParams(2),gevParams(3));
gfit = COMPARISON.evpdf2(x, gFitTrunc(1), gFitTrunc(2));
gfit3 = evpdf(x, gevFit22.mu, gevFit22.sigma);

SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
SSres = sum((f/trapz(x,f)-gev).^2);
SSres2 = sum((f/trapz(x,f)-gev2).^2);
SSres3 = sum((f/trapz(x,f)-gfit).^2);
SSres4 = sum((f/trapz(x,f)-gfit3).^2);

RsqrdGumbel= 1-SSres/SSTot;

RsqrdGEV = 1-SSres2/SSTot;

RsqrdTrunc = 1-SSres3/SSTot;
RsqrdTrunc2 = 1-SSres4/SSTot;

[cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barCur,barcode);

CMN_HelperFunctions.p_calc_GEV(0.8,gev2)

[f,x]=  hist(corCoefVec(:),100);
%     figure
%     bar(x,f/trapz(x,f))

%[cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(barcode,barC);
