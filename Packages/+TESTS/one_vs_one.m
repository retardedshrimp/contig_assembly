%https://se.mathworks.com/matlabcentral/answers/35894-return-value-calculation-from-gumbel-cdf-for-maxima-not-minima
% computes truncated Gumbel

addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));
settings = SETTINGS.settings(); % 
settings.nRandom = 1000;
load ZERO.mat; % load zero model
%load ZERO_markov.mat; % load zero model
settings.zeroModel = zeroModel;

distToPlot = 'Gumbel'; 


    binNum = 30;

sizeRand = 373;
sizeRand2 = 373;

intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

barLength = ceil(size(intZeroModel,2)/(settings.bpPerNm*settings.camRes));

randBarcode = zeros(1,barLength);   

for iNew = 1:1
    barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
    barc = barcodePSF;
    randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
    randBarcode = (randBarcode-mean(randBarcode))/std(randBarcode);
end
%randBarcode = randBarcode(1:sizeRand2);

[gumbelFit, gevParams, corCoef, corCoefRev, corCoefVec] = COMPARISON.evd_with_shorter_barcode_randomised( randBarcode, sizeRand2, settings );

x0 = [corCoef, corCoefRev];
gumbelFit2 = evfit2(-x0(:));


[f,x]=  hist([corCoef, corCoefRev],binNum);

gev = evpdf(-x, gumbelFit(1), gumbelFit(2));
gfit = evpdf(-x, gumbelFit2(1), gumbelFit2(2));

gev2 = gevpdf(x, gevParams(1), gevParams(2),gevParams(3));


SSTot = sum((f/trapz(x,f)-mean(f/trapz(x,f))).^2);
SSres = sum((f/trapz(x,f)-gev).^2);
SSres2 = sum((f/trapz(x,f)-gfit).^2);
SSres3 = sum((f/trapz(x,f)-gev2).^2);

RsqrdGumbel1= 1-SSres/SSTot;
RsqrdGumbel2= 1-SSres2/SSTot;
RsqrdGEV= 1-SSres3/SSTot;


cc = linspace(-0.1,1,101);

gev = evpdf(-cc, gumbelFit(1), gumbelFit(2));
gev2 = gevpdf(cc, gevParams(1), gevParams(2),gevParams(3));
 gfit = evpdf(-cc, gumbelFit2(1), gumbelFit2(2));

figure
bar(x,f/trapz(x,f));hold on
plot(cc, gev, 'r', 'linewidth',2)     ;
plot(cc, gev2, 'black', 'linewidth',2) ;    
plot(cc, gfit, 'green', 'linewidth',2) ;    

legend({'Histogram', 'Gumbel', 'GEV','Truncated Gumbel'})



