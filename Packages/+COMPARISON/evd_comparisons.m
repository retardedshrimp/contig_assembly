function [evdPar] = evd_comparisons( coef)
    % 01/12/16
    % computes evd par.

    evdParGEV =  COMPARISON.compute_distribution_parameters(coef,'gev');
    COMPARISON.compare_distribution_to_data( coef, evdParGEV, 'gev' )
    rSquaredGEV = COMPARISON.compute_r_squared(coef, evdParGEV, 'gev' );

    
    evdParExact =  COMPARISON.compute_distribution_parameters(coef,'exact');
    COMPARISON.compare_distribution_to_data( coef, evdParExact, 'exactfull' )
    rSquaredExact = COMPARISON.compute_r_squared(coef, evdParExact, 'exactfull' );

    
	evdParGumbel =  COMPARISON.compute_distribution_parameters(coef,'gumbel');
	rSquaredGumbel = COMPARISON.compute_r_squared(coef, evdParGumbel, 'gumbel' );
   % [ rSquaredGEV rSquaredExact rSquaredGumbel]
    
    evdPar.evdParGEV = evdParGEV;
    evdPar.evdParExact = evdParExact;
    evdPar.evdParGumbel = evdParGumbel;
end

