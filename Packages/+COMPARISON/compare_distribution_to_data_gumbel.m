function [] = compare_distribution_to_data_gumbel(corcoefEx,phat,~)
%16/09/16
% compare normal distribution to data 
    if nargin<3
        return;
    end
            
	cc = linspace(-1,1,101);
    
    
    gev = evpdf(-cc, phat(1), phat(2));
    %gev = CMN_HelperFunctions.prob_gumbel(cc,phat);
    

    [f,x]=  hist(corcoefEx,20);
        
    figure
    bar(x,f/trapz(x,f));hold on
    plot(cc, gev, 'r', 'linewidth',3)
    ylabel('PDF')
    xlabel('CC')
    legend({'Correlation coefficient hist', 'Extreme value distribution'})

   % figure, plot(cc, 1-evcdf(-cc, phat(1), phat(2)));

end

