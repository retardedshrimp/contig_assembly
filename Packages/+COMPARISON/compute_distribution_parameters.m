function [ parameters ] = compute_distribution_parameters(data, method )
    % 04/10/16
    % This function computes distribution parameters for specified
    % distribution. Somewhat overlaps with inbuilt functions from matlab

    if nargin < 2
        method = 'Gumbel';
    end
    
    switch method
        case 'normal' 
            parameters = fitdist(data,'Normal'); 
        case 'gumbel'
            parameters = evfit(-data);
        case 'gev'   
            parameters = fitdist(data,'GeneralizedExtremeValue'); 
        case 'exact'

            f = @(y) FUNCTIONS.n_fun_test(y,data);
            x0 = [30];
            x2 = fsolve(f,x0,optimoptions('fsolve','Display','off'));

            N2 = FUNCTIONS.rootN_numeric(x2,data);
            
            parameters = [x2 N2];
        otherwise
            parameters = [];
            warning('Unexpected choice of distribution. No parameters computed')
    end
    
end

