function [ corCoef, corCoefAll ] = compute_correlation(randBarcodes, refCurve,interpData, numRand )
    % 01/12/16 
    % computes correlation coefficients for a given set of random vectors
    % vs. a reference barcode.
    corCoefAll = [];
    if nargin < 5
        secondBar = zscore(refCurve);
        secondBarRev = fliplr(refCurve);
    end
     
    % create vectors to save output data
    
    corCoef = zeros(numRand,1);
   % corCoef = zeros(length(randBarcodes),1);
    %corCoefAll = zeros(numRand,2*size(refCurve,2));

    seqLen = length(refCurve);
    for iNew = 1:numRand
        
          
        halfL = floor(seqLen/2);
        PR1 = exp(2i*pi*rand(1,halfL));
        PR2 = fliplr(conj(PR1));

        if mod(seqLen,2)==0
            PR = [1 PR1(1:end-1) 1 PR2(2:end)];
        else
             PR = [1 PR1 PR2];
        end
        randomBarcode = ifft(interpData.*PR);
        
        firstBar = transpose(zscore(randomBarcode));
        %curRand = randBarcodes{iNew}(1+settings.uncReg:settings.contigSize-settings.uncReg);

        ccForward = ifft(fft(firstBar,length(secondBar) ).*conj(transpose(fft(secondBar))))/(length(firstBar)-1);
        ccBackward = ifft(fft(length(length(secondBar))).*conj(transpose(fft(secondBarRev))))/(length(firstBar)-1);
        
        % store output coefficients
        corCoef(iNew) = max(max(ccForward,ccBackward));
       %corCoefAll(iNew,:) = [ccForward;ccBackward];
    end
end

