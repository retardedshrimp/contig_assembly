function [ evdFit ] = evd_parameters(corCoef, method )
    % evd_parameters
    % 01/10/16 
    
    % input: corCoef, vector of all correlation coefficients to 
    % compute the evd for, and optional parameter method,
    % which chooses which method to transform data to use
    % this is needed since original correlation coefficients are restricted
    % between -1 and 1, and the extreme value distribution is supposed to 
    % be unbounded
    
    % output evdFit
    
    if nargin < 2
        method = 'tan';
    end
    
    if isequal(method, 'tan')
        transformedCorCoef = tan((pi/2)*corCoef);
    else if isequal(method, 'fisher')
            transformedCorCoef = (1/2)*log((1+corCoef)./(1-corCoef));
        end
    end
        
    evdFit = evfit(-transformedCorCoef); % originally evfits fits for minima, so we have to be a bit careful


end

