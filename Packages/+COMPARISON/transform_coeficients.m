function [ transformedCorCoef ] = transform_coeficients(corCoef, method )
    % 04/10/16 
    % tranforms correlation coefficients based on the method chosen
    if nargin < 2
        method = 'fisher';
    end
    
    switch method
    case 'tan' 
        transformedCorCoef = tan((pi/2)*corCoef);
    case 'fisher'
        transformedCorCoef = (1/2)*log((1+corCoef)./(1-corCoef));
    otherwise
        transformedCorCoef = corCoef;
        warning('Unexpected transformation method. No transformation performed')
    end


end

