function [rSq,xN,nN] = autocorr_comparison(alphaLim, tries)

	% 24/10/16 in order to make better comparisons related to autocorr function


	%settings = SETTINGS.settings(); % 


	%saveFolder ='Results/Autocor';


	% defaults settings for the test

	
	alphaSet = linspace(alphaLim(1), alphaLim(2),tries);
	
	rSq1 = cell(1,tries);
	rSq2 = cell(1,tries);
	rSq3 = cell(1,tries);
	nN = cell(1,tries);
	xN = cell(1,tries);

	
	parfor parChange=1:tries
		tempStruct = struct();
		tempStruct.nRandom = 1000;
		tempStruct.contigSize = 70;
		tempStruct.uncReg = 0;
		m = 100;

		alpha = alphaSet(parChange);

		bar = ZEROMODEL.generate_autocorr_rand_seq(m,1 ,'gaussian',1,alpha);
		
		fBarcode = bar{1};

		%tt = randi(1000,1);

		%saveFile = sprintf('refBar%d.mat',tt);

		%saveF = strcat(saveFolder, saveFile);
		%save(saveF, '-v7.3', 'bar');

		randBar = ZEROMODEL.generate_autocorr_rand_seq(tempStruct.contigSize, tempStruct.nRandom ,'gaussian',1,alpha );

		%save(saveF, '-append','randBar');

		%size(randBar)

		[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, tempStruct );

		%save(saveF, '-append', 'corCoef', 'corCoefAll');


		f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
		x0 = [30];
		x = fsolve(f,x0,optimoptions('fsolve','Display','off'));
		xN{parChange} = x;

		%m =size(corCoefAll(:),1);

		%x = 20;
		N = FUNCTIONS.rootN_numeric(x,corCoef(:));
		nN{parChange} = N;

		%save(saveF, '-append', 'x', 'N');
		%N2 = FUNCTIONS.rootN(x,corCoef(:));

		rSquared = COMPARISON.compute_r_squared( corCoef(:), [x,N], 'exactfull' );

		evdPar =  COMPARISON.compute_distribution_parameters(corCoef(:),'gumbel');
		rSquared2 = COMPARISON.compute_r_squared(corCoef(:), evdPar, 'gumbel' );

		evdParGEV =  COMPARISON.compute_distribution_parameters(corCoef(:),'gev');
		%COMPARISON.compare_distribution_to_data( corCoef(:), evdPar, 'gev' )

		rSquared3 = COMPARISON.compute_r_squared(corCoef(:), evdParGEV, 'gev' );

		rSq1{parChange} = rSquared;
		rSq2{parChange} = rSquared2;
		rSq3{parChange} = rSquared3;
		%save(saveF, '-append', 'rSq', 'evdParGEV');
		%save(saveF, '-append', 'settings');% 

	end
	
	rSq =[cell2mat(rSq1); cell2mat(rSq2);cell2mat(rSq3)];
	xN = cell2mat(xN);
	nN = cell2mat(nN);








end 
