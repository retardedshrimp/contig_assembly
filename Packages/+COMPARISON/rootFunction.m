function [ F ] = rootFunction( x, cc )
% function for maximum likelihood stuff
    
    
    m = size(cc,1);
    
    C1 = 1/2.*sum(log(1-cc.^2));
    
    F = m/2.*(psi((x-1)/2)-psi((x-2)/2))+C1; 

end

