function [] = compare_eigenvalues(acor1, acor2 )
 % 22/11/16
 % plot eigenvalue comparison
 
    corMat1 = toeplitz(acor1);
    corMat2 = toeplitz(acor2);
    
    eig1 = eig(corMat1);
    eig2 = eig(corMat2);

    figure, plot(eig1)
    hold on
    plot(eig2)
    legend({'Eigenvalues from Gaussian autocorrelation matrix','Eigenvalues from sample autocorrelation' })

end

