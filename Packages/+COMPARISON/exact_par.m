function [ evdPar ] = exact_par(n0,corCoef,corCoefAll )
% 12/10/16 calculates parameters for exact dist

    f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));

    n = fsolve(f,n0);

    m = size(corCoefAll(:),1);
   
    N = FUNCTIONS.rootN(n,corCoef(:));

    evdPar = [n, N];
end

