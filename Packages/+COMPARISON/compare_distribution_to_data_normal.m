function [] = compare_distribution_to_data_normal(corcoefEx,phat,~)
    %16/09/16
    % compare normal distribution to data 
    if nargin<3
        return;
    end
            
	cc = linspace(-1,1,1001);
    gev = normpdf(cc, phat.mu, phat.sigma);

    [f,x]=  hist(corcoefEx,30);
        
    figure
    bar(x,f/trapz(x,f));hold on
    plot(cc, gev, 'r', 'linewidth',3)
    ylabel('PDF')
    xlabel('CC')
    legend({'Correlation coefficient hist', 'Normal distribution'})


end

