function [] = compare_distribution_to_data_GEV(corcoefEx,phat,~)
% compare distribution to data 
    if nargin<3
        return;
    end
            
	cc = linspace(0.1,1,101);
    gev = gevpdf(cc, phat(1), phat(2), phat(3));

    [f,x]=  hist(corcoefEx,20);
        
    figure
    bar(x,f/trapz(x,f));hold on
    plot(cc, gev, 'r', 'linewidth',3)
    ylabel('PDF')
    xlabel('CC')
    legend({'Correlation coefficient hist', 'Generalised extreme value distribution'})


end

