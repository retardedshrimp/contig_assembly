function [ corCoef, corCoefAll ] = compute_correlation_between_two(randomBarcode, refCurve,interpData, numRand )
    % 01/12/16 
    % computes correlation coefficients for a given set of random vectors
    % vs. a reference barcode.
    corCoefAll = [];
    if nargin < 5
        secondBar = zscore(refCurve);
        secondBarRev = fliplr(zscore(refCurve));
    end
     
    % create vectors to save output data
    
    corCoef = zeros(numRand,1);
   % corCoef = zeros(length(randBarcodes),1);
    %corCoefAll = zeros(settings.nRandom,2*size(refCurve,2));


    firstBar =zscore(randomBarcode);
    %curRand = randBarcodes{iNew}(1+settings.uncReg:settings.contigSize-settings.uncReg);

    ccForward = ifft(fft(firstBar, length(secondBar)).*conj(transpose(fft(secondBar))))/(length(firstBar)-1);
    ccBackward = ifft(fft(firstBar,length(secondBar)).*conj(transpose(fft(secondBarRev))))/(length(firstBar)-1);

    % store output coefficients
    corCoef=[ccForward ccBackward];
        
end

