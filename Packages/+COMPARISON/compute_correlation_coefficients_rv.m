function [ corCoef, corCoefAll ] = compute_correlation_coefficients_rv(randBarcodes, refCurve, settings )
    % 26/10/16 
    % computes correlation coefficients for a given set of random vectors
    % vs. a reference barcode.
    % ref barcode is shorter
    % create vectors to save output data
    
    corCoef = zeros(settings.nRandom,1);
    corCoefAll = zeros(settings.nRandom,2*size(randBarcodes{1},2));
    
    
    for iNew = 1:settings.nRandom
        % make a cut of a random barcode, based on the untrusted zone
        curRand = randBarcodes{iNew}(1+settings.uncReg:size(randBarcodes{1},2)-settings.uncReg);
        
        % reisner rescale
        curRand = (curRand-mean(curRand))./std(curRand);
        
        % compute forward and reverse correlation coefficients
        [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(refCurve,curRand);
        
        % store output coefficients
        corCoef(iNew) = max(max(cc1,cc2));
  
        corCoefAll(iNew,:) = [cc1 cc2];
    end
end

