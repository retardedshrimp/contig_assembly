function [ exactpdf ] = exact_dist_PDF(cc, evdPar)
% 10/10/16 exact PDF for i.i.d. data
   if mod(evdPar(1),2)== 0
                C1 = 1./sqrt(pi) *gamma((evdPar(1)-1)/2)/gamma((evdPar(1)-2)/2);
                I = (evdPar(1)-4)/2;
                sum = 0;
                for k = 0:I
                    sum = sum + (-1)^k*nchoosek(I,k)*(cc.^(2*k+1)/(2*k+1) + 1/(2*k+1));
                end
                sum = sum*C1;
                %figure, plot(xx,sum)
                %exactcdf = sum.^N;
                %figure, plot(xx,exactcdf)
                exactpdf = evdPar(2)*sum.^(evdPar(2)-1).*C1.*(1-cc.^2).^I;
            else
                J = (evdPar(1)-5)/2;
                ss = 0;
                for k=0:J
                    ss = ss + NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1)*(1-cc.^2).^(k+1/2);
                end
                nmS = ss;
                ss = 1./pi*(asin(cc) +ss.*cc-asin(-1));
                %figure, plot(cc,ss)

                %N= 40;

                %exactcdf = ss.^evdPar(2);
                %figure, plot(cc,exactcdf)

                ds = 0;
                for k=0:J
                    ds = ds + (k+1/2).*(NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1).*(1-cc.^2).^(k-1/2));
                end
                exactpdf =  1/(pi)*evdPar(2)*ss.^(evdPar(2)-1).*(1./(sqrt(1-cc.^2))+nmS-2*cc.^2.*ds );

end

