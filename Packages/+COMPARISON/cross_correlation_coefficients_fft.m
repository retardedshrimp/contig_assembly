%=========================================================================%
function [ ccForward, ccBackward] = cross_correlation_coefficients_fft(shortVec, longVec)
% cross_correlation_coefficients_fft
% input shortVec, longVec
% output ccForward, ccBackward
% Uses FFT to calculate cross correlations, and is faster than just
% calculating by definition
% 07/06/16 by Albertas Dvirnas

    shortLength = size(shortVec,2);
    longLength = size(longVec,2);
    
    shortVecFlip = fliplr(shortVec);
     
    
    movMean = conv([longVec,longVec],ones(1,shortLength));
    movMean = movMean(shortLength:longLength+shortLength-1)./shortLength;
    
    movStd = conv([longVec.^2,longVec.^2],ones(1,shortLength));
    movStd = movStd(shortLength:longLength+shortLength-1);

    stdVal = sqrt((movStd-shortLength.*movMean.^2)./(shortLength-1 ));
    stdVal(1:5)

    %     conVecm = conj(fft(movMean));
%     ccForward = (ifft(fft(shortVec,longLength).*conVecm))/(shortLength-1);
%     ccForward = circshift(ccForward,[0,-1]); 
%     ccForward2 = fliplr(ccForward);
%     
    %
    
    
    conVec = conj(fft(longVec));
    % Forward cross correlations
    ccForward = (ifft(fft(shortVec,longLength).*conVec))/(shortLength-1);
    ccForward = circshift(ccForward,[0,-1]); 
    ccForward = fliplr(ccForward);

    % Backward cross correlations
    ccBackward = (ifft(fft(shortVecFlip,longLength).*conVec))/(shortLength-1);
    ccBackward = circshift(ccBackward,[0,-1]); 
    ccBackward = fliplr(ccBackward);
    
    % to get Pearson correlation coefficient, need to divide by std
    
    stdForward = COMPARISON.movingstd([longVec longVec(1:shortLength)], shortLength, 'forward'); % will be an inbuilt function in matlab 2016b
    stdForward = stdForward(1:longLength);
    stdForward(1:5)
%     stdForward = zeros(1,longLength);
%     for i=1:longLength
%         if i+shortLength <= longLength
%             stdForward(i) = std(longVec(i:i+shortLength-1));
%         else
%             stdForward(i) = std([longVec(i:end) longVec(1:i+shortLength-1-end)]);
%         end
%     end

    ccForward = ccForward ./ stdForward; 
    ccBackward = ccBackward ./stdForward; % std is the same for both forward and backward case


end

