%=========================================================================%
function [ ccForward, lag] = autocor_coefficients_fft(shortVec, method)
% cross_correlation_coefficients_fft
% input shortVec, longVec
% output ccForward, ccBackward
% Uses FFT to calculate cross correlations, and is faster than just
% calculating by definition
% 07/06/16 by Albertas Dvirnas

    
    if nargin < 2 % if no parameter selected
        method = 'linear';
    end
    
    
        switch method
            case 'linear2'
                [ccForward,lag] = xcorr(shortVec,'unbiased');
                ccForward = ccForward(floor(size(lag,2)/2)+1:end);
            case 'linear'
                [ccForward,lag] = xcorr(shortVec-mean(shortVec),'coeff');
                ccForward = ccForward(floor(size(lag,2)/2)+1:end);
            case 'circ' % linear but with symmetric
                [ccForward,lag] = xcorr(shortVec-mean(shortVec),'coeff');
                %ccForward = ccForward(floor(size(lag,2)/2)+1:end);
            case 'covar'
                [ccForward,lag] = xcov(shortVec,'coeff');
                ccForward = ccForward(floor(size(lag,2)/2)+1:end);
            case 'covarM'
                [ccForward,lag] = xcov(shortVec,'coeff');
                ccForward = ccForward(:,floor(size(lag,2)/2)+1:end);

            case 'circular'
                shortVec = shortVec - mean(shortVec);
                shortLength = size(shortVec,2);

                conVec = conj(fft(shortVec));
                % Forward cross correlations
                ccForward = (ifft(fft(shortVec,shortLength).*conVec))/(shortLength-1);
                ccForward = circshift(ccForward,[0,-1]); 
                ccForward = fliplr(ccForward);

                % to get Pearson correlation coefficient, need to divide by std

                stdForward = COMPARISON.movingstd([shortVec shortVec(1:shortLength)], shortLength, 'forward'); % will be an inbuilt function in matlab 2016b
                stdForward = stdForward(1:shortLength);

                ccForward = ccForward ./ stdForward.^2; 
                lag = [0:size(ccForward,2)-1];
                %ccBackward = ccBackward ./stdForward; % std is the same for both forward and backward case

            otherwise
                ccForward = [];
                lag = [];
                warning('Unexpected choice of autocorr')
                return;
        end


end

