
settings = SETTINGS.settings(); % 

settings.contigSize = 20;
settings.nRandom = 100;
settings.uncReg = 0;
m = 30;

fBarcode = normrnd(0,1,1,m);


randBar = cell(1,settings.nRandom);
for i=1:settings.nRandom
    randBar{i} = normrnd(0,1,1,settings.contigSize);
end

[ corCoef, corCoefAll ] = COMPARISON.compute_correlation_coefficients(randBar, fBarcode, settings );


f = @(y) COMPARISON.rootFunction(y,corCoefAll(:));
x0 = [20];
x = fsolve(f,x0);

m =size(corCoefAll(:),1);
%x = 20;
N = FUNCTIONS.rootN(x,corCoef(:));
