function [ corCoefCell, corCoefAllCell ] = decorrelated_cor_coef(corCoefAll, corLength )
    % 19/10/16 computes detrended coefficients for all shifts
    
    corCoefCell = cell(1, corLength);
    corCoefAllCell = cell(1, corLength);
    
    cofNumb = size(corCoefAll,2)/2;
    
    for i=1:corLength
        xx=i:corLength:cofNumb;
        
        corCoefAllCell{i} = [corCoefAll(:,xx) corCoefAll(:,xx+cofNumb)];       
        corCoefCell{i} = max(transpose(corCoefAllCell{i}));
    end
    

end

