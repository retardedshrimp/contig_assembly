function [] = compare_distribution_to_data( data, evdPar, dist )
    % 04/10/16 compares data to a selected distribution and draws a plot
    if nargin < 3
        dist = 'Gumbel';
    end
    
    cc = linspace(0,1,1001);
    if isequal(dist,'ccfisher')
    	data = COMPARISON.transform_coeficients(data,'fisher');
    end

    
    [f,x]=  hist(data,floor(sqrt(size(data,1))));

        
    figure
    bar(x,f/trapz(x,f));
    hold on
                
    switch dist
        case 'normal' 
            normDistPDF = normpdf(cc, evdPar.mu, evdPar.sigma);
            plot(cc, normDistPDF, 'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', 'Normal distribution'})
        case 'gumbel'
            gumbelDistPDF = evpdf(-cc, evdPar(1), evdPar(2));
            plot(cc, gumbelDistPDF, 'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', 'gumbel distribution'})
        case 'gev'   
            gumbelDistPDF = gevpdf(cc, evdPar.k, evdPar.sigma, evdPar.mu);
            plot(cc, gumbelDistPDF, 'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', 'gev distribution'})
        case 'cc'   
            gumbelDistPDF = (1/sqrt(pi)) * (gamma((evdPar-1)/2 )/gamma((evdPar-2)/2)) * (1-cc.^2).^((evdPar-4)/2);
            plot(cc, gumbelDistPDF, 'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', 'Theoretical distribution for binormal and idnependent data'})
        case 'ccfisher'   
            cc = (exp(2*cc)-1)./(exp(2*cc)+1);
            gumbelDistPDF = (1/sqrt(pi)) * (gamma((evdPar-1)/2 )/gamma((evdPar-2)/2)) * (1-cc.^2).^((evdPar-4)/2);
            %gumbelDistPDF = COMPARISON.transform_coeficients(gumbelDistPDF,'fisher');
            plot(cc, gumbelDistPDF, 'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', 'Theoretical distribution for binormal and idnependent data'})
        case 'beta'   
            gumbelDistPDF = (1/sqrt(pi))*((n-4)/2) *(1-cc)*(gamma((evdPar-1)/2 )/gamma((evdPar-2)/2)) * (1-cc.^2).^((evdPar-4)/2);;
            %gumbelDistPDF = COMPARISON.transform_coeficients(gumbelDistPDF,'fisher');
            plot(cc, gumbelDistPDF, 'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', ' Generalised beta distribution for binormal and idnependent data'})
        case 'exact'   
            if mod(evdPar(1),2)== 0
                C1 = 1./sqrt(pi) *gamma((evdPar(1)-1)/2)/gamma((evdPar(1)-2)/2);
                I = (evdPar(1)-4)/2;
                sum = 0;
                for k = 0:I
                    sum = sum + (-1)^k*nchoosek(I,k)*(cc.^(2*k+1)/(2*k+1) + 1/(2*k+1));
                end
                sum = sum*C1;
                %figure, plot(xx,sum)
                %exactcdf = sum.^N;
                %figure, plot(xx,exactcdf)
                exactpdf = evdPar(2)*sum.^(evdPar(2)-1).*C1.*(1-cc.^2).^I;
            else
                J = (evdPar(1)-5)/2;
                ss = 0;
                for k=0:J
                    ss = ss + NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1)*(1-cc.^2).^(k+1/2);
                end
                nmS = ss;
                ss = 1./pi*(asin(cc) +ss.*cc-asin(-1));
                %figure, plot(cc,ss)

                %N= 40;

                %exactcdf = ss.^evdPar(2);
                %figure, plot(cc,exactcdf)

                ds = 0;
                for k=0:J
                    ds = ds + (k+1/2).*(NUMERICAL.factd(2*k)/NUMERICAL.factd(2*k+1).*(1-cc.^2).^(k-1/2));
                end
                exactpdf =  1/(pi)*evdPar(2)*ss.^(evdPar(2)-1).*(1./(sqrt(1-cc.^2))+nmS-2*cc.^2.*ds );

            end
                plot(cc,exactpdf,'r', 'linewidth',3)
                ylabel('PDF')
                xlabel('CC')
                legend({'Correlation coefficient hist', 'Exact PDF'})
        case 'exactfull'   
            exactpdf = COMPARISON.exact_full_PDF(cc,evdPar);

%         	C1 = 1./sqrt(pi) *gamma((evdPar(1)-1)/2)/gamma((evdPar(1)-2)/2);
%             sum = C1*(cc.*NUMERICAL.hypergeometric2f1(1/2,-(evdPar(1)-4)/2 ,3/2,cc.^2,15))+1/2;
%             %figure, plot(sum)
%             exactpdf = evdPar(2)*sum.^(evdPar(2)-1).*C1.* ((1-cc.^2).^((evdPar(1)-4)/2));
            plot(cc,exactpdf,'r', 'linewidth',3)
            ylabel('PDF')
            xlabel('CC')
            legend({'Correlation coefficient hist', 'Exact PDF'})
        otherwise
            parameters = [];
            warning('Unexpected choice of distribution. No parameters computed')
    end
    

end

