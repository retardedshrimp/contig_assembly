function [ F] = ev_find_n(x, cc)
    % 14/10/16 find n using maximum likelihood method
    
    % function for maximum likelihood stuff
    
    % size of the sample set
    m = size(cc,1);
    
    denom = (-1/m*sum(log(1+betainc(cc.^2,1/2,x/2-1)))+log(2));
    N = 1./denom;
    
    
    nVal = 2*(x+1);
    
    derivVals = zeros(m,1);
    
    nn = 0:0.01:2*nVal;
    h=nn(2)-nn(1);

    for i=1:m
        uu = betainc(cc(i).^2,1/2,nn);
        dFCenteral=(uu(3:end)-uu(1:end-2))/(2*h);
        derivVals(i) = dFCenteral(round(nVal/h));
    end
    
    term1 = (N-1)*sum(derivVals./(1+betainc(cc.^2,1/2,x/2-1)));
    
    % now first comput the N value
       

    % psi function difference
    psiDif = 1/2*( psi((x-1)/2)-psi((x-2)/2) );
    

    % now compute the second term
    
    term2 = m*psiDif;
    
    % finally the last term
    
    term3 =1/2*sum(log(1-cc.^2));
    
    F = term1+term2+term3; 
    x
    F
    
    %x
    %F


end

