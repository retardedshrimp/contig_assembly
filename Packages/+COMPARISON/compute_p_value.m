function [ pval ] = compute_p_value( cc, evdPar, method )
% 18/10/16 computes p-value

    if nargin<3
        method = 'exact';
    end
          
    switch method
        case 'exact' 
            pval = 1 - (0.5+0.5*betainc(cc.^2,0.5,evdPar(1)/2-1)).^evdPar(2);
        case 'gev'
            % Using Gumbeldistribution as a null model in order to calculate
            % p-values
            k = evdPar(1);
            sigma = evdPar(2);
            mu = evdPar(3);
        
            pd = makedist('Generalized Extreme Value',k,sigma,mu);
            pValue = 1-cdf(pd,cc);
        case 'gumbel'
            % Using Gumbeldistribution as a null model in order to calculate
            % p-values
            kappa = evdPar(1);
            beta = evdPar(2);
            pValue = evcdf(-cc, kappa, beta);
        otherwise
        pval = 1;
    end
end

