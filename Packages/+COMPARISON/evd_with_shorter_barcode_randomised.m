function [ gumbelFit, gevParams, corCoef, corCoefRev, corCoefVec] = evd_with_shorter_barcode_randomised( barcode2, sizeRand, settings )
% 21/09/16 by Albertas Dvirnas
% randomises the shorter barcode1 and slides the randomised barcodes along
% barcode2

    corCoef = zeros(settings.nRandom,1);
    corCoefRev = zeros(settings.nRandom,1);
    corCoefVec = zeros(settings.nRandom,2*size(barcode2,2));
    
    intZeroModel = NUMERICAL.interpolate_fourier_data(settings.zeroModel, sizeRand*(settings.bpPerNm*settings.camRes)); 

    %whether first interpolate and then randomise or randomise& then
    %interpolate?
    barLength = ceil(size(intZeroModel,2)/(settings.bpPerNm*settings.camRes));
    
    randBarcodes = zeros(settings.nRandom,barLength);   
    
    tic % just to check how fast it is, can be commented out
    for iNew = 1:settings.nRandom
        barc = ZEROMODEL.FourierSymmPhase(intZeroModel);
        randBarcode = interp1(barc,linspace(1,size(barc,2),barLength));
        randBarcodes(iNew,:) = (randBarcode-mean(randBarcode))/std(randBarcode);
    end
    %figure, plot(randBarcodes(1,:))
    toc
    %figure, plot(randBarcodes(1,:))
    for iNew = 1:settings.nRandom
        [cc1,cc2] = COMPARISON.cross_correlation_coefficients_fft(randBarcodes(iNew,:),barcode2);
        corCoef(iNew) = max(cc1);
        corCoefRev(iNew) = max(cc2);
        corCoefVec(iNew,:) = [cc1 cc2];
    end
        
    gumbelFit = evfit(-[corCoef;corCoefRev]); % originally evfits fits for minima, so we have to be a bit careful
    gevFit = fitdist([corCoef; corCoefRev],'GeneralizedExtremeValue'); % since this includes gumbel, results should be similar

    gevParams = [gevFit.k, gevFit.sigma, gevFit.mu];

    %compare_distribution_to_data_gumbel(corcoefEx,parmhat,1);
    %compare_distribution_to_data(corcoefEx,phat,1);
    
end

