function [ output_args ] = cb_one_ligand( input_args )
    % 01/11/16 this function is for calculating the binding probabilities
    % for non-competitive binding, i.e. there is only one ligand this time.
    % 
    
    % in an ideal case there will be no competition, for example TCGA from
    % Birmingham's data will not introduce any competitiveness at all, and
    % the probabilities of a ligand being bound at the spots where TCGA is
    % will be equal to 1 all the time theoretically.


end

