function [ barcodeBP,ntNetrospinSeq ] = cb_two_ligand(ntSeq, settings)
% CB_TWO_LIGAND method
% input ntSeq and settings (which must include NETROPSINconc,
% yoyo1BindingConstant, and YOYO1conc fields)

if nargin < 2 % if no method was selected
    settings = struct();
    settings.NETROPSINconc = 6E-6;
    settings.YOYO1conc = 4E-8;
    settings.yoyo1BindingConstant =  1E11;
    settings.netropsinBindingConstant =[ 5E5 1E8 ];
end

% Author: Albertas Dvirnas

% convert leters to digits
ntIntSeq = nt2int(ntSeq);
nn = size(ntIntSeq,2);

% This will be our output vector
barcodeBP = zeros(1,nn);

% First, based on sequence ntIntSeq, we want to calculate which binding are
% possible for each ligand. YOYO1 can bind always, while Netrospin is more
% probable to bind if there are G's or C's

nNum = 1000;
ntIntSeq = [ntIntSeq(end-nNum:end) ntIntSeq ntIntSeq(1:nNum)];

ntNetrospinSeq = zeros(1,size(ntIntSeq,2));

was = 0;
for i=1:size(ntIntSeq,2) %2 = 'C', 3 = 'G'
    if ntIntSeq(i) == 2 || ntIntSeq(i) == 3
        if was == 3
            ntNetrospinSeq(i) = 2;
        else
            was = was + 1;
            ntNetrospinSeq(i) = 1;
        end
    else
        was = 0;
        ntNetrospinSeq(i) = 1;
    end     
end


% transfer matrix
transferMat =  [1 0 0 0 1 0 0 0 1;
                       1 0 0 0 1 0 0 0 1;
                       0 1 0 0 0 0 0 0 0;
                       0 0 1 0 0 0 0 0 0;
                       0 0 0 settings.NETROPSINconc 0 0 0 0 0;
                       1 0 0 0 1 0 0 0 1;
                       0 0 0 0 0 1 0 0 0;
                       0 0 0 0 0 0 1 0 0;
                       0 0 0 0 0 0 0 settings.YOYO1conc*settings.yoyo1BindingConstant 0];
                                  
%full(transferMat)

% two choices for Netropsin, based on if there are G and C letters in a
% given quadromer or not
choice = [transferMat(5,4)*settings.netropsinBindingConstant(1) transferMat(5,4)*settings.netropsinBindingConstant(2)];

leftVec = zeros(nn+1,9);
rightVec = zeros(9,nn+1);
maxEltLeft = zeros(1, nn);
maxEltRight = zeros(1, nn);

% the initial state. Before this state, three states are allowed: there was
% nothing before, the last element of netropsin ligand was bound, the last
% element of YOYO1 ligand was bound
leftVec(1,:) = [1 0 0 0 1 0 0 0 1];
%leftVec(1,:) = [1 1 1 1 1 1 1 1 1];

leftVec(1,:) = leftVec(1,:)./norm(leftVec(1,:));

rightVec(:,nn+1) = transpose([1 1 1 1 1 1 1 1 1]); 
%rightVec(:,nn+1) = transpose([0 1 0 0 0 0 0 0 0]);
rightVec(:,nn+1)  = rightVec(:,nn+1) ./norm( rightVec(:,nn+1) );

for i=1:nn
     transferMat(5,4)= choice(ntNetrospinSeq(i));
     leftVec(i+1,:) = leftVec(i,:)*transferMat;

     transferMat(5,4)= choice(ntNetrospinSeq(nn-i+1)) ;
     rightVec(:,nn+1-i) = transferMat*rightVec(:,nn-i+2);
     
     maxEltLeft(i) = norm(leftVec(i+1,:));
     
     maxEltRight(nn+1-i) =norm(rightVec(:,nn+1-i));

     leftVec(i+1,:) = leftVec(i+1,:)./maxEltLeft(i);
     rightVec(:,nn+1-i) = rightVec(:,nn+1-i)./maxEltRight(nn+1-i);
end

maxVecDiv =  zeros(1,nn);
maxVecDiv(1) = maxEltLeft(1)/maxEltRight(1);

for i=2:nn
    maxVecDiv(i) = maxVecDiv(i-1)*maxEltLeft(i)/maxEltRight(i);
end


oMat = diag([0,0,0,0,0,1,1,1,1]);

denominator = leftVec(1,:)*rightVec(:,1);


barcodeBP(1) = leftVec(1,:)*oMat*rightVec(:,1)/denominator;

for i=2:nn
    barcodeBP(i) = leftVec(i,:)*oMat*rightVec(:,i)*maxVecDiv(i-1)/denominator;
end

barcodeBP = barcodeBP(nNum+1:end-nNum);

