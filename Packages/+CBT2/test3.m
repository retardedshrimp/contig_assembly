% In this folder we will have functions to do competitive binding transfer
% matrix stuff, but we look at it with fresh eyes and try to use more
% mathematical tools to speed it up/

% main idea is to be able to use markov models instead of phase
% randomisation for the best model possible

addpath(genpath([pwd '/Packages']),genpath([pwd '/Files']));
settings = SETTINGS.settings(); % 

%kk = 100000;

plasmid = getgenbank('NC_016966','SequenceOnly',true);
kk= size(plasmid,2);

kk=10;
dd = 0;

tic
%barcodeBP = CBT.cb_netropsin_vs_yoyo1_plasmid(plasmid(1:kk),settings.NETROPSINconc,settings.YOYO1conc,1000,true);
[barcodeBP] = CBT2.cb_transfer_matrix2(plasmid(1:kk));

toc
figure, plot(barcodeBP(1:kk)),hold on

tic
%[barcodeBP2] = CBT2.cb_two_ligand_temporary(plasmid(1:kk), settings);
[barcodeBP2] = CBT2.cb_transfer_matrix_editable(plasmid(1:kk));
toc 

plot(barcodeBP2(1:kk))
legend({'first method','my realisation'});

barcodePSF = CBT.apply_point_spread_function(barcodeBP2, settings.psfWidth*settings.bpPerNm, false); %false if circular
figure, plot(barcodePSF)
hold on
barcodePSF = CBT.apply_point_spread_function(barcodeBP, settings.psfWidth*settings.bpPerNm, false); %false if circular
plot(barcodePSF)

%     barLen = size(theoreticBarcode,1);
%     newRes = round((1/(settings.bpPerNm*settings.camRes))*(barLen-1)); %could stretch more precisely, so that kbp/pixel ratio would be 0.57
%     rescaledBarcode = interp1(theoreticBarcode, 1:(barLen-1)/newRes:barLen);
%     rescaledBarcode = (rescaledBarcode-mean(rescaledBarcode))/std(rescaledBarcode);

barcodeResc =  interp1(barcodeBP, 1:(size(barcodeBP,1)-1)/(round(settings.bpPerNm*(size(barcodeBP,1)-1))):size(barcodeBP,1));
barcodeRescPSF = CBT.apply_point_spread_function(barcodeResc, settings.psfWidth, false); %false if circular
barcodeFin =  interp1(barcodeRescPSF, 1:(size(barcodeRescPSF,2)-1)/(round(settings.camRes*(size(barcodeRescPSF,2)-1))):size(barcodeRescPSF,2));

plot(barcodeFin)

