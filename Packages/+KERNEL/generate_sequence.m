function [ ySix, acor ] = generate_sequence( sizeSeq, numPeaks, settings, method)
% 22/11/16

    if nargin < 4
        method = 'linear';
    end
    
    % psf settings
    psfSigmaWidth_bp = settings.psfWidth*settings.bpPerNm;
    pixelWidth_bps = round(settings.bpPerNm*settings.camRes);
    untrustedRegion = 5;

    % where to save results
    %saveFolder ='Results/kernel';
    %label = 'IncreasingPeaks';

    % random number
    %tt = randi(1000,1);

    %savePlot = sprintf('%s%d.eps',label,tt);

    %nn = 100;

    %savePlot = sprintf('%d%s%d.eps',tt,label,nn);
    %saveP =  strcat(saveFolder, savePlot);

    aacor = zeros(1,319);



    numPeaks=2000;
    % save directory

    % this generates a specified number of peaks at random places
    SixMPG = 0+randi(sizeSeq,numPeaks,1);
    SixMPG = sort(SixMPG);

    % fit a kernel of specified width on these
    pdSix = fitdist(SixMPG,'Kernel','BandWidth',2*psfSigmaWidth_bp);

    % choose sampling
    x = 0:pixelWidth_bps:sizeSeq+0;

    % output sequece
    ySix = pdf(pdSix,x);

    %ySix = ySix-mean(ySix);

    %figure, plot(x(5:end-5), ySix(5:end-5),'k-','LineWidth',1)

    % compute correlation, this assumes zero mean!
    %[acor,lag] = xcorr(zscore(ySix(5:end-5)),'coeff');
    
    [acor, lag] =  COMPARISON.autocor_coefficients_fft(ySix(untrustedRegion:end-untrustedRegion+1)-mean(ySix(untrustedRegion:end-untrustedRegion+1)), method);




end

