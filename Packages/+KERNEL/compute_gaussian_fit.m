function [ sigmaEst ] = compute_gaussian_fit( aacor, method )
% 22/11/16
% only linear !

    if nargin < 2
        method = 'linear';
    end

    testSeq = transpose(aacor);

    if isequal(method, 'linear')
    	t = transpose(0:size(testSeq,1)-1);
    else
        t = transpose(-round(size(aacor,2))/2:round(size(aacor,2)/2)-1);
    end
    
    f = @(y) exp(-t.^2./(2*y.^2))-testSeq;

    initialGuess = 4;

   
    
    sigmaEst = [lsqnonlin(f,initialGuess)];
    

    
end

