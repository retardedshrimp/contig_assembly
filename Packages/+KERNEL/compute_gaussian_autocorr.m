function [ autocorSeq ] = compute_gaussian_autocorr(sigmaEst, timeSeq )
% 
    gg =  @(y) exp(-timeSeq.^2./(2*y.^2));
    autocorSeq = gg(sigmaEst);
end

