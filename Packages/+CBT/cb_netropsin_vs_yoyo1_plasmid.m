function [probBinding, ntBitsmart_theorySeq_uint8] = cb_netropsin_vs_yoyo1_plasmid(ntSeq, NETROPSINconc, YOYO1conc, untrustedMargin, onlyYoyo1Prob, roundingPrecision)
    % CB_NETROPSIN_VS_YOYO1 = Calculate Competitive binding
    %   probabilities for Netropsin vs YOYO1 on a plasmid
    %
    % Calculates the equilibrium binding probability for each basepair
    % along a DNA strand for the two types of competing ligands
    %
    % The transfer matrix method is from
    %  Teif, Nucl. Ac. Res. 35, e80 (2007)
    %   http://nar.oxfordjournals.org/content/35/11/e80.full
    %   http://nar.oxfordjournals.org/content/42/15/e118.full
    %
    % Inputs:
    %   ntSeq
    %     DNA sequence with standard character/matlab uint8 encoding
    %   NETROPSINconc
    %     Netropsin concentration (units M)
    %   YOYO1conc
    %     YOYO1 concentration (units M)
    %   untrustedMargin (optional, defaults to 1000)
    %     since calculate_competitive_binding_probs gives questionable
    %     results influenced by the choice of the initial and final state,
    %     at the start and end of the sequence, this parameter specifies
    %     how much temporary wrap-around padding (with padding values 
    %     taken from other end of the provided sequence as if it was
    %     cyclical) must be prepended to the ends of the sequence prior to
    %     computations such that the probabilities are at a reasonable
    %     equilibrium in the states surrounding the original sequence area
    %     which is extracted after computations are complete
    %   onlyYoyo1Prob (optional, default false)
    %     only outputs the probability of YOYO1 bind (array not struct of
    %     arrays)
    %   roundingPrecision (optional, default 8)
    %     rounds answers to roundingPrecision digits to the right of the
    %     decimal point (the results are not that accurate anyway so by
    %     rounding to some fixed precision it should make it easier to check if
    %     results are essentially the same when negligible rounding errors
    %     may be the result of negligible asymmetries in the
    %     implementation)
    %
    % Outputs:
    %   probBinding
    %     probability of binding for YOYO1 and Netropsin along DNA
    %      as a struct if onlyYoyo1Prob is not true, or as an array with only the
    %      probability of YOYO1 binding if onlyYoyo1Prob is true
    %   ntBitsmart_theorySeq_uint8
    %     the theory sequence in our custom bitsmart encoding
    %
    % Authors: Saair Quaderi

    import CBT.get_bitsmart_ACGT;
    import CBT.calculate_competitive_binding_probs;
    import CBT.calculate_competitive_binding_probs_2;

    import CBT.get_binding_constant_rules;

    if nargin < 4
        onlyYoyo1Prob = false;
    end
    
    if nargin < 5
        untrustedMargin = 1000;
    end
    
    if nargin < 6
        roundingPrecision = 8;
    end
    
    competitors.Netropsin.bindingConstantRules = get_binding_constant_rules('Netropsin');
    competitors.Netropsin.bulkConc = NETROPSINconc;
    
    competitors.Yoyo1.bindingConstantRules = get_binding_constant_rules('Yoyo1');
    competitors.Yoyo1.bulkConc = YOYO1conc;

    if onlyYoyo1Prob
        skipProbCalcCompetitors = {'Netropsin'};
    else
        skipProbCalcCompetitors = {};
    end
    
    seqLen = length(ntSeq);
    if untrustedMargin > 0        
        repeatedTimes = ceil(untrustedMargin/seqLen);
        paddedNtSeq = repmat(ntSeq(:), [repeatedTimes, 1]);
        paddedNtSeq = [paddedNtSeq((end - untrustedMargin) + (1:untrustedMargin)); paddedNtSeq; paddedNtSeq(1:untrustedMargin)]';
    else
        untrustedMargin = 0;
        paddedNtSeq = ntSeq;
    end
    
    [probBinding, ~, ntBitsmart_theorySeq_uint8] = calculate_competitive_binding_probs(paddedNtSeq, competitors, skipProbCalcCompetitors);
    
    unpaddedIdxs = untrustedMargin + (1:seqLen);
    ntBitsmart_theorySeq_uint8 = ntBitsmart_theorySeq_uint8(unpaddedIdxs);
    fnames = fieldnames(probBinding);
    
    numFnames = length(fnames);
    for fnameNum = 1:numFnames
        fname = fnames{fnameNum};
        probBinding.(fname) = round(probBinding.(fname)(unpaddedIdxs), roundingPrecision);
    end
    if onlyYoyo1Prob
        probBinding = probBinding.Yoyo1;
    end
    
end