function [ mat_logical ] = uint8_in_binary( vect_uint8, k)
%UINT8_2_LOGICAL_CELL - Converts a vector of N uint8 values into a matrix
% of Mxk logical containing the bit values of the binary
% representations of each of the uint8 values. If k is specified only the
% last k bits of the uint8s are procided. If k is not specified, k
% defaults to 8 
    if nargin < 2
        k = 8;
    end
    mat_logical = logical(rem(floor(double(vect_uint8(:))*pow2(1 - k:0)),2));
end

