function [ bindingConstantRules ] = get_binding_constant_rules( ligandName )
    %GET_BINDING_CONSTANT_RULES - Returns the binding constant rules for a
    %  particular ligand
    %
    % Inputs:
    %   ligandName
    %     the name of the ligand
    %
    % Outputs:
    %   bindingConstantRules
    %      a struct specifying the binding constant rules for the ligand
    %      (these can be decompressed by CBT.gen_binding_constants into a
    %      matrix for all the possibilities)
    
    % TODO: Stop hardcoding this and retrieve it from some settings file or
    %   database or something
    switch ligandName
        case 'Netropsin'
            bindingConstantRules = {...
                'NNNN', 5*(10^5);...
                'WWWW', 1*(10^8)...
            };
        case 'Yoyo1'
            bindingConstantRules = {...
                'NNNN', 1*(10^11)...
             };
        otherwise
            bindingConstantRules = struct;
            warning('Ligand binding constant rules not found');
    end

end

