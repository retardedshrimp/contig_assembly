function []=plot_autocorr_comparison(barcode, l, barLen, method )
   % 26/10/16
   % comparison plot between barcode's autocorrelation and plotted autocor
   
    if nargin < 4 % if no method was selected
       method = 'circular';
    end
    
    
    switch method
    
		case 'linear'
			autComp = autocorr(barcode,barLen-1);
			t = 0:barLen-1;
			autoCorr = exp(-(t./l).^2);
			
		
		case 'circular'
			[autoC,~] = COMPARISON.cross_correlation_coefficients_fft(barcode,barcode);
			
			
			t=0:floor(barLen/2)-1;
			
			
			gg = exp(-(t./l).^2);
			
			ipt = -floor(barLen/2):-1;
			
			
			ff =  exp(-(ipt./l).^2);
			autoCorr = [gg ff];
			
			autComp = [autoC(1:floor(barLen/2)) autoC(end-floor(barLen/2)+1:end)]

			
		otherwise
             warning('Unrecognised method selected')
			return;
    end
    
    h = figure; plot(autoCorr)
    hold on
    plot(autComp)
    
    ylabel('correlation coef.')
    xlabel('pixel')
    
    legend({'Barcode autocorrelation', 'Fitted autocorrelation'})

	saveFolder ='Results/Plot';

	tt = randi(1000,1);

	savePlot = sprintf('%s%d.eps','compar',tt);
	
	saveP =  strcat(saveFolder, savePlot);

	saveas(h,saveP,'epsc') 
	
	saveFile = sprintf('%s%d.mat','compar',tt);
	saveF = strcat(saveFolder, saveFile);

	save(saveF, '-v7.3', 'autoCorr', 'autComp', 'barcode','l','barLen','method');



end
