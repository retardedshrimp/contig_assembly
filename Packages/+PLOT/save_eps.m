function []=save_eps(xset, dataset,xaxis,yaxis,legendT, label,method)

	if nargin < 7 % if no method was selected
        method = 'circular';
    end
	% 25/10/16 save eps for paper
	
	saveFolder ='Results/Plot';
    
	
	tt = randi(1000,1);


	saveFile = sprintf('%s%d.mat',label,tt);

	savePlot = sprintf('%s%d.eps',label,tt);
	
	saveF = strcat(saveFolder, saveFile);
	save(saveF, '-v7.3', 'xset', 'dataset', 'xaxis','yaxis');

	f= figure('visible','off');
	h2 = plot(xset,dataset,'r', 'linewidth',2); % 'r', 'linewidth',3

    ylabel(yaxis)
    xlabel(xaxis)
    
    legend({legendT})
    
    saveP =  strcat(saveFolder, savePlot);

	saveas(h2,saveP,'epsc') 
	
	if isequal(method,'circular')
		[autCor,b] = COMPARISON.cross_correlation_coefficients_fft(dataset, dataset); 
	else
		autCor = autocorr(dataset, size(dataset,2)-1);
	end
	
	f2 = figure('visible','off');
	h3 = plot(xset-1,autCor,'r', 'linewidth',2); % 'r', 'linewidth',3
	ylabel('correlation')
    xlabel('lag')
    
    legend({'autocorrelation'})
    
	savePlot = sprintf('%s%d.eps','autocorr',tt);
    saveP = strcat(saveFolder, savePlot);

	saveas(h3,saveP,'epsc') 

	save(saveF, '-append','autCor');

end
