classdef CMN_HelperFunctions

  %    
  % CMN_HelperFunctions - Common "helper" functions  related to 
  % DNA barcode analysis and DNA barcode prediction  
  %  Syntax: [output] = CMN_HelperFunctions.functionname( input )
  %
  % NOTE: No functions defined herein can use 
  % Mainstruct, Miscstruct or Settingsstruct
  %
  % Other class-files required:
  % CMN_CompetitiveBindingTheory.
  %
  % Matlab Toolboxes required:
  % Bioinformatics Toolbox , Curve Fitting Toolbox , 
  % Image Processing Toolbox , Statistics Toolbox
  % 
  % This class  containing the following (static) functions:
  %      {'ccorr_lincirc', 'ccorr_circcirc' ,
  %        'find_best_cc' , 'reisnerrescaling' 'findsignalregion' , ...
  %        'robustextrema', ...
  %        'calcintinfo', 'calcinfotheory', 'ccorr_all', ...
  %        'generate_subplots', 'cb_findpvalues2', ...
  %        'cb_pvalue', 'find_effective_correlationlength', ...
  %        'stretch_theory_to_pixel_resolution',
  %        'read_consensus_file', 's_val_calc'}; 
 
  
methods(Static)   
  
    %=========================================================================%
    function [ outputCC, flip , shift ] = ccorr_lincirc( B1 , B2)

    % CCORR_LINCIRC - Cross-correlation between two barcodes (using local
    % rescaling) where the shorter barcode is assumed to be linear and the
    % longer barcode is circular.
    %
    %  Calculates the cross-correlation values
    %  between two barcodes. One barcode is slided across the other one 
    %  and a set of cross-correlation values are obtain. LOCAL Resiner-rescaling 
    %  is used, i.e. the parts of the barcodes being compared 
    %  are rescaled to have mean = 0 and standard deviation = 1.
    %  The relative orientation of the barcodes
    %  is also identified (by maximizing the cross-correlation)
    % 
    % Inputs: 
    % B1 = barcode 1
    % B2 = barcode 2
    %
    % Outputs: 
    % outputCC = list of cross-correlation for correct flip
    % flip = is equal to '0' is no flip, otherwise = '1'.
    % shift = shift of B1 w r t B2 
    %  [Note that shift can be negative: -(n-1)<=shift<=n-1 ]
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): none
    %
    % By: Tobias Ambjornsson, Christoffer Pichler, Erik Lagerstedt
    %

    
    %  Store longest input-barcode in the vector longVec 
    %  and the shortest input-barcode in vector shortVec 
    %  Reisner-rescale the shortest barcode  
    %  (the longer one is rescaled while calculating 
    %   the cross-correlation values)  
    l_B1=length(B1);
    l_B2=length(B2);
    shortLength=min(l_B2,l_B1);
    longLength=max(l_B2,l_B1);
    if longLength==l_B1
        longVec=B1;
        shortVec=B2;
    else
        longVec=B2;
        shortVec=B1;
    end;
    clear B1; clear B2;
    %  Reisner-rescale the shortest barcode
    shortVec=(shortVec-mean(shortVec))/std(shortVec);


    %
    %  Calculate forward and backward cross-correlations 
    %  by "sliding" one barcode across the other 
    %
    longVec_perm_cut=zeros(1,shortLength);
    ccForward=zeros(1,longLength);
    ccBackward=zeros(1,longLength);


    for i=1:longLength
        %  circularly permutate and "cut out"
        %  the relevant part of the longest vector
        %  (the cut out part is of the same length as the shortest barcode;
        if i+shortLength > longLength
            longVec_perm_cut(1:longLength-i+1) = longVec(i:end);
            longVec_perm_cut(longLength-i+2:end) = longVec(1:shortLength-end+i-1);
        else
            longVec_perm_cut = longVec(i:shortLength+i-1);
        end
        %longVec_perm_cut
        %  Reisner-rescale the permutated and cut-out barcode
        longVec_perm_cut=(longVec_perm_cut-mean(longVec_perm_cut))/std(longVec_perm_cut);

        %  calculate forward and backward cross-correlation values
        ccForward(i) = sum(shortVec.*longVec_perm_cut);
        ccBackward(i) = sum(fliplr(shortVec).*longVec_perm_cut);
    end

    %  Normalize by the length of the shortest original   
    %  barcodes and identify the direction and cut position
    %  of the shorter barcode w r t the longer one  

    ccForward=ccForward/(shortLength-1);
    ccBackward=ccBackward/(shortLength-1);
    [maxCCForward,indexForward]=max(ccForward);
    [maxCCBackward,indexBackward]=max(ccBackward);

    if maxCCBackward>maxCCForward
        outputCC=ccBackward;
        flip=1;
        if l_B1==longLength
            shift=-(indexBackward-1);
        else
            shift=indexBackward-1;
        end
    else
        outputCC=ccForward;
        flip=0;
        if l_B1==longLength
            shift=-(indexForward-1);
        else
            shift=indexForward-1;

        end
    end
  
    end
    %=========================================================================%
    function [ outputCC1, outputCC2, flip , shift ] = ccorr_lincirc_both( B1 , B2)

    % CCORR_LINCIRC - Cross-correlation between two barcodes (using local
    % rescaling) where the shorter barcode is assumed to be linear and the
    % longer barcode is circular.
    %
    %  Calculates the cross-correlation values
    %  between two barcodes. One barcode is slided across the other one 
    %  and a set of cross-correlation values are obtain. LOCAL Resiner-rescaling 
    %  is used, i.e. the parts of the barcodes being compared 
    %  are rescaled to have mean = 0 and standard deviation = 1.
    %  The relative orientation of the barcodes
    %  is also identified (by maximizing the cross-correlation)
    % 
    % Inputs: 
    % B1 = barcode 1
    % B2 = barcode 2
    %
    % Outputs: 
    % outputCC = list of cross-correlation for correct flip
    % flip = is equal to '0' is no flip, otherwise = '1'.
    % shift = shift of B1 w r t B2 
    %  [Note that shift can be negative: -(n-1)<=shift<=n-1 ]
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): none
    %
    % By: Tobias Ambjornsson, Christoffer Pichler, Erik Lagerstedt
    %

    
    %  Store longest input-barcode in the vector longVec 
    %  and the shortest input-barcode in vector shortVec 
    %  Reisner-rescale the shortest barcode  
    %  (the longer one is rescaled while calculating 
    %   the cross-correlation values)  
    l_B1=length(B1);
    l_B2=length(B2);
    shortLength=min(l_B2,l_B1);
    longLength=max(l_B2,l_B1);
    if longLength==l_B1
        longVec=B1;
        shortVec=B2;
    else
        longVec=B2;
        shortVec=B1;
    end;
    clear B1; clear B2;
    %  Reisner-rescale the shortest barcode
    shortVec=(shortVec-mean(shortVec))/std(shortVec);


    %
    %  Calculate forward and backward cross-correlations 
    %  by "sliding" one barcode across the other 
    %
    longVec_perm_cut=zeros(1,shortLength);
    ccForward=zeros(1,longLength);
    ccBackward=zeros(1,longLength);


    for i=1:longLength
        %  circularly permutate and "cut out"
        %  the relevant part of the longest vector
        %  (the cut out part is of the same length as the shortest barcode;
        if i+shortLength > longLength
            longVec_perm_cut(1:longLength-i+1) = longVec(i:end);
            longVec_perm_cut(longLength-i+2:end) = longVec(1:shortLength-end+i-1);
        else
            longVec_perm_cut = longVec(i:shortLength+i-1);
        end
        %longVec_perm_cut
        %  Reisner-rescale the permutated and cut-out barcode
        longVec_perm_cut=(longVec_perm_cut-mean(longVec_perm_cut))/std(longVec_perm_cut);

        %  calculate forward and backward cross-correlation values
        ccForward(i) = sum(shortVec.*longVec_perm_cut);
        ccBackward(i) = sum(fliplr(shortVec).*longVec_perm_cut);
    end

    %  Normalize by the length of the shortest original   
    %  barcodes and identify the direction and cut position
    %  of the shorter barcode w r t the longer one  

    ccForward=ccForward/(shortLength-1);
    ccBackward=ccBackward/(shortLength-1);
    [maxCCForward,indexForward]=max(ccForward);
    [maxCCBackward,indexBackward]=max(ccBackward);

    if maxCCBackward>maxCCForward
        outputCC1=ccBackward;
        outputCC2=ccForward;
        flip=1;
        if l_B1==longLength
            shift=-(indexBackward-1);
        else
            shift=indexBackward-1;
        end
    else
        outputCC1=ccForward;
        outputCC2=ccBackward;
        flip=0;
        if l_B1==longLength
            shift=-(indexForward-1);
        else
            shift=indexForward-1;

        end
    end
  
    end
    %=========================================================================%
    function [ bestCC, flip, shiftShortVsLong, cut ] = ccorr_circcirc_nofft(B1,B2)
        
    %  CCORR_CIRCCIRC calculates the cross correlation between two barcodes,
    %  where both barcodes are assumed to be circular ('circ'). 
    %
    % The shortest code ('short') is cut at a certain position ('cut'), and 
    % rearranged in such a way that the cut position will be the beginning 
    % and the positions before the cut, relocated at the end of the code;
    % the short vector once rearranged is called 'shortc' .
    %
    % Shift shortc across the longest code ('long') and for every shift, the
    % cross-correlation value is obtain in two ways: using the mirror ('ccb') 
    % and the non-mirror ('ccf') image of 'shortc'. 
    % 
    % This process is done for every possible cut in 'short'.
    %
    % ccf/ccb = matrix with the cross correlation value for every shift 
    % and for every cut in 'short', in "forward"/"backward(mirror)"
    %
    % The cross correlation 'cc' will be the maximum value in both ccf and ccb.
    %
    % Resiner-rescaling is used: the parts of the barcodes being compared 
    % are rescaled to have mean = 0 and standard deviation = 1.
    % 
    % Inputs:   barcodes B1, B2 
    %           shortest/longest of B1 and B2 will be called 'short'/'long'
    %           if B1, B2 same length, B1='short', B2='long'
    % 
    % Outputs:  bestCC = best cross correlation value
    %           flip = orientation of 'short': mirror(flip=1), non-mirror(flip=0)   
    %           shiftShortVsLong' = shift of 'short' w r t 'long'   %           
    %           cut = position of cut in 'short'
    %           NOTE: 'shiftShortVsLong' is differently defined from 'shift'
    %                  in function CMN_HelperFunctions.ccorr_lincirc
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): CMN_HelperFunctions.reisnerrescaling
    %
    % By Paola Torche, edited for speed by Erik Lagerstedt, 2015.
    %
    % TO DO: if barcodes are of the same length then replace 
    % "for z=1:S" (rearrangement of short barcode) 
    % by "for z=1:Zmax with Zmax=1 or S" 
    
        if length(B1)<=length(B2)
            short=B1;
            long=B2;
        else
            short=B2;
            long=B1;
        end

        S=length(short); L=length(long); long_crop=zeros(1,S); 
        ccf=zeros(S,L); ccb=zeros(S,L); %shortc=zeros(1,S);
        ccfbestposition=zeros(1,S); ccbbestposition=zeros(1,S);
        fbestposition=zeros(1,L); bbestposition=zeros(1,L);

        for z=1:S     % rearrangement of the shortest creates 'shortc'
            shortc=circshift(short,[1,1-z]);
             % %%%%%%%%%%%%%%%%%%%%%%
             % TA: mean and std dev needed for resner-rescaling
             %  can be done outside of for - loop. 
             % %%%%%%%%%%%%%%%%%%%%%%%
            shortc=CMN_HelperFunctions.reisnerrescaling(shortc);     % reisner-rescale
            for i=1:L     % shortc is relocated at the position i in 'long'
                if i+S > L
                    long_crop(1:L-i+1) = long(i:end);
                    long_crop(L-i+2:end) = long(1:S-end+i-1);
                else
                    long_crop = long(i:S+i-1);
                end
                
                long_crop=CMN_HelperFunctions.reisnerrescaling(long_crop);    % reisner-rescale
                ccf(z,i)=sum(shortc.*long_crop);
                ccb(z,i)=sum(fliplr(shortc).*long_crop);
                ccf(z,i)=ccf(z,i)/(S-1);
                ccb(z,i)=ccb(z,i)/(S-1);
            end

            % find the best shift for a given cut z :  ccfbestposition / ccbbestposition
            [ccfbestposition(z),fbestposition(z)]=max(ccf(z,:));  
            [ccbbestposition(z),bbestposition(z)]=max(ccb(z,:));
        end
       

        % find the best cut z :  ccfbestcut / ccbbestcut
        [ccfbestcut,bestcutf]=max(ccfbestposition);
        [ccbbestcut,bestcutb]=max(ccbbestposition);

        if ccbbestcut>ccfbestcut     % if "mirror" is best
            bestCC=ccbbestcut;
            flip=1;
            position = bbestposition(bestcutb);
            cut=bestcutb;
        else                         % if "non-mirror" is best
            bestCC=ccfbestcut;
            flip=0;
            position = fbestposition(bestcutf);
            cut=bestcutf;
        end

        shiftShortVsLong = position-1;

        clear short; clear shortc; clear long; clear long_crop;
    end
    %=========================================================================%
    function [ outputCC1, outputCC2, flip , shift ] = ccorr_all(B1,B2,B1circ,rescale)
        
        % ccorr_all - Even though it might say all, it is actually only for
        % lincirc and linlin.
        %
        %  Calculates the cross-correlation values
        %  between two barcodes. One barcode is slided across the other one
        %  and a set of cross-correlation values are obtain. LOCAL Resiner-rescaling
        %  is used, i.e. the parts of the barcodes being compared
        %  are rescaled to have mean = 0 and standard deviation = 1.
        %  The relative orientation of the barcodes
        %  is also identified (by maximizing the cross-correlation)
        %
        % Inputs:
        % B1 = barcode 1
        % B2 = barcode 2
        % B1circ = boolean value if B1 is circular or not
        % rescale = boolean value if the barcodes should be rescaled or not
        %
        % Outputs:
        % outputCC1 = list of cross-correlation for correct flip
        % outputCC2 = rest of the cross-correlation values
        % flip = is equal to '0' is no flip, otherwise = '1'.
        % shift = shift of B1 w r t B2
        %  [Note that shift can be negative: -(n-1)<=shift<=n-1 ]
        %
        % Effect on Mainstruct: none
        %
        % Dependencies (matlab-functions, MAT-files): none
        %
        % By: Tobias Ambjornsson, Christoffer Pichler, Erik Lagerstedt
        %
        
        
        %  Store longest input-barcode in the vector longVec
        %  and the shortest input-barcode in vector shortVec
        %  Reisner-rescale the shortest barcode
        %  (the longer one is rescaled while calculating
        %   the cross-correlation values)
        l_B1=length(B1);
        l_B2=length(B2);
        shortLength=min(l_B2,l_B1);
        longLength=max(l_B2,l_B1);
        if longLength==l_B1
            longVec=B1;
            shortVec=B2;
        else
            longVec=B2;
            shortVec=B1;
        end;
        clear B1; clear B2;
        if rescale
            %  Reisner-rescale the shortest barcode
            shortVec=(shortVec-mean(shortVec))/std(shortVec);
        end
        
        shortVecFlip = fliplr(shortVec);
        
        %  Calculate forward and backward cross-correlations
        %  by "sliding" one barcode across the other
        if B1circ
            longVec_perm_cut=zeros(1,shortLength);
            ccForward=zeros(1,longLength);
            ccBackward=zeros(1,longLength);
            for i=1:longLength
                %  Circularly permutate and "cut out"
                %  the relevant part of the longest vector
                %  (the cut out part is of the same length as the shortest barcode;
                if i+shortLength > longLength
                    longVec_perm_cut(1:longLength-i+1) = longVec(i:end);
                    longVec_perm_cut(longLength-i+2:end) = longVec(1:shortLength-end+i-1);
                else
                    longVec_perm_cut = longVec(i:shortLength+i-1);
                end
                if rescale
                    %  Reisner-rescale the permutated and cut-out barcode
                    longVec_perm_cut=(longVec_perm_cut-mean(longVec_perm_cut))/std(longVec_perm_cut);
                end
                %  Calculate forward and backward cross-correlation values
                ccForward(i) = sum(shortVec.*longVec_perm_cut);
                ccBackward(i) = sum(shortVecFlip.*longVec_perm_cut);
            end
        else
            ccForward=zeros(1,longLength-shortLength+1);
            ccBackward=zeros(1,longLength-shortLength+1);
            for i=1:longLength-shortLength+1
                %  (the cut out part is of the same length as the shortest barcode;
                longVec_perm_cut = longVec(i:shortLength+i-1);
                if rescale
                    %  Reisner-rescale the permutated and cut-out barcode
                    longVec_perm_cut=(longVec_perm_cut-mean(longVec_perm_cut))/std(longVec_perm_cut);
                end
                %  calculate forward and backward cross-correlation values
                ccForward(i) = sum(shortVec.*longVec_perm_cut);
                ccBackward(i) = sum(shortVecFlip.*longVec_perm_cut);
            end
        end
        
        %  Normalize by the length of the shortest original
        %  barcodes and identify the direction and cut position
        %  of the shorter barcode w r t the longer one
        
        ccForward=ccForward/(shortLength-1);
        ccBackward=ccBackward/(shortLength-1);
        [maxCCForward,indexForward]=max(ccForward);
        [maxCCBackward,indexBackward]=max(ccBackward);
        
        if maxCCBackward>maxCCForward
            outputCC1 = ccBackward;
            outputCC2 = ccForward;
            flip=1;
            if l_B1==longLength
                shift=-(indexBackward-1);
            else
                shift=indexBackward-1;
            end
        else
            outputCC1 = ccForward;
            outputCC2 = ccBackward;
            flip=0;
            if l_B1==longLength
                shift=-(indexForward-1);
            else
                shift=indexForward-1;
                
            end
        end
        
    end
    %=========================================================================%
    function [ bestCC, flip, shiftShortVsLong, cut] = ccorr_circcirc(B1,B2)
    %  CCORR_CIRCCIRC calculates the cross correlation between two barcodes,
    %  where both barcodes are assumed to be circular ('circ'). Faster
    %  version of ccorr_circcirc_nofft
    %
    % The shortest barcode ('short') is cut at a certain position ('cut'), and 
    % circularly permutated. Then short is "slided" across the longest code
    % ('long') and for every shift, the cross-correlation value is 
    % obtained in two ways: using the mirror ('ccb') 
    % and the non-mirror ('ccf') image of 'shortc'. 
    % 
    % This process is done for every possible cut in 'short'.
    %
    % ccf/ccb = matrix with the cross correlation value for every shift 
    % and for every cut in 'short', in "forward"/"backward(mirror)"
    %
    % The cross correlation 'cc' will be the maximum value in both ccf and ccb.
    %
    % Resiner-rescaling is used: the parts of the barcodes being compared 
    % are rescaled to have mean = 0 and standard deviation = 1.
    % 
    % Inputs:   barcodes B1, B2 
    %           shortest/longest of B1 and B2 will be called 'short'/'long'
    %           if B1, B2 same length, B1='short', B2='long'
    % 
    % Outputs:  bestCC = best cross correlation value
    %           flip = orientation of 'short': mirror(flip=1), non-mirror(flip=0)   
    %           shiftShortVsLong' = shift of 'short' w r t 'long'   %           
    %           cut = position of cut in 'short'
    %           NOTE: 'shiftShortVsLong' is differently defined from 'shift'
    %                  in function CMN_HelperFunctions.ccorr_lincirc
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): CMN_HelperFunctions.reisnerrescaling
    %
    % By Paola Torche, Erik Lagerstedt and Tobias Ambjörnsson, 2015.
    %
    % TO DO: if barcodes are of the same length then replace 
    % "for i=1:L" 
    % by "for i=1:iMax with iMax=1 or L"  
        
        % Convert B1 and B2 to column vectors
        B1 = B1(:)'; 
        B2 = B2(:)';
        
        % Some preprocessing
        if length(B1)<=length(B2)
            short=B1;
            long=B2;
        else
            short=B2;
            long=B1;
        end
        S=length(short); L=length(long); 
        long_crop=zeros(1,S); 
        ccfbestcut=zeros(1,L); ccbbestcut=zeros(1,L);
        bestcutf=zeros(1,L); bestcutb=zeros(1,L);
        
        short=CMN_HelperFunctions.reisnerrescaling(short);     % reisner-rescale
        
        % Mu and Sigma calculation
        for i=1:L     % cut out barcode from 'long' starting at position i
            if i+S > L
                long_crop(1:L-i+1) = long(i:end);
                long_crop(L-i+2:end) = long(1:S-end+i-1);
            else
                long_crop = long(i:S+i-1);
            end
            long_crop=CMN_HelperFunctions.reisnerrescaling(long_crop);    % reisner-rescale
            
            % Use FFT to evaluate the cross correlation
            conj_fft_longcrop = conj(fft(long_crop));
            ccf = ifft(fft(short).*conj_fft_longcrop); 
            ccb = ifft(fft(fliplr(short)).*conj_fft_longcrop);
            % "clean up" backward vector
            ccb = circshift(ccb,[0,-1]); 
            ccb = fliplr(ccb);
 
            % find the best cut for a given position i   
            [ccfbestcut(i),bestcutf(i)]=max(ccf);
            [ccbbestcut(i),bestcutb(i)]=max(ccb);
        end 
        
        % find the best position 
        [ccfbestposition,fbestposition]=max(ccfbestcut); 
        [ccbbestposition,bbestposition]=max(ccbbestcut);

        if ccbbestposition>ccfbestposition     % if "mirror" is best
            bestCC=ccbbestposition/(S-1); % normalize
            flip=1;
            position = bbestposition;
            cut=bestcutb(bbestposition);
        else                         % if "non-mirror" is best
            bestCC=ccfbestposition/(S-1); % normalize
            flip=0;
            position = fbestposition;
            cut=bestcutf(fbestposition);
        end

        shiftShortVsLong = position-1;
    end
    %=========================================================================%
    function [bestCC , flip , shiftShortVsLong, cut, stretchFactor] = find_best_cc(firstCurve, secondCurve, strat)
        
    % FIND_BEST_CC - find the best cross-correlation between two curves
    %                given one of three strategies to handle differences in
    %                length.
    %
    % Inputs:
    %     firstCurve = the first curve (is stretched)
    %     secondCurve = the second curve (not stretched)
    %     strat  = strategy; 
    %              1=no stretch, 2=stretch to equal lengths,
    %              3=find optimal stretch, 4=hybrid of 2 and 3
    %
    % Outputs:
    %     bestCC = best cross-correlation value
    %     flip = is equal to '0' is no flip, otherwise = '1'. 
    %     shiftShortVsLong' = shift of 'short' w r t 'long'  
    %     cut = position of cut in short barcode
    %     stretchFactor = optimal strech factor (cases 3 and 4)
    %                      For cases 1 and 2, stretchFactor = NaN
    %
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions,
    %                                             CMN_HelperFunctions.ccorr_circcirc
    %
    % By: Erik Lagerstedt adn Tobias Ambjörnsson
    %
    
    noOfStretchings=21;       % The number of stretched versions to be tested.
    optInterval=0.05;          % Interval, [1-optInterval,1+optInterval],
                              % in which stretchings are distributed, for
                              % cases 3 and 4
    
    if strat == 1  % no stretch
        
        [bestCC , flip , shiftShortVsLong , cut] = CMN_HelperFunctions.ccorr_circcirc(firstCurve,secondCurve);
        stretchFactor = 1;
        
    else if strat == 2 % stretch to equal lengths
            
     	firstCurve=interp1(firstCurve,1:(length(firstCurve)-1)/(length(secondCurve)-1):length(firstCurve));
        [bestCC , flip , shiftShortVsLong , cut] = CMN_HelperFunctions.ccorr_circcirc(firstCurve,secondCurve);
        stretchFactor = length(secondCurve)/length(firstCurve);
        
        else if strat == 3  % find optimal stretch
                
            bestCCTemp=zeros(1,noOfStretchings); flipTemp=zeros(1,noOfStretchings); 
            shiftTemp=zeros(1,noOfStretchings);  cutTemp=zeros(1,noOfStretchings);
            stretchFactorTemp=zeros(1,noOfStretchings);
            ki=0;
            for k=1-optInterval:2*optInterval/(noOfStretchings-1):1+optInterval
                ki=ki+1;
                firstCurveT=interp1(firstCurve,1:(length(firstCurve)-1)/(round(k*(length(firstCurve)-1))):length(firstCurve));
                [bestCCTemp(ki) , flipTemp(ki) , shiftTemp(ki) , cutTemp(ki)] ...
                    = CMN_HelperFunctions.ccorr_circcirc(firstCurveT,secondCurve);
                stretchFactorTemp(ki) = k; 
            end
            [bestCC indexBestCC] = max(bestCCTemp);
            flip = flipTemp(indexBestCC);
            shiftShortVsLong = shiftTemp(indexBestCC);
            cut = cutTemp(indexBestCC);
            stretchFactor = stretchFactorTemp(indexBestCC);
            
            else  % strat = 4 % first stretch to equal lengths, then find optimal stretch
                

                bestCCTemp=zeros(1,noOfStretchings); flipTemp=zeros(1,noOfStretchings); 
                shiftTemp=zeros(1,noOfStretchings); cutTemp=zeros(1,noOfStretchings);
                ki=0;
                for k=1-optInterval:2*optInterval/(noOfStretchings-1):1+optInterval 
                    ki=ki+1;
                      firstCurveT=interp1(firstCurve,1:(length(firstCurve)-1)/(round(k*(length(secondCurve)-1))):length(firstCurve));
                                  [bestCCTemp(ki) , flipTemp(ki) , shiftTemp(ki) , cutTemp(ki)] ...
                        = CMN_HelperFunctions.ccorr_circcirc(firstCurveT,secondCurve);
                    stretchFactorTemp(ki) = k; 
                end
                    [bestCC indexBestCC] = max(bestCCTemp);
                    flip = flipTemp(indexBestCC);
                    shiftShortVsLong = shiftTemp(indexBestCC);
                    cut = cutTemp(indexBestCC);
                    stretchFactor = stretchFactorTemp(indexBestCC);  
            end
        end
    end
    
    end
    %=========================================================================%
    function [curve] = reisnerrescaling(curve)

    % REISNERRESCALING - Scales a barcode to have mean = 0 and std dev = 1
    %
    % Inputs: curve (the barcode)
    %
    % Outputs: curve (the rescaled barcode)
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    curve = (curve - mean(curve)) / std(curve);

    end
    %=========================================================================%
    function [ x1, x2 ] = findsignalregion( curve )

    % FINDSIGNALREGION - finds the "signal region" of a molecule intensity 
    %	trace (i.e., the part of the curve that corresponds to a molecule and
    %	not to background)
    %
    % Inputs: the curve
    %
    % Outputs: the left- and right- boundaries of the molecule
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Charleston Noble
    %

    curve = abs(curve);

    mCurve = mean(curve);

    x1 = find(curve > mCurve,1,'first');
    x2 = find(curve > mCurve,1,'last');

    end
    %=========================================================================%
    function [ x1, x2 ] = findsignalregion_otsu( curve )

    % FINDSIGNALREGION_OTSU - finds the "signal region" of a molecule intensity 
    %	trace (i.e., the part of the curve that corresponds to a molecule and
    %	not to background) using the Otsu method
    %
    % Inputs: the curve
    %
    % Outputs: the left- and right- boundaries of the molecule
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Tobias Ambjörnsson (copy-n-paste from DBM_Statistics.m)
    %
  
    
        smoothCurve = smooth(curve, 5);
        thresh = multithresh(smoothCurve,1);
        quantCurve = imquantize(smoothCurve,thresh) > 1;

        % Remove molecules which are at the edges of the full signal 
        if quantCurve(1) > 0
            idx = find(quantCurve == 0, 1, 'first');
            quantCurve(1:idx) = 0;
        end
        if quantCurve(end) > 0
            idx = find(quantCurve == 0, 1, 'last');
            quantCurve(idx:end) = 0; 
        end

        % Remove small gaps.
        quantCurve=imclose(quantCurve,strel('line',5,90));

        % Get the largest "molecule".
        x = bwlabel(quantCurve);
        y = regionprops(logical(quantCurve));
        [~, largestRegion] = max([y.Area]);
        if isempty(largestRegion)
           disp('No signal detected using the Otsu method')
        end
        quantCurve(x ~= largestRegion) = 0;
        curve(x ~= largestRegion) = min(curve(x == largestRegion));

        % Find the signal region start and end points
        x1 = find(quantCurve > 0, 1, 'first');
        x2 = find(quantCurve > 0, 1, 'last');

    end
    %=========================================================================%
    function [ extVals ] = robustextrema(curve, noiseThreshold)

    % ROBUSTEXTREMA - Find robust maxima and minuma in a curve
    %
    % Inputs: 
    % curve = 1D curve to be analyzed
    % noiseThreshold = threshold for deeming a peak/valley "robust" or not.
    %
    % Outputs: extVals, the extrema values
    % 
    % Effect on Mainstruct:
    %
    % Dependencies (matlab-functions, MAT-files): none
    %
    % By: Tobias Ambjornsson
    %

    E_hb = diff(curve);
    E_th = noiseThreshold;
    max_no_boundaries = 100;

    k=0;                            % k labels different basepairs
    boundary_counter=1;             % counter for number of helix-coil
                                    % and coil-helix boundaries 
    hc=zeros(max_no_boundaries,1);         
                                    % positions of helix-coil boundaries
                                    % hc=position of helix-part
                                    % of boundary  
    ch=zeros(max_no_boundaries,1);         
                                    % positions of coil-helix boundaries
                                    % ch=position of coil-part
                                    % of boundary  
    no_of_hc_boundaries=0;          % number of helix-coil boundaries
    no_of_ch_boundaries=0;          % number of coil-helix boundaries

    M = length(E_hb);
    while k<=M-1 

       % -- helical segment --

       j=k;  % (a possible) energy minimum at position j
       E=0;
       while k<=M-1 && E<=E_th
          k=k+1;
          E=E+E_hb(k);
          if E<0  % a new possible robust local minima found
             j=k;
             E=0;    
          else % E>=0
             if E>E_th
                hc(boundary_counter)=j;       % store position of
                                              % helix-coil boundary
                                              % hc=position of helix-part
                                              % of boundary
                no_of_hc_boundaries=no_of_hc_boundaries+1;  

             end
          end
       end

       % -- coil segment --

       j=k;   % (a possible) energy maximum at position j
       E=0;
       while k<=M-1 && E>=-E_th
          k=k+1;
          E=E+E_hb(k);
          if E>0 % a new possible robust local maxima found
             j=k;
             E=0;
          else % E<0
             if E<-E_th
                ch(boundary_counter)=j;       % store position of
                                              % coil-helix boundary
                                              % ch=position of helix-part
                                              % of boundary
                no_of_ch_boundaries=no_of_ch_boundaries+1;  

             end
          end
       end

       boundary_counter=boundary_counter+1;    
    end

    extVals = curve(sort([ch(ch>0)' hc(hc>0)'])+1);

    end
    %=========================================================================%
    function [ ha ]  = generate_subplots(figHandle, Nh, Nw, gap, marg_h, marg_w)

    % tight_subplot creates "subplot" axes with adjustable gaps and margins
    %
    % ha = tight_subplot(Nh, Nw, gap, marg_h, marg_w)
    %
    %   in:  Nh      number of axes in height (vertical direction)
    %        Nw      number of axes in width (horizontaldirection)
    %        gap     gaps between the axes in normalized units (0...1)
    %                   or [gap_h gap_w] for different gaps in height and width 
    %        marg_h  margins in height in normalized units (0...1)
    %                   or [lower upper] for different lower and upper margins 
    %        marg_w  margins in width in normalized units (0...1)
    %                   or [left right] for different left and right margins 
    %
    %  out:  ha     array of handles of the axes objects
    %                   starting from upper left corner, going row-wise as in
    %                   going row-wise as in
    %
    %  Example: ha = tight_subplot(3,2,[.01 .03],[.1 .01],[.01 .01])
    %           for ii = 1:6; axes(ha(ii)); plot(randn(10,ii)); end
    %           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')

    % Pekka Kumpulainen 20.6.2010   @tut.fi
    % Tampere University of Technology / Automation Science and Engineering

    figure(figHandle);

    if nargin<3; gap = .02; end
    if nargin<4 || isempty(marg_h); marg_h = .05; end
    if nargin<5; marg_w = .05; end

    if numel(gap)==1; 
        gap = [gap gap];
    end
    if numel(marg_w)==1; 
        marg_w = [marg_w marg_w];
    end
    if numel(marg_h)==1; 
        marg_h = [marg_h marg_h];
    end

    axh = (1-sum(marg_h)-(Nh-1)*gap(1))/Nh; 
    axw = (1-sum(marg_w)-(Nw-1)*gap(2))/Nw;

    py = 1-marg_h(2)-axh; 

    ha = zeros(Nh*Nw,1);
    ii = 0;
    for ih = 1:Nh
        px = marg_w(1);

        for ix = 1:Nw
            ii = ii+1;
            ha(ii) = axes( ...
                        'Units','normalized', ...
                        'Position',[px py axw axh], ...
                        'XTickLabel','', ...
                        'YTickLabel','' ...
                    );
            px = px+axw+gap(2);
        end
        py = py-axh-gap(1);
    end
    end
    %=========================================================================%
    function [ info ] = calcintinfo(imgArr)

    % CALCINTINFO - calculates the information score for a barcode. See the
    %	WPAlign paper for details.
    %
    % Inputs: imgArr (the barcode)
    %
    % Outputs: info (the information score)
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): CMN_HelperFunctions.findsignalregion, 
    %	built-in functions
    %
    % By: Charleston Noble
    %

    % Average
    curve = mean(imgArr);

    noiseEstCurve = zeros(size(imgArr));

    for i = 1:size(imgArr,2)
        noiseEstCurve(:,i) = imgArr(:,i) - mean(imgArr(:,i));    
    end

    sigma_noise = log(std(noiseEstCurve(:)));% +1;

    % Find the region
    [ x1, x2 ] = CMN_HelperFunctions.findsignalregion(curve);

    % Smooth the curve
    smoothWindow = 5;
    curve = smooth(curve, smoothWindow);

    % Get the extrema
    [ extvals ] = CMN_HelperFunctions.robustextrema(curve(x1:x2), sigma_noise);

    % Get the diffs of the extrema
    diffs = abs(diff(extvals));

    % Calculate information
    probVals = normpdf(log(diffs).^2, 0, log(sigma_noise));
    % probVals = (1/(sqrt(2*pi*sigma_noise)))*exp(-log(diffs).^2/(2*sigma_noise));
    probVals(probVals == 0) = [];
    info = sum(-log(probVals));

    end
    %=========================================================================%
    function [ info ] = calcinfotheory(curve)

    % CALCINFOTHEORY - an example on how to calculate information score for a
    %                  theoretical barcode.
    %
    % Inputs: curve (the barcode)
    %
    % Outputs: info (the information score)
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions,
    %                                             CMN_HelperFunctions.robustextrema
    %
    % By: Erik Lagerstedt
    %


    % Smooth the curve
    smoothWindow = 5;
    curve = smooth(curve, smoothWindow);
    % TC=0.075;

    % Get the extrema
    [ extvals ] = CMN_HelperFunctions.robustextrema(curve, 0.1);
    % [ extvals ] = CMN_HelperFunctions.robustextrema(curve, TC);

    % Get the diffs of the extrema
    diffs = abs(diff(extvals));

    % Calculate information
    probVals = normpdf(log(diffs).^2, 0, 1);
    % probVals = (1/(sqrt(pi*TC)))*exp(-log(diffs).^2/(TC));
    probVals(probVals == 0) = [];
    info = sum(-log(probVals));

    end
    %=========================================================================%
    function [ pValueAdam,delta_p ] = cb_pvalue(thyCurve,expCurve,N)

    % CB_PVALUE - A way to figure out the probablity that the theoretical
    %             barcode fits the barcode from experiment by chance.
    % 
    % Inputs:
    %     thyCurve      - barcode from sequence
    %     expCurve      - barcode from experiment
    %     N             - number of base pairs in the genetical sequence
    %
    % Outputs:
    %     pValueAdam    - p-value for the fit
    %     delta_p       - error in the p-value
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions, CMN_HelperFunctions.ccorr_lincirc,
    %                                             CMN_HelperFunctions.find_effective_correlationlength,
    %                                             CMN_CompetitiveBindingTheory.cb_stretchtheory_to_nm_resolution
    %                                             CMN_HelperFunctions.reisnerrescaling
    %
    % By: Erik Lagerstedt
    %

        stretchFactor=1/3.5;

        CVec=CMN_HelperFunctions.ccorr_lincirc(thyCurve,expCurve);
        CHat=max(CVec);

        theoryTest=load('rand5050.mat');
        nbrRandSeq=1000;
        corrInRand=zeros(nbrRandSeq,(max(length(thyCurve),length(expCurve))));
        for iT1=1:nbrRandSeq
            randCurve=theoryTest.theoryTest((iT1-1)*N+1:(iT1)*N,1);
            randCurve=CMN_CompetitiveBindingTheory.cb_stretchtheory_to_nm_resolution(randCurve, stretchFactor);
            randCurve=smooth(randCurve(1:160:end),5);
            randCurve=CMN_HelperFunctions.reisnerrescaling(randCurve');
            [corrInRand(iT1,:),~]=(CMN_HelperFunctions.ccorr_lincirc(randCurve,expCurve));
    %        figure
    %        imagesc(randCurve), colormap('gray');
    %        set(gca,'ytick',[])
        end

        cMean=mean2(corrInRand);
        cStd=std2(corrInRand);
        avCHat=mean(max(corrInRand,[],2));
        stdEOMCHat=std(max(corrInRand,[],2))/sqrt(length(max(corrInRand,[],2)));

        %Calculates the probability that the maximum cross-correlation is as high or higher than C
        KEff=CMN_HelperFunctions.find_effective_correlationlength(avCHat,cMean,cStd);
        pValueAdam = 1 - (0.5 * (1+erf((CHat-cMean)/(sqrt(2)*cStd))))^KEff;

        KEffmin=CMN_HelperFunctions.find_effective_correlationlength((avCHat-stdEOMCHat),cMean,cStd);
        pValueAdamMin = 1 - (0.5 * (1+erf((CHat-cMean)/(sqrt(2)*cStd))))^KEffmin;
        KEffmax=CMN_HelperFunctions.find_effective_correlationlength((avCHat+stdEOMCHat),cMean,cStd);
        pValueAdamMax = 1 - (0.5 * (1+erf((CHat-cMean)/(sqrt(2)*cStd))))^KEffmax;

    % %%%%%%%%%%%%%%%%%%%%%%%%%%%    
    % %     [f,x]=hist(corrInRand(:),30);%# create histogram from a normal distribution.
    % %     g=1/sqrt(2*pi*(cStd^2))*exp(-((x-cMean).^2)/(2*(cStd^2)));%# pdf of the normal distribution
    % %     figure(2)
    % %     %trapz(x,f)
    % %     f=f/trapz(x,f);
    % %     bar(x,f);hold on
    % %     %bar(x,f/trapz(x,f));hold on
    % %     plot(x,g,'r');%hold off
    % %     %trapz(x,f)
    % %     
    % %     [f,x]=hist(max(corrInRand,[],2),30);%# create histogram from a normal distribution.
    % %     %g=1/sqrt(2*pi*(cStd^2))*exp(-((x-cMean).^2)/(2*(cStd^2)));
    % %     f=f/trapz(x,f);
    % %     bar(x,f,'g');%hold off
    %     
    %     figure
    % 
    %     %nmbBi=31;
    %     [j,h]=hist(corrInRand(:),31);
    %     j=j/trapz(h,j);%((h(2)-h(1))*sum(j));
    %     [I,H]=hist(max(corrInRand,[],2),15);
    %     I=I/trapz(H,I);%((H(2)-H(1))*sum(I));
    %     bar(h,j,'b')
    %     hold on;
    %     bar(H,I,'g')
    % 
    % %     cl=4.88;
    % %     r=1/cl;
    % %     K=159;
    % %     Keff=K*r;
    % %     randStd=std(cirth);
    % %     randMean=mean(cirth);
    % %     x=-1:1/150:1;
    % %     delta_x=x(2)-x(1);
    %     probOfC = (1/sqrt(2*pi*(cStd^2)))*exp(-((h-cMean).^2)/(2*(cStd^2)));%/(delta_x); 
    %     plot(h,probOfC,'LineWidth',2,'Color','y')
    %     distr = KEff * probOfC  .* (0.5 .* (1 + erf((h - cMean) ./ (sqrt(2 * (cStd^2))  )))).^(KEff-1);
    %     plot(h,distr,'LineWidth',2,'Color','r')
    %     %line([CHat;CHat],[0;5],'LineWidth',2,'Color','m')
    %     hold off
    %     xlabel('Cross-correlation');
    %     %ylabel('Count');
    %     set(gca,'ytick',[]);
    %     
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        delta_p=(pValueAdamMax-pValueAdamMin)/2;
    % function [ retCon ] = CMN_HelperFunctions.ccorr_lincirc(templ,tvec)
    % 
    % if length(tvec)>length(templ)
    %     temp=tvec;
    %     tvec=templ;
    %     templ=temp;
    % end
    % 
    % res=zeros(1,length(templ));
    % convForward=zeros(1,length(templ));
    % convBackward=zeros(1,length(templ));
    % 
    % for iT2=1:length(templ)
    %     for j=1:length(tvec)
    %         res(j)=templ(1+mod(iT2+j,length(templ)));
    %     end
    %     for j=1:length(tvec)
    %         convForward(1,iT2)=convForward(1,iT2)+tvec(1,j)*res(1,j);
    %         convBackward(1,iT2)=convBackward(1,iT2)+tvec(1,j)*res(1,length(tvec)-j+1);
    %     end
    % end
    % convForward=convForward/(norm(templ)*norm(tvec));
    % convBackward=convBackward/(norm(templ)*norm(tvec));
    % 
    % if max(convBackward)>max(convForward)
    %     retCon=convBackward;
    % else
    %     retCon=convForward;
    % end
    % end
    end
    %=========================================================================%
    function [Kr] = find_effective_correlationlength(largestC,meanC,standardD)

    % FIND_EFFECTIVE_CORRELATIONLENGTH - find the correlation length in a
    %                                    function (commonly cross correlation
    %                                    values for two barcodes)
    %
    % Inputs:
    %     largestC    - largest value in the function
    %     meanC       - mean value of the function
    %     standardD   - standard deviation of values in the function
    %
    % Outputs:
    %     Kr          - correlation length
    % 
    % Effect on Mainstruct: none
    %
    % Dependencies (matlab-functions, MAT-files): built-in functions
    %
    % By: Erik Lagerstedt
    %


    Tempstruct = struct();

    
        Tempstruct.intRes=load('KeffTable.mat');
        %Tempstruct.intRes=intR;
    %     iMax=100000;
    %     intR=zeros(1,iMax);
    %     z=-10:0.01:10;
    %     for i=1:iMax
    %        intR(i)= (i/sqrt(pi))*trapz(z,z.*exp(-(z.^2)).*(((1+erf(z))/2).^(i-1)));
    %     end
    %     Tempstruct.intRes=intR;
    %end
    Kr=0;
    %err=0;
    errL=zeros(10,1);

    a=1;
    b=1000;%Not sure what value of b is good

    %infinity=1e4;
    %allowedError=0.00005;%-infinity;

    if f(a)*f(b)>0
        disp('Bad choise of b')
    else
       Kr = (a + b)/2;
       %err = abs(f(Kr));
       errL(1)=abs(f(Kr));
       while std(errL)>0
            if f(a)*f(Kr)<0 
               b = Kr;
            else
               a = Kr;
            end
            Kr = (a + b)/2;
            errL=circshift(errL,1);
            errL(1)=abs(f(Kr));
            %err = abs(f(Kr));
            %disp(errL(1));
            %disp(Kr);
       end
    end
    %Kr;
    %err;
    function [res] = f(KrT)
        %KeffInt=z.*exp(-(z.^2)).*(((1+erf(z))/2).^(KrT-1));
        %z=-infinity:infinity;
        res=-largestC+meanC+(sqrt(2*standardD^2)*((KrT-floor(KrT))*...
            Tempstruct.intRes.intR(floor(KrT))+(1-(KrT-floor(KrT)))*...
            Tempstruct.intRes.intR(ceil(KrT))));
    end
        %z=-10:0.01:10;
        %fRes=(KrT/sqrt(pi))*trapz(z,z.*exp(-(z.^2)).*(((1+erf(z))/2).^(KrT-1)));
    end
    %=========================================================================%
    function [ rtnThy ] = stretch_theory_to_pixel_resolution( thyCurve, nmPerPixel )
        % Takes a theory curve with nm resolution and converts it into pixel
        % resolution.
        pixelPerNm = 1/nmPerPixel;
        
        %---Changing resolution to pixel instead of nm---
        r = ones(length(thyCurve),1);             % r is in units of base-pairs
        r = r * pixelPerNm;                   % Stretch the whole thing.
        
        % Now take the cumulative sum to get an actual x-coordinate for each
        % base-pair.
        stretchXVals = cumsum(r) - r(1) + 1;
        desiredXVals = 1:round(sum(r));
        thyCurve = interp1(stretchXVals, thyCurve, desiredXVals);
        thyCurve(isnan(thyCurve)) = [];
        
        rtnThy = thyCurve;
    end
    %=========================================================================%
    function [ outputCC1, outputCC2, flipped ] = ccorr_linlin_both(B1, B2)
        
        % CCORR_linlin_both - Cross-correlation between two linear barcodes
        % with both C.C. output vectors.
        %
        %  Calculates the cross-correlation values
        %  between two barcodes. One barcode is slided across the other one
        %  and a set of cross-correlation values are obtain. LOCAL Resiner-rescaling
        %  is used, i.e. the parts of the barcodes being compared
        %  are rescaled to have mean = 0 and standard deviation = 1.
        %  The relative orientation of the barcodes
        %  is also identified (by maximizing the cross-correlation)
        %
        % Inputs:
        % B1 = barcode 1
        % B2 = barcode 2
        %
        % Outputs:
        % outputCC = list of cross-correlation for correct flip
        % flipped = is equal to '0' is no flip, otherwise = '1'.
        % shift = shift of B1 w r t B2
        %  [Note that shift can be negative: -(n-1)<=shift<=n-1 ]
        %
        % Effect on Mainstruct: none
        %
        % Dependencies (matlab-functions, MAT-files): none
        %
        % By: Tobias Ambjornsson, Christoffer Pichler, Erik Lagerstedt
        %
        
        
        %  Store longest input-barcode in the vector longVec
        %  and the shortest input-barcode in vector shortVec
        %  Reisner-rescale the shortest barcode
        %  (the longer one is rescaled while calculating
        %   the cross-correlation values)
        l_B1=length(B1);
        l_B2=length(B2);
        shortLength=min(l_B2,l_B1);
        longLength=max(l_B2,l_B1);
        if longLength==l_B1
            longVec=B1;
            shortVec=B2;
        else
            longVec=B2;
            shortVec=B1;
        end;
        clear B1; clear B2;
        %  Reisner-rescale the shortest barcode
        shortVec=(shortVec-mean(shortVec))/std(shortVec);
        shortVecFlip = fliplr(shortVec);
        
        
        %  Calculate forward and backward cross-correlations
        %  by "sliding" one barcode across the other
        ccForward=zeros(1,longLength-shortLength+1);
        ccBackward=zeros(1,longLength-shortLength+1);
        
        
        for i=1:longLength-shortLength+1
            %  (the cut out part is of the same length as the shortest barcode;
            longVec_perm_cut = longVec(i:shortLength+i-1);
            
            %  Reisner-rescale the permutated and cut-out barcode
            longVec_perm_cut=(longVec_perm_cut-mean(longVec_perm_cut))/std(longVec_perm_cut);
            
            %  calculate forward and backward cross-correlation values
            ccForward(i) = sum(shortVec.*longVec_perm_cut);
            ccBackward(i) = sum(shortVecFlip.*longVec_perm_cut);
        end
        
        % Normalize with the length of the shortest - 1
        ccForward=ccForward/(shortLength-1);
        ccBackward=ccBackward/(shortLength-1);
        maxCCForward=max(ccForward);
        maxCCBackward=max(ccBackward);
        
        if maxCCBackward>maxCCForward
            outputCC1=ccBackward;
            outputCC2=ccForward;
            flipped=true;
        else
            outputCC1=ccForward;
            outputCC2=ccBackward;
            flipped=false;
        end
        
    end
    %=========================================================================%
    function [ consensusBarcode, consensusBitmask ] = read_consensus_file(fileName)
        % Reads a consensus barcode from a file saved from automated consensus.
        if CMN_Utils.string_ends_with(fileName, '.mat')
            dataStruct = load(fileName, 'clusterConsensusData');
            if not(isfield(dataStruct, 'clusterConsensusData')) || not(isfield(dataStruct.clusterConsensusData, 'barcode')) || not(isfield(dataStruct.clusterConsensusData, 'bitmask'))
                error('Unrecognized consensus file format');
            end
            consensusBarcode = dataStruct.clusterConsensusData.barcode;
            consensusBitmask = dataStruct.clusterConsensusData.bitmask;
            return;
        end
        
        
        fid = fopen(fileName,'rt');
        tline = fgets(fid);
        while length(tline) < 11 || (~strcmp(tline(1:11),'# consensus') && ~strcmp(tline(1:10),'#   theory'))
            tline = fgets(fid);
        end
        if strcmp(tline(1:11),'# consensus')
            secondCol = false;
        else
            secondCol = true;
        end
        p = 1;
        numb = char(zeros(1,6));
        consensusBarcode = zeros(1,500);
        tline = fgets(fid);
        while ischar(tline)
            j = 1;
            while strcmp(tline(j),' ')
                j = j + 1;
            end
            k = 1;
            if secondCol
                while ~strcmp(tline(j),' ')
                    j = j + 1;
                end
                while strcmp(tline(j),' ')
                    j = j + 1;
                end
            end
            while ~strcmp(tline(j),' ')
                numb(k) = tline(j);
                k = k + 1;
                j = j + 1;
            end
            consensusBarcode(p) = str2double(numb);
            p = p + 1;
            tline = fgets(fid);
        end
        consensusBarcode(consensusBarcode == 0) = [];
        fclose(fid);
    end
    %=========================================================================%
    function pValue = p_calc_gumbel(bestCC,exParameters)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        kappa = exParameters(1);
        beta = exParameters(2);
        
        pValue = exp(-exp(-(-bestCC-kappa)/beta));
    end
    
    function pValue = p_val_exact(cc, evdPar)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        C1 = 1./sqrt(pi) *gamma((evdPar(1)-1)/2)/gamma((evdPar(1)-2)/2);
        sum = C1*(cc.*hypergeom([1/2,-(evdPar(1)-4)/2 ],3/2,cc.^2))+1/2;
        pValue =1 - sum^(evdPar(2));
    end
    
    function pValue = p_val_trunc_gumbel(bestCC,exParameters)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        kappa = exParameters(1);
        beta = exParameters(2);
        pValue = evcdf(bestCC, kappa, beta);
    end
    
    function pValue = p_val_gumbel(bestCC,exParameters)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        kappa = exParameters(1);
        beta = exParameters(2);
        pValue = evcdf(-bestCC, kappa, beta);
    end
    
    function pValue = prob_trunc_gumbel(x,exParameters)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        kappa = exParameters(1);
        beta = exParameters(2);
        pValue =(1/beta)*exp(- ((x-kappa)/beta+ exp(-((x-kappa)/beta) )));
        end
    
    function pValue = prob_gumbel(x,exParameters)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        kappa = exParameters(1);
        beta = exParameters(2);
        pValue =(1/beta)*exp(- ((x-kappa)/beta+ exp(-((x-kappa)/beta) )));
    end
    
    
    function pValue = p_calc_GEV(bestCC,exParameters)
        % Using Gumbeldistribution as a null model in order to calculate
        % p-values
        k = exParameters(1);
        sigma = exParameters(2);
        mu = exParameters(3);
        
        pd = makedist('Generalized Extreme Value',k,sigma,mu);
        pValue = 1-cdf(pd,bestCC);
        %pValue = 1 - exp(-exp(-(bestCC-kappa)/beta));
    end
    
    %=========================================================================%
    function [stretchCurve] = stretch_curve( curve , desiredLength )
        % Takes a curve and stretches it to the desired length of data points.
        
        if ~isempty(curve)
            
            desiredLength = round(desiredLength);
            
            stretch = (length(curve)-1)/(desiredLength-1);
            
            % Calculate the new coordinates
            r = stretch*ones(1,desiredLength);
            r(1) = 1;
            r = cumsum(r);
            r(end) = length(curve);
            
            stretchCurve = zeros(1,desiredLength);
            
            % Startpoint
            stretchCurve(1) = curve(1);
            % Endpoint
            stretchCurve(end) = curve(end);
            
            forIndex = 2;
            for i = r(2:end-1)
                stretchCurve(forIndex) = (curve(ceil(i))-curve(floor(i)))*(i-floor(i))+curve(floor(i));
                
                forIndex = forIndex + 1;
            end
            
        else
            stretchCurve = [];
        end
        
    end
    %=========================================================================%
    function [barcodes,names] = read_barcodes_manually(title)
        % Lets the user select several consensus files to use as the reference
        % barcodes for extreme value distribution generation.
        
        [barcodeFile,barcodePath] = uigetfile({'*.mat;';'*.txt;'},title,'MultiSelect','on');
        
        
        if ischar(barcodePath)
            if iscell(barcodeFile)
                barcodeCellPath = cell(1,length(barcodeFile));
                names = cell(1,length(barcodeFile));
                for iPath = 1:length(barcodeFile)
                    barcodeCellPath{iPath} = [barcodePath barcodeFile{iPath}];
                    names{iPath} = barcodeFile{iPath}(1:end-4);
                end
                clear barcodePath
            else
                barcodeCellPath = {[barcodePath barcodeFile]};
                names = {barcodeFile(1:end-4)};
                disp('Only one consensus was selected.')
            end
        else
            disp('No barcodes were selected.')
            barcodes = {};
            names = {};
            return
        end
        
        %---Load barcodes---
        barcodes = cell(1,length(barcodeFile));
        for iLoad = 1:length(barcodeCellPath)
            barcodes{iLoad} = CMN_HelperFunctions.read_consensus_file(barcodeCellPath{iLoad});
        end
    end
    %=========================================================================%
end


end
