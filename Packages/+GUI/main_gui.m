function main_gui(settings)
    % updated 21/09/16 by Albertas Dvirnas
    % http://se.mathworks.com/help/matlab/examples/creating-a-user-interface-with-tab-panels.html
    % Main GUI WINDOW

    fignum = figure('NumberTitle', 'off');
    set(fignum, 'MenuBar', 'none');

    fignum.Name = 'Contig assembly and assembly validation tool';
    figname = fignum.Name;

    panel1 = uipanel('Parent',fignum);


    panel2 = uipanel('Parent',panel1);

    set(panel1,'Position',[0 0 0.95 1]);

    set(panel2,'Position',[0 -1 1 2]);

    tgroup = uitabgroup('Parent', panel2);
    
    tabSet = uitab('Parent', tgroup, 'Title', 'Introduction');
    box1 = uicontrol('Parent', tabSet, 'Style', 'text','String', 'This is a tool for the contig assembly and related tasks ','Units','normalized','Position', [0.1 0.8 .2 .1]);

    tab0 = uitab('Parent', tgroup, 'Title', 'Settings');

    tab1 = uitab('Parent', tgroup, 'Title', 'Zero models');
    
    
    % include slider
    s = uicontrol('Style','Slider','Parent',fignum,'Units','normalized','Position',[0.95 0 0.05 1],'Value',1,'Callback',{@slider_callback1,panel2});
      
    % include boxes for all settings, so that we can change them
    fields = fieldnames(settings);
    for k=1:numel(fields)
        uiNames.(sprintf('%s', fields{k})) = uicontrol('Parent', tab0, 'Style', 'text','String', fields{k},'Units','normalized','Position',[.05 0.2+k*.05-0.02 .3 .05]);
        uiBoxes.(sprintf('%s', fields{k})) = uicontrol('Parent', tab0, 'Tag','a1','CallBack',@callb, 'Style', 'edit','Units','normalized','Position',[.4  0.2+k*.05 .15 .03], 'String',num2str(settings.(fields{k})),'UserData',struct('val', settings.(fields{k})));
    end
    
    button = uicontrol('Parent', tab0, 'Style','pushbutton',...
             'String','Update settings','Tag','button1','Units', 'normal', 'Position', [0.7 0.8 .2 .1],'Callback', @button_callback);

    uiZero = uicontrol('Parent', tab0, 'Style', 'text','String', 'Zero model','Units','normalized','Position',[0.7 0.6 .1 .05]);
    uiZeroBox = uicontrol('Parent', tab0, 'Tag','a1','CallBack',@callb, 'Style', 'edit','Units','normalized','Position',[0.8 0.6 .1 .05], 'String','1','UserData',struct('val', 0));
 
end

function slider_callback1(src,~,arg1)
    val = get(src,'Value');
    set(arg1,'Position',[0 -val 1 2])
end

function [] = callb(H, ~)
    a = str2num(get(H,'string'));
    data = struct('val', a);
    H.UserData = data;   
end

function button_callback(~,~)
    % dummy button, to make sure that all the struct values were updates
end

function zero_callback(~,~)
    [FileName,PathName] = uigetfile('*.m','Select the MATLAB code file');
end