function [ theoreticalBarcodes ] = generate_barcodes_for_database(theoreticalSeq, settings)
% 21/09/16 by Albertas Dvirnas

% based on theoreticalSeq and settings, a set of barcodes in base-pair
% resolution is generated

% theoreticalSeq could come from contig .fasta file, but it could as well
% come from a plasmid database, then we would need a command like this:
% [theoreticalSeq,nameSeq] = read_fasta('plasmid.1.1.genomic.fna');
% if barcodes are generated for the whole database, it is recommended for
% the user to save this for later use
%
    import ZEROMODEL.*;
     
    theoreticalBarcodes = cell(1,size(theoreticalSeq,2)); % allocate space

    nonZerKer = settings.nonzeroKernelLen;
    
    if iscell(theoreticalSeq) % might be that there is only one sequence, hence no cell
        % parfor loop to use multiple processors
        parfor i=1:size(theoreticalSeq,2)
            if size(theoreticalSeq{i},2) > nonZerKer
                theoreticalBarcodes{i} = generate_barcode(theoreticalSeq{i},settings);
            end
        end
    else  % we could also update it if theoreticalSeq have a lot of sequences of the same length, therefore not a cell either
        if size(theoreticalSeq{i},2) > nonZerKer
                theoreticalBarcodes{1} = generate_barcode(theoreticalSeq,settings);
        end
    end
end

