function [ markovDNA ] = generate_markov_barcodes(averageProb, nSeq, lenSeq)
% 21/09/16

    ss = averageProb^3;
    initialProb = ss(1,:);

    tr0 = cumsum(initialProb);
    trc =  cumsum(averageProb,2);
    
    tic
    markovDNA = cell(1,nSeq);
    for k = 1:nSeq
        randvals = rand(1,lenSeq);

        randSeq = zeros(1,lenSeq);
        randSeq(1) = find(randvals(1) < tr0,1);            

        for i=2:lenSeq
            randSeq(i) = find(randvals(i) < trc(randSeq(i-1),:),1);
        end

        outPut = int2nt(randSeq);
        markovDNA{k} = outPut;
    end
    toc

end

