function [ averageProb ] = zero_model_markov( theoreticalSeq )
 % 21/09/16 by Albertas Dvirnas
 % this method calculates a zero model based on Markov property

 % http://stats.stackexchange.com/questions/36099/estimating-markov-transition-probabilities-from-sequence-data
    
    averageProb = zeros(4,4);
    
    numSeq = size(theoreticalSeq,2);

    for seqInd=1:numSeq
        %seqInd
        x = theoreticalSeq{seqInd};
       % x = firstSeq{seqInd};

        alphabet = ['A', 'C', 'G', 'T'];

        prob = zeros(4,4);

        [~,a] = ismember(x(1),alphabet);

        for i=1:size(x,2)-1
           [~,b] = ismember(x(i+1),alphabet);
           if b~=0 && a~= 0
               prob(a, b) = prob(a, b) + 1;
           else
               if a == 0
                   a = b;
               end
           end
           a = b;    
        end

        for i =1:4
            prob(i, :) = prob(i, :)/sum(prob(i, :));
        end

        averageProb = averageProb + prob;    
    end

    averageProb = averageProb/numSeq;

end

