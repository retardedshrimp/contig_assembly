function [ randomSequences ] = generate_autocorr_rand_seq_im(seqLen,seqNum,method, autoCorr,alpha,nn )
    % 26/10/16
    % Try to do some improvements since it still doesnt work as it should!
    %
    %
    % generates a set of stationary gaussian processes following a given
    % autocorrelation function
    % based on "Fast simulation of stationary gaussian processes", pg 1091
    
    % example ->
    % seqLen = 40 %- the length of the output sequence
    % seqNum = 100 %- 2*seqNum is the number of sequences outputed
    % method = 'power' %- what is the autocorrelation function like?
    % alpha = 2 % autocorrelation function parameter
    
    % [ randomSequences ] = generate_autocorr_rand_seq(seqLen,seqNum,method, barcode,alpha )
    
    %
    
    if nargin < 6 % if no method was selected
       nn = 6;
    end
    
    if nargin < 3 % if no method was selected
        method = 'power';
    end
    
    if nargin < 5 % if no parameter selected
        alpha = 5; % decay parameter
    end
    
    switch method
		case 'random'
			randomSequences = cell(1,seqNum);
			for i=1:seqNum
				randomSequences{i} = normrnd(0,1,1,seqLen);
			end
			return;
        case 'power'  % exponential decay
            
            t = (0:seqLen-1); % time steps
            
            autoCorr = 1./(t.^(alpha)+1); % autocorrelation function
            
         case 'exp'  % exponential decay
            t = (0:seqLen-1); % time steps
           
            autoCorr = alpha.^(-t);
            
        case 'gaussian'
            % in this case we are guaranteed nonnegative definite minimal
            % embedding for l>=1/sqrt(pi) and m>=sqrt(pi) l^2
            


            l = alpha; 
	   %m = 10*round(sqrt(pi)*l^2);
            
            m = seqLen;
            
            t = 0:m;

            autoCorr = exp(-(t./l).^2 );

        case 'gaussian2'
            % in this case we are guaranteed nonnegative definite minimal
            % embedding for l>=1/sqrt(pi) and m>=sqrt(pi) l^2
            l = alpha; 
            
            %m = max(seqLen,ceil(sqrt(pi)*l^2));
            
            t=0:seqLen-1;
			gg = exp(-(t./l).^2);
			ipt = -seqLen:-1;
			ff =  exp(-(ipt./l).^2);
			autoCorr = [gg ff];
			
			sHat = real(fft(autoCorr)); % fft of C1, always real

			
			
			if any(real(sHat) <  -0.000000001)     % eigevalues have to be nonnegative
				disp('Matrix is not positive definite, bad choice of embedding') % this is yet to be implemented
				randomSequences = [];
				return;
			end
			
			M = 2*seqLen;

			sqrtsHat = sqrt(sHat/size(autoCorr,2)); % square root
			
			randomSequences = cell(1,2*seqNum);
			for i=1:seqNum

				U = randn(1,M) + sqrt(-1)*randn(1,M);
		
				vecE = U.*sqrtsHat;

				vecEe = fft(vecE);

				vec1 = real(vecEe);
				vec2 = imag(vecEe);

				%randomSequences{2*i-1} = (vec1(1:M/2+1)-mean(vec1(1:M/2+1)))/std(vec1(1:M/2+1));
				randomSequences{2*i-1} = (vec1-mean(vec1))/std(vec1);
				%randomSequences{2*i} = (vec2(1:M/2+1)-mean(vec2(1:M/2+1)))/std(vec2(1:M/2+1));
				randomSequences{2*i} = (vec2-mean(vec2))/std(vec2);
			end
			return;
	

        case 'sample' % sample decay
        	if nargin < 4
            	autoCorr = [1 zeros(1,seqLen-1)]; % default is no correlation
            else
				autoCorr = autocorr(autoCorr,nn);
                %autoCorr = autocorr(barcode(1:300), 9);
                t = 0:size(autoCorr,2)-1;
                
           
                f = @(y) sum((autoCorr-exp(-t.^2./y.^2)).*exp(-t.^2./y.^2).*t.^2/y.^3);
                
                l = fsolve(f, 5.1, optimoptions('fsolve', 'Display','off'));
                l
                t = (0:seqLen-1); % time steps
                %size(t)
                autoCorr = exp(-(t./l).^2);
                %autoCorr
            end
    
            
        case 'circular'
			[autoC,~] = COMPARISON.cross_correlation_coefficients_fft(autoCorr,autoCorr);
			%nn = 10; % fine tune this parameter!
			autoCorr = [autoC(end-nn+1:end) autoC(1:nn)];
			t = -nn+1:1:nn;
			
			f = @(y) sum((autoCorr-exp(-t.^2./y.^2)).*exp(-t.^2./y.^2).*t.^2/y.^3);
                
	        l = fsolve(f,5.1, optimoptions('fsolve', 'Display','off'));
			l
			t=0:seqLen-1;
			gg = exp(-(t./l).^2);
			ipt = -seqLen:-1;
			ff =  exp(-(ipt./l).^2);
			autoCorr = [gg ff];
	       	sHat = real(fft(autoCorr)); % fft of C1, always real

			
			% If we want to plot
			%figure, plot(autoCorr)
			%hold on
			%plot(autoC)
			
			if any(real(sHat) <  -0.000000001)     % eigevalues have to be nonnegative
				disp('Matrix is not positive definite, bad choice of embedding') % this is yet to be implemented
				randomSequences = [];
				return;
			end
			
			M = 2*seqLen;

			sqrtsHat = sqrt(sHat/size(autoCorr,2)); % square root
			
			randomSequences = cell(1,2*seqNum);
			for i=1:seqNum

				U = randn(1,M) + sqrt(-1)*randn(1,M);
		
				vecE = U.*sqrtsHat;

				vecEe = fft(vecE);

				vec1 = real(vecEe);
				vec2 = imag(vecEe);

				%randomSequences{2*i-1} = (vec1(1:M/2+1)-mean(vec1(1:M/2+1)))/std(vec1(1:M/2+1));
				randomSequences{2*i-1} = (vec1-mean(vec1))/std(vec1);
				%randomSequences{2*i} = (vec2(1:M/2+1)-mean(vec2(1:M/2+1)))/std(vec2(1:M/2+1));
				randomSequences{2*i} = (vec2-mean(vec2))/std(vec2);
			end
			return;
    
        case 'predefined'
            randomSequences = [];
            return;
           
        otherwise
            randomSequences = [];
            warning('Unexpected choice of autocorr')
            return;
    end
    
    C1 = [autoCorr fliplr(autoCorr(2:end-1))]; % first row of the circulant matrix
    
    M = size(C1,2); % size of the circulant matrix
            

    sHat = real(fft(C1)); % fft of C1, always real
    
    if any(sHat <  -0.000000001)     % eigevalues have to be nonnegative
        disp('Matrix is not positive definite, bad choice of embedding') % this is yet to be implemented
        randomSequences = [];
        return;
    else
        sHat(sHat <  0) = 0;
    end
    
    
	
    sqrtsHat = sqrt(sHat/size(C1,2)); % square root
    
    %sHat/size(C1,2)
    %sqrtsHat*sqrtsHat
    
    randomSequences = cell(1,2*seqNum);
    for i=1:seqNum

        U = randn(1,M) + sqrt(-1)*randn(1,M);
		%U = randn(1,M);
		
        vecE = U.*sqrtsHat;

        vecEe = fft(vecE); 

        vec1 = real(vecEe);
        vec2 = imag(vecEe);

		%vecEe
		randomSequences{2*i-1} = (vec1(1:M/2+1)-mean(vec1(1:M/2+1)))/std(vec1(1:M/2+1));
		%randomSequences{2*i} = (vec2(1:M/2+1)-mean(vec2(1:M/2+1)))/std(vec2(1:M/2+1));

    end
    %autoC = autocorr(vec1(1:M/2+1),size(vec1(1:M/2+1),2)-1);

    %figure, plot(autoC)

end

