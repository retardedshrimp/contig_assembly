function [ randomSequences ] = generate_random_sequences(seqLen,seqNum,method, autoCorr,alpha,nn )
    % 27/10/16
    % generates a set of stationary gaussian processes following a given
    % autocorrelation function
    % based on "Fast simulation of stationary gaussian processes", pg 1091
    
    % example ->
    % seqLen = 40 %- the length of the output sequence
    % seqNum = 100 %- 2*seqNum is the number of sequences outputed
    % method = 'power' %- what is the autocorrelation function like?
    % alpha = 2 % autocorrelation function parameter
    
    % [ randomSequences ] = generate_autocorr_rand_seq(seqLen,seqNum,method, barcode,alpha )
    
    %
    
    if nargin < 3 % if no method was selected
        method = 'gaussian';
    end
    
    if nargin < 6 % if no method was selected
       nn = 6;
    end
    

    
    if nargin < 5 % if no parameter selected
        alpha = 6; % decay parameter
    end
    
    % include all possible random sequence models
    switch method
		case 'random'
            % in case completely random model is selected, just draw seqLen
            % numbers from normrnd seqNum times
			randomSequences = cell(1,seqNum);
			for i=1:seqNum
				randomSequences{i} = normrnd(0,1,1,seqLen);
			end
			return;
        case 'power'  % exponential decay
            
            t = (0:seqLen-1); % time steps
            
            autoCorr = 1./(t.^(alpha)+1); % autocorrelation function
            
         case 'exp'  % exponential decay
            t = (0:seqLen-1); % time steps
           
            autoCorr = alpha.^(-t);
            
            
		case 'phase'
			%load 'thyDatabase3.mat'; 
			
            randomSequences = cell(1,seqNum);

            interpData = NUMERICAL.interpolate_in_fourier_space(autoCorr, seqLen);
            
%             randomSequences= CBT.RandBarcodeGen.PhaseRandomization.generate_rand_barcodes_from_fft_zero_model(interpData,seqNum);
            
            halfL = floor(seqLen/2);
            PR1 = exp(2i*pi*rand(1,halfL));
            PR2 = fliplr(conj(PR1));
            
            if mod(seqLen,2)==0
                PR = [1 PR1(1:end-1) 1 PR2(2:end)];
            else
                 PR = [1 PR1 PR2];
            end
            randomBarcode = ifft(interpData.*PR);

            figure,plot(randomBarcode)
            randomSequences{1} = randomBarcode; 
            return;
            

		case 'notphase'
			%load 'thyDatabase3.mat'; 
			
            randomSequences = cell(1,seqNum);

            interpData = NUMERICAL.interpolate_in_fourier_space(autoCorr, seqLen)/seqLen;
            
            M = seqLen;
            
            U = randn(1,M) + sqrt(-1)*randn(1,M);
            vecE = U.*interpData;

            vecEe = ifft(vecE); 

            vec1 = real(vecEe);
            vec2 = imag(vecEe);
            randomSequences{1} =vec1;
            randomSequences{2} =vec2;
            
            %randomSequences= CBT.RandBarcodeGen.PhaseRandomization.generate_rand_barcodes_from_fft_zero_model(interpData,seqNum);
            
%             figure,plot(ifft(interpData.^2));
% 
%             halfL = floor(seqLen/2+1);
% 			intV = linspace(1,round(length(autoCorr)/2)-1,halfL-1);
% 			tempAut = autoCorr(2:round(length(autoCorr)/2));
% 			
% 			intVal = [interp1(tempAut, intV )];
% 			figure,plot(intVal)
% 			PR1 = exp(2i*pi*rand(1,halfL-1));
% 			
% 			if mod(seqLen,2)==0
%                 size(intVal)
%                 size(PR1)
% 				randomBarcode = [autoCorr(1) intVal.*PR1 fliplr(intVal(1:end-1).*conj(PR1(1:end-1)))];
% 			else
% 				randomBarcode = [autoCorr(1) intVal*PR1 fliplr(intVal(1:end-1).*conj(PR1))];
%             end
%             
%             randomSequences = ifft(randomBarcode);
            return;
            

		case 'phaserand'
			load 'thyDatabase.mat'; % need this file in working dir

			expkbpPerPixel =  0.5921;
			ZMkbpPerPixel = kbpPerPixel;
			ZMfft = meanFFT;
			
			refLen = seqLen;

			N = seqNum;
	
		    % Stretch the ZM FFT to have correct kbp/pixel
		    if expkbpPerPixel ~= ZMkbpPerPixel
				stretchZM = true;
				kbpRatio = ZMkbpPerPixel/expkbpPerPixel;
			else
		        stretchZM = false;
		        kbpRatio = 1;
		    end
		            
		    newLen = round(refLen/kbpRatio);
		    ZMfftEven = newLen/2 == floor(newLen/2);
			if round(length(ZMfft)*kbpRatio) ~= refLen
				% Interpolate
				if floor(length(ZMfft)/2) == length(ZMfft)/2
					intplfft = interp1(ZMfft(2:length(ZMfft)/2),linspace(1,length(ZMfft)/2-1,floor((newLen-1)/2)));
				else
					intplfft = interp1(ZMfft(2:length(ZMfft)/2+0.5),linspace(1,length(ZMfft)/2-0.5,floor((newLen-1)/2)));
				end
				
				% "Fold" negative frequences over positive
				if newLen/2 == floor(newLen/2)
					ZMfft = [ZMfft(1) intplfft ZMfft(floor(length(ZMfft)/2)+1) fliplr(intplfft)];
				else
					ZMfft = [ZMfft(1) intplfft fliplr(intplfft)];
				end
				norm = sqrt(sum(ZMfft.^2)/((newLen-1)*newLen));
				ZMfft = ZMfft/norm;
			end
			
			% Generate the random barcodes and calculate cross-correlation
			randomSequences = cell(1,N);
			
			for iNew = 1:N
				fi = rand(1,floor((length(ZMfft)-1)/2));
				PR1 = exp(2i*pi*fi);
				PR2 = fliplr(exp(2i*pi*(-fi)));
				if ZMfftEven
					fftRand = ZMfft.*[1 PR1 1 PR2];
				else
					fftRand = ZMfft.*[1 PR1 PR2];
				end
				randomBarcode = ifft(fftRand);
				
				% Stretch the random barcode to same length as exp to
				% obtain correct kbp/pixel
				if stretchZM
					randomBarcode = interp1(randomBarcode,linspace(1,length(ZMfft),refLen));
				end
				
				randomSequences{iNew} = randomBarcode;
				
		
			end
			return;
       case 'gaussian'
            % in this case we are guaranteed nonnegative definite minimal
            % embedding for l>=1/sqrt(pi) and m>=sqrt(pi) l^2
            
	   %m = 10*round(sqrt(pi)*l^2);
            
            m = seqLen;
            
            t = 0:seqLen-1;

            autoCorr = exp(-(t./alpha).^2 );
            
            method2 = 'linear';

        case 'gaussian2'
            % in this case we are guaranteed nonnegative definite minimal
            % embedding for l>=1/sqrt(pi) and m>=sqrt(pi) l^2
            
            %m = max(seqLen,ceil(sqrt(pi)*l^2));
            
            t=0:seqLen-1;
			gg = exp(-(t./alpha).^2);
			ipt = -seqLen:-1;
			ff =  exp(-(ipt./alpha).^2);
			autoCorr = [gg ff];
		
            method2 = 'circular';

        case 'sample' % sample decay
        	if nargin < 4
            	autoCorr = [1 zeros(1,seqLen-1)]; % default is no correlation
            else
				autoCorr = autocorr(autoCorr,nn);
                %autoCorr = autocorr(barcode(1:300), 9);
                t = 0:size(autoCorr,2)-1;
                
           
                f = @(y) sum((autoCorr-exp(-t.^2./y.^2)).*exp(-t.^2./y.^2).*t.^2/y.^3);
                
                l = fsolve(f, 5.1, optimoptions('fsolve', 'Display','off'));
                l
                t = (0:seqLen-1); % time steps
                %size(t)
                autoCorr = exp(-(t./l).^2);
                %autoCorr
                
                method2 = 'linear';

            end
    
            
        case 'circular'
			[autoC,~] = COMPARISON.cross_correlation_coefficients_fft(autoCorr,autoCorr);
			%nn = 10; % fine tune this parameter!
			autoCorr = [autoC(end-nn+1:end) autoC(1:nn)];
			t = -nn+1:1:nn;
			
			f = @(y) sum((autoCorr-exp(-t.^2./y.^2)).*exp(-t.^2./y.^2).*t.^2/y.^3);
                
	        l = fsolve(f,5.1, optimoptions('fsolve', 'Display','off'));
			l
			t=0:seqLen-1;
			gg = exp(-(t./l).^2);
			ipt = -seqLen:-1;
			ff =  exp(-(ipt./l).^2);
			autoCorr = [gg ff];
            
            method2 = 'circular';

           
            
        case 'predefined'
            randomSequences = [];
            return;
           
        otherwise
            randomSequences = [];
            warning('Unexpected choice of autocorr')
            return;
    end
    
    
    sHatSqrt = ZEROMODEL.compute_circulant_matrix( autoCorr, method2 );
  
    randomSequences = ZEROMODEL.compute_circulant_matrix_barcodes( sHatSqrt, seqLen,seqNum,  method2 );
    %  randomSequences = ZEROMODEL.compute_circulant_matrix_barcodes_2( sHatSqrt, seqLen,seqNum,  method2 );

    %autoC = autocorr(vec1(1:M/2+1),size(vec1(1:M/2+1),2)-1);

    %figure, plot(autoC)

end

