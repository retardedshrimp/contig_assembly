function [ randomSequences ] = rand_seq_with_same_autocorr(refBar,seqLen,seqNum,method )
    % 17/10/16
    % this function generates a set of rand sequences, with the same
    % autocorr function as an input sequence
    % input refBar, seqLen and seqNum
    
    
    if nargin < 4
        method = 'sample';
    end
    
    
    switch method
        case 'exponential'  % exponential decay
            t = (0:seqLen-1)'; % time ZEsteps
            m = 2*(seqLen-1);

            alpha = 2;
            %c_fun = alpha.^t;
            corPoly = exp(-0.1*t);

        case 'sample' % sample decay
              
            x=1:seqLen;

            m = 2*(seqLen-1);

            t = (0:seqLen-1)'; % time ZEsteps

            [autoCor] = autocorr(refBar, size(refBar,2)-1);
            corPolyCoefs = polyfit(x,autoCor,5); %20 is rather arbitrary, should estimate the number of roots
            corPolyVal = polyval(corPolyCoefs,x);

            corPoly = transpose(corPolyVal(1:seqLen));
            
        case 'predefined'
             autoCor = [1.0000    0.8641    0.6622    0.4341    0.2103    0.0100];

        otherwise
            randomSequences = [];
            warning('Unexpected choice of autocorr')
    end
    
      % First row of the circulant matrix C, Eq. (2.2) (p. 412)
    C1 = [corPoly; corPoly(seqLen-1:-1:2)];

    % Eigenvalues lambda of C1 via FFT, Eq. (2.4) (p. 413)
    lambda = real(fft(C1));


%    Check that eigenvalues are non-negative
    if any(lambda < 0)       
        disp('Try new lengths n and m')
    end

    %randomSequences = zeros(seqNum,seqLen);
    randomSequences = cell(1,seqNum);

    hist = 1000; % number of realizations = 2*hist
    
    for i=1:seqNum
        pos1 = zeros(seqLen,1); % vector to hold mean position
        pos2 = zeros(seqLen,1); % vector to hold mean squared position
        %cov_fun = zeros(n,1); % vector to hold simulated covariance function
        for dum1 = 1:hist    
            % Steps S1, S2 and S3 (p. 413)
            % W_j = sqrt(lambda)*Q*Z; Q*Z = randn(m,1)+irandn(m,1) Eq. (3.21) (p. 417)
            U = randn(m,1) + sqrt(-1)*randn(m,1);
            x = ifft(sqrt(lambda).*U)*sqrt(m); %sqrt(1/m) in Eq. (2.5) ???
                                               %Because of MATLAB def of FFT ???
            out1 = real(x(1:seqLen));
            out2 = imag(x(1:seqLen));

            % compute covariance function of simulated process
            %cov_fun = cov_fun + out1(1)*out1 + out2(1)*out2;

            % compute first and second moment
            pos1 = pos1 + out1 + out2; %should be close to zero
            pos2 = pos2 + out1.^2 + out2.^2; %should be close to c_fun(t=0)
        end
        %normalization
        %cov_fun = cov_fun/(2*hist); 
        pos1 = pos1/(2*hist);
        pos2 = pos2/(2*hist);

        seq =  pos2-pos1.^2;
        seq = (seq-mean(seq))/std(seq);
        randomSequences{i} = transpose(seq);
    end
  
    

end

