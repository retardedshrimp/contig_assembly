function [ randomSequences ] = compute_circulant_matrix_barcodes( sHatSqrt, seqLen,seqNum,  method )
    % 27/10/16 
    % Implements circular embedding method from Dietrich and Newsam
    
    % corFun is the correlation function.
    % method linear or circular, depending on what outcome we want
    
    if nargin < 4 % if no parameter selected
        method = 'linear';
    end
    
    M = size(sHatSqrt,2);
    randomSequences = cell(1,2*seqNum);

    for i=1:seqNum
        U = randn(1,M) + sqrt(-1)*randn(1,M);
        vecE = U.*sHatSqrt;

        vecEe = fft(vecE); 

        vec1 = real(vecEe);
        vec2 = imag(vecEe);

        switch method
            case 'linear'
               	%randomSequences{2*i-1} = (vec1(1:seqLen)-mean(vec1(1:seqLen)))/std(vec1(1:seqLen));
                %randomSequences{2*i} = (vec2(1:seqLen)-mean(vec2(1:seqLen)))/std(vec2(1:seqLen));
                randomSequences{2*i-1} = vec1(1:seqLen);
                randomSequences{2*i}  = vec2(1:seqLen);
            case 'circular'
                %in case of circular have to take the whole vectors/ unless
                %different embedding is used. 
                randomSequences{2*i-1} = (vec1-mean(vec1))/std(vec1);
				randomSequences{2*i} = (vec2-mean(vec2))/std(vec2);
            otherwise
                randomSequences = [];
                warning('Unexpected choice of autocorr')
                return;
        end
    
    end
end

