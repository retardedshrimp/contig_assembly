function [ Y ] = simulated_autoregr( settings, method )
%18/10/16 simulate autoregression models

    if nargin < 2
        method = 'exponential';
    end
    
    t = 1:5;
    alpha = 2;


    switch method
        case 'AR(1)'  % exponential decay
            model = arima('Constant',0,'AR',{0.8},'Variance',1);
            
       
        case 'power'
            powerLaw = 1./(t.^(alpha)+1);
            model = arima('Constant',0,'AR',num2cell(powerLaw),'Variance',1);

        case 'sample' % sample decay
            randomSequences = [];

        otherwise
            randomSequences = [];
            warning('Unexpected choice of autocorr')
    end
    
    rng('default')
    Y = simulate(model,settings.contigSize,'NumPaths',settings.nRandom);
    

end

