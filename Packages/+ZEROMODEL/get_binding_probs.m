function [ probVec ] = get_binding_probs( seqq,low,settings )
 %30/11/16

% seqq = IMPORT.read
%seqq = IMPORT.read_fasta('plasmid.1.1.genomic.fna');
set = {};

%for i=1:
for i=1:size(seqq,2)
    if size(seqq{i},2)> low
        set = [set seqq{i}];
    end
end

%xx = CBT.gen_zscaled_cbt_barcodes(set{1},settings);

% compute binding probs

nNum = size(set,2);
nNum
%nNum = 2000;
probVec = cell(1,nNum);

parfor j=1:nNum
   %hSet = cb_netropsin_vs_yoyo1_plasmid(set{j}, settings.concNetropsin_molar, concYOYO1_molar, untrustedMargin, onlyYoyo1Prob, roundingPrecision)
   hSet = CBT.cb_netropsin_vs_yoyo1_plasmid(set{j},settings.NETROPSINconc,settings.YOYO1conc,1000,true);
   probVec{j} = hSet;
end
%load 'DATABASE_3430_prob_seq.mat';



end

