function [ ntSeq ] = simulate_peaks( peaks,seqBetweenPeaks )

    seq=repmat('A',1,seqBetweenPeaks);
    ntSeq = [seq(1:round(size(seq,2)/2)) ];
    for i=1:peaks-1
        ntSeq = [ntSeq 'CCCC' seq];
    end
 
    ntSeq = [ntSeq 'CCCC' seq(1:round(size(seq,2)/2))];
      
end

