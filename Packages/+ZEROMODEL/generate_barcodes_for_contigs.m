function [ theoreticalBarcodes ] = generate_barcodes_for_contigs(theoreticalSeq, settings, method)
% 01/10/16 by Albertas Dvirnas

% based on theoreticalSeq and settings, a set of barcodes in base-pair
% resolution is generated

% theoreticalSeq could come from contig .fasta file, but it could as well
% come from a plasmid database, then we would need a command like this:
% [theoreticalSeq,nameSeq] = read_fasta('plasmid.1.1.genomic.fna');
% if barcodes are generated for the whole database, it is recommended for
% the user to save this for later use
%
    import ZEROMODEL.*;
 
    if nargin < 3
        method = 'old';
    end
    

    nonZerKer = settings.nonzeroKernelLen;
    
    if iscell(theoreticalSeq) % might be that there is only one sequence, hence no cell
        theoreticalBarcodes = cell(1,size(theoreticalSeq,2)); % allocate space

        % parfor loop to use multiple processors
        parfor i=1:size(theoreticalSeq,2)
            if size(theoreticalSeq{i},2) > nonZerKer
                theoreticalBarcodes{i} = generate_contig_barcode(theoreticalSeq{i},settings,method);
            end
        end
    else  % we could also update it if theoreticalSeq have a lot of sequences of the same length, therefore not a cell either
        if size(theoreticalSeq,2) > nonZerKer
                theoreticalBarcodes = generate_contig_barcode(theoreticalSeq,settings,method);
        end
    end
end

