function [ randBar ] = generate_random_barcodes( settings )
    % 10/10/16 by Albertas Dvirnas
    % generates a set of random barcodes based on a zero model.
    randBar = cell(1,settings.nRandom);
    for i=1:settings.nRandom
        randBar{i} = normrnd(0,1,1,settings.contigSize);
    end

end

