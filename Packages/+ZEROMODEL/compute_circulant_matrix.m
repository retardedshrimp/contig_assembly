function [ sHatSqrt ] = compute_circulant_matrix( corFun, method )
    % 27/10/16 
    % Implements circular embedding method from Dietrich and Newsam
    
    % corFun is the correlation function.
    % method linear or circular, depending on what outcome we want
    
    if nargin < 2 % if no parameter selected
        method = 'linear';
    end
    

    switch method
		case 'linear'
        	circularFun = [corFun fliplr(corFun(2:end-1))]; % first row of the circulant matrix

        case 'circular'
            circularFun = corFun;
            
        otherwise
            randomSequences = [];
            warning('Unexpected choice of autocorr')
            return;
    end
    
    
    % compute the eigenvalues of the circular matrix via the fft
    sHat = real(fft(circularFun)); % fft of C1, will always be real
    figure,plot(sHat)
    min(sHat)
    if any(sHat <  -0.0001)     % eigevalues have to be nonnegative
        disp('Matrix is not positive definite, bad choice of embedding') % this is yet to be implemented
        randomSequences = []; % in the future include approximate method here
        return;
    else
        sHat(sHat <  0) = 0; % to get rid of negativity coming from round-off
    end
    
    % square root of sHat/2M
    sHatSqrt = sqrt(sHat/size(sHat,2)); 
    
    % now ready to compute random sequences

end

