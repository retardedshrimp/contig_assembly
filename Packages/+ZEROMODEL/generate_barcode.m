function [ newSeq ] = generate_barcode(dnaSequence,settings )
    %  21/09/16 by Albertas Dvirnas
    % generate_barcode calculates a barcode of dnaSequence, based on the
    % competitive binding method, which in turn uses transfer matrices
    % the methods for this are imported from the CBT library
    
    % all can be envelopped as one function
    barcodeBP = CBT.cb_netropsin_vs_yoyo1_plasmid(dnaSequence,settings.NETROPSINconc,settings.YOYO1conc,1000,true);
    %save('data.mat', '-v7.3', 'barcodeBP')
    barcodePSF = CBT.apply_point_spread_function(barcodeBP, settings.psfWidth*settings.bpPerNm, false); %false if circular

    newSeq = (barcodePSF-mean(barcodePSF))/std(barcodePSF); 
end
