function [ newSeq ] = generate_contig_barcode(dnaSequence,settings, method )
    %  01/10/16 by Albertas Dvirnas
    % generate_barcode calculates a barcode of dnaSequence, based on the
    % competitive binding method, which in turn uses transfer matrices
    % the methods for this are imported from the CBT library
    
    if nargin < 3
        method = 'old';
    end
    
    if isequal(method,'old')
        % all can be envelopped as one function
        barcodeBP = CBT.cb_netropsin_vs_yoyo1_plasmid(dnaSequence,settings.NETROPSINconc,settings.YOYO1conc,1000,true);
    else
        % if a different metho is used
        % barcodeBP = CBT2.cb_two_ligand(dnaSequence,settings);
    end
    
    barcodePSF = CBT.apply_point_spread_function(barcodeBP, settings.psfWidth*settings.bpPerNm, false); %false if circular
    
    resSeq = NUMERICAL.rescale_barc(barcodePSF, settings);
    
    newSeq = (resSeq-mean(resSeq))/std(resSeq); 
    
end
