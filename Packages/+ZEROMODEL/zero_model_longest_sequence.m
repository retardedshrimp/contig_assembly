function [ zeroModel] = zero_model_longest_sequence( theoreticalBarcodes )
% updated 21/09/16 by Albertas Dvirnas
% generates a zeroModel which is of the same length as the longest sequence
% in the set of theoreticalBarcodes

% first define the vector of contigLengths
contigLengths = zeros(1,size(theoreticalBarcodes,2));
for i=1:size(theoreticalBarcodes,2)
   contigLengths(i) =  size(theoreticalBarcodes{i},2);
end

% then find a maximum
[ sMax, ~ ] = max(contigLengths);

zeroModel = 0; % this will be output
nonEmptyBarcodes = 0; % how many non-zero barcodes there are

for i=1:size(theoreticalBarcodes,2)
    if ~isempty(theoreticalBarcodes{i})
        nonEmptyBarcodes = nonEmptyBarcodes+1;
        % calculate the fourier transform for the theoretical sequence
        fourierTransform = fft(theoreticalBarcodes{i});
        %size(fourierTransform)
        % interpolate to the size of maximum sequence
        interpFourierTransform = NUMERICAL.interpolate_fourier_data(abs(fourierTransform), sMax);
        %size(interpFourierTransform)
        %sMax
        % average 
        zeroModel = zeroModel + (interpFourierTransform).^2; % could have zeroModel.^2 and then sqrt to preserve the std
    end
end

% outcome is the null Model
zeroModel = zeroModel/nonEmptyBarcodes;

end

