function [randBar] = FourierSymmPhase(zeroModel)
%21/09/16 applies phase shift and returns a randomised barcode
%%
    barLen = size(zeroModel,2);
    
    even = mod(barLen,2)==0; 

    fi = rand(1,floor((barLen-1)/2));
    
    PR1 = exp(2i*pi*fi);
    PR2 = fliplr(exp(2i*pi*(-fi)));
    
    
    if even
        fftRand = zeroModel.*[1 PR1 1 PR2];
        
    else
        fftRand = zeroModel.*[1 PR1 PR2];
    end
    
    randBar = ifft(fftRand);
            
end